<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneNumberRule implements Rule
{
	private $phoneUtil;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
    }

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param  string $attribute
	 * @param  mixed $value
	 * @return bool
	 * @throws \libphonenumber\NumberParseException
	 */
    public function passes($attribute, $value)
    {
    	try {
			$phoneNumberProto = $this->phoneUtil->parse($value, 'UA');
			return $this->phoneUtil->isValidNumber($phoneNumberProto);
		} catch (\libphonenumber\NumberParseException $e) {
    		return false;
		}

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
		return trans('validation.phone_number');
    }
}
