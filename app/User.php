<?php

namespace App;

use App\Models\ClientInfo;
use App\Models\SmsCode;
use App\Traits\Searchable;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable
{
    use Notifiable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_client', 'name', 'email', 'number', 'password', 'is_phone_verified', 'is_email_verified', 'email_verify_hash', 'provider_id', 'provider', 'nickname', 'reset_password_token'
    ];

	/**
	 * The attributes that are searchable.
	 *
	 * @var array
	 */

	protected $searchable = [
		'users.name'  => 'text',
		'users.email'  => 'text',
		'users.number'  => 'number',
	];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
		'remember_token',
    ];

	/**
	 * Scope a query to only include users of a given type.
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder $query
	 * @param Request $request
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeApplyFilters($query, Request $request)
	{
		$query->select(['users.*']);

		$role_id = $request->get('role_id');

		if ($role_id) {
			// get only users with Warehouse role
			$query->whereHas('roles', function ($query) use ($role_id) {
				$query->where('roles.id', '=', $role_id);
			});
		}

		if ($request->get('user_id') > 0) {
			$query->where('users.id', $request->get('user_id') );
		}

		$fieldByLikeMappings = [
			'user_number' => 'users.number',
			'user_email' => 'users.email',
			'user_name' => 'users.name'
		];

		foreach ($fieldByLikeMappings as $fieldInRequest => $fieldInDb) {
			$fieldInRequesValue = $request->get($fieldInRequest);

			if (!empty($fieldInRequesValue)) {
				$query->where($fieldInDb,  'like', '%'.$fieldInRequesValue. '%');
			}
		}

		$query->groupBy('users.id');

		return $query;
	}

    public function getNameAttribute($value) {
        return $value;
    }

    /**
     * Check is user online.
     * @return bool
     */
    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    protected function setPrimaryKey($key)
    {
        $this->primaryKey = $key;
    }

    public function roles()
    {
        $this->setPrimaryKey('id');
        $this->keyType = 'string';
        $relation = $this->belongsToMany('App\Roles', 'users_roles', 'user_id', 'role_id');
        return $relation;
    }

	public function isAdmin()
	{
		return $this->roles()->where('name', 'Admin')->exists();
	}

	public function isWarehouseAdmin()
	{
		return $this->roles()->where('name', 'Warehouse Admin')->exists();
	}

	public function isWarehouseOperator()
	{
		return $this->roles()->where('name', 'Warehouse Operator')->exists();
	}

	public function isWarehouseCourier()
	{
		return $this->roles()->where('name', 'Warehouse Courier')->exists();
	}

	public function isWarehouseUser()
	{
		return ($this->isWarehouseAdmin() || $this->isWarehouseOperator() || $this->isWarehouseCourier());
	}

	public function isModerator()
	{
		return $this->roles()->where('name', 'Moderator')->exists();
	}

	public function isCustom()
	{
		return $this->roles()->where('name', 'Custom')->exists();
	}

	public function isUser()
	{
		return $this->roles()->where('name', 'User')->exists();
	}

	public function getWarehouseId()
	{
		if (!$this->isWarehouseUser()) {
			throw new \Exception('You trying to get warehouse id of not warehouse user');
		}

		if (!$this->warehouses()->count()) {
			throw new \Exception('This user is not connected with any warehouse');
		}

		return $this->warehouses()->first()->id;
	}

	public function warehouses()
	{
		return $this->belongsToMany(Warehouse::class, 'warehouses_users', 'user_id', 'warehouse_id');
	}

    public function printRole()
    {
        $role = new Roles();
        return $role->where('id', '=', $this->roles()->get()[0]->pivot->role_id)->select('name')->first();
    }

    public function hasAnyRoles($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()){
            return true;
        }
        return false;
    }

    public static function createRandomClientID()
    {
        $randomClientID = null;
        do{
            $randomClientID = mt_rand(10000, 99999);
            $userFind = self::where('id_client', $randomClientID)->get();
        } while ($userFind->count() > 0);

        return $randomClientID;
    }

    public static function getNameById($id)
    {
        return self::where('id', $id)->select('name')->first();
    }

    public function smsCodes()
	{
		return $this->hasMany(SmsCode::class);
	}

	public function getLastValidSmsCodeAttribute()
	{
		// $this->smsCodes() --- it is HasMany query builder
		// $this->smsCodes  --- it is ready collection
		return $this->smsCodes()->isValid()->first();
	}

    public function clientInfos()
	{
		return $this->hasMany(ClientInfo::class, 'client_id', 'id_client');
	}

	public function getClientInfoAttribute()
	{
		// $this->clientInfos() --- it is HasMany query builder
		// $this->clientInfos  --- it is ready collection
		return $this->clientInfos()
			->where('type', ClientInfo::TYPE_USER)
			->first();
	}

}
