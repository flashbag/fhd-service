<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileComment extends Model
{
    protected $fillable = [
        'comment_id',
        'link',
    ];
}
