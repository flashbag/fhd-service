<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategories extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'parent_id',
        'description',
        'is_active'
    ];
}
