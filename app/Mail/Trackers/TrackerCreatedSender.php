<?php

namespace App\Mail\Trackers;

use App\Trackers;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrackerCreatedSender extends Mailable
{
    use Queueable, SerializesModels;

	/**
	 * The tracker instance.
	 *
	 * @var Trackers
	 */
	public $tracker;

	/**
	 * Create a new message instance.
	 *
	 * @param Trackers $tracker
	 */
    public function __construct(Trackers $tracker)
    {
        $this->tracker = $tracker;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		return $this
			->subject('Ваше отправление создано')
			->view('emails.trackers.created.sender');

    }
}
