<?php

namespace App\Http\Middleware;

use App\Roles;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckModerator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userRole = Roles::join('users_roles', 'role_id', '=', 'roles.id')->where('user_id', '=', Auth::user()->id)->first();

        $isModerator = false;

        if($userRole->name == 'Moderator') {
            $isModerator = true;
        }

        if( !$isModerator )
        {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->back();
            }
        }
        return $next($request);
    }
}
