<?php

namespace App\Http\Middleware;

use App\Roles;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $userRole = Roles::join('users_roles', 'role_id', '=', 'roles.id')->where('user_id', '=', Auth::user()->id)->first();
        }else {
            return redirect()->route('login');
        }


        $isCustom = false;

        if ($userRole->name == 'Custom') {
            $isCustom = true;
        }

        if (!$isCustom) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->back();
            }
        }
        return $next($request);
    }
}
