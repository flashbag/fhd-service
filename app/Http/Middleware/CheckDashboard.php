<?php

namespace App\Http\Middleware;

use App\Roles;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CheckDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$authUser = Auth::user();

        if (!$authUser) {

			return redirect('/login');

        }

        if (!$authUser->isAdmin() && !$authUser->isModerator() && !$authUser->isWarehouseUser()) {

			if ($request->ajax()) {
				return response('Unauthorized.', 401);
			}

			return redirect()->back();
		}

		$expiresAt = Carbon::now()->addMinutes(5);
		Cache::put('user-is-online-' . $authUser->id, true, $expiresAt);

        return $next($request);
    }
}
