<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Agent;
use Postmark\PostmarkClient;

class PasswordResetCustomController extends Controller
{

   public function email(Request $request){
       $client = new PostmarkClient(config('mail.postmark_key'));
       $agent = new Agent();
       $email = $request->email;
       $user = \App\User::where('email', $email)->first();
       $mail = config('mail.from.address');
       if(isset($user)) {
           if (App::isLocale('ru')) {
               $templateID = 11010387;
           } else {
               $templateID = 11010179;
           }

           $sendResult = $client->sendEmailWithTemplate(
               "$mail",
               "$email",
               "$templateID",
               [
                   "name" => "FHD",
                   'username' => $user->name,
                   'browser_name' => $agent->browser(),
                   'operating_system' => $agent->platform(),
                   'action_url' => url('/passwords', $user->reset_password_token),
                   'date_y' => date('Y'),
               ]
           );
           Session::flash('success-message', trans('main.reset_pass.send_mess'));
       }
       else{
           Session::flash('danger-message', trans('main.reset_pass.dont_send_mess'));
       }
       return redirect()->back();
   }

   public function passwords($token){

       $this->setTitleAndDescription(trans('main.reset_pass.reset_pass'));

       return view('auth.passwords.password', compact(['token']));
   }

    public function verifiedPass($token, Request $request){
        $user = \App\User::where('reset_password_token', $token)->first();
        if(isset($user)){
            $pass1 = $request['password1'];
            $pass2 = $request['password2'];
            if($pass1 == $pass2){
                $user->password = bcrypt($request['password1']);
                $user->save();
                Session::flash('success-message', trans('main.reset_pass.email_verify'));
                return redirect('/login');
            }
            else{
                Session::flash('success-message', trans('main.reset_pass.password'));
                return redirect(URL::previous());
            }
        }else{
            return abort(404);
        }
    }
}
