<?php

namespace App\Http\Controllers\Auth;


use App\User;
use App;
use App\Http\Controllers\Controller;
use App\UsersRoles;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/trackers/history';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setTitleAndDescription('Аккаунт');
        $statusSite = $this->getStatusSite();
        view()->share('is_offline', $statusSite->is_offline);
        view()->share('desc_offline_site', $statusSite->desc_offline_site);
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        \Illuminate\Support\Facades\Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('email', $user->email)->first();

        if($authUser) {
            User::where('email', $user->email)->update([
                'provider_id' =>  $user->id,
                'provider' => $provider,
            ]);
            return $authUser;
        }
        $user = User::create([
            'id_client' => User::createRandomClientID(),
            'name' => $user->name,
            'email' => $user->email,
            'nickname' => $user->nickname,
            'provider_id' => $user->id,
            'provider' => $provider,
            'password' => bcrypt($user->id),
            'is_active' => 1,
            'is_phone_verified' => 0,
            'is_email_verified' => 0,
            'email_verify_hash' => str_random(20),
        ]);

        UsersRoles::createUserRole($user->id, 1);
        App\Models\ClientInfo::createClientInformation($user->id_client, $user->id);

        return $user;
    }


    protected function credentials(Request $request)
	{
		if(is_numeric($request->get('email'))){
			return ['number'=>$request->get('email'),'password'=>$request->get('password')];
		} elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
			return ['email' => $request->get('email'), 'password'=>$request->get('password')];
		}

		return ['email' => $request->get('email'), 'password'=>$request->get('password')];
	}

    public function login(Request $request) {
       
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

		$errors = [];

        $credentials = $this->credentials($request);

        if(Auth::attempt($credentials)) {

			if (request()->ajax()) {
				return response()->json();
			}

			$user = Auth::user();

            if($user->isAdmin() || $user->isModerator()){
                return redirect('/admin');
            } else if ($user->isWarehouseAdmin() || $user->isWarehouseOperator() || $user->isWarehouseCourier()) {
            	if ($user->warehouses->count() > 0) {
					return redirect('/admin/trackers');
				} else {
					Auth::logout();
					$errors = ['email' => 'Пользователь не прикреплен к складу'];
				}

			} else {
                return redirect('/trackers/history');
            }
        } else {

        	// TODO move all i18n phrases to lang files
			if(App::isLocale('ru')){
				$errors = ['password' => 'Пароль введен неверно!'];
			}elseif(App::isLocale('ua')){
				$errors = ['password' => 'Пароль введено невірно!'];
			}else{
				$errors = ['password' => 'Incorrect password!'];
			}
		}

		if (request()->ajax()) {
			return response()->json([ 'errors' => $errors], 422);
		}

		return redirect()->back()->withInput(
			$request->except('password')
		)->withErrors($errors);

    }


}
