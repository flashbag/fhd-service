<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Mail\VerifyEmail;
use App\Models\SmsCode;
use App\UsersRoles;
use App\Models\ClientInfo;
use App\Jobs\SendSmsCodeJob;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\RegisterInitializeRequest;
use App\Http\Requests\Auth\RegisterVerifyRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use Postmark\PostmarkClient;

class RegisterAjaxController extends Controller
{
	/**
	 * @param RegisterInitializeRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function registerInitialize(RegisterInitializeRequest $request)
	{
		if (! request()->ajax()) return abort(404);

		$phone = $request->get('phone_number');

		$user = User::where('number', $phone)->first();

		$smsCode = SmsCode::getLatestActiveCode($phone);

		if ($smsCode && $smsCode->isValid()) {
			return response()->json([
				'errors' => [
					'phone_number' => 'Вы недавно уже запрашивали СМС код. Подождите три минуты.'
				]
			], 422);
		} else {
			$smsCode = SmsCode::createNewSmsCode($phone, $user);
			SendSmsCodeJob::dispatch($smsCode);

			return response()->json([
				'code' => $smsCode->code,
				'user' => $user
			], 200);
		}
	}

	/**
	 * @param RegisterVerifyRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function registerVerify(RegisterVerifyRequest $request)
	{
		if (! request()->ajax()) return abort(404);

		$phone = $request->get('phone_number');
		$code = $request->get('code');

		$smsCode = SmsCode::getLatestActiveCode($phone);

		if (!$smsCode || ($smsCode->code != $code)) {
			return response()->json([
				'errors' => [
					'code' => 'Неправильный код'
				]
			], 422);
		}

		$user = User::where('number', $phone)->first();

		if ($user) {
			$user->update([
				'is_phone_verified' => 1
			]);
		} else {
			User::create([
				'name' => $request->get('full_name'),
				'email' => $request->get('email'),
				'password' => bcrypt($request->get('password')),
				'number' => $phone,
				'is_phone_verified' => 1,
                'is_email_verified' => 0,
                'email_verify_hash' => str_random(20),
                'reset_password_token' => str_random(20),
			]);

		}

		$smsCode->update([
			'is_active' => 0,
			'is_used' => 1
		]);

		$request->session()->put('phone_verified', $phone);

		return response()->json([
			'need_to_login' => !Auth::check()
		], 200);
	}

	public function registerSimple(RegisterRequest $request)
	{
		$data = $request->all();

		$user = User::create([
			'id_client' => User::createRandomClientID(),
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
			'number' => $data['number'],
			'email_verify_hash' => str_random(20),
			'reset_password_token' => str_random(20),
		]);

		UsersRoles::createUserRole($user->id, 1);
		ClientInfo::createClientInformation($user->id_client);

//		Mail::to($user->email)->queue(new VerifyEmail($user));
        $client = new PostmarkClient(config('mail.postmark_key'));
        $agent = new Agent();
        $mail = config('mail.from.address');
        if (App::isLocale('ru')) {
            $templateID = 11010385;
        } else {
            $templateID = 11010177;
        }
        $sendResult = $client->sendEmailWithTemplate(
            "$mail",
            "$user->email",
            "$templateID",
            [
                "name" => "FHD",
                'username' => $user->name,
                'browser_name' => $agent->browser(),
                'operating_system' => $agent->platform(),
                'action_url' => url('/verification', $user->reset_password_token),
                'date_y' => date('Y'),
            ]
        );

		$userRegisteredEvent = new Registered($user);

		event($userRegisteredEvent);

		$credentials = [
			'email' => $data['email'],
			'password' => $data['password']
		];

		$redirect = route('create-trackers');

		if (Auth::attempt($credentials)) {
			$redirect = route('history-trackers');
		}

		return response()->json(['redirect' => $redirect], 200);
	}

	/**
	 * @param $emailVerifyHash
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
	 */
	public function verifiedEmail($emailVerifyHash){
        $user = User::where('email_verify_hash', $emailVerifyHash)->first();
        if ($user) {
            $user->update([
                'is_email_verified' => 1
            ]);
            $user->save();
        }
        else {
            return abort(404);
        }
        Session::flash('success-message', 'Email верефицирован');
        return redirect('/trackers/history');
    }
}
