<?php

namespace App\Http\Controllers\Auth;

use App\Models\ClientInfo;
use App\User;
use App;
use App\Http\Controllers\Controller;
use App\UsersRoles;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\VerifyEmail;
use Illuminate\Support\Facades\Mail;
use Jenssegers\Agent\Agent;
use Postmark\PostmarkClient;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/trackers/history';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->setTitleAndDescription('Регистрация');
        $this->middleware('guest');
    }

	/**
	 * Handle a registration request for the application.
	 *
	 * @param RegisterRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function register(RegisterRequest $request)
	{
		$user = $this->create($request->all());
        $user->update([
           'reset_password_token' => str_random(20),
        ]);

//		Mail::to($user->email)->queue(new VerifyEmail($user));
        $client = new PostmarkClient(config('mail.postmark_key'));
        $agent = new Agent();
        $mail = config('mail.from.address');
        if (\Illuminate\Support\Facades\App::isLocale('ru')) {
            $templateID = 11010385;
        } else {
            $templateID = 11010177;
        }
        $sendResult = $client->sendEmailWithTemplate(
            "$mail",
            "$user->email",
            "$templateID",
            [
                "name" => "FHD",
                'username' => $user->name,
                'browser_name' => $agent->browser(),
                'operating_system' => $agent->platform(),
                'action_url' => url('/verification', $user->email_verify_hash),
                'date_y' => date('Y'),
            ]
        );
		$userRegisteredEvent = new Registered($user);

		event($userRegisteredEvent);

		$this->guard()->login($user);

		return $this->registered($request, $user)
			?: redirect($this->redirectPath());
	}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'id_client' => User::createRandomClientID(),
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'number' => $data['number'],
            'email_verify_hash' => str_random(20),
        ]);

        UsersRoles::createUserRole($user->id, 1);
        ClientInfo::createClientInformation($user->id_client);

        return $user;
    }
}
