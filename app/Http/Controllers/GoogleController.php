<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GoogleController extends Controller
{
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getPlaceDetails($placeid)
	{
		if (! request()->ajax()) return abort(404);

		$API_KEY = "AIzaSyCFpcv67_0e4zDzXROMA-AVxc5sUJnCIyc";

		// this request cannot be executed from JavaScript because CORS error
		$url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' . $placeid . '&key=' . $API_KEY . '&language=en&region=US';

		$response = file_get_contents($url);

		$response = json_decode($response);

		return response()->json($response);

	}
}
