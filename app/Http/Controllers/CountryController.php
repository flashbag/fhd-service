<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function getCountryWarehouses($countryId, Request $request)
	{
		if (! request()->ajax()) return abort(404);

		$country = Country::findOrFail($countryId);

		return response()->json($country->warehouses);
	}
}
