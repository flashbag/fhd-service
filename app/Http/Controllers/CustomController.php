<?php
/**
 * Created by PhpStorm.
 * User: chigivara
 * Date: 26.09.18
 * Time: 16:23
 */

namespace App\Http\Controllers;

use App\Trackers;
use App\Models\TrackerStatus;
use Illuminate\Support\Facades\Session;
use Config;
use App;
use Illuminate\Http\Request;


class CustomController extends Controller
{

    public function __construct()
    {
        $this->middleware('checkCustom');


        $raw_locale = Session::get('locale');
        if (in_array($raw_locale, Config::get('app.locales'))) {
            $locale = $raw_locale;
        } else $locale = Config::get('app.locale');
        $this->locale = $locale;

        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Таможня');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Митниця');

        } else {

            $this->setTitleAndDescription('Custom');

        }

    }

    public function index(Request $request)
    {
        $queryBuilder =  $trackers = Trackers::applyFilters($request)->onlyActive()
            ->where('to_warehouse_id', 1)->with('status');
        $trackers = $queryBuilder->orderByDesc('date_registered')->paginate(10);

		$statuses = TrackerStatus::all();

		$viewData = compact('trackers', 'statuses');

		if (request()->ajax()) {
			return view('/custom-table-ajax')->with($viewData);
		}

        return view('/custom')->with($viewData);
    }
}
