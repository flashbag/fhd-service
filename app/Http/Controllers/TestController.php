<?php

namespace App\Http\Controllers;

use App\Trackers;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;

class TestController extends Controller
{
	public function testStuff(Request $request)
	{
//		dd($request);
		$translator = Lang::getFacadeRoot();

		$key = 'trackers.parts.countries.by_iso_3166_2.US';

//		dd($translator->get('trackers.parts.countries.by_iso_3166_2.US', [], 'en'));
		$tracker = Trackers::findOrFail(246);

		$sumActualWeight = 0;
		$sumVolumeWeight = 0;

		if ($tracker) {

			foreach ($tracker->receptacles as $receptacle) {

				if ($receptacle->weightUnit && (int)$receptacle->weightUnit->multiplier_to_base != 1) {

					$multiplier = $receptacle->weightUnit->multiplier_to_base;
					$sumActualWeight = $sumActualWeight + ($receptacle->actual_weight / $multiplier);
					$sumVolumeWeight = $sumVolumeWeight + ($receptacle->volume_weight / $multiplier);

				} else {

					$sumActualWeight += $receptacle->actual_weight;
					$sumVolumeWeight += $receptacle->volume_weight;
				}

			}


			$sumActualWeight = round($sumActualWeight);
			$sumVolumeWeight = round($sumVolumeWeight);
		}


		echo '$sumActualWeight:' . $sumActualWeight . PHP_EOL;
		echo '$sumVolumeWeight:' . $sumVolumeWeight . PHP_EOL;

	}

	public function testEmailView(Request $request)
	{
		$tracker = Trackers::find(88);

		return view('emails.trackers.created.recipient', [
			'tracker' => $tracker
		]);

	}
}
