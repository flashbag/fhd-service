<?php

namespace App\Http\Controllers;


use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Mail;
use App\Mail\Trackers\TrackerCreatedSender;
use App\Mail\Trackers\TrackerCreatedRecipient;
use App\Models\ClientInfo;
use Illuminate\Support\Arr;
use App\Models\Cargo;
use App\TypeContainers;
use App\Models\Country;
use App\Models\Currency;
use App\Models\TrackerStatus;
use App\Models\TypeTransport;
use App\Models\Additional\BillDuty;
use App\Models\Additional\BillTransportation;
use App\Models\Additional\FreightService;
use App\Models\Additional\SpecialHandling;
use App\Models\Additional\ServiceDelivery;
use App\Models\MeasureUnit;
use App\Models\HistoryStatuses;
use App\Models\HistoryWarehouse;
use App\Trackers;
use App\TypeInventory;
use App\Models\Warehouse;
use App\User;
use App\UsersRoles;
use App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Trackers\SearchTrackersRequest;
use App\Http\Requests\Trackers\StoreTrackerRequest;
use App\Http\Requests\Trackers\UpdateTrackerRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Description;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Postmark\PostmarkClient;

class TrackerController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTrackers()
    {

        if(Auth::check() && !Auth::user()->is_email_verified){
            return redirect()->route('history-trackers');
        }

        if (Auth::check() && Auth::user()->hasRole('Warehouse Operator')) {
            return redirect()->route('admin-create-trackers');
        }
		$this->setTitleAndDescription(__('trackers.add_tracker'));

        $type_inventories = TypeInventory::all();
        $currencies = Currency::all();
		$type_containers = TypeContainers::all();
		$type_transports = TypeTransport::all();
		$bill_duties = BillDuty::orderBy('order_number','asc')->get();
		$bill_transportations = BillTransportation::orderBy('order_number','asc')->get();
		$freight_services = FreightService::orderedByNumber()->get();
		$special_handlings = SpecialHandling::orderBy('order_number','asc')->get();
		$service_deliveries = ServiceDelivery::all();

		$countries = Country::withWarehouses()->get();
		$units_of_length = MeasureUnit::onlyLength()->get();
		$units_of_weight = MeasureUnit::onlyWeight()->get();

        if (Auth::check()) {
            if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderator')) {
                $data = [
                    "id" => 0,
                    "client_id" => 0,
                    "inn" => null,
                    "passport_data" => null,
                    "country" => null,
                    "city" => null,
                    "address" => null,
                    "index" => null,
                    "created_at" => null,
                    "updated_at" => null
                ];
                $client = (object)$data;
            } elseif (Auth::user()->hasRole('User') || Auth::user()->hasRole('Custom')) {
                $client = ClientInfo::where('client_id', '=', Auth::user()->id_client)->first();
            }
        }


        return view('create-tracker')->with(compact(
			'type_inventories',
			'currencies',
			'type_containers',
			'type_transports',
			'bill_duties',
			'bill_transportations',
			'freight_services',
			'special_handlings',
			'service_deliveries',
			'units_of_length',
			'units_of_weight',
			'client',
            'countries'
		));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreTrackerRequest $request
	 * @return \Illuminate\Http\Response
	 */
    public function store(StoreTrackerRequest $request)
    {
		if (! request()->ajax()) return abort(404);

		$isNewSender = null;
        $isNewRecipient = null;

        $newSender = null;
		$newRecipient = null;

		$clientInfoSenderAsUser = null; // ClientInfo model where type USER
		$clientInfoSenderAsSender = null; // ClientInfo model where type USER
		$clientInfoRecipientAsUser = null; // ClientInfo model where type USER
		$clientInfoRecipientAsRecipient = null; // ClientInfo model where type USER

        $senderCommonData = ClientInfo::getSenderDataFromRequest($request);
		$recipientCommonData = ClientInfo::getRecipientDataFromRequest($request);

		$numberSender = str_replace(' ', '', $request->get('number_sender'));
		$numberRecipient = str_replace(' ', '', $request->get('number_recipient'));

		$checkUserSender = User::where('number', $numberSender)->first();

		if (!Auth::check()) {

			$sender = $checkUserSender;

//			// if not logged
//
//			if (!$request->session()->get('phone_verified')) {
//				// if phone not verified
//				if ($checkUserSender && $checkUserSender->is_phone_verified) {
//					// if sender found and confirmed
//					$modalId = 'modalLogin';
//				} else {
//					// if sender not found or not confirmed
//					$modalId = 'modalRegister';
//				}
//			} else {
//				// if phone is verified
//				$modalId = 'modalLogin';
//				$request->session()->forget('phone_verified');
//			}
//
//			return response()->json([
//				'modal' => [
//					'id' => $modalId,
//					'action' => 'show',
//				]
//			]);

			return response()->json([
				'modal' => [
					'id' => 'modalRegisterSimple',
					'action' => 'show',
				]
			]);

		} else {

			$sender = Auth::user();
//
//			if (!$sender->is_phone_verified) {
//
//				// if sender not found or not confirmed
//				$modalId = 'modalRegister';
//
//				return response()->json([
//					'modal' => [
//						'id' => $modalId,
//						'action' => 'show',
//					]
//				]);
//			}
//
		}

		DB::beginTransaction();

		$clientInfoUser = ClientInfo::where([
			'type' => ClientInfo::TYPE_USER,
			'number' => $numberSender
		])->first();

		if (!$clientInfoUser) {

			$userData = array_merge($senderCommonData, [
				'type' => ClientInfo::TYPE_USER,
			]);

			if ($sender) {
				$userData['user_id'] = $sender->id;
			}

			ClientInfo::create($userData);
		}

		$senderData = array_merge($senderCommonData, [
			'type' => ClientInfo::TYPE_SENDER,
		]);

		if ($sender) {
			$senderData['user_id'] = $sender->id;
		}

		$clientInfoSender = ClientInfo::create($senderData);

		$recipientUserId = null;
		$recipientUser = User::where('number', $numberRecipient)->first();

		if ($recipientUser) {
			$recipientUserId = $recipientUser->id;
		}

		$recipientData = array_merge($recipientCommonData, [
			'type' => ClientInfo::TYPE_RECIPIENT,
		]);

		if ($recipientUser) {
			$recipientData['user_id'] = $recipientUserId;
		}

		$clientInfoRecipient = ClientInfo::create($recipientData);

		$validatedData = [
			'type_transport_id' => $request->get('type_transport_id'),

			'number_seats' => count($request->get('receptacles')),

			'number_invoice' => $request->get('number_invoice'),
			'date_registered' => $request->get('date_registered'),


			'bill_duty_id' => $request->get('bill_duty_id'),
			'bill_transportation_id' => $request->get('bill_transportation_id'),
			'freight_service_id' => $request->get('freight_service_id'),
			'from_warehouse_id' => $request->get('from_warehouse_id'),
			'to_warehouse_id' => $request->get('to_warehouse_id')
		];

		$specialHandlingId = $request->get('special_handling_id');

		if ($specialHandlingId != '--' && $specialHandlingId != 'null') {
			$validatedData['special_handling_id'] = $specialHandlingId;
		}

		$is_draft = $request->get('is_draft') ? 1 : 0;

		$uniqueData = [
			'id_tracker' => Carbon::now()->format('dmy') . mt_rand(10000, 99999),
            'barcode' => '1',

			'date_registered' => Carbon::now()->format('Y-m-d'),

			'is_active' => 0,
			'is_unprocessed' => 1,
			'is_draft' => $is_draft,
            'status_id' => 11,
		];

		$allData = array_merge($validatedData, $uniqueData, $senderCommonData, $recipientCommonData);

		$allData = Trackers::presaveModifyAdditionalData($allData);

		$tracker = Trackers::create($allData);

		$clientInfoSender->update([ 'tracker_id' => $tracker->id ]);
		$clientInfoRecipient->update([ 'tracker_id' => $tracker->id ]);

		$tracker->setAdditionalData($request->all());

		$tracker->processTrackersReceptacles($request);
		$tracker->processTrackersDocuments($request);

        HistoryStatuses::create([
            'status_id' => $tracker->status_id,
            'id_tracker' => $tracker->id,
            'id_user' => Auth::user()->id
        ]);

//		if ($clientInfoSenderAsSender->email) {
//			Mail::to($clientInfoSenderAsSender->email)->queue(new TrackerCreatedSender($tracker));
//		}
//
//		if ($clientInfoRecipientAsRecipient->email) {
//			Mail::to($clientInfoRecipientAsRecipient->email)->queue(new TrackerCreatedRecipient($tracker));
//		}

//		if ($clientInfoSender->email) {
//			Mail::to($clientInfoSender->email)->queue(new TrackerCreatedSender($tracker));
//		}
//
//		if ($clientInfoRecipient->email) {
//			Mail::to($clientInfoRecipient->email)->queue(new TrackerCreatedRecipient($tracker));
//		}

		DB::commit();

        if (App::isLocale('ru')) {
            Session::flash('success-message', 'Трекер успешно добавлен на модерацию!');
        } elseif (App::isLocale('ua')) {
            Session::flash('success-message', 'Трекер успішно додан на модерацію!');
        } else {
            Session::flash('success-message', 'Tracker successfully added to moderation!');
        }
        $redirect = null;

        if ($request->user()) {
            $redirect = route('history-trackers');
        } else {
            $redirect = route('create-trackers');
        }

		return response()->json([
			'redirect' => $redirect
		]);

    }

	/**
	 * Search tracker by id_tracker, number
	 *
	 * @param SearchTrackersRequest $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
    public function searchTracker(SearchTrackersRequest $request)
	{
		$id = null;

		$number = $request->get('number');
		$id_tracker = $request->get('id_tracker');

		$tracker = Trackers::where('id_tracker', '=', $id_tracker)->first();

		$clientNumber = ClientInfo::where('number', $number)->first();

		if (!$tracker) {
			if(!$clientNumber){
                return redirect()->back()->withErrors(__('trackers.tracker_not_found'));
            }  else {
                return redirect()->back()->withErrors('Введите номер накладной');
			}
		}

		$this->setTitleAndDescription(__('trackers.view_tracker') . ': ' . $id);

		$statusTrackers = [];
		$historyStatuses = [];

		if($tracker) {
		    $statusTrackers =  TrackerStatus::orderBy('number')->get();
            $historyStatuses = HistoryStatuses::where('id_tracker', '=', $tracker->id)->first();
        }

//		dd($historyStatuses);
		if (is_null($statusTrackers) && is_null($historyStatuses)) {
			return redirect()->back()->withErrors(__('trackers.tracker_history_not_found'));
		}

		$viewData = compact('tracker', 'statusTrackers', 'historyStatuses', 'id_tracker');

		if ($number) {
			$viewData['number'] = $number;
		}

            return view('show-tracker')->with($viewData);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
    public function showTracker(Request $request, $id = null)
    {
        $validator = Validator::make($request->all(), [
            'number' => 'nullable|max:18',
            'id_tracker' => 'nullable|min:11',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

		$checkSearch = false;
		$res = null;
		$number = null;
		$id_tracker = null;


		if ($request->get('number') !== null && $request->get('id_tracker') !== null) {

			// search by id_tracker and number
			$tracker = Trackers::where('id_tracker', '=', $request->get('id_tracker'))->first();

			if (!$tracker) {
				return redirect()->back()->withErrors(__('trackers.tracker_not_found'));
			}

		} else {
			// search by id_tracker or number

			if (!empty($request->get('id_tracker'))) {
				// search by id_tracker

				$tracker = Trackers::where('id_tracker', '=', $request->get('id_tracker'))->first();
				if (!empty($tracker)) {
					$id = $tracker->id_tracker;
					$id_tracker = $request->get('id_tracker');
					$checkSearch = false;
				} else {
					return redirect()->back()->withErrors(__('trackers.tracker_not_found'));
				}

			} elseif (!empty($request->get('number'))) {

				//TODO check by number from ClientInfo
			}
		}

		$this->setTitleAndDescription(__('trackers.view_tracker') . ': ' . $id);

		$tracker = Trackers::where('id_tracker', '=', $id)->first();

		$statusTrackers = TrackerStatus::orderBy('number')->get();

		// TODO fix this bug, getting only first HistoryStatus for tracker
		$historyStatuses = HistoryStatuses::where('id_tracker', '=', $tracker->id)->first();

		if (is_null($statusTrackers) && is_null($historyStatuses)) {
			return redirect()->back()->withErrors(__('trackers.tracker_history_not_found'));
		}

		if ($checkSearch === false) {
			$res = view('show-tracker')->with(compact('tracker', 'statusTrackers', 'historyStatuses', 'checkSearch'));
		} else {
			$res = view('show-tracker')->with(compact('tracker', 'statusTrackers', 'historyStatuses', 'number', 'id_tracker', 'checkSearch'));
		}
		return $res;


    }

    public function showTrackerWithUser($id)
    {
		$this->setTitleAndDescription(__('trackers.view_tracker') . ': ' . $id);

        $tracker = Trackers::where('id_tracker', '=', $id)->first();

        $statusTrackers = TrackerStatus::orderBy('number')->get();
        $historyStatuses = HistoryStatuses::where('id_tracker', '=', $tracker->id)->first();

        $id_tracker = $id;

        return view('show-tracker-with-user')->with(compact(
        	'tracker',
			'statusTrackers',
			'historyStatuses',
			'number',
			'id_tracker'
		));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editTrackers($id)
    {
        if (App::isLocale('ru')) {
            $this->setTitleAndDescription('Редактирование трекера');
        } elseif (App::isLocale('ua')) {
            $this->setTitleAndDescription('Редагування трекера');
        } else {
            $this->setTitleAndDescription('Editing a tracker');
        }

        $tracker = Trackers::withTrashed()->where('id_tracker', $id)->first();
        if (!$tracker) {
            return abort(404);
        }
        $formAction = route('update-trackers', $tracker->id);

        $currentTypeStatuses = TrackerStatus::all();

        $historyStatuses = HistoryStatuses::where('id_tracker', '=', $tracker->id)
            ->select('history_statuses.*')
            ->get();

        $type_containers = TypeContainers::all();
        $type_inventories = TypeInventory::all();
        $type_transports = TypeTransport::all();
        $bill_duties = BillDuty::orderBy('order_number','asc')->get();
        $bill_transportations = BillTransportation::orderBy('order_number','asc')->get();
        $freight_services = FreightService::orderedByNumber()->get();
        $special_handlings = SpecialHandling::orderBy('order_number','asc')->get();
        $service_deliveries = ServiceDelivery::all();

		$countries = Country::withWarehouses()->get();
		$units_of_length = MeasureUnit::onlyLength()->get();
		$units_of_weight = MeasureUnit::onlyWeight()->get();

        $historyStatusTrackers = HistoryStatuses::where('id_tracker', '=', $tracker->id)->latest()->first();
        $currencies = Currency::all();

        return view('edit-tracker')->with(compact(
            'tracker',
            'formAction',
            'currentTypeStatuses',
            'historyStatuses',
            'type_containers',
            'type_inventories',
            'type_transports',
            'bill_duties',
            'bill_transportations',
            'freight_services',
            'special_handlings',
            'service_deliveries',
            'historyStatusTrackers',
            'currencies',
            'units_of_length',
			'units_of_weight',
            'countries'
        ));
    }

    public function printInvoice($id)
    {
        $tracker = Trackers::withTrashed()->leftJoin('containers', 'containers.id', '=', 'trackers.container_id')
            ->leftJoin('type_containers', 'type_containers.id', '=', 'containers.id_type_container')
            ->select('trackers.*')
            ->where('trackers.id', '=', $id)->first();
        return view('admin/print-invoice')->with(compact('tracker'));
    }

    public function updateTrackers(UpdateTrackerRequest $request, $idTracker)
    {
        if (! request()->ajax()) return abort(404);

		$tracker = Trackers::where('id_tracker', $idTracker)->first();

		if (!$tracker) {
			return abort(404);
		}

		$mergedData = Trackers::getCommonRequestData($request);

		$mergedData = Trackers::presaveModifyAdditionalData($mergedData);

		$mergedData = Arr::except($mergedData, [
			'duty_price',
			'duty_price_currency',
			'total_price',
			'total_price_currency',
			'is_paid',
			'id_tracker',
			'is_unprocessed',
			'is_processed',
			'is_active'
		]);

        $tracker->update($mergedData);

        $tracker->setAdditionalData($request->all());

        $tracker->processTrackersReceptacles($request);
        $tracker->processTrackersDocuments($request);

		$tracker->updateSenderAndRecipient($request);

        if (App::isLocale('ru')) {
            Session::flash('success-message', 'Трекер успешно обновлен!');
        } elseif (App::isLocale('ua')) {
            Session::flash('success-message', 'Трекер успішно оновлено!');
        } else {
            Session::flash('success-message', 'Tracker successfully updated!');
        }
        $response = [];
		$response['redirect'] = route('history-trackers');

        return response()->json($response, 200);

    }

    public function deleteTrackers($idTracker, Request $request)
    {
        $tracker = Trackers::withTrashed()->where('id_tracker', $idTracker)->first();

        if (!$tracker) {
        	return abort(404);
		}

		$tracker->update([
			'deleted_at' => date("Y-m-d H:i:s")
		]);
        if (App::isLocale('ru')) {
            Session::flash('success-message', 'Трекер успешно удален!');
        } elseif (App::isLocale('ua')) {
            Session::flash('success-message', 'Трекер успішно видалено!');
        } else {
            Session::flash('success-message', 'Tracker successfully deleted!');
        }
		return redirect()->route('history-trackers');
    }

    public function resetTrackers($idTracker, Request $request)
    {
        $tracker = DB::table('trackers')->where('id_tracker', $idTracker)->limit(1);

        if (!$tracker) {
            return abort(404);
        }

        $tracker->update([
            'deleted_at' => NULL
        ]);
        if (App::isLocale('ru')) {
            Session::flash('success-message', 'Трекер успешно восстановлен!');
        } elseif (App::isLocale('ua')) {
            Session::flash('success-message', 'Трекер успішно восстановлен!');
        } else {
            Session::flash('success-message', 'Tracker successfully restored!');
        }
        return redirect()->route('history-trackers');
    }

    public function history(Request $request)
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        if (Auth::user()->hasRole('Custom')) {
            return redirect('custom');
        }


        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('История заказов');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Історія замовлень');

        } else {

            $this->setTitleAndDescription('History of orders');

        }

		// don't touch this QueryBuilder!
		$queryBuilder = Trackers::select('trackers.*')
			->with('status')
			->join('client_infos', function ($join) {
				$join->on('client_infos.tracker_id', '=', 'trackers.id')
					->where(function($q){
						return
							$q->where('client_infos.type', '=', ClientInfo::TYPE_SENDER)
								->orWhere('client_infos.type', '=', ClientInfo::TYPE_RECIPIENT);
					})
					->where('client_infos.user_id', '=', Auth::user()->id);
			})
			->groupBy('trackers.id');
        $trackers = $queryBuilder->paginate(10);

        $trackersTrash =$queryBuilder->onlyTrashed()->paginate(10);

        $queryBuilderDraft = $queryBuilder->where('trackers.is_draft','=',1);

        $trackersDraft = $queryBuilderDraft->paginate(10);

        if (!Auth::user()->is_email_verified) {
            if (App::isLocale('ru')) {
                $msg = 'Email не верифицирован, для повторной отправки письма на почту нажмите на <a href="'. url("/sendEmail").'">ссылку</a>';
                Session::flash('danger-message', $msg);
            } elseif (App::isLocale('ua')) {
                $msg = 'Email не верiфiкован, для повторного відправлення листа на почту натиснiть на <a href="'. url("/sendEmail").'">посилання</a>';
                Session::flash('danger-message', $msg);
            } else {
                $msg = 'Email is not verified, to resend the letter to the mail, click on <a href="'. url("/sendEmail").'">link</a>';
                Session::flash('danger-message', $msg);
            }
        }
        return view('history-trackers')->with(compact('trackers', 'trackersDraft', 'trackersTrash'));
    }

    public function editProfile()
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();
        $clientInfo = ClientInfo::where('client_id', '=', $user->id_client)->first();

        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Редактирование профиля');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Редагування профілю');

        } else {

            $this->setTitleAndDescription('Editing a profile');

        }


        return view('edit-profile')->with(compact('user', 'clientInfo'));
    }

    public function updateProfile(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $messages = [];

        if (App::isLocale('ru')) {

            $messages = [
                'inn.required' => 'Вы не ввели ИНН.',
                'passport_data.required' => 'Вы не ввели Паспортные данные.',
                'country.required' => 'Вы не ввели Страну.',
                'city.required' => 'Вы не ввели Город.',
                'address.required' => 'Вы не ввели Адрес.',
                'index.required' => 'Вы не ввели Индекс.',
                'number.required' => 'Вы не ввели Номер телефона.',
                'email.required' => 'Вы не ввели Email.',
                'username.required' => 'Вы не ввели ФИО или название компании.',
                'repeat_password.required' => 'Пароли не совпадают.',
                'password.required' => 'Пароли не совпадают.',
            ];

        } elseif (App::isLocale('ua')) {

            $messages = [
                'inn.required' => 'Ви не ввели IПН.',
                'passport_data.required' => 'Ви не ввели Паспортні дані.',
                'country.required' => 'Ви не ввели Країну.',
                'city.required' => 'Ви не ввели Мiсто.',
                'address.required' => 'Ви не ввели Адресу.',
                'index.required' => 'Ви не ввели Індекс.',
                'number.required' => 'Ви не ввели Номер телефону.',
                'email.required' => 'Ви не ввели Email.',
                'username.required' => 'Ви не ввели ПІБ або назву компанії.',
                'repeat_password.required' => 'Паролі не збігаються.',
                'password.required' => 'Паролі не збігаються.',
            ];

        } else {

            $messages = [
                'inn.required' => 'You did not enter INN.',
                'passport_data.required' => 'You did not enter Pasport data.',
                'country.required' => 'You did not enter Country .',
                'city.required' => 'You did not enter City.',
                'address.required' => 'You did not enter Address.',
                'index.required' => 'You did not enter Index',
                'number.required' => 'You did not enter Phone number.',
                'email.required' => 'You did not enter Email.',
                'username.required' => 'You did not enter the Full name or company name.',
                'repeat_password.required' => 'Passwords do not match.',
                'password.required' => 'Passwords do not match.',
            ];

        }


        $validator = Validator::make($request->all(), [
            'inn' => 'nullable|numeric',
            'passport_data' => 'nullable|alpha_num',
            'country' => 'nullable|alpha',
            'city' => 'nullable|alpha',
            'address' => 'nullable',
            'index' => 'nullable|numeric',
            'number' => 'required|unique:users,number,' . Auth::id(),
            'email' => 'required|email|unique:users,email,' . Auth::id(),
            'username' => 'required|alpha_spaces',
            'password' => 'nullable',
            'repeat_password' => 'nullable|same:password'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $user = User::where('id',  Auth::user()->id)->first();

        if($request->get('email') !== $user->email){
            $user->update([
                'name' => $request->get('username'),
                'email' => $request->get('email'),
                'number' => $request->get('number')
            ]);
            $client = new PostmarkClient(config('mail.postmark_key'));
            $agent = new Agent();
            $email = $user->email;
            $mail = config('mail.from.address');

            if (App::isLocale('ru')) {
                $templateID = 11010385;
            }else {
                $templateID = 11010177;
            }

            $sendResult = $client->sendEmailWithTemplate(
                "$mail",
                "$email",
                "$templateID",
                [
                    "name" => "FHD",
                    'username' => $user->name,
                    'browser_name' => $agent->browser(),
                    'operating_system' => $agent->platform(),
                    'action_url' => url('/verification', $user->email_verify_hash),
                    'date_y' => date('Y'),
                ]
            );
            Session::flash('success-message', trans('main.reset_pass.verify_email'));
            $user->update([
               'is_email_verified' => 0
            ]);
        }

        ClientInfo::where('client_id', '=', Auth::user()->id_client)->update([
            'inn' => $request->get('inn'),
            'passport_data' => $request->get('passport_data'),
            'country' => $request->get('country'),
            'city' => $request->get('city'),
            'address' => $request->get('address'),
            'index' => $request->get('index'),
        ]);
        $user->update([
            'name' => $request->get('username'),
            'email' => $request->get('email'),
            'number' => $request->get('number')
        ]);

        if ($request->get('password') == '' && $request->get('repeat_password') == '') {
            if($request->get('email') != $user->email) {
                Session::flash('success-message', 'Профиль успешно обновлен, вам на почту отправлено письмо 
                для верификации e-mail!');
            }
            else {
                Session::flash('success-message', 'Профиль успешно обновлен!');
            }
        }
        else {
            if ($request->get('password') == $request->get('repeat_password')) {
                User::where('id_client', '=', Auth::user()->id_client)->update([
                    'password' => bcrypt($request->get('password'))
                ]);
                Session::flash('success-message', 'Профиль и пароль успешно обновлен!');
            }
            else {
                Session::flash('error-message', 'Пароли не совпадают!');
            }
        }

        return redirect()->route('edit-profile');
    }

    public function sendEmail(Request $request){
        $user = Auth::user();
        $email = Auth::user()->email;
//        Mail::to($email)->queue(new App\Mail\ReVerifyEmail($user));
        $client = new PostmarkClient(config('mail.postmark_key'));
        $agent = new Agent();
        $mail = config('mail.from.address');

        if (App::isLocale('ru')) {
            $templateID = 11010385;
        }else {
            $templateID = 11010177;
        }
        $sendResult = $client->sendEmailWithTemplate(
            "$mail",
            "$email",
            "$templateID",
            [
                "name" => "FHD",
                'username' => $user->name,
                'browser_name' => $agent->browser(),
                'operating_system' => $agent->platform(),
                'action_url' => url('/verification', $user->email_verify_hash),
                'date_y' => date('Y'),
            ]
        );

        return redirect()->route('history-trackers');
    }
}
