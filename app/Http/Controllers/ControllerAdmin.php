<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class ControllerAdmin extends Controller
{
	public function __construct()
	{
		$this->middleware('checkDashboard');

		$raw_locale = Session::get('locale');

		if (in_array($raw_locale, Config::get('app.locales'))) {
			$locale = $raw_locale;
		} else {
			$locale = Config::get('app.locale');
		}

		$this->locale = $locale;
	}
}
