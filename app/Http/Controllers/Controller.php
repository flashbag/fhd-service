<?php

namespace App\Http\Controllers;

use App\News;
use Closure;
use App\Settings;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setTitleAndDescription($title, $description = null)
    {
        $settings = Settings::first();
        view()->share('title_config', $title . ' - ' . $settings->title_site);

        if ($description !== null) {
            view()->share('description_config', $description);
        } else {
            view()->share('description_config', $settings->short_description);
        }

    }

    public function getStatusSite()
    {
        return Settings::select('is_offline', 'desc_offline_site')->first();
    }

	protected function searchAjaxResponse(LengthAwarePaginator $searchResult, Closure $resultMapFunction)
	{
		$select2format = ['results' => []];

		$select2format['results'] = array_map($resultMapFunction, $searchResult->items());

		$select2format['pagination']['more'] = $searchResult['current_page'] < $searchResult['last_page'];

		return response()->json($select2format, 200);

	}
}
