<?php

namespace App\Http\Controllers;

use App\Trackers;
use HeadlessChromium\BrowserFactory;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
	public $pdfPath;

	/**
	 * PrintController constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{
		$dirMode = 0777;

		$storageFullPath = realpath(__DIR__ . '/../../../storage');

		$this->pdfPath = $storageFullPath . '/app/public/pdf';

		if (!file_exists($this->pdfPath)) {

			if (!mkdir($this->pdfPath, $dirMode)) {
				throw new \Exception('Failed to create ' . $this->pdfPath . ' for PDF generating');
			}

			chmod($this->pdfPath, $dirMode);

			if (!is_writable($this->pdfPath)) {
				throw new \Exception('Directory ' . $this->pdfPath . ' is not writable!');
			}

		}

	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
	public function downloadEmptyTrackerPdf(Request $request)
	{
		if (request()->ajax()) return response(200);

		$url = url('/print/tracker/0/?color=' . $request->get('color'));

		$pdfPath = $this->pdfPath . '/empty-' . uniqIdReal()  . '.pdf';

		$this->processChrome($url, $pdfPath);

		$downloadFilename =  'new-tracker-' . $request->get('color') . '.pdf';

		return response()
			->download($pdfPath, $downloadFilename, [])
			->deleteFileAfterSend(true);
	}

	/**
	 * @param $idTracker
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
	 * @throws \Exception
	 */
	public function downloadTrackerPdf($idTracker, Request $request)
	{
		if (request()->ajax()) return response(200);

		$tracker = Trackers::where('id_tracker', $idTracker)->first();

		if (!$tracker) {
			return abort(404);
		}

		$url = url('/print/tracker/' . $tracker->id_tracker . '?color=' . $request->get('color'));

		$pdfPath = $this->pdfPath . '/' . $tracker->id  . '.pdf';

		$this->processChrome($url, $pdfPath);

		$downloadFilename =  $tracker->id_tracker .  '.pdf';

		return response()
			->download($pdfPath, $downloadFilename, [])
			->deleteFileAfterSend(true);
	}

	/**
	 * @param $url
	 * @param $pdfPath
	 * @throws \Exception
	 */
	private function processChrome($url, $pdfPath)
	{
		try {

			$browserFactory = new BrowserFactory('google-chrome --aggressive-cache-discard --disable-notifications --disable-remote-fonts --disable-reading-from-canvas --disable-remote-playback-api --disable-shared-workers --disable-voice-input --enable-aggressive-domstorage-flushing');

			// starts headless chrome
			$browser = $browserFactory->createBrowser();

			// creates a new page and navigate to an url
			$page = $browser->createPage();

			$page->navigate($url)->waitForNavigation();

			// pdf
			$page->pdf(['printBackground'=>true])->saveToFile($pdfPath);

			// bye
			$browser->close();

		} catch (\Exception $e) {
			throw $e;
		}
	}

}
