<?php

namespace App\Http\Controllers;

use App\TypeInventory;
use App\Models\Currency;
use App\Models\MeasureUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Http\Requests\Trackers\GetNewFileTemplateRequest;
use App\Http\Requests\Trackers\GetNewReceptacleTemplateRequest;
use App\Http\Requests\Trackers\GetNewReceptacleCargoTemplateRequest;

class TemplateController extends Controller
{
	/**
	 * @param GetNewReceptacleTemplateRequest $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
	 */
	public function getNewReceptacleTemplate(GetNewReceptacleTemplateRequest $request)
	{
		if (! request()->ajax()) return abort(404);

		$currencies = Currency::all();
		$type_inventories = TypeInventory::all();
		$units_of_length = MeasureUnit::onlyLength()->get();
		$units_of_weight = MeasureUnit::onlyWeight()->get();

		$new_block_index = $request->get('new_block_index');

		$viewData = compact(
			'new_block_index',
			'type_inventories',
			'units_of_length',
			'units_of_weight',
			'currencies'
		);

		if ($request->user() &&
			( $request->user()->isAdmin() || $request->user()->isWarehouseUser() )
		) {
			return view('admin/tracker-parts/receptacles/receptacle-new')->with($viewData);
		}

		return view('tracker-parts/receptacles/receptacle-new')->with($viewData);

	}

	/**
	 * @param GetNewReceptacleCargoTemplateRequest $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
	 */
	public function getNewReceptacleCargoTemplate(GetNewReceptacleCargoTemplateRequest $request)
	{
		if (! request()->ajax()) return abort(404);

		$new_block_index = $request->get('new_block_index');
		$receptacle_index = $request->get('receptacle_index');

		$viewData = compact(
			'new_block_index',
			'receptacle_index'
		);

		if ($request->user() &&
			( $request->user()->isAdmin() || $request->user()->isWarehouseUser() )
		) {
			return view('admin/tracker-parts/receptacles/new-cargo-template')->with($viewData);
		}

		return view('tracker-parts/receptacles/new-cargo-template')->with($viewData);

	}

	public function getNewFileTemplate(GetNewFileTemplateRequest $request)
	{
		if (! request()->ajax()) return abort(404);

		$new_block_index = $request->get('new_block_index');
		$input_name = $request->get('input_name');
		$container_selector = $request->get('container_selector');

		$viewData = compact(
			'new_block_index',
			'input_name',
			'container_selector'
		);

		return view('new-file-general-template')->with($viewData);
	}
}
