<?php

namespace App\Http\Controllers;


use App\Pivots\WarehouseUser;
use App\Roles;
use App\Settings;
use App\Trackers;
use App\TypeContainers;
use App\TypeInventory;
use App\User;
use App\News;
use App\UsersRoles;
//use App\Models\Batch;
use App\Models\Cargo;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Warehouse;
use App\Models\ClientInfo;
use App\Models\MeasureUnit;
use App\Models\TypeTransport;
use App\Models\TrackerStatus;
use App\Models\HistoryStatuses;
use App\Models\HistoryWarehouse;
use App\Models\Additional\BillDuty;
use App\Models\Additional\BillTransportation;
use App\Models\Additional\FreightService;
use App\Models\Additional\SpecialHandling;
use App\Models\Additional\ServiceDelivery;
use App\Models\Additional\SubAddress;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Trackers\StoreTrackersRequest;
use App\Http\Requests\Admin\Trackers\UpdateTrackersRequest;
use App\Http\Requests\Admin\Trackers\UpdateLevelTrackersRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Containers;
use Config;
use Response;

class AdminController extends ControllerAdmin
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (!Auth::check()) {
            return redirect('login');
        }

        $users = User::all();
        $countUserOnline = null;

        foreach ($users as $user) {
            if ($user->isOnline()) {
                ++$countUserOnline;
            }
        }

        $today = Carbon::now();
        $last3Months = Carbon::now()->subMonth(3);
        $last6Months = Carbon::now()->subMonth(6);
        $last12Months = Carbon::now()->subYear(1);

        $trackers = Trackers::all();
        $countTrackers = $trackers->count();
        $countTrackersToday = 0;
        $countTrackersWeek = 0;
        $countTrackersMonth = 0;

        $countTrackers3Months = Trackers::where('date_registered', '>=', $last3Months->toDateString())->count();
        $countTrackers6Months = Trackers::where('date_registered', '>=', $last6Months->toDateString())->count();
        $countTrackers12Months = Trackers::where('date_registered', '>=', $last12Months->toDateString())->count();
        for ($i = 0; $i < $trackers->count(); $i++) {
            if ($trackers[$i]->date_registered == $today->format('Y-m-d')) {
                ++$countTrackersToday;
            }
            if (Carbon::parse($trackers[$i]->date_registered)->weekOfMonth == $today->weekOfMonth) {
                ++$countTrackersWeek;
            }
            if (Carbon::parse($trackers[$i]->date_registered)->format('m') == $today->format('m')) {
                ++$countTrackersMonth;
            }
        }

        $news = News::all();
        $countNews = $news->count();
        $countNewsToday = 0;
        $countNewsWeek = 0;
        $countNewsMonth = 0;
        for ($i = 0; $i < $news->count(); $i++) {
            if ($news[$i]->date_registered == $today->format('Y-m-d')) {
                ++$countNewsToday;
            }
            if (Carbon::parse($news[$i]->date_registered)->weekOfMonth == $today->weekOfMonth) {
                ++$countNewsWeek;
            }
            if (Carbon::parse($news[$i]->date_registered)->format('m') == $today->format('m')) {
                ++$countNewsMonth;
            }
        }

        $users = User::all();
        $countUsers = $users->count();
        $countUsersToday = 0;
        $countUsersWeek = 0;
        $countUsersMonth = 0;
        for ($i = 0; $i < $users->count(); $i++) {
            if (Carbon::parse($users[$i]->created_at)->format('Y-m-d') == $today->format('Y-m-d')) {
                ++$countUsersToday;
            }
            if (Carbon::parse($users[$i]->created_at)->weekOfMonth == $today->weekOfMonth) {
                ++$countUsersWeek;
            }
            if (Carbon::parse($users[$i]->created_at)->format('m') == $today->format('m')) {
                ++$countUsersMonth;
            }
        }

        $currencies = Currency::onlyActive()->get();

        return view('admin/home')->with(compact(
            'countTrackers',
            'countTrackersToday',
            'countTrackersWeek',
            'countTrackersMonth',
            'countTrackers3Months',
            'countTrackers6Months',
            'countTrackers12Months',
            'countNews',
            'countNewsToday',
            'countNewsWeek',
            'countNewsMonth',
            'countUsers',
            'countUsersToday',
            'countUsersWeek',
            'countUsersMonth',
            'users',
            'countUserOnline',
            'currencies'
        ));
    }

    /* --- Trackers --- */

    public function trackers(Request $request)
    {
        view()->share('title', 'Список всех трекеров');

        $statuses = TrackerStatus::groupBy('number')->get();
        $warehouses = Warehouse::all();
        $type_transport = TypeTransport::where('disabled', 0)->get();
        $trackers = Trackers::applyFilters($request)
            ->warehouseCheck($request)
            ->orderByDesc('date_registered')
            ->paginate(10);

        $viewData = compact('trackers',  'statuses', 'warehouses', 'type_transport');

        if (request()->ajax()) {
            return view('admin/trackers-table')->with($viewData);
        }

        return view('admin/trackers')->with($viewData);
    }

    public function trackersUnprocessed(Request $request)
    {
        view()->share('title', 'Список трекеров в "Необработанных"');
        $type_transport = TypeTransport::all();
        $statuses = TrackerStatus::groupBy('number')->get();
        $warehouses = Warehouse::all();

        $trackers = Trackers::applyFilters($request)
            ->warehouseCheck($request)
            ->where('is_unprocessed', '=', '1')
            ->orderBy('date_registered', 'desc')
            ->paginate(10);

        $viewData = compact('trackers', 'statuses', 'warehouses', 'type_transport');

        if (request()->ajax()) {
            return view('admin/trackers-table')->with($viewData);
        }

        return view('admin/trackers')->with($viewData);

    }

    public function trackersProcessed(Request $request)
    {
        view()->share('title', 'Список трекеров в "Обработаных"');
        $type_transport = TypeTransport::where('disabled', 0)->get();
        $statuses = TrackerStatus::groupBy('number')->get();
        $warehouses = Warehouse::all();

        $trackers = Trackers::applyFilters($request)
            ->warehouseCheck($request)
            ->where('is_processed', '=', '1')
            ->orderByDesc('date_registered')
            ->paginate(10);

        $viewData = compact('trackers', 'statuses', 'warehouses', 'type_transport');

        if (request()->ajax()) {
            return view('admin/trackers-table')->with($viewData);
        }

        return view('admin/trackers')->with($viewData);
    }

    public function trackersReady(Request $request)
    {
        view()->share('title', 'Список готовых к отправлению трекеров');
        $type_transport = TypeTransport::where('disabled', 0)->get();
        $statuses = TrackerStatus::groupBy('number')->get();
        $warehouses = Warehouse::all();

        $trackers = Trackers::with('status')->applyFilters($request)
            ->warehouseCheck($request)
            ->where('is_active', '=', '1')
            ->orderByDesc('date_registered')
            ->paginate(10);

        $viewData = compact('trackers', 'statuses', 'warehouses', 'type_transport');

        if (request()->ajax()) {
            return view('admin/trackers-table')->with($viewData);
        }

        return view('admin/trackers')->with($viewData);

    }

    public function trackersTrash(Request $request)
    {
        view()->share('title', 'Список трекеров в корзине');
        $type_transport = TypeTransport::where('disabled', 0)->get();
        $statuses = TrackerStatus::groupBy('number')->get();
        $warehouses = Warehouse::all();

        $trackers = Trackers::onlyTrashed()
            ->applyFilters($request)
            ->warehouseCheck($request)
            ->with('status')
            ->paginate(10);

        $viewData = compact('trackers', 'statuses', 'warehouses', 'type_transport');

        if (request()->ajax()) {
            return view('admin/trackers-table')->with($viewData);
        }

        return view('admin/trackers')->with($viewData);
    }

    public function trackersFromWarehouse(Request $request)
    {
        if(!$request->user()->isWarehouseOperator()) {
            return abort(404);
        }

        view()->share('title', 'Список трекеров к отправлению');
        $type_transport = TypeTransport::where('disabled', 0)->get();
        $statuses = TrackerStatus::groupBy('number')->get();
        $warehouses = Warehouse::all();

        $warehouseId = $request->user()->getWarehouseId();

        $trackers = Trackers::applyFilters($request)
            ->warehouseCheck($request)
            ->where('from_warehouse_id', '=', $warehouseId)
            ->with('status')
            ->paginate(10);

        $viewData = compact('trackers', 'statuses', 'warehouses', 'type_transport');

        if (request()->ajax()) {
            return view('admin/trackers-table')->with($viewData);
        }

        return view('admin/trackers')->with($viewData);

    }

    public function trackersToWarehouse(Request $request)
    {
        if(!$request->user()->isWarehouseOperator()) {
            return abort(404);
        }

        view()->share('title', 'Список трекеров к получению');
        $type_transport = TypeTransport::where('disabled', 0)->get();
        $statuses = TrackerStatus::groupBy('number')->get();
        $warehouses = Warehouse::all();

        $warehouseId = $request->user()->getWarehouseId();

        $trackers = Trackers::applyFilters($request)
            ->warehouseCheck($request)
            ->where('to_warehouse_id', '=', $warehouseId)
            ->with('status')
            ->paginate(10);

        $viewData = compact('trackers', 'statuses', 'warehouses', 'type_transport');

        if (request()->ajax()) {
            return view('admin/trackers-table')->with($viewData);
        }

        return view('admin/trackers')->with($viewData);

    }

    public function editTrackers($id, Request $request)
    {
        $tracker = Trackers::leftJoin('containers', 'containers.id', '=', 'trackers.container_id')
            ->select('containers.number_container', 'containers.id_type_container', 'trackers.*')
            ->warehouseCheck($request)
            ->findOrFail($id);

        $formAction = route('admin-update-trackers', $tracker->id);

        if ($tracker->is_unprocessed == 1) {
            $formAction = route('update-lvl-trackers', $tracker->id);
        }

        $currentTypeStatuses = TrackerStatus::groupBy('number')->get();

        $historyStatuses = HistoryStatuses::where('id_tracker', '=', $tracker->id)
            ->select('history_statuses.*')
            ->get();

        $currencies = Currency::onlyActive()->get();
        $type_containers = TypeContainers::all();
        $type_inventories = TypeInventory::all();
        $type_transports = TypeTransport::all();
        $bill_duties = BillDuty::orderBy('order_number','asc')->get();
        $bill_transportations = BillTransportation::orderBy('order_number','asc')->get();
        $freight_services = FreightService::orderedByNumber()->get();
        $special_handlings = SpecialHandling::orderBy('order_number','asc')->get();
        $service_deliveries = ServiceDelivery::all();

        $historyStatusTrackers = HistoryStatuses::where('id_tracker', '=', $tracker->id)->latest()->first();

//		$batches = Batch::all();
        $countries = Country::withWarehouses()->get();
        $units_of_length = MeasureUnit::onlyLength()->get();
        $units_of_weight = MeasureUnit::onlyWeight()->get();

        return view('admin/edit-trackers')->with(compact(
            'tracker',
            'formAction',
            'currentTypeStatuses',
            'historyStatuses',
            'type_containers',
            'type_inventories',
            'type_transports',
            'bill_duties',
            'bill_transportations',
            'freight_services',
            'special_handlings',
            'service_deliveries',
            'historyStatusTrackers',
            'currencies',
            'units_of_length',
            'units_of_weight',
            'countries'
        ));
    }

    public function deleteTrackers($id, Request $request)
    {
        $user = $request->user();

        if($request->reason === null && $request->reason_second != null){
            $reason = $request->reason_second;
        }elseif($request->reason != null && $request->reason_second != null) {
            $reason = $request->reason_second;
        } elseif($request->reason === null && $request->reason_second === null) {
            Session::flash('success-message', trans('admin.cant_del_without_reason'));
            return redirect()->route('trackers');
        }else {
            $reason =  $request->reason;
        }

        Trackers::findOrFail($id)->update([
            'delete_reason' => $reason,
            'delete_by' => Auth::user()->id,
        ]);
        Trackers::warehouseCheck($request)->findOrFail($id)->delete();
        Session::flash('success-message', trans('admin.success_del'));
        return redirect()->route('trackers');
    }

    public function recoveryTrackers($id, Request $request)
    {
        Trackers::withTrashed()->warehouseCheck($request)->findOrFail($id)->restore();
        Session::flash('success-message', trans('admin.success_recovery'));
        return redirect()->route('trackers-trash');
    }

    public function forceDeleteTrackers($id, Request $request)
    {
        Trackers::warehouseCheck($request)->findOrFail($id)->forceDelete();
        Session::flash('success-message', 'Трекер успешно удален!');
        return redirect()->route('trackers-trash');
    }

    public function createTrackers($id = null)
    {
        $statusTrackers = TrackerStatus::groupBy('number')->get();
        $type_containers = TypeContainers::all();
        $type_inventories = TypeInventory::all();
        $type_transports = TypeTransport::all();
        $currencies = Currency::onlyActive()->get();
        $bill_duties = BillDuty::orderBy('order_number','asc')->get();
        $bill_transportations = BillTransportation::orderBy('order_number','asc')->get();
        $freight_services = FreightService::orderedByNumber()->get();
        $special_handlings = SpecialHandling::orderBy('order_number','asc')->get();
        $service_deliveries = ServiceDelivery::all();

//		$batches = Batch::all();
        $countries = Country::withWarehouses()->get();
        $units_of_length = MeasureUnit::onlyLength()->get();
        $units_of_weight = MeasureUnit::onlyWeight()->get();

        $warehouse_user = WarehouseUser::where('user_id', Auth::user()->id)->first();
        if($warehouse_user){
            $warehouse_id = $warehouse_user->warehouse_id;
            $warehouse = Warehouse::where('id', $warehouse_id)->first();
            $country_id = Warehouse::where('id', $warehouse_id)->first()->country_id;
            $warehouse_country = Country::where('id', $country_id)->first();
        }else{
            $warehouse_country = null;
            $warehouse = null;
        }


        return view('admin/create-trackers')->with(compact(
            'statusTrackers',
            'type_containers',
            'type_inventories',
            'type_transports',
            'currencies',
            'bill_duties',
            'bill_transportations',
            'freight_services',
            'special_handlings',
            'service_deliveries',
            'units_of_length',
            'units_of_weight',
            'countries',
            'warehouse_country',
            'warehouse'
        ));
    }

    public function getStatusTrackers()
    {
        return TrackerStatus::groupBy('number')->get();
    }

    public function getCurrencies()
    {
        return Currency::all();
    }


    public function getAdditional($id)
    {
        $tracker = Trackers::select('id', 'type_transport', 'express_freight', 'special_handling')->where('id', $id)->first();
        return Response::json($tracker);
    }

    /**
     * Отвечает за отправление в "Обработаные"
     */

    public function updateLevelTrackers(UpdateLevelTrackersRequest $request, $id)
    {
        if (! request()->ajax()) return abort(404);

        $uniqueData = [
            'is_unprocessed' => 0,
            'is_processed' => 1,
            'is_active' => 0,
            'id_moderator' => Auth::id(),
        ];

        $commonData = Trackers::getCommonRequestData($request);
        $mergedData = array_merge($commonData, $uniqueData);

        $mergedData = Trackers::presaveModifyAdditionalData($mergedData);

        $mergedData = Arr::except($mergedData, [
            'is_draft'
        ]);

        $mergedData['name_employee_accepted'] = $request->user()->name;

        $tracker = Trackers::warehouseCheck($request)->findOrFail($id);
        $tracker->update($mergedData);

        $tracker->setAdditionalData($request->all());

        $statusId = $request->get('status_id');

        if ($request->get('old_status_id') < $statusId && $statusId !== null) {
            HistoryStatuses::create([
                'status_id' => $statusId,
                'id_tracker' => $id,
                'id_user' => Auth::user()->id
            ]);
        }

//        $batchId = $request->get('batch_id');

//		if ($batchId !== null) {
//			$tracker->attachAndSyncBatch(Batch::find($batchId), Auth::user()->id);
//		}

        $tracker->processTrackersReceptacles($request);
        $tracker->processTrackersDocuments($request);

        $tracker->updateSenderAndRecipient($request);

        Session::flash('success-message', 'Трекер успешно добавлен в раздел "Обработаные"');
        return response()->json([
            'redirect' => route('admin-edit-trackers', $id)
        ], 200);


    }

    /*
     * Отвечает за подтверждение и отправку в "Готовые к отправлению"
     * */

    public function updateTrackers(UpdateTrackersRequest $request,  $id, $save=false)
    {
        if (! request()->ajax()) return abort(404);

        $tracker = Trackers::warehouseCheck($request)->findOrFail($id);

        if ($save) {
            $uniqueData = [
                'is_unprocessed' => $tracker->is_unprocessed,
                'is_processed' => $tracker->is_processed,
                'is_active' => $tracker->is_active,
                'id_moderator' => Auth::id(),
            ];
        } else {
            $uniqueData = [
                'is_unprocessed' => 0,
                'is_processed' => 0,
                'is_active' => 1,
                'id_moderator' => Auth::id(),
            ];
        }

        $commonData = Trackers::getCommonRequestData($request);

        $mergedData = array_merge($commonData, $uniqueData);

        $mergedData = Trackers::presaveModifyAdditionalData($mergedData);

        $mergedData = Arr::except($mergedData, [
            'is_draft'
        ]);

        $mergedData['name_employee_issued'] = $request->user()->name;
        $mergedData['name_employee_accepted'] = $tracker->name_employee_accepted;

        $tracker->update($mergedData);

        $tracker->setAdditionalData($request->all());

        $statusId = $request->get('status_id');

        if ($request->get('old_status_id') < $statusId && $statusId !== null) {
            HistoryStatuses::create([
                'status_id' => $statusId,
                'id_tracker' => $id,
                'id_user' => Auth::user()->id
            ]);
        }

//		$batchId = $request->get('batch_id');

//		if ($batchId !== null) {
//			$tracker->attachAndSyncBatch(Batch::find($batchId), Auth::user()->id);
//		}

        $tracker->processTrackersReceptacles($request);
        $tracker->processTrackersDocuments($request);

        $tracker->updateSenderAndRecipient($request);

        Session::flash('success-message', 'Трекер успешно обновлен!');

        if ($save) {
            $response['redirect'] = route('admin-edit-trackers', $tracker->id);
        }  else{
            $response['redirect'] = route('trackers');
        }

        return response()->json($response, 200);

    }

    public function saveTracker(UpdateTrackersRequest $request, $id)
    {
        return $this->updateTrackers($request, $id, true);
    }


    public function updateStatusTrackers(Request $request)
    {
        if ($request->get('trackers') !== null) {

            foreach ($request->get('trackers') as $item) {

                $tracker = Trackers::warehouseCheck($request)->find($item);

                if (!$tracker) {
                    continue;
                }

                if ($request->get('status_id') !== null) {

                    $tracker->update([
                        'status_id' => $request->get('status_id')
                    ]);

                    HistoryStatuses::create([
                        'status_id' => $request->get('status_id'),
                        'id_tracker' => $item,
                        'id_user' => Auth::user()->id
                    ]);

                }

//                if ($request->get('tracker_to_warehouse_id') !== null) {
//					$tracker->update([
//						'warehouse_id' => $request->get('tracker_to_warehouse_id')
//					]);
//
//					HistoryWarehouse::create([
//						'warehouse_id' => $request->get('tracker_to_warehouse_id'),
//						'tracker_id' => $item,
//						'user_id' => Auth::user()->id
//					]);
//				}

            }

            if ($request->get('status_id') !== null || $request->get('status-available-tracker') !== null) {
                Session::flash('success-message', "Успешно обновлено!");
            }
        }

        return redirect()->back();
    }

    public function searchTrackers(Request $request)
    {
        view()->share('title', 'Список найденных трекеров');
        $searchParams = [];

        foreach ($request->all() as $key => $param) {
            if ($request->get($key) != null) {
                if ($key != 'min_date_registered'
                    && $key != 'max_date_registered'
                    && $key != 'min_duty_price'
                    && $key != 'max_duty_price'
//                    && $key != 'min_assessed_price'
//                    && $key != 'max_assessed_price'
                    && $key != 'min_total_price'
                    && $key != 'max_total_price'
                    && $key != '_url'
                    && $key != 'id_tracker'
                    && $key != 'number_invoice'
                ) {
                    array_push($searchParams, [$key, $request->get($key)]);
                } elseif ($key == 'min_date_registered') {
                    array_push($searchParams, ['date_registered', '>=', date($request->get($key))]);
                } elseif ($key == 'max_date_registered') {
                    array_push($searchParams, ['date_registered', '<=', date($request->get($key))]);
                } elseif ($key == 'min_total_price') {
                    array_push($searchParams, ['total_price', '>=', $request->get($key)]);
                } elseif ($key == 'max_total_price') {
                    array_push($searchParams, ['total_price', '<=', $request->get($key)]);
                }
                if ($key == 'id_tracker') {
                    array_push($searchParams, ['id_tracker', 'like', '%' . $request->get($key) . '%']);
                }
                if ($key == 'number_invoice') {
                    array_push($searchParams, ['number_invoice', 'like', '%' . $request->get($key) . '%']);
                }
            }
        }

        $trackers = Trackers::where($searchParams)->warehouseCheck($request)->paginate(10);
        return view('admin/trackers')->with(compact('trackers'));
    }

    public function trashSearchTrackers(Request $request)
    {
        view()->share('title', 'Список найденных трекеров');
        $searchParams = [];

        foreach ($request->all() as $key => $param) {
            if ($request->get($key) != null) {
                if ($key != 'min_date_registered'
                    && $key != 'max_date_registered'
                    && $key != 'min_duty_price'
                    && $key != 'max_duty_price'
                    && $key != 'min_total_price'
                    && $key != 'max_total_price'
                    && $key != '_url'
                ) {
                    array_push($searchParams, [$key, $request->get($key)]);
                } elseif ($key == 'min_date_registered') {
                    array_push($searchParams, ['date_registered', '>=', date($request->get($key))]);
                } elseif ($key == 'max_date_registered') {
                    array_push($searchParams, ['date_registered', '<=', date($request->get($key))]);
//                } elseif ($key == 'min_duty_price') {
//                    array_push($searchParams, ['duty_price', '>=', $request->get($key)]);
//                } elseif ($key == 'max_duty_price') {
//                    array_push($searchParams, ['duty_price', '<=', $request->get($key)]);
//                } elseif ($key == 'min_assessed_price') {
//                    array_push($searchParams, ['assessed_price', '>=', $request->get($key)]);
//                } elseif ($key == 'max_assessed_price') {
//                    array_push($searchParams, ['assessed_price', '<=', $request->get($key)]);
                } elseif ($key == 'min_total_price') {
                    array_push($searchParams, ['total_price', '>=', $request->get($key)]);
                } elseif ($key == 'max_total_price') {
                    array_push($searchParams, ['total_price', '<=', $request->get($key)]);
                }
            }
        }

        //dd($searchParams);

        //dd(Trackers::where($searchParams)->onlyTrashed()->toSql());
        $trackers = Trackers::where($searchParams)->warehouseCheck($request)->onlyTrashed()->paginate(10);
        return view('admin/trash-trackers')->with(compact('trackers'));
    }

    public function storeTrackers(StoreTrackersRequest $request)
    {
        if (! request()->ajax()) return abort(404);

        $newSender = null;
        $newRecipient = null;
        $clientInfoSenderAsUser = null; // ClientInfo model where type USER
        $clientInfoSenderAsSender = null; // ClientInfo model where type USER
        $clientInfoRecipientAsUser = null; // ClientInfo model where type USER
        $clientInfoRecipientAsRecipient = null; // ClientInfo model where type USER

        $isNewSender = false;
        $isNewRecipient = false;

        $nameFileSender = null;
        $nameFileRecipient = null;

        $senderCommonData = ClientInfo::getSenderDataFromRequest($request);

        $recipientCommonData = ClientInfo::getRecipientDataFromRequest($request);

        DB::beginTransaction();

        $checkUserSender = User::where('number', $request->get('number_sender'))->first();

        if ($checkUserSender !== null) {
            $newSender = $checkUserSender;
        } else {

            // create new user from sender data
            $randomClientID = User::createRandomClientID();
            $newSenderPass = str_random(8);
            $newSender = User::create([
                'id_client' => $randomClientID,
                'name' => $request->get('full_name_sender'),
                'email' => $request->get('email_sender'),
                'password' => bcrypt($newSenderPass),
                'number' => $request->get('number_sender')
            ]);

            // attach role to new user
            UsersRoles::createUserRole($newSender->id, 1); // role_id = 1 is user role

            // create new ClientInfo from sender data as user
            $clientInfoSenderAsUser = ClientInfo::createClientInformation($newSender->id_client);
            $clientInfoSenderAsUser->update(array_merge($senderCommonData, [
                'type' => ClientInfo::TYPE_USER,
            ]));

            $clientInfoSenderAsSender = ClientInfo::createClientInformation($newSender->id_client);
            $clientInfoSenderAsSender->update(array_merge($senderCommonData, [
                'type' => ClientInfo::TYPE_SENDER,
            ]));

            $isNewSender = true;
        }


        $checkUserRecipient = User::where('number', $request->get('number_recipient'))->first();

        if ($checkUserRecipient !== null) {
            $newRecipient = $checkUserRecipient;
        } else {

            // create new user from recipient data
            $randomRecipientID = User::createRandomClientID();
            $newRecipientPass = str_random(8);
            $newRecipient = User::create([
                'id_client' => $randomRecipientID,
                'name' => $request->get('full_name_recipient'),
                'email' => $request->get('email_recipient'),
                'password' => bcrypt($newRecipientPass),
                'number' => $request->get('number_recipient')
            ]);

            // attach role to new user
            UsersRoles::createUserRole($newRecipient->id, 1);

            // create new ClientInfo from recipient data as user
            $clientInfoRecipientAsUser = ClientInfo::createClientInformation($newRecipient->id_client);
            $clientInfoRecipientAsUser->update(array_merge($recipientCommonData, [
                'type' => ClientInfo::TYPE_USER,
            ]));

            $clientInfoRecipientAsRecipient = ClientInfo::createClientInformation($newRecipient->id_client);
            $clientInfoRecipientAsRecipient->update(array_merge($recipientCommonData, [
                'type' => ClientInfo::TYPE_RECIPIENT,
            ]));

            $isNewRecipient = true;

        }

        $uniqueData = [
            'is_active' => 0,
            'is_unprocessed' => 0,
            'is_processed' => 1,
            'id_moderator' => Auth::id(),
        ];

        $commonData = Trackers::getCommonRequestData($request);
        $mergedData = array_merge($commonData, $uniqueData);

        $mergedData = Trackers::presaveModifyAdditionalData($mergedData);

        $mergedData = Arr::except($mergedData, [
            'is_draft'
        ]);

        $tracker = Trackers::create($mergedData);

        if ($isNewSender) {
            // if new fill up tracker_id
            $clientInfoSenderAsSender->update([ 'tracker_id' => $tracker->id ]);
        } else {

            $clientInfoSenderAsSender = ClientInfo::createClientInformation($newSender->id_client);
            $clientInfoSenderAsSender->update(array_merge($senderCommonData, [
                'type' => ClientInfo::TYPE_SENDER,
                'tracker_id' => $tracker->id
            ]));

//			$clientInfoSenderAsSender = ClientInfo::getClientInfoAsSender($newSender->id_client, $tracker->id);
//			if ($clientInfoSenderAsSender) {
//				$clientInfoSenderAsSender->update($senderCommonData);
//			} else {
//				$clientInfoSenderAsSender = ClientInfo::createClientInformation($newSender->id_client);
//				$clientInfoSenderAsSender->update(array_merge($senderCommonData, [
//					'type' => ClientInfo::TYPE_SENDER,
//				]));
//			}
        }

        if ($isNewRecipient) {
            // if new fill up tracker_id
            $clientInfoRecipientAsRecipient->update([ 'tracker_id' => $tracker->id ]);
        } else {

            $clientInfoRecipientAsRecipient = ClientInfo::createClientInformation($newRecipient->id_client);
            $clientInfoRecipientAsRecipient->update(array_merge($recipientCommonData, [
                'type' => ClientInfo::TYPE_RECIPIENT,
                'tracker_id' => $tracker->id
            ]));

//			$clientInfoRecipientAsRecipient  = ClientInfo::getClientInfoAsRecipient($newRecipient->id_client, $tracker->id);
//			if ($clientInfoRecipientAsRecipient) {
//				$clientInfoRecipientAsRecipient->update($recipientCommonData);
//			} else {
//				$clientInfoRecipientAsRecipient = ClientInfo::createClientInformation($newRecipient->id_client);
//				$clientInfoRecipientAsRecipient->update(array_merge($recipientCommonData, [
//					'type' => ClientInfo::TYPE_RECIPIENT,
//				]));
//			}
        }

        $tracker->setAdditionalData($request->all());

        $statusId = $request->get('status_id');

        if ($statusId !== null) {
            HistoryStatuses::create([
                'status_id' => $statusId,
                'id_tracker' => $tracker->id,
                'id_user' => $newSender->id
            ]);
        }

//		$batchId = $request->get('batch_id');

//		if ($batchId !== null) {
//			$tracker->attachAndSyncBatch(Batch::find($batchId), Auth::user()->id);
//		}

//		HistoryWarehouse::create([
//			'warehouse_id' => $request->get('warehouse_id'),
//			'tracker_id' => $tracker->id,
//			'user_id' => Auth::user()->id
//		]);


        $tracker->processTrackersReceptacles($request);
        $tracker->processTrackersDocuments($request);

        DB::commit();

        // времено выпилил email

//        if ($isNewSender != false) {
//            mail("info@fhd.com.ua", "Создание трекера", "Спасибо, что нас выбрали. \nВаши данные для входа в Ваш кабинет:\nЛогин: " . $request->get('email_sender') . "\nПароль: " . $newSenderPass . "");
//        } else {
//            mail("info@fhd.com.ua", "Создание трекера", "Спасибо, что нас выбрали.");
//        }
//
//        if ($isNewRecipient != false) {
//            mail("info@fhd.com.ua", "Создание трекера", "Спасибо, что нас выбрали. \nВаши данные для входа в Ваш кабинет:\nЛогин: " . $request->get('email_recipient') . "\nПароль: " . $newRecipientPass . "");
//        } else {
//            mail("info@fhd.com.ua", "Создание трекера", "Спасибо, что нас выбрали.");
//        }
//
//        $mailData = [
//            'info' => 'На Вашем сайте оформлена заявка (" . $tracker->created_at . ")"'
//        ];
//        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
//        $beautymail->send('emails.mm', $mailData, function ($message) {
//            $message
//                ->from('info@fhd.com.ua')
//                ->to('leetruk12@gmail.com', 'FHD')
//                ->subject('Создание трекера!');
//        });
//
//        $mailData1 = [
//            'info' => 'На вашем сайте оформлена заявка: http://fhd.com.ua/admin/trackers/edit/". $tracker->id .", пожалуйста, перейдите на неё и отработайте.',
//        ];
//        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
//        $beautymail->send('emails.mm', $mailData1, function ($message) {
//            $message
//                ->from('info@fhd.com.ua')
//                ->to('leetruk12@gmail.com', 'FHD')
//                ->subject('Создание трекера!');
//        });

        Session::flash('success-message', 'Трекер создан!');

        return response()->json([
            'redirect' => route('trackers-processed')
        ]);
    }

    public function searchContainers($id)
    {
        return response()->json(Containers::where('number_container', 'like', '%' . $id . '%')->get()->take(10));
    }

    public function getContainerType($id)
    {
        return response()->json(TypeContainers::find($id));
    }

    public function getTypeContainers()
    {
        return response()->json(TypeContainers::all());
    }

    /* --- History clients --- */

    public function historyClients()
    {
        $queryBuilder = ClientInfo::onlySenders()->withTrackers()->distinct();

        $clients = $queryBuilder->paginate(10);
        $viewData = compact('clients');

        return view('admin/clients-history')->with($viewData);
    }

    public function getHistoryClient($id)
    {
        $trackers = Trackers::with('status')
            ->leftJoin('client_infos', 'client_infos.tracker_id', '=', 'trackers.id')
            ->where('client_infos.type', '=', ClientInfo::TYPE_SENDER)
            ->where('client_infos.client_id', '=', $id)
            ->paginate(10);

        return view('admin/clients-history-details')->with(compact('trackers'));
    }

    public function appendAdminNote(Request $request, $id)
    {
        Trackers::find($id)->update([
            'note_moder' => $request->get('note_moder')
        ]);
        return redirect()->back();
    }




    /* --- Settings --- */

    public function settings()
    {
        $settings = Settings::orderBy('id')->first();
        return view('admin/settings')->with(compact('settings'));
    }

    public function updateSettings(Request $request)
    {
        Settings::first()->update([
            'title_site' => $request->get('title_site'),
            'short_description' => $request->get('short_description'),
            'email' => $request->get('email'),
            'count_news_admin' => $request->get('count_news_admin'),
            'count_news_blog' => $request->get('count_news_blog'),
            'count_trackers_admin' => $request->get('count_trackers_admin'),
            'count_result_trackers_admin' => $request->get('count_result_trackers_admin'),
            'is_offline' => $request->get('is_offline'),
            'desc_offline_site' => $request->get('desc_offline_site')
        ]);

        Session::flash('success-message', 'Настройки сохранены!');

        return redirect()->back();
    }

}