<?php

namespace App\Http\Controllers\Admin;

use App\News;
use App\NewsCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\ControllerAdmin;
use IvanLemeshev\Laravel5CyrillicSlug\Slug;

class NewsController extends ControllerAdmin
{
	/* --- News --- */

	public function news()
	{
		$news = News::paginate(10);
		return view('admin/news')->with(compact('news'));
	}

	public function newsCreate()
	{
		$newsCategories = NewsCategories::count();
		if ($newsCategories < 1) {
			return redirect()->route('create-news-category')->withErrors(['Необходимо сначала создать категорию!']);
		}
		return view('admin/create-news');
	}

	public function newsStore(Request $request)
	{
		News::create([
			'title' => $request->get('title'),
			'slug' => Slug::make($request->get('title')),
			'id_category' => $request->get('id_category'),
			'image' => '1',
			'short_description' => $request->get('short_description'),
			'description' => $request->get('description'),
			'is_active' => $request->get('is_active')
		]);

		Session::flash('success-message', 'Новость создана!');

		return redirect()->route('news');
	}

	public function editNews($id)
	{
		$news = News::find($id);
		$categories = NewsCategories::all();
		return view('admin/edit-news')->with(compact('news', 'categories'));
	}

	public function updateNews($id, Request $request)
	{
		News::find($id)->update([
			'title' => $request->get('title'),
			'slug' => Slug::make($request->get('title')),
			'id_category' => $request->get('id_category'),
			'image' => '1',
			'short_description' => $request->get('short_description'),
			'description' => $request->get('description'),
			'is_active' => $request->get('is_active') !== null ? 1 : 0
		]);

		Session::flash('success-message', 'Новость изменена!');

		return redirect()->route('news');
	}

	public function newsCategories()
	{
		$newsCategories = NewsCategories::paginate(10);
		return view('admin/news-categories')->with(compact('newsCategories'));
	}

	public function newsCategoriesCreate()
	{
		return view('admin/create-news-category');
	}

	public function getNewsCategories()
	{
		return response()->json(NewsCategories::all());
	}

	public function newsCategoriesStore(Request $request)
	{
		$newsCategory = new NewsCategories();
		$newsCategory->create([
			'title' => $request->get('title'),
			'slug' => Slug::make($request->get('title')),
			'parent_id' => $request->get('parent_id') !== null ? $request->get('parent_id') : null,
			'description' => $request->get('description'),
			'is_active' => $request->get('is_active') !== null ? $request->get('is_active') : 0
		]);

		Session::flash('success-message', 'Категория создана!');

		return redirect()->route('news-categories');
	}

	public function newsCategoriesEdit($id)
	{
		$category = NewsCategories::find($id);
		$categories = NewsCategories::all();
		return view('admin/edit-news-category')->with(compact('category', 'categories'));
	}

	public function newsCategoriesUpdate($id, Request $request)
	{
		NewsCategories::find($id)->update([
			'title' => $request->get('title'),
			'slug' => Slug::make($request->get('title')),
			'parent_id' => $request->get('parent_id') !== null ? $request->get('parent_id') : null,
			'description' => $request->get('description'),
			'is_active' => $request->get('is_active') !== null ? $request->get('is_active') : 0
		]);

		Session::flash('success-message', 'Категория изменена!');

		return redirect()->route('news-categories');
	}
}
