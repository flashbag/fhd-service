<?php

namespace App\Http\Controllers\Admin;

use App\Models\BatchesTrackersHistory;
use App\User;
use App\Roles;
use App\Trackers;
use App\Models\Batch;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\ControllerAdmin;
use App\Http\Requests\Admin\Batch\CreateBatchRequest;
use App\Http\Requests\Admin\Batch\UpdateBatchRequest;

class BatchController extends ControllerAdmin
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$batches = Batch::paginate(10);

		return view('admin.batch.index')->with(compact('batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$warehouses = Warehouse::all();
    	$viewData = compact('warehouses');

		return view('admin.batch.create')->with($viewData);
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateBatchRequest $request
	 * @return void
	 */
    public function store(CreateBatchRequest $request)
    {
        $validated = $request->validated();

        $batch = Batch::create($validated);

        return redirect(route('admin.batch.edit', $batch->id));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$batch = Batch::findOrFail($id);
		$warehouses = Warehouse::all();

		$viewData = compact('warehouses', 'batch');

		return view('admin.batch.edit')->with($viewData);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param UpdateBatchRequest $request
	 * @param  int $id
	 * @return void
	 */
    public function update(UpdateBatchRequest $request, $id)
    {
		$batch = Batch::findOrFail($id);
		$batch->update($request->validated());

		session()->flash('success-message', __('admin.batches.delete_success'));

		return redirect(route('admin.batch.edit', $batch->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroySoft($id)
    {
		$batch = Batch::findOrFail($id);

		if ($batch->trackers->count()) {
			session()->flash('danger-message', __('admin.batches.cannot_delete'));
		} else {
			session()->flash('success-message', __('admin.batches.delete_success'));
			$batch->delete();
		}
    }

    public function batchTrackers($id)
	{
		$batch = Batch::findOrFail($id);

		$trackers = $batch->trackers()->paginate(10);

		return view('admin/batch/trackers')->with(compact('trackers'));
	}

}
