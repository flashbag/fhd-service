<?php /** @noinspection PhpInconsistentReturnPointsInspection */

namespace App\Http\Controllers\Admin;

use App\User;
use App\Roles;
use App\Trackers;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\ControllerAdmin;
use App\Http\Requests\Admin\Warehouse\UpdateWarehouseRequest;

class WarehouseController extends ControllerAdmin
{
	public function warehouse()
	{
		$warehouses = Warehouse::paginate(10);
		return view('admin/warehouse/warehouse')->with(compact('warehouses'));
	}

	public function editWarehouse($id)
	{
		$warehouse = Warehouse::find($id);

		return view('admin/warehouse/edit-warehouse')->with(compact('warehouse'));
	}

	public function updateWarehouse($id, UpdateWarehouseRequest $request)
	{
		$validated = $request->validated();

		$warehouse = Warehouse::find($id);

		$warehouse->update($validated);

		$allWarehouseUsers = [];

		if (isset($validated['warehouse_admins'])) {
			$allWarehouseUsers = array_merge($allWarehouseUsers, $validated['warehouse_admins']);
		}

		if (isset($validated['warehouse_operators'])) {
			$allWarehouseUsers = array_merge($allWarehouseUsers, $validated['warehouse_operators']);
		}

		if (isset($validated['warehouse_couriers'])) {
			$allWarehouseUsers = array_merge($allWarehouseUsers, $validated['warehouse_couriers']);
		}

		$warehouse->users()->sync($allWarehouseUsers);

		Session::flash('success-message', 'Успешно сохранено!');

		return redirect()->back();
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|void
	 * @throws \Exception
	 */
	public function getFreeWarehouseAdmins(Request $request)
	{
		$role = Roles::where('name', 'Warehouse Admin')->first();
		return $this->getFreeWarehouseUsersByRole($request, $role);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|void
	 * @throws \Exception
	 */
	public function getFreeWarehouseOperators(Request $request)
	{
		$role = Roles::where('name', 'Warehouse Operator')->first();
		return $this->getFreeWarehouseUsersByRole($request, $role);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse|void
	 * @throws \Exception
	 */
	public function getFreeWarehouseCouriers(Request $request)
	{
		$role = Roles::where('name', 'Warehouse Courier')->first();
		return $this->getFreeWarehouseUsersByRole($request, $role);
	}

	public function trackerWarehouse($id)
    {

	    $trackers = Trackers::where(function ($query) use ($id){
            $query->where('from_warehouse_id', $id)
                ->where('status_id', '<', 6);
        })->orWhere(function ($query) use ($id){
            $query->where('to_warehouse_id', $id)
                ->where('status_id', '>=', 6);
        })->paginate(10);


        return view('admin/warehouse/tracker-warehouse')->with(compact('trackers'));
    }

    private function getFreeWarehouseUsersByRole(Request $request, Roles $role)
	{
		if (! request()->ajax()) {
			return abort(404);
		}

		if (!$role) {
			throw new \Exception('Cant find role!');
		}

		$query = $request->get('query');
		$usersIds = $request->get('users_ids');

		$queryBuilder = User::search($query);

		if (!empty($usersIds)) {
			$queryBuilder->whereNotIn('users.id', $usersIds);
		}

		// get only users with Warehouse role
		$queryBuilder->whereHas('roles', function ($query) use($role) {
			$query->where('name', '=', $role->name);
		});

		$queryBuilder->doesntHave('warehouses');

		$searchResult = $queryBuilder->paginate(10);

		return $this->searchAjaxResponse($searchResult, function($value) {
			return [
				'id' => $value['id'],
				'text' => $value['name'] . ' - ' . $value['email']
			];
		});
	}
}
