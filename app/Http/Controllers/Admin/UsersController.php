<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Roles;
use App\UsersRoles;
use App\Models\ClientInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\ControllerAdmin;

class UsersController extends ControllerAdmin
{
	/* --- Users --- */

	public function users(Request $request)
	{
		$roles = Roles::all();
		$users = User::applyFilters($request)->paginate(10);

		$viewData = compact('users', 'roles');

		if (request()->ajax()) {
			return view('admin/users-table')->with($viewData);
		}

		return view('admin/users')->with($viewData);
	}

	public function createUsers()
	{
		return view('admin/create-users');
	}

	public function storeUsers(Request $request)
	{
		$randomClientID = mt_rand(10000, 99999);
		$userFind = User::where('id_client', '=', $randomClientID)->get();
		$roles = Roles::all();

		if ($userFind->count() > 0) {
			$randomClientID = mt_rand(10000, 99999);
		}

		$validator = Validator::make($request->all(), [
			'name_user' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'number' => 'required|string|unique:users',
			'password' => 'required|string|min:6',
			'repeat-password' => 'required|string|min:6|same:password',
			'role_id' => 'required',

		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		$user = User::create([
			'id_client' => $randomClientID,
			'name' => $request->get('name_user'),
			'email' => $request->get('email'),
			'number' => $request->get('number'),
			'password' => bcrypt($request->get('password'))
		]);
		if ($user->id !== null) {
			$userRole = new UsersRoles();
			$userRole->create([
				'user_id' => $user->id,
				'role_id' => $request->get('role_id')
			]);
		}
		Session::flash('success-message', 'Пользователь создан!');

		return redirect()->route('users');


	}

	public function editUsers($id)
	{
		$user = User::find($id);
		$roles = Roles::all();
		$userRole = UsersRoles::where('user_id', '=', $id)->select('role_id')->first()->role_id;
		return view('admin/edit-users')->with(compact('user', 'roles', 'userRole'));
	}

	public function updateUser($id, Request $request)
	{
		User::find($id)->update([
			'name' => $request->get('name_user'),
			'email' => $request->get('email')
		]);
		$getUserRole = UsersRoles::where('user_id', $id);

		$getUserRole->update([
			'role_id' => $request->get('role_id')
		]);

		Session::flash('success-message', 'Пользователь изменен!');

		return redirect()->route('users');
	}

	/* --- User roles --- */

	public function getRoles()
	{
		return response()->json(Roles::all());
	}

	public function searchUserByNumber($number)
	{
		$queryBuilder = User::leftJoin('client_infos', 'users.id_client', '=', 'client_infos.client_id')
			->select([
				'users.*',
				// TODO https://trello.com/c/KGlHRAi1
//				'client_infos.inn',
//				'client_infos.full_name',
//				'client_infos.company_name',
//				'client_infos.passport_data',
//				'client_infos.country',
//				'client_infos.city',
//				'client_infos.address',
//				'client_infos.index',
			])
			->where('users.number', 'like', '%' . $number . '%')
			->where('client_infos.type', '=', ClientInfo::TYPE_USER)
			->groupBy('users.id');

//		dd($queryBuilder->toSql());
		$results =  $queryBuilder->get()->take(10);

		return response()->json($results);
	}
}
