<?php

namespace App\Http\Controllers;

use App\Trackers;
use App\Models\Warehouse;
use App\Models\TypeTransport;
use Illuminate\Http\Request;

class PrintController extends Controller
{

    public function printTracker($idTracker, Request $request)
	{
		$tracker = Trackers::leftJoin('containers', 'containers.id', '=', 'trackers.container_id')
			->leftJoin('type_containers', 'type_containers.id', '=', 'containers.id_type_container')
			->select('containers.number_container', 'containers.id_type_container',
				'type_containers.title as type_containers_title',
				'trackers.*'
			)
			->where('id_tracker', $idTracker)
			->first();

		$isChinaTracker = false;
		$typeTransport = false;

		// TODO fetch warehouse using unique_id field after new merging dev to master
		$warehouseChinaMain = Warehouse::where('short_country', 'CH')->first();

		if ($tracker) {
			$typeTransport = TypeTransport::find($tracker->type_transport_id);

			$isChinaTracker = $tracker->isFromWarehouse($warehouseChinaMain);
		}

		return view('print-trackers')->with(compact(
			'tracker',
			'typeTransport',
			'isChinaTracker'
		));
	}


}
