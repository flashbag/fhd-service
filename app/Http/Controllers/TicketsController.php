<?php

namespace App\Http\Controllers;

use App\Comment;
use App\FileComment;
use App\Mailers\AppMailer;
use App\Models\Cargo;
use App\Pivots\WarehouseUser;
use App\Ticket;
use App\Trackers;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Postmark\PostmarkClient;

class TicketsController extends Controller
{
    public function create()
    {
        if (!Auth::check()) {
            return redirect('login');
        }

        $this->setTitleAndDescription('Создание трекера');
        $trackers = Trackers::leftJoin('client_infos', 'client_infos.tracker_id', '=', 'trackers.id')
            ->where('client_infos.user_id', '=', Auth::user()->id)
            ->get();
        return view('tickets.create', compact('trackers'));
    }

    public function createTickets($tracker_id)
    {
        if (!Auth::check()) {
            return redirect('login');
        }


        $this->setTitleAndDescription('Создание трекера');
        $trackers = Trackers::leftJoin('client_infos', 'client_infos.tracker_id', '=', 'trackers.id')
            ->where('client_infos.user_id', '=', Auth::user()->id)
            ->get();
        $id_tracker = Trackers::where('id', $tracker_id)->value('id_tracker');
        $ticket = Ticket::where('tracker_id', $tracker_id)->value('id');
        if($ticket){
            if(Auth::user()->isUser()){
                return redirect()->route('warehouse.tickets.show', ['id_tracker' => $id_tracker]);
            }
            else {
                return redirect()->route('warehouse.tickets.show', ['id_tracker' => $id_tracker]);
            }
        }
        else {
            return view('tickets.create', compact('trackers', 'tracker_id', 'id_tracker'));
        }
    }

    public function store(Request $request, AppMailer $mailer)
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->validate($request, [
            'tracker'  => 'required',
            'message'   => 'required'
        ]);

        $ticket = new Ticket([
            'user_id' => Auth::user()->id,
            'tracker_id' => $request->input('tracker'),
            'status' => false,
        ]);
        $ticket->save();

        $comment = new Comment([
            'user_id' => Auth::user()->id,
            'ticket_id' => $ticket->id,
            'text' => $request->input('message'),
        ]);
        $comment->save();

        if($request->hasFile('file')) {
            foreach ($request->file() as $file) {
                foreach ($file as $f) {
                    $filename = time() . '.' . $f->getClientOriginalName();
                    $location = public_path('../storage/app/public/' . $filename);
                    if(pathinfo($filename, PATHINFO_EXTENSION) == "jpg" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "jpeg" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "png" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "gif" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "webp") {
                        Image::make($f)->save($location);
                    }
                    else{
                        $f->move(storage_path('../storage/app/public/'), time().'.'.$f->getClientOriginalName());
                    }
                    $f = new FileComment([
                        'comment_id' => $comment->id,
                        'link' => $filename
                    ]);
                    $f->save();
                }
            }
        }

        //$mailer->sendTicketInformation(Auth::user(), $ticket);
        return redirect('/tickets')->with("status", "A ticket with ID: #$ticket->id has been opened.");
    }

    public function userTickets()
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->setTitleAndDescription('Мои тикеты');

        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderator')) {
            $tickets = Ticket::leftJoin('trackers', 'trackers.id', '=', 'tickets.tracker_id')
                ->select('tickets.*', 'id_tracker')
                ->paginate(10);
        }
        if(Auth::user()->hasRole('Warehouse Operator')){
            $user = WarehouseUser::where('user_id', Auth::user()->id)->first();
            if(isset($user))
            {
                $user->warehouse_id;
            }
            else{
                $user=null;
            }
            $tickets = Ticket::leftJoin('trackers', 'trackers.id', '=', 'tickets.tracker_id')
                ->where(function($query) use($user) {
                    $query->where('from_warehouse_id', $user)
                        ->orWhere('to_warehouse_id', $user);
                })
                ->select('tickets.*', 'id_tracker');
        }
        else {
            $tickets = Ticket::leftJoin('trackers', 'trackers.id', '=', 'tickets.tracker_id')
                ->leftJoin('client_infos', 'client_infos.tracker_id', '=', 'trackers.id')
                ->whereNull('trackers.deleted_at')
                ->where('client_infos.user_id', '=', Auth::user()->id)
                ->select('tickets.*', 'id_tracker')
                ->paginate(10);
        }
        return view('tickets.user_tickets', compact('tickets'));
    }

    public function show($tracker_id)
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->setTitleAndDescription('Мои тикеты');

        $id = Trackers::where('id_tracker', $tracker_id)->value('id');
        $ticket_id = Ticket::where('tracker_id', $id)->value('id');
        $ticket = Ticket::where('id', $ticket_id)->firstOrFail();
        $tracker = $ticket->trackers;
        $comments = Comment::with('user')->where('ticket_id', $ticket_id)->get();
        $user = $ticket->trackers->sender->user_id;
        $comment = $comments->last();

        if(Auth::user()->id != $comment->user_id) {
            if ($comment->date_read == NULL) {
                $ticket->update([
                    'status' => 0
                ]);
                $ticket->save;
                $comment->update([
                    'date_read' => date("Y-m-d H:i:s")
                ]);
                $comment->save();
            } else {
                $ticket->update([
                    'status' => 1
                ]);
                $ticket->save;
            }
        }
        return view('tickets.show', compact('ticket', 'tracker', 'comments', 'user'));
    }

    public function postComment(Request $request)
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->validate($request, [
            'comment'   => 'required',
//            'file' => 'mimes:jpeg,pdf,doc,docx,pdf,txt,xls,xlsx,zip,rar,jpg',
        ]);

        $comment = Comment::create([
            'ticket_id' => $request->input('ticket_id'),
            'user_id' => Auth::user()->id,
            'text' => $request->input('comment'),
            'links' => '',
        ]);
        $comment->save();

        if($request->hasFile('file')) {
            foreach ($request->file() as $file) {
                foreach ($file as $f) {
                    $filename = time() . '.' . $f->getClientOriginalName();
                    $location = public_path('../storage/app/public/' . $filename);
                    if(pathinfo($filename, PATHINFO_EXTENSION) == "jpg" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "jpeg" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "png" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "gif" ||
                        pathinfo($filename, PATHINFO_EXTENSION) == "webp") {
                        Image::make($f)->save($location);
                    }
                    else{
                        $f->move(storage_path('../storage/app/public/'), time().'.'.$f->getClientOriginalName());
                    }
                    $f = new FileComment([
                        'comment_id' => $comment->id,
                        'link' => $filename
                    ]);
                    $f->save();
                }
            }
        }

        $ticket =Ticket::where('id', $comment->ticket_id)->firstOrFail();
        $ticket->update([
            'status' => 0
        ]);
        $ticket->save();

        return redirect('/tickets')->with("status", "Your comment has be submitted.");
    }

    public function download($file){
        $pathToFile = public_path('/storage/' . $file);
        return response()->download($pathToFile);
    }

    public function sendMessage(Request $request, $id_tracker, AppMailer $mailer)
    {
        $email = $request->input('send_email');

        $mailer->sendTicket($email, $id_tracker);

//        $user  = Auth::user()->name;
//        $client = new PostmarkClient("171f57b8-8749-4dfb-8b2b-0cf22f0ad5dc");
//        $agent = new Agent();
//
//        if (App::isLocale('ru')) {
//            $sendResult = $client->sendEmailWithTemplate(
//                "info@fhd.com.ua",
//                "$email",
//                11010182,
//                [
//                    "name" => "FHD",
//                    'username' => $user->name,
//                    'browser_name' => $agent->browser(),
//                    'operating_system' => $agent->platform(),
//                    'action_url' => url('/verification', $user->email_verify_hash),
//                    'date_y' => date('Y'),
//                ]
//            );
//        }
//        else {
//            $sendResult = $client->sendEmailWithTemplate(
//                "info@fhd.com.ua",
//                "$email",
//                11010182,
//                [
//                    "name" => "FHD",
//                    'username' => $user->name,
//                    'browser_name' => $agent->browser(),
//                    'operating_system' => $agent->platform(),
//                    'action_url' => url('/verification', $user->email_verify_hash),
//                    'date_y' => date('Y'),
//                ]
//            );
//        };
        return redirect()->back()->with("status", "Message send");
    }

    public function warehouseTickets()
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->setTitleAndDescription('Мои тикеты');

        $user = WarehouseUser::where('user_id', Auth::user()->id)->first();
        if(isset($user))
        {
            $user->warehouse_id;
        }
        else{
            $user=null;
        }

        $tickets = Ticket::leftJoin('trackers', 'trackers.id', '=', 'tickets.tracker_id')
           ->where(function($query) use($user) {
               $query->where('from_warehouse_id', $user)
                   ->orWhere('to_warehouse_id', $user);
           })
            ->select('tickets.*', 'id_tracker');

        $comments = Comment::with('user')->get();
        $all_tickets = $tickets->paginate(24);
        $new_tickets = $tickets->where('status', 0)->paginate(24);

        foreach ($comments as $comment){
            $user_mess[] = $comment->user_id;
        }
        $user_mess = array_unique($user_mess);


        return view('tickets.warehouse.index', compact('all_tickets', 'comments', 'new_tickets'));
    }

    public function warehouseShow($tracker_id){
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->setTitleAndDescription('Мои тикеты');

        $id = Trackers::where('id_tracker', $tracker_id)->value('id');
        $ticket_id = Ticket::where('tracker_id', $id)->value('id');
        $ticket = Ticket::where('id', $ticket_id)->firstOrFail();
        $tracker = $ticket->trackers;
        $comments = Comment::with('user')->where('ticket_id', $ticket_id)->get();
        $user = $ticket->trackers->sender->user_id;
        $comment = $comments->last();

        if(Auth::user()->id != $comment->user_id) {
            if ($comment->date_read == NULL) {
                $ticket->update([
                    'status' => 0
                ]);
                $ticket->save;
                $comment->update([
                    'date_read' => date("Y-m-d H:i:s")
                ]);
                $comment->save();
            } else {
                $ticket->update([
                    'status' => 1
                ]);
                $ticket->save;
            }
        }
        return view('admin.tickets.show', compact('ticket', 'tracker', 'comments', 'user'));
    }

///////////////////////////////////////////////////

    public function warehouseIndex(Request $request)
	{
		view()->share('title_config', 'Warehouse Tickets');
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->setTitleAndDescription('Мои тикеты');

        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderator')) {
            $tickets = Ticket::applyFilters($request)->select('tickets.*', 'id_tracker');

        }
        elseif(Auth::user()->hasRole('Warehouse Operator')){
            $user = WarehouseUser::where('user_id', Auth::user()->id)->first();
            if(isset($user))
            {
                $user = $user->warehouse_id;
            }
            else{
                $user=null;
            }
            $tickets = Ticket::with('cargos')
                ->leftJoin('trackers', 'trackers.id', '=', 'tickets.tracker_id')
                ->whereNull('trackers.deleted_at')
                ->where(function($query) use($user) {
                    $query->where('from_warehouse_id', $user)
                        ->orWhere('to_warehouse_id', $user);
                })
                ->select('id_tracker', 'tickets.*');
        }
        else {
            $tickets = Ticket::with('cargos')
            ->leftJoin('trackers', 'trackers.id', '=', 'tickets.tracker_id')
                ->leftJoin('client_infos', 'client_infos.tracker_id', '=', 'trackers.id')
                ->whereNull('trackers.deleted_at')
                ->where('client_infos.user_id', '=', Auth::user()->id)
                ->select('tickets.*', 'id_tracker','trackers.*', 'tickets.id');
        }
        $comments = Comment::with('user')->get();
        $all_tickets = $tickets->paginate(24);
        $new_tickets = $tickets->where('status', 0)->paginate(24);
        foreach ($comments as $comment){
            $user_mess[] = $comment->user_id;
        }
        $user_mess = array_unique($user_mess);
        if (request()->ajax()) {
            return view('tickets.warehouse.index', compact('all_tickets', 'comments', 'new_tickets'));
        }
		return view('tickets.warehouse.index', compact('all_tickets', 'comments', 'new_tickets'));
	}

	public function warehouseTicketShow(Request $request, $tracker_id)
	{
        if (!Auth::check()) {
            return redirect('login');
        }

        view()->share('title_config', 'Warehouse Single Ticket');
        $id = Trackers::where('id_tracker', $tracker_id)->value('id');
        $ticket_id = Ticket::where('tracker_id', $id)->value('id');
        $ticket = Ticket::where('id', $ticket_id)->firstOrFail();
        $tracker = $ticket->trackers;
        $comments = Comment::with('user')->where('ticket_id', $ticket_id)->get();
        $user = Auth::user()->id;
        $comment = $comments->last();

        foreach ($comments as $comment){
            $user_mess[] = $comment->user_id;
        }
        $user_mess = array_unique($user_mess);

        if(Auth::user()->id != $comment->user_id) {
            if ($comment->date_read == NULL) {
                $ticket->update([
                    'status' => 0
                ]);
                $ticket->save;
                $comment->update([
                    'date_read' => date("Y-m-d H:i:s")
                ]);
                $comment->save();
            } else {
                $ticket->update([
                    'status' => 1
                ]);
                $ticket->save;
            }
        }
		return view('tickets.warehouse.ticket.show', compact('ticket', 'tracker', 'comments', 'user', 'user_mess'));
	}

}
