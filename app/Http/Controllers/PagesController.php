<?php

namespace App\Http\Controllers;

use App;
use App\Trackers;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Calculator;
use App\Models\TrackerStatus;

use App\Http\Requests\CalculatorRequest;

// TODO remove news from project totally?
use App\News;
use App\NewsCategories;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Postmark\PostmarkClient;

class PagesController extends Controller
{
    function __construct()
    {
        $statusSite = $this->getStatusSite();
        view()->share('is_offline', $statusSite->is_offline);
        view()->share('desc_offline_site', $statusSite->desc_offline_site);
    }

    public function index()
    {

        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Главная');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Головна');

        } else {

            $this->setTitleAndDescription('Home');

        }

		$countriesFrom = Country::withWarehouses()->where(function($q){
			return $q->where('iso_3166_2', 'US')
					->orWhere('iso_3166_2', 'CN');
		})->get();

		$countriesTo = Country::withWarehouses()->where('iso_3166_2', 'UA')->get();

		$currencies = Currency::onlyActive()->get();

		$currencyEur = Currency::where('code', 'EUR')->first();
		$eurMultiplierToUsd = $currencyEur->usd;

        return view('home', compact('countriesFrom', 'countriesTo', 'currencies', 'eurMultiplierToUsd'));
    }

    public function calculatorStatistic(CalculatorRequest $request)
	{
		if (!$request->ajax()) { abort(404); }

		Calculator::create($request->validated());

		return response(['message' => 'big brother is watching you']);

	}
    public function documents()
    {

        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Документы');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Документи');

        } else {

            $this->setTitleAndDescription('Documents');

        }

        return view('documents');
    }

    public function news()
    {
        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Новости');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Новини');

        } else {

            $this->setTitleAndDescription('News');

        }

        $articles = News::paginate(10);
        $newsCategories = NewsCategories::all();
        return view('news')->with(compact('articles', 'newsCategories'));
    }

    public function contacts()
    {
        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Контакты');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Контакти');

        } else {

            $this->setTitleAndDescription('Contacts');

        }

        return view('contacts');
    }

    public function aboutUs()
    {
        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('О нас');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Про нас');

        } else {

            $this->setTitleAndDescription('About us');

        }

        return view('about-us');
    }

    public function faq()
    {
        $this->setTitleAndDescription('FAQ');
        return view('faq');
    }

    public function searchTracker(Request $request)
    {

        $trackerInputID = $request->get('id_tracker');
        $trackerInputNumber = $request->get('number_recipient');

        $validator = Validator::make($request->all(), [
            'id_tracker' => 'nullable|numeric|min:11',
            'number_recipient' => 'nullable|numeric|min:8'
        ]);

        if ($validator->fails()) {
            return redirect()->route('index-trackers')
                ->withErrors($validator)
                ->withInput();
        }

        $tracker = Trackers::where('id_tracker', '=', $trackerInputID)->first();

        if (empty($tracker)) {
            return redirect()->route('index-trackers')->with('info', 'Не найдено!')->withInput();
        }

        if ($tracker->number != $trackerInputNumber) {
            return redirect()->route('index-trackers')->with('info', 'Не найдено!')->withInput();
        }

        $statuses = TrackerStatus::all();

        return view('trackers')->with(compact('tracker', 'trackerInputID', 'trackerInputNumber', 'statuses'));

    }

    public function rules()
    {

        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Правила и условия');

        } elseif (App::isLocale('ua')) {

            $this->setTitleAndDescription('Правила та умови');

        } else {

            $this->setTitleAndDescription('Terms & Conditions');

        }
        return view('rules');
    }

    public function agreement_legal()
    {
        if (App::isLocale('ru')) {

            $this->setTitleAndDescription('Договор для юридических лиц');

        }
        else {

            $this->setTitleAndDescription('Contract for legal entities');

        }
        return view('agreement_legal');
    }

    public function agreement_individual()
    {
        if (App::isLocale('ru')) {

        $this->setTitleAndDescription('Договор для физ лиц');

    } else {

        $this->setTitleAndDescription('Contract for individual entities');

    }
        return view('agreement_individual');

    }

    public function feedback(Request $request){

        $messages = [];
        if (App::isLocale('ru')) {
            $messages = [
                'textarea.required' => 'Вы не ввели сообщение.',
                'theme.required' => 'Вы не ввели тему.',
                'captcha.required' => 'Вы не ввели капчу.',
                'email.required' => 'Вы не ввели Email.',
                'username.required' => 'Вы не ввели ФИО или название компании.',
            ];
        } else {
            $messages = [
                'textarea.required' => 'You did not enter textarea.',
                'theme.required' => 'You did not enter theme.',
                'captcha.required' => 'You did not enter Country .',
                'email.required' => 'You did not enter Email.',
                'username.required' => 'You did not enter the Full name or company name.',
            ];

        }
        $validator = Validator::make($request->all(), [
            'textarea' => 'required',
            'theme' => 'required',
            'captcha' => 'required|captcha',
            'email' => 'required|email',
            'username' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $email = $request->email;
        $username = $request->username;
        $theme = $request->theme;
        $textarea = $request->textarea;

        $client = new PostmarkClient(config('mail.postmark_key'));
        $agent = new Agent();
        $mail = config('mail.from.address');
        if (\Illuminate\Support\Facades\App::isLocale('ru')) {
            $templateID = 11157550;
        } else {
            $templateID = 11157550;
        }
        $sendResult = $client->sendEmailWithTemplate(
            "noreplay@fhd.com.ua",
            "$mail",
            "$templateID",
            [
                'username' => $username,
                'email' => $email,
                'theme' => $theme,
                'textarea' => $textarea,
                'date_y' => date('Y'),
            ]
        );
        Session::flash('success-message', trans('main.reset_pass.send_mess'));
        return redirect()->back();
    }

    public function address(){
        if (App::isLocale('ru')) {
            $this->setTitleAndDescription('Адреса складов');
        } else {
            $this->setTitleAndDescription('Warehouse Addresses');
        }
        return view('address-warehouse');
    }
}
