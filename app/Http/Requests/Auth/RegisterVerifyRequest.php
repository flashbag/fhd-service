<?php

namespace App\Http\Requests\Auth;

use App\Rules\PhoneNumberRule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
	{
		$rules = [
			'phone_number' => [
				'required',
				new PhoneNumberRule()
			],
			'code' => 'required|numeric',

			'is_existing' => 'required|boolean',

			'full_name' => 'nullable|string',
		];

		return $rules;
	}

	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();

		$validator->sometimes('password', 'required|min:6', function($input) {
			return $input->is_existing == 0;
		});

		$validator->sometimes('password_confirmation', 'required|same:password', function($input) {
			return $input->is_existing == 0;
		});

		return $validator;
	}
}
