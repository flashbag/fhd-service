<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalculatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'type_transport' => 'nullable',
			'country_from' => 'nullable',
			'country_to' => 'nullable',
			'is_imperial' => 'nullable',
			'with_dimensions' => 'nullable',
			'with_custom_duty' => 'nullable',
			'actual_weight' => 'nullable',
			'width' => 'nullable',
			'length' => 'nullable',
			'height' => 'nullable',
			'assessed_price' => 'nullable',
			'currency' => 'nullable',
			'results' => 'nullable'
        ];
    }
}
