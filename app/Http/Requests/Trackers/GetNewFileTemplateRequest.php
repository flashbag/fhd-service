<?php

namespace App\Http\Requests\Trackers;

use Illuminate\Foundation\Http\FormRequest;

class GetNewFileTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'new_block_index' => 'required|integer',
			'input_name' => 'required',
			'container_selector' => 'required'
        ];
    }
}
