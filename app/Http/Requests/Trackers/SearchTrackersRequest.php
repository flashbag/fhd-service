<?php

namespace App\Http\Requests\Trackers;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PhoneNumberRule;

class SearchTrackersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'id_tracker' => 'nullable|min:11',
			'number' => [
				'nullable',
				new PhoneNumberRule()
			]
        ];
    }
}
