<?php

namespace App\Http\Requests\Admin\Trackers;

use App\Models\Cargo;
use App\Models\ClientInfo;
use App\Models\Additional\ServiceDelivery;
use App\Rules\PhoneNumberRule;
use Illuminate\Foundation\Http\FormRequest;

class CommonTrackersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$trackerDocumentsMimes = 'mimes:png,jpg,jpeg,pdf,doc,docx|max:5242880'; // 5MB

		$personTypes = ClientInfo::getPersonTypes();
		$serviceDeliveriesIdsImploded = implode(',', ServiceDelivery::getAllIds());

    	return [
			'type_transport_id' => 'required',

			'from_warehouse_id' => 'required|exists:warehouses,id',
			'to_warehouse_id' => 'required|exists:warehouses,id',

			'batch_id' => 'nullable|exists:batches,id',

			'receptacles.*.type_inventory_id' => 'required_without:receptacles.*.delete',

			'receptacles.*.assessed_price' => 'required_without:receptacles.*.delete',
			'receptacles.*.width' => 'required_without:receptacles.*.delete',
			'receptacles.*.length' => 'required_without:receptacles.*.delete',
			'receptacles.*.height' => 'required_without:receptacles.*.delete',
			'receptacles.*.actual_weight' => 'required_without:receptacles.*.delete',
			'receptacles.*.unit_of_length_id' => 'required_without:receptacles.*.delete|exists:measure_units,id',
			'receptacles.*.unit_of_weight_id' => 'required_without:receptacles.*.delete|exists:measure_units,id',
			'receptacles.*.currency_type' => 'required_without:receptacles.*.delete|exists:currencies,id',

			'receptacles.*.cargos.*.quantity' => 'required_without:receptacles.*.cargos.*.delete|numeric|min:1',
			'receptacles.*.cargos.*.description' => 'required_without:receptacles.*.cargos.*.delete|string|max:' . Cargo::CARGO_NAME_LIMIT,
			'receptacles.*.cargos.*.delete' => 'boolean',

			/**
			 * SENDER
			 */
			'number_sender' => [
				'required',
				new PhoneNumberRule()
			],
			'email_sender' => 'required|email',
			'sender_person_type' => [
				'required',
				'in:' . implode(',', $personTypes),
			],
			'full_name_sender' => 'required_without:sender_company_name',
			'sender_company_name' => 'required_without:full_name_sender',
			'number_vat' => 'nullable|string',
			'sender_number_service' => 'string|nullable|max:15',
			'sender_number_account' => 'string|nullable',
			'sender_service_delivery_id' => 'string|nullable',
			'sender_service_delivery_other' => 'required_without:sender_service_delivery_id|string|nullable',
//            'inn_sender' => 'required',
			'country_id_sender' => 'required',
			'region_sender' => 'nullable|string',
			'city_id_sender' => 'required',
			'address_sender' => 'required',
			'index_sender' => 'required|string',
			'sender_documents' => 'array',
			'sender_documents.*.file' => [
				'file',
				$trackerDocumentsMimes
			],
			'sender_documents.*.delete' => 'boolean',


			/**
			 * RECIPIENT
			 */
			'number_recipient' => [
				'required',
				new PhoneNumberRule()
			],
			'email_recipient' => 'nullable|email',
			'recipient_person_type' => [
				'required',
				'in:' . implode(',', $personTypes),
			],
			'full_name_recipient' => 'required_without:recipient_company_name',
			'recipient_company_name' => 'required_without:full_name_recipient',
			'custom_id' => 'nullable|string',
			'recipient_number_service' => 'string|nullable|max:15',
			'recipient_number_account' => 'string|nullable',
			'recipient_service_delivery_id' => 'string|nullable',
			'recipient_service_delivery_other' => 'required_without:recipient_service_delivery_id|string|nullable',
			'representative_recipient' => 'nullable',
			'country_id_recipient' => 'required',
			'region_recipient' => 'nullable|string',
			'city_id_recipient' => 'required',
			'address_recipient' => 'required',
			'index_recipient' => 'required|string',
			'recipient_documents' => 'array',
			'recipient_documents.*.file' => [
				'file',
				$trackerDocumentsMimes
			],
			'recipient_documents.*.delete' => 'boolean',

//			'number_invoice' => 'nullable|min:2|max:11',
			'date_registered' => 'required',
//			'carrier' => 'required|min:2',
			'number_container' => 'nullable|required_with:id_type_container|min:2|max:50',
			'id_type_container' => 'nullable|required_with:number_container',

			'delivery_rate_kg' => 'required|numeric',
			'delivery_rate_cur' => 'required|numeric',
			'total_ship_cost' => 'required|numeric',
			'ship_rate_cur' => 'required|numeric',


			'name_employee_issued' => 'required_if:status_id,==,10||required_if:status_id,==,20',
//            'name_employee_accepted' => 'required',

			// ADDITIONAL
			'bill_duty_id'  => 'nullable',
			'bill_duty_type' => 'nullable',

			'bill_duty_third_party' => 'required_if:bill_duty_type,third_party|email|nullable',
			'bill_duty_fhd_account' => 'required_if:bill_duty_type,fhd_account',

			'bill_transportation_id' => 'nullable',
			'bill_transportation_type' => 'nullable',

			'bill_transportation_third_party' => 'required_if:bill_transportation_type,third_party|email|nullable',

			'freight_service_id'  => 'required|exists:freight_services,id',
			'freight_service_type' => 'required|exists:freight_services,type',

			'freight_service_booking_number' => 'required_if:freight_service_type,booking_number',

			'special_handling_id' => 'required',

		];

    }
}
