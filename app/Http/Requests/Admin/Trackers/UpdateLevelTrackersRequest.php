<?php

namespace App\Http\Requests\Admin\Trackers;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Admin\Trackers\CommonTrackersRequest;

class UpdateLevelTrackersRequest extends FormRequest
{
	/**
	 * Get all of the input and files for the request.
	 *
	 * @param  array|mixed  $keys
	 * @return array
	 */
	public function all($keys = null)
	{
		$all = parent::all();
		$all['number_sender'] = str_replace(' ', '', $all['number_sender']);
		$all['number_recipient'] = str_replace(' ', '', $all['number_recipient']);
		return $all;
	}

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$commonTrackersRequest = new CommonTrackersRequest();
		$commonRules = $commonTrackersRequest->rules();

		$commonRules['id_tracker'] = 'required|min:10';

		return $commonRules;
    }
}
