<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeInventory extends Model
{
    protected $fillable = ['title'];
}
