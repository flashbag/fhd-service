<?php
/**
 * Created by PhpStorm.
 * User: flashbag
 * Date: 31.01.19
 * Time: 9:42
 */

namespace App\Traits;

trait Searchable
{

	/**
	 * Add search fields OR WHERE LIKE
	 *
	 * @param string $search
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	protected function search($search)
	{
		$searchInteger = intval($search);

		$query = self::query();

		$groupWhereParameters = [];

		$searchWords = explode(' ', $search);

		foreach ($searchWords as $searchWord) {

			$whereParametersTmp = [];

			foreach ($this->searchable as $field => $fieldType) {

				if ($fieldType == 'text') {

					if (substr_count($searchWord, '_')) {
						$searchWord = str_replace('_', '[_]', $searchWord);
					}

					$whereParametersTmp[$field] = $searchWord;
				}

				if ($fieldType == 'number' && $searchInteger) {
					$whereParametersTmp[$field] = $searchWord;
				}

			}

			$groupWhereParameters[] = $whereParametersTmp;
		}

		foreach ($groupWhereParameters as $groupWhereParameter) {

			$query->where(function ($q) use($groupWhereParameter){

				foreach ($groupWhereParameter as $field => $search) {
					$q->orWhere($field, 'like', '%' . $search . '%');
				}

			});
		}

		return $query;

	}

}
