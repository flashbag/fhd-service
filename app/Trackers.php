<?php

namespace App;

use App\Models\HistoryStatuses;
use App\Models\Warehouse;
use App\Models\Batch;
use App\Models\BatchesTrackers;
use App\Models\BatchesTrackersHistory;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Cargo;
use App\Models\Currency;
use App\Models\Receptacle;
use App\Models\ClientInfo;
use App\Models\TrackerStatus;
use App\Models\Document;
use App\Models\Additional\BillDuty;
use App\Models\Additional\BillTransportation;
use App\Models\Additional\TypeTax;
use App\Models\Additional\FHDAccount;
use App\Models\Additional\ThirdPartyEmail;
use App\Models\Additional\FreightService;
use App\Models\Additional\BookingNumber;
use App\Models\Additional\SpecialHandling;

class Trackers extends Model
{

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'id_tracker',
		'type_transport_id',
		'id_moderator',
		'number_invoice',
		'date_registered',
		'date_delivery',
		'status_id',
		'carrier',
		'container_id',
		'number_seats',
		'duty_price',
		'total_price',
		'duty_price_currency',
		'total_price_currency',
		'note_moder',

		'delivery_rate_kg',
		'delivery_rate_cur',
		'total_ship_cost',
		'ship_rate_cur',

		'name_employee_issued',
		'name_employee_accepted',
		'is_unprocessed',
		'is_processed',
		'is_active',
		'is_draft',
        'is_paid',
		// FK
		'bill_duty_id',
		'bill_transportation_id',
		'freight_service_id',
		'special_handling_id',
        'from_warehouse_id',
		'to_warehouse_id',
        'deleted_at',
        'delete_reason',
        'delete_by',
	];

	/**
	 * Scope a query to only include users of a given type.
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder $query
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeApplyFilters($query, Request $request)
	{
		$query->select(['trackers.*']);

		$query->leftJoin('client_infos AS sender', function ($join) {
			$join->on('sender.tracker_id', '=', 'trackers.id')
				->where('sender.type', '=', ClientInfo::TYPE_SENDER);
		});

		$query->leftJoin('client_infos AS recipient', function ($join) {
			$join->on('recipient.tracker_id', '=', 'trackers.id')
				->where('recipient.type', '=', ClientInfo::TYPE_RECIPIENT);
		});

		if ($request->get('statuses') > 0) {
			$query->where('status_id', $request->get('statuses') );
		}

        if ($request->get('type_transport') > 0) {
            $query->where('type_transport_id', $request->get('type_transport') );
        }

		$startDateRegistered = $request->get('start_date_registered');
		$endDateRegistered = $request->get('end_date_registered');

		if (!empty($startDateRegistered) && empty($endDateRegistered)) {
			$query->where('date_registered', '>=', $startDateRegistered );
		} else if (!empty($endDateRegistered) && empty($startDateRegistered)) {
			$query->where('date_registered', '<=', $endDateRegistered );
		} else if (!empty($startDateRegistered) && !empty($endDateRegistered)) {
			$query->whereBetween('date_registered', [$startDateRegistered, $endDateRegistered] );
		}

		$fieldByLikeMappings = [
			'id' => 'trackers.id',
			'country_id_sender' => 'sender.country',
			'city_id_sender' => 'sender.city',
			'sender_full_name' => 'sender.full_name',
			'sender_number' => 'sender.number',
			'sender_email' => 'sender.email',
			'sender_company_name' => 'sender.company_name',
			'country_id_recipient' => 'recipient.country',
			'city_id_recipient' => 'recipient.city',
			'recipient_full_name' => 'recipient.full_name',
			'recipient_company_name' => 'recipient.company_name',
			'recipient_number' => 'recipient.number',
			'recipient_email' => 'recipient.email',
            'id_tracker' => 'id_tracker',
            'number_invoice' => 'number_invoice',
            'carrier' => 'carrier',
            'container_id' => 'container_id',
		];

		foreach ($fieldByLikeMappings as $fieldInRequest => $fieldInDb) {
			$fieldInRequesValue = $request->get($fieldInRequest);

			if (!empty($fieldInRequesValue)) {
				$query->where($fieldInDb,  'like', '%'.$fieldInRequesValue. '%');
			}
		}

		$warehouseId = $request->get('warehouse_id');

		if ($warehouseId) {
			$query->where(function ($query) use ($warehouseId){
				$query->where('from_warehouse_id', $warehouseId)
					->where('status_id', '<', 6);
			})->orWhere(function ($query) use ($warehouseId){
				$query->where('to_warehouse_id', $warehouseId)
					->where('status_id', '>=', 6);
			});
		}

		$query->groupBy('trackers.id');

		return $query;
	}

	/**
	 * Scope a query to only include users of a given type.
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder $query
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeWarehouseCheck($query, Request $request)
	{
		if (
			!$request->user()->isWarehouseAdmin() &&
			!$request->user()->isWarehouseOperator() &&
			!$request->user()->isWarehouseCourier()
		) {
			// if not Warehouse user, return all trackers
			// access to dashboard have Admin, Moderator
			return $query;
		}

		// For users which is not connected to the Warehouse
		// show only trackers which also is not connected to the Warehouse, it means show nothing
		$warehouseId = null;

		if ($request->user()->warehouses->count()) {
			// Warehouse user can have only one warehouse, so get the first and one only
			$warehouseId = $request->user()->warehouses->first()->id;
		}

		return $query->where(function($q) use($warehouseId) {
			return $q->where('from_warehouse_id', '=', $warehouseId)
					->orWhere('to_warehouse_id', '=', $warehouseId);
		});
	}

	/**
	 * Scope a query to only include active trackers.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeOnlyActive($query)
	{
		return $query->where('is_active', '=', '1');
	}

	public function warehouseFrom()
	{
		return $this->belongsTo(Warehouse::class, 'from_warehouse_id', 'id');
	}

	public function warehouseTo()
	{
		return $this->belongsTo(Warehouse::class, 'to_warehouse_id', 'id');
	}

	public function batches()
	{
		return $this->belongsToMany(
			Batch::class,
			'batches_trackers',
			'tracker_id',
			'batch_id'
		)->using(BatchesTrackers::class);
	}

	public function getBatchAttribute()
	{
		return $this->batches->first();
	}

	public function batchHistory()
	{
		return $this->belongsToMany(BatchesTrackersHistory::class, 'batches_trackers_history', 'tracker_id' , 'batch_id');
	}

	public function isFromWarehouse(Warehouse $warehouse)
	{
		return $this->from_warehouse_id == $warehouse->id;
	}

	public function receptacles()
	{
		return $this->hasMany(Receptacle::class, 'tracker_id');
	}

	public function cargos()
	{
		return $this->hasManyThrough(Cargo::class, Receptacle::class, 'tracker_id');
	}

	public function getAllCargosAttribute()
	{
		$returnArray = [];


		if (!$this->receptacles || !$this->receptacles->count()) {
			return $returnArray;
		}

		foreach ($this->receptacles as $receptacleKey => $receptacle) {
			foreach ($receptacle->cargos as $cargoKey => $cargo) {
				$index = ($receptacleKey + 1) . '.' . ($cargoKey + 1);
				$returnArray[] = [
					'description' => $index . ' '. $cargo->description,
					'quantity' => $cargo->quantity,
					//'is_used' => $cargo->is_used,
				];
			}
		}

		return $returnArray;
	}

	public function documents()
	{
		return $this->hasMany(Document::class, 'tracker_id');
	}

	public function clients()
	{
		return $this->hasMany(ClientInfo::class, 'tracker_id');
	}

	public function getSenderAttribute()
	{
		// $this->clients() --- it is HasMany query builder
		// $this->clients  --- it is ready collection

		return $this->clients()
			->where('type', ClientInfo::TYPE_SENDER)
			->where('tracker_id', $this->id)
			->first();
	}

	public function getRecipientAttribute()
	{
		// $this->clients() --- it is HasMany query builder
		// $this->clients  --- it is ready collection
		return $this->clients()
			->where('type', ClientInfo::TYPE_RECIPIENT)
			->where('tracker_id', $this->id)
			->first();
	}

	public function getSenderDocumentsAttribute()
	{
		return $this->documents->where('type', Document::SENDER);
	}

	public function getRecipientDocumentsAttribute()
	{
		return $this->documents->where('type', Document::RECIPIENT);
	}

	public function status()
	{
		return $this->belongsTo(TrackerStatus::class, 'status_id');
	}

	public function billDuty()
	{
		return $this->belongsTo(BillDuty::class);
	}

	public function billTransportation()
	{
		return $this->belongsTo(BillTransportation::class);
	}

	public function freightService()
	{
		return $this->belongsTo(FreightService::class);
	}

	public function specialHandling()
	{
		return $this->belongsTo(SpecialHandling::class);
	}

	public function getBillDutyThirdPartyAttribute()
	{
		$typeTaxBillDuty = TypeTax::where('type', 'bill_duty')->first();

		if (!$typeTaxBillDuty) {
			return '';
		}

		$trackerBillDutyThirdPartyEmail = ThirdPartyEmail::where([
			'tracker_id' => $this->id,
			'type_tax_id' => $typeTaxBillDuty->id
		])->first();

		if (!$trackerBillDutyThirdPartyEmail) {
			return '';
		}

		return $trackerBillDutyThirdPartyEmail->email;
	}

	public function getBillDutyFHDAccountAttribute()
	{
		$typeTaxBillDuty = TypeTax::where('type', 'bill_duty')->first();

		if (!$typeTaxBillDuty) {
			return '';
		}

		$trackerBillDutyFHDAccount = FHDAccount::where([
			'tracker_id' => $this->id,
			'type_tax_id' => $typeTaxBillDuty->id
		])->first();

		if (!$trackerBillDutyFHDAccount) {
			return '';
		}

		return $trackerBillDutyFHDAccount->fhd_account;
	}

	public function getBillTransportationThirdPartyAttribute()
	{
		$typeTaxBillTransportation = TypeTax::where('type', 'bill_transportation')->first();

		if (!$typeTaxBillTransportation) {
			return '';
		}

		$trackerBillTransportationThirdPartyEmail = ThirdPartyEmail::where([
			'tracker_id' => $this->id,
			'type_tax_id' => $typeTaxBillTransportation->id
		])->first();

		if (!$trackerBillTransportationThirdPartyEmail) {
			return '';
		}

		return $trackerBillTransportationThirdPartyEmail->email;
	}

	public function getFreightServiceBookingNumberAttribute()
	{
		$freightService = BookingNumber::where('tracker_id', $this->id)->first();

		if (!$freightService) {
			return '';
		}

		return $freightService->booking_number;
	}

	public static function presaveModifyAdditionalData($data)
	{
		if($data['bill_transportation_id'] == null
			|| $data['bill_transportation_id'] == 'null'
			|| trim($data['bill_transportation_id']) == ''
		){
			$data['bill_transportation_id'] = null;
		}

		if($data['bill_duty_id'] == null
			|| $data['bill_duty_id'] == 'null'
			|| trim($data['bill_duty_id']) == ''){
			$data['bill_duty_id'] = null;
		}

		return $data;
	}

	public function attachAndSyncBatch(Batch $batch, $userId)
	{
		$magicArray = [
			$batch->id => [
				'user_id' => $userId,
				'warehouse_id' => $batch->id
			]
		];

		$this->batches()->sync($magicArray);
		$this->batchHistory()->attach($magicArray);
	}

	public function setAdditionalData($data)
	{
		$typeTaxBillDuty = TypeTax::where('type', 'bill_duty')->first();
		$typeTaxBillTransportation = TypeTax::where('type', 'bill_transportation')->first();



		$billDutyFindParams = [
			'tracker_id' => $this->id,
			'type_tax_id' => $typeTaxBillDuty->id
		];

		$billTransportationFindParams = [
			'tracker_id' => $this->id,
			'type_tax_id' => $typeTaxBillTransportation->id
		];

		if ($data['bill_duty_type'] == 'third_party') {
			ThirdPartyEmail::updateOrCreate($billDutyFindParams,[
				'email' => $data['bill_duty_third_party']
			]);
		}

		if ($data['bill_duty_type'] == 'fhd_account') {
			FHDAccount::updateOrCreate($billDutyFindParams,[
				'fhd_account' => $data['bill_duty_fhd_account']
			]);
		}

		if ($data['bill_transportation_type'] == 'third_party') {
			// dd($billTransportationFindParams);
			ThirdPartyEmail::updateOrCreate($billTransportationFindParams,[
				'email' => $data['bill_transportation_third_party']
			]);
		}

		if ($data['freight_service_type'] == 'booking_number') {
			// dd($billTransportationFindParams);
			BookingNumber::updateOrCreate([
				'tracker_id' => $this->id
			],[
				'booking_number' => $data['freight_service_booking_number']
			]);
		}
	}

	/**
	 * @param $request
	 * @throws \Exception
	 */
	public function processTrackersDocuments($request)
	{
		// different methods to get data and files
		$senderDocumentsFiles = $request->file('sender_documents');
		$senderDocumentsFields = $request->get('sender_documents');

		// looping fields because it is length will be always equal file inputs
		// event no file selected in any file input, field input will be just beacon
		if (!empty($senderDocumentsFields)) {

			foreach ($senderDocumentsFields as $index => $fields) {
				$senderDocuments[$index] = $fields;

				if (isset($senderDocumentsFiles[$index])) {
					$senderDocuments[$index] = array_merge($fields, $senderDocumentsFiles[$index]);
				}
			}

			if (!empty($senderDocuments)) {
				foreach ($senderDocuments as $file) {
					Document::processTrackerFile($this, $file, Document::SENDER);
				}
			}
		}


		// different methods to get data and files
		$recipientDocumentsFiles = $request->file('recipient_documents');
		$recipientDocumentsFields = $request->get('recipient_documents');

		// looping fields because it is length will be always equal file inputs
		// event no file selected in any file input, field input will be just beacon
		if (!empty($recipientDocumentsFields)) {

			foreach ($recipientDocumentsFields as $index => $fields) {
				$recipientDocuments[$index] = $fields;

				if (isset($recipientDocumentsFiles[$index])) {
					$recipientDocuments[$index] = array_merge($fields, $recipientDocumentsFiles[$index]);
				}
			}


			if (!empty($recipientDocuments)) {
				foreach ($recipientDocuments as $file) {
					Document::processTrackerFile($this, $file, Document::RECIPIENT);
				}
			}
		}
	}

	/**
	 * @param $request
	 * @return bool
	 */
	public function processTrackersReceptacles($request)
	{
		$receptacles = $request->get('receptacles');

		if (empty($receptacles)) {
			return false;
		}

		foreach ($receptacles as $receptacleIndex => $receptacleData) {
			$receptacleModel = Receptacle::processSingleReceptacle($this, $receptacleData);

			$receptacleModel->processReceptacleDocuments($request, $receptacleIndex);

			if ($receptacleModel instanceof Receptacle && isset($receptacleData['cargos'])) {
				foreach ($receptacleData['cargos'] as $cargo) {
					Cargo::processSingleCargo($this, $receptacleModel, $cargo);
				}
			}
		}
	}

	/**
	 * @param Trackers $tracker
	 * @param $request
	 * @throws \Exception
	 */
	public function updateSenderAndRecipient($request)
	{
		if (!$this->sender) {
			throw new \Exception('Tracker sender relation not exist!');
		}

		if (!$this->recipient) {
			throw new \Exception('Tracker recipient relation not exist!');
		}

		$senderData = ClientInfo::getSenderDataFromRequest($request);
		$recipientData = ClientInfo::getRecipientDataFromRequest($request);

		$this->sender->update($senderData);
		$this->recipient->update($recipientData);
	}

    public function del_rate_currency(){
        return $this->belongsTo(Currency::class, 'delivery_rate_cur', 'id');
    }
    public function ship_rate_currency(){
        return $this->belongsTo(Currency::class, 'ship_rate_cur', 'id');
    }


    public static function getCommonRequestData(Request $request)
	{
		// TODO separate this function for admin and client parts
		$containerId = null;

		$numberContainer = $request->get('number_container');
		$typeContainer = $request->get('id_type_container');

		if (!empty($numberContainer) && !empty($typeContainer)) {

			$container = Containers::where('number_container', '=', $numberContainer)->first();

			if (!$container) {
				$container = Containers::create([
					'number_container' => $numberContainer,
					'id_type_container' => $typeContainer
				]);
			}

			$containerId = $container->id;

		}

		$is_paid = $request->get('is_paid') ? 1 : 0;
		$is_draft = $request->get('is_draft') ? 1 : 0;

		$data = [
			'id_tracker' => $request->get('id_tracker'),
			'type_transport_id' => $request->get('type_transport_id'),
			'from_warehouse_id' => $request->get('from_warehouse_id'),
			'to_warehouse_id' => $request->get('to_warehouse_id'),

			'number_invoice' => $request->get('number_invoice'),
			'date_registered' => $request->get('date_registered'),
			'date_delivery' => $request->get('date_delivery'),
			'status_id' => $request->get('status_id'),
			'carrier' => $request->get('carrier'),
			'container_id' => $containerId,

			'number_seats' => count($request->get('receptacles')),

			'delivery_rate_kg' => $request->get('delivery_rate_kg'),
			'delivery_rate_cur' => $request->get('delivery_rate_cur'),
			'total_ship_cost' => $request->get('total_ship_cost'),
			'ship_rate_cur' => $request->get('ship_rate_cur'),

			'duty_price' => $request->get('duty_price'),
			'total_price' => $request->get('total_price'),

			'duty_price_currency' => $request->get('duty_price_currency'),
			'total_price_currency' => $request->get('total_price_currency'),

			'bill_duty_id' => $request->get('bill_duty_id'),
			'bill_transportation_id' => $request->get('bill_transportation_id'),
			'freight_service_id' => $request->get('freight_service_id'),

			'name_employee_issued' => $request->get('name_employee_issued'),
			'name_employee_accepted' => $request->get('name_employee_accepted'),
			'is_paid' => $is_paid,
			'is_draft' => $is_draft,
		];


		$specialHandlingId = $request->get('special_handling_id');
		if ($specialHandlingId != '--' && $specialHandlingId != 'null') {
			$data['special_handling_id'] = $specialHandlingId;
		}

		if (!filterNumbers($data['total_price_currency'])) {
			unset($data['total_price_currency']);
		}

		if (!filterNumbers($data['duty_price_currency'])) {
			unset($data['duty_price_currency']);
		}

		return $data;
	}
}
