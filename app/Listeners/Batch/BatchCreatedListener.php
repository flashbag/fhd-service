<?php

namespace App\Listeners\Batch;

use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BatchCreatedListener
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Handle the event.
	 *
	 * @param  object  $event
	 * @return void
	 */
	public function handle($event)
	{
		$randNumbers = '';

		for ($i = 0; $i <= 3; $i++) {
			$randNumbers = $randNumbers . mt_rand(0, 9);
		}

		$number = Carbon::now()->format('ymd') . $randNumbers;

		$event->batch->update([
			'number' => $number
		]);
	}
}
