<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Console\Command;

class UpdateCurrenciesExchangeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all foreign currencies exchange rate to UAH, calculate coefficient between two foreign currencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	echo 'starting updating currencies exchange rates from NBU ' . PHP_EOL;

		$url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
		$currencyUAHRates = json_decode(file_get_contents($url), true);

		if (empty($currencyUAHRates)) {
			echo 'invalid response! exiting ' . PHP_EOL;
			return false;
		}

		echo 'updating all currencies rate to UAH ' . PHP_EOL;
		// fill up all currencies rate to UAH
		foreach ($currencyUAHRates as $rate2uah) {

			$currency = Currency::where('code', $rate2uah['cc'])->first();

			if ($currency) {
				$currency->update(['uah' => $rate2uah['rate']]);
			}
		}

		$currenciesAll = Currency::all();

		echo 'updating all currencies rates between each other' . PHP_EOL;

		foreach ($currenciesAll as $currency) {

			$currency->clearCoefficients();

			foreach (Currency::ALL_CODES as $code) {

				$currencyByCode = $currenciesAll->first(function ($item) use ($code) {
					return $item->code == $code;
				});

				if ($currencyByCode->uah) {

					$coefficient = round($currency->uah / $currencyByCode->uah, 4);

					$currency->update([
						strtolower($currencyByCode->code) => $coefficient
					]);
				}

			}

		}
    }
}
