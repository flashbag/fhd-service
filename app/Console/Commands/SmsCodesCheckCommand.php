<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\SmsCode;
use Illuminate\Console\Command;

class SmsCodesCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms-codes:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check unused sent sms codes for expiration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $smsCodes = SmsCode::isActive()->get();

        foreach($smsCodes as $smsCode) {

        	if (!$smsCode->isValid()) {
				$smsCode->update([ 'is_active' => 0]);
			}

		}
    }
}
