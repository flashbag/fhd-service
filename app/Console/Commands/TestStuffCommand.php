<?php

namespace App\Console\Commands;

use App\User;
use App\Trackers;
use App\Models\HistoryStatuses;
use App\Mail\ReVerifyEmail;
use App\Mail\Trackers\TrackerCreatedSender;
use App\Mail\Trackers\TrackerCreatedRecipient;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use HeadlessChromium\BrowserFactory;

class TestStuffCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:stuff';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		echo "TEST STUFF COMMAND!" . PHP_EOL;

//    	$this->testMailTracker();
//		$this->testMailReVerify();
//		$this->testNexmo();
//		$this->testHistoryStatus();
		$this->testPdfGeneration();
    }

    public function testPdfGeneration()
	{
		$browserFactory = new BrowserFactory('google-chrome --aggressive-cache-discard --disable-notifications --disable-remote-fonts --disable-reading-from-canvas --disable-remote-playback-api --disable-shared-workers --disable-voice-input --enable-aggressive-domstorage-flushing');

		// starts headless chrome
		$browser = $browserFactory->createBrowser();

		// creates a new page and navigate to an url
		$page = $browser->createPage();

		$printUrl = url('/print/tracker/10041942580');

		$page->navigate($printUrl)->waitForNavigation();

//		// get page title
//		$pageTitle = $page->evaluate('document.title')->getReturnValue();

//		// screenshot - Say "Cheese"! 😄
//		$page->screenshot()->saveToFile('/foo/bar.png');

		// pdf
		$page->pdf(['printBackground'=>true])->saveToFile('tracker.pdf');

		// bye
		$browser->close();

	}

    public function testHistoryStatus()
	{
		HistoryStatuses::create([
			'status_id' => 1,
			'id_tracker' => 80,
			'id_user' => 1,
			'notes' => 'notes'
		]);
	}

	public function testMailReVerify()
	{
		$email = 'oleggg.normal@gmail.com';

		$user = User::where('email', $email)->first();

		if (!$user) {
			return false;
		}

		$url = url('/verification', $user->email_verify_hash);

		dd($url);

		Mail::to($email)->queue(new ReVerifyEmail($user));
	}

    public function testMailTracker()
	{
		echo "TEST STUFF COMMAND!" . PHP_EOL;

		$emailTo = 'igorshinal@gmail.com';

		$tracker = Trackers::find(88);

//		Mail::to($emailTo)->send(new TrackerCreatedSender($tracker));
//		Mail::to($emailTo)->queue(new TrackerCreatedSender($tracker));
		Mail::to($emailTo)->queue(new TrackerCreatedRecipient($tracker));
	}

	/**
	 * @throws \Nexmo\Client\Exception\Exception
	 * @throws \Nexmo\Client\Exception\Request
	 * @throws \Nexmo\Client\Exception\Server
	 */
	public function testNexmo()
	{
		$key = env('NEXMO_KEY');
		$secret = env('NEXMO_SECRET');

		$credentials = new \Nexmo\Client\Credentials\Basic($key, $secret);
		$client = new \Nexmo\Client($credentials);

		$message = $client->message()->send([
			'to' => '380681402868',
			'from' => "FHD",
			'text' => 'Test message from the Nexmo PHP Client'
		]);

		dd($message);
	}
}
