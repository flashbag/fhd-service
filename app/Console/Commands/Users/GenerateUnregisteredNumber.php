<?php

namespace App\Console\Commands\Users;

use App\Rules\PhoneNumberRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Console\Command;

class GenerateUnregisteredNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:generate-number';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate correct phone number which is not registered';

    public $countryCode = '+380';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

		do {
			$number = $this->compileNumber();

			echo  $number . PHP_EOL;

			$foundNewNumber =  $this->testNumberByRule($number);

			if ($foundNewNumber) {
				echo "new unregistered number: " . $number . PHP_EOL;
				break;
			}

		} while (!$foundNewNumber);

    }

    private function compileNumber()
	{
		$number = '';

		for ($a = 0; $a < 10; $a++) {
			$digit = rand(0,9);
			$number = $number . $digit;

		}

		return $this->countryCode . $number;
	}

	private function testNumberByRule($number)
	{
		$data = [
			'number' => $number
		];

		$rules = [
			'number' => [
				'required',
				'unique:users',
				new PhoneNumberRule()
			],
		];

		$validator = Validator::make($data, $rules);

		return !$validator->fails();
	}
}
