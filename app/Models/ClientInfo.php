<?php

namespace App\Models;

use App\Trackers;
use Illuminate\Database\Eloquent\Model;

class ClientInfo extends Model
{
	const TYPE_USER = -1;
	const TYPE_SENDER = 1;
	const TYPE_RECIPIENT = 2;

	const PERSON_TYPE_INDIVIDUAL = 1;
	const PERSON_TYPE_LEGAL_ENTITY = 2;

    protected $fillable = [
    	'user_id',
		'client_id',
		'type',
		'person_type',

		'tracker_id',
		'number',
		'full_name',
		'company_name',
		'email',
		'inn',
		'passport_data',
		'country',
		'region',
		'city',
		'address',
		'index',
		'vat',
		'number_account',
		'number_service',
		'custom_identify',
		'service_delivery_id',
		'service_delivery_other',
		'representative_recipient',
		'created_at',
		'updated_at',
	];

    public static function getPersonTypes()
	{
		return [ self::PERSON_TYPE_INDIVIDUAL, self::PERSON_TYPE_LEGAL_ENTITY ];
	}

    public static function createClientInformation($client_id, $user_id = false)
    {
        $clientInfo = null;
        if ((!is_null($client_id) && is_int($client_id)))
        {
        	$data = [
				'client_id' => $client_id
			];

        	if ($user_id) {
        		$data['user_id'] = $user_id;
			}

            $clientInfo = self::create($data);
        }
        return $clientInfo;
    }

    public function trackers()
	{
		return $this->belongsTo(Trackers::class, 'client_id', 'tracker_id');

//		return $this->hasMany(Trackers::class);
//		return $this->hasMany(Trackers::class, '');
	}

    public static function getClientInfoAsUser($client_id)
	{
		return self::where([
			'client_id' => $client_id,
			'type' => self::TYPE_USER,
		])->first();
	}

	public static function getClientInfoAsSender($client_id, $tracker_id)
	{
		// these three fields has unique index
		return self::where([
			'client_id' => $client_id,
			'type' => self::TYPE_SENDER,
			'tracker_id' => $tracker_id
		])->first();
	}

	public static function getClientInfoAsRecipient($client_id, $tracker_id)
	{
		// these three fields has unique index
		return self::where([
			'client_id' => $client_id,
			'type' => self::TYPE_RECIPIENT,
			'tracker_id' => $tracker_id
		])->first();
	}

	public static function getSenderDataFromRequest($request)
	{
		$data = [
			'person_type' => $request->get('sender_person_type'),
			'email' => $request->get('email_sender'),
			'number' => str_replace(' ', '', $request->get('number_sender')),
			'full_name' => $request->get('full_name_sender'),
			'company_name' => $request->get('sender_company_name'),
			'number_account' => $request->get('sender_number_account'),
			'number_service' => $request->get('sender_number_service'),
			'service_delivery_other' => $request->get('sender_service_delivery_other'),
			'inn' => $request->get('inn_sender'),
			'passport_data' => $request->get('passport_data_sender'),
			'country' => $request->get('country_id_sender'),
			'region' => $request->get('region_sender'),
			'city' => $request->get('city_id_sender'),
			'address' => $request->get('address_sender'),
			'index' => $request->get('index_sender'),
			'vat' => $request->get('number_vat'),
		];

		$serviceDeliveryId = $request->get('sender_service_delivery_id');

		if ($serviceDeliveryId != '--' && $serviceDeliveryId != 'null') {
			$data['service_delivery_id'] = $serviceDeliveryId;
		}

		return $data;
	}

	public static function getRecipientDataFromRequest($request)
	{
		$data = [
			'person_type' => $request->get('recipient_person_type'),
			'email' => $request->get('email_recipient'),
			'number' => str_replace(' ', '', $request->get('number_recipient')),
			'full_name' => $request->get('full_name_recipient'),
			'company_name' => $request->get('recipient_company_name'),
			'number_account' => $request->get('recipient_number_account'),
			'number_service' => $request->get('recipient_number_service'),
			'service_delivery_other' => $request->get('recipient_service_delivery_other'),
			'country' => $request->get('country_id_recipient'),
			'region' => $request->get('region_recipient'),
			'city' => $request->get('city_id_recipient'),
			'address' => $request->get('address_recipient'),
			'index' => $request->get('index_recipient'),
			'custom_identify' => $request->get('custom_id'),
			'representative_recipient' => $request->get('representative_recipient'),
		];

		$serviceDeliveryId = $request->get('recipient_service_delivery_id');

		if ($serviceDeliveryId != '--' && $serviceDeliveryId != 'null') {
			$data['service_delivery_id'] = $serviceDeliveryId;
		}

		return $data;
	}

	public function scopeOnlySenders($query)
	{
		$query->where('type', '=', self::TYPE_SENDER);

		return $query;
	}

	public function scopeWithTrackers($query)
	{
		$query->where('tracker_id', '!=', null);

		return $query;
	}
}
