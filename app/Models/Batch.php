<?php

namespace App\Models;

use App\Events\Batch\BatchCreatedEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batch extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'name',
		'number',
		'warehouse_id',

		'created_at',
		'updated_at',
	];

	protected $dispatchesEvents = [
		'created' => BatchCreatedEvent::class,
	];

	public function trackers()
	{
		return $this->belongsToMany(\App\Trackers::class, 'batches_trackers', 'batch_id', 'tracker_id');
	}

	public function warehouse()
	{
		return $this->belongsTo(Warehouse::class);
	}
}
