<?php

namespace App\Models;

use App\Trackers;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	const SENDER = 1;
	const RECIPIENT = 2;
	const RECEPTACLE = 3;

	const STORAGE_DIRECTORY = 'public/documents';

	public static $typeNames = [
		self::SENDER => 'sender',
		self::RECIPIENT => 'recipient',
		self::RECEPTACLE => 'receptacle'
	];

    protected $fillable = [

    	'tracker_id',
		'receptacle_id',

    	'type',
		'index',

		'filename'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tracker()
	{
		return $this->belongsTo(Trackers::class);
	}

	public function getDocumentUrl()
	{
		return '/storage/documents/' . $this->filename;
	}

	/**
	 * @param Trackers $tracker
	 * @param $file
	 * @param int $type
	 *
	 * @throws \Exception
	 */
	public static function processTrackerFile(Trackers $tracker, $file, $type)
	{

		if (isset($file['id'])) {

			// find with tracker id and type to avoid removing another tracker documents!
			$document = self::where([
				'id' => $file['id'],
				'type' => $type,
				'tracker_id' => $tracker->id,
			])->first();


			// only delete
			if ($document && isset($file['delete'])) {
				self::deleteDocument($document);
			}

			// replace existing document with newly uploaded
			if (isset($file['file']) && $file['file'] instanceof UploadedFile) {
				if ($document) {
					self::deleteDocument($document);
				}
				self::createNewTrackerDocument($tracker, $file['file'], $type);
			}

		} else {

			// create new document
			if (isset($file['file']) && $file['file'] instanceof UploadedFile) {
				self::createNewTrackerDocument($tracker, $file['file'], $type);
			}

		}

	}

	/**
	 * @param Trackers $tracker
	 * @param UploadedFile $uploadedFile
	 * @param $type
	 * @throws \Exception
	 */
	private static function createNewTrackerDocument(Trackers $tracker, UploadedFile $uploadedFile, $type)
	{
		$filename = self::$typeNames[$type];
		$filename .= '_' . $tracker->id_tracker;
		$filename .= '_' . uniqIdReal(20);
		$filename .= '.' . $uploadedFile->getClientOriginalExtension();

		self::create([
			'tracker_id' => $tracker->id,
			'type' => $type,
			'filename' => $filename
		]);

		$uploadedFile->storeAs(self::STORAGE_DIRECTORY, $filename);
	}

	/**
	 * @param Receptacle $receptacle
	 * @param $file
	 * @param int $type
	 *
	 * @throws \Exception
	 */
	public static function processReceptacleFile(Receptacle $receptacle, $file, $type = self::RECEPTACLE)
	{

		if (isset($file['id'])) {

			// find with tracker id and type to avoid removing another tracker documents!
			$document = self::where([
				'id' => $file['id'],
				'type' => $type,
				'tracker_id' => $receptacle->tracker->id,
				'receptacle_id' => $receptacle->id,
			])->first();


			// only delete
			if ($document && isset($file['delete'])) {
				self::deleteDocument($document);
			}

			// replace existing document with newly uploaded
			if (isset($file['file']) && $file['file'] instanceof UploadedFile) {
				if ($document) {
					self::deleteDocument($document);
				}
				self::createNewReceptacleDocument($receptacle, $file['file'], $type);
			}

		} else {

			// create new document
			if (isset($file['file']) && $file['file'] instanceof UploadedFile) {
				self::createNewReceptacleDocument($receptacle, $file['file'], $type);
			}

		}

	}

	/**
	 * @param Receptacle $receptacle
	 * @param UploadedFile $uploadedFile
	 * @param $type
	 * @throws \Exception
	 */
	private static function createNewReceptacleDocument(Receptacle $receptacle, UploadedFile $uploadedFile, $type)
	{
		$tracker = $receptacle->tracker;

		$filename = self::$typeNames[$type];
		$filename .= '_' . $tracker->id_tracker;
		$filename .= '_' . uniqIdReal(20);
		$filename .= '.' . $uploadedFile->getClientOriginalExtension();

		self::create([
			'tracker_id' => $tracker->id,
			'receptacle_id' => $receptacle->id,
			'type' => $type,
			'filename' => $filename
		]);

		$uploadedFile->storeAs(self::STORAGE_DIRECTORY, $filename);
	}

	/**
	 * @param Document $document
	 * @throws \Exception
	 */
	private static function deleteDocument(Document $document)
	{
		Storage::delete(self::STORAGE_DIRECTORY . '/' . $document->filename);
		$document->delete();
	}

}
