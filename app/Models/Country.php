<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
		'iso_3166_2',
		'iso_3166_3',

		'country_code',

		'name',
		'full_name',

		'phone_code',
		'eea',

		'capital',
		'citizenship',

		'region_code',
		'sub_region_code',

		'currency',
		'currency_code',
		'currency_symbol',
		'currency_sub_unit',
		'currency_decimals',

		'flag'
	];

	public function scopeWithWarehouses($query)
	{
		return $query->has('warehouses');
	}

	public function warehouses()
	{
		return $this->hasMany(Warehouse::class);
	}

}
