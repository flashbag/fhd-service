<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
	const EXPIRATION_TIME_IN_MINUTES = 3;

    protected $fillable = [
		'user_id',

		'is_active',
		'is_sent',
		'is_used',

		'code',
		'phone'
	];

    public function user()
	{
		return $this->belongsTo(User::class);
	}

	public static function getLatestActiveCode($phone)
	{
		return SmsCode::where('phone', $phone)->isActive()->isSent()->notUsed()->orderBy('id','desc')->first();
	}

	public static function createNewSmsCode($phone, $user = null)
	{
		$smsCode = new SmsCode();
		$smsCode->generateCode();
		$smsCode->phone = $phone;
		if ($user) {
			$smsCode->user_id = $user->id;
		}
		$smsCode->save();

		return $smsCode;
	}

	public function generateCode()
	{
		$code = '';

		for($i=0; $i<=4; $i++) {
			$code .= mt_rand(0, 9);
		}

		$this->code = $code;
	}

	/**
	 * Check if code is valid
	 *
	 * @return bool
	 */
	public function isValid()
	{
		$now = Carbon::now();
		$date = Carbon::parse($this->created_at);

		$diff = $date->diffInMinutes($now);

		return $diff < SmsCode::EXPIRATION_TIME_IN_MINUTES;
	}

	/**
	 * Scope a query to only include active codes.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeIsActive($query)
	{
		return $query->where('is_active', '=', 1);
	}

	/**
	 * Scope a query to only include not sent codes.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeNotSent($query)
	{
		return $query->where('is_sent', '=', 0);
	}

	/**
	 * Scope a query to only include not sent codes.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeIsSent($query)
	{
		return $query->where('is_sent', '=', 1);
	}

	/**
	 * Scope a query to only include not used codes.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeNotUsed($query)
	{
		return $query->where('is_used', 0);
	}


}
