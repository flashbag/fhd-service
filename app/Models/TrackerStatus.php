<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackerStatus extends Model
{
	protected $fillable = [
		'number',
		'title',

		'created_at',
		'updated_at'
	];

    public function getTitleAttribute()
    {
        return trans('trackers.parts.status.'. $this->number);
    }
}
