<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
	const ALL_CODES = [
		'USD', 'EUR', 'RUB', 'TRY', 'CAD', 'AED',
		'RON', 'ILS', 'SGD', 'KRW', 'JPY', 'IDR', 'CHF',
		'AUD', 'BRL', 'ARS', 'MDL', 'MXN', 'JOD', 'BGN',
		'BYN', 'INR', 'AZN', 'GEL', 'TJS', 'UZS', 'KZT',
		'EGP', 'LYD', 'TND', 'GBP', 'PLN', 'CNY'
	];

    protected $fillable = [

    	'id', 'name', 'code',

		'uah', 'usd', 'eur', 'rub', 'try', 'cad', 'aed',
		'ron', 'ils', 'sgd', 'krw', 'jpy', 'idr', 'chf',
		'aud', 'brl', 'ars', 'mdl', 'mxn', 'jod', 'bgn',
		'byn', 'inr', 'azn', 'gel', 'tjs', 'uzs', 'kzt',
		'egp', 'lyd', 'tnd', 'gbp', 'pln', 'cny',

		'created_at', 'updated_at'

	];

    public function scopeOnlyActive($query)
	{
		return $query->where(function($q){
			return $q->where('code', 'UAH')
				->orWhere('code', 'USD')
				->orWhere('code', 'EUR')
				->orWhere('code', 'CNY');
		});
	}

    public function clearCoefficients()
	{
		$updateData = [];
		foreach (self::ALL_CODES as $code) {
			if ($code != $this->code) {
				$updateData[strtolower($code)] = 0;
			}
		}
		$this->update($updateData);
	}
}
