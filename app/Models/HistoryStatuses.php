<?php

namespace App\Models;

use App\Models\TrackerStatus;
use Illuminate\Database\Eloquent\Model;

class HistoryStatuses extends Model
{
    protected $fillable = [
    	'status_id',
		'id_tracker',
		'id_user',
		'notes'
	];

	public function status()
	{
		return $this->belongsTo(TrackerStatus::class, 'status_id', 'id');
	}
}
