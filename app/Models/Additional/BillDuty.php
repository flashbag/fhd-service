<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class BillDuty extends Model
{
	protected $fillable = [
		'type',

		'created_at',
		'updated_at'
	];

}
