<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class ServiceDelivery extends Model
{
    protected $fillable = [
    	'name',
    	'type',

		'created_at',
		'updated_at'
	];

    protected static function getAllIds()
	{
		return array_pluck(self::all(), 'id');
	}

    public function getNameAttribute()
    {
        return trans('trackers.parts.service_delivery.'. $this->type);
    }
}
