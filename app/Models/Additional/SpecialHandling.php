<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class SpecialHandling extends Model
{
   	protected $fillable = [
		'type',

		'created_at',
		'updated_at'
	];
}
