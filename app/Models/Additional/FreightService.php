<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class FreightService extends Model
{
	protected $fillable = [
		'type',

		'created_at',
		'updated_at'
	];

	public function scopeOrderedByNumber($query)
	{
		return $query->orderBy('order_number','asc');
	}
}
