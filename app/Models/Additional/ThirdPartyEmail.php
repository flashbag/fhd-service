<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class ThirdPartyEmail extends Model
{
	protected $fillable = [
		'email',

		'created_at',
		'updated_at',

		// FK
		'type_tax_id',
		'tracker_id'
	];
}
