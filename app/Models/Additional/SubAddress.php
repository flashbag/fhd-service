<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class SubAddress extends Model
{
    protected $fillable = [
    	'tracker_id',
    	'city',
		'address',

		'created_at',
		'updated_at'
	];
}
