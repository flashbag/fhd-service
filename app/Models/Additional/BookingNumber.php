<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class BookingNumber extends Model
{
	protected $fillable = [
		'tracker_id',
		'booking_number',

		'created_at',
		'updated_at'
	];
}
