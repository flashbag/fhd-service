<?php

namespace App\Models\Additional;

use Illuminate\Database\Eloquent\Model;

class FHDAccount extends Model
{
	protected $table = 'fhd_accounts';

	protected $fillable = [
		'fhd_account',
		

		'created_at',
		'updated_at',


		// FK
		'type_tax_id',
		'tracker_id'
	];
}
