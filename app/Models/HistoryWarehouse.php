<?php

namespace App\Models;

use App\User;
use App\Trackers;
use Illuminate\Database\Eloquent\Model;

class HistoryWarehouse extends Model
{
	protected $fillable = [
		'user_id',
		'tracker_id',
		'warehouse_id',
		'notes'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function tracker()
	{
		return $this->belongsTo(Trackers::class);
	}

	public function warehouse()
	{
		return $this->belongsTo(Warehouse::class);
	}

}
