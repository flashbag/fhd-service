<?php

namespace App\Models;

use App\User;
use App\Trackers;
use App\Pivots\WarehouseUser;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = [
    	'name',
		'city',
		'region',
		'address',
		'short_country',


		// FK
		'country_id'
	];

    public function getNameAttribute()
	{
		return trans('trackers.parts.warehouses.'. $this->unique_id);
	}

    public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function admins()
	{
		return $this->warehouseUsersRelation()->whereHas('roles', function ($query) {
			$query->where('name', '=', 'Warehouse Admin');
		});
	}

    public function operators()
	{
		return $this->warehouseUsersRelation()->whereHas('roles', function ($query) {
			$query->where('name', '=', 'Warehouse Operator');
		});
	}

	public function couriers()
	{
		return $this->warehouseUsersRelation()->whereHas('roles', function ($query) {
			$query->where('name', '=', 'Warehouse Courier');
		});
	}

	public function users()
	{
		return $this->warehouseUsersRelation();
	}

	private function warehouseUsersRelation()
	{
		return $this->belongsToMany(
			User::class,
			'warehouses_users',
			'warehouse_id',
			'user_id'
		)->using(WarehouseUser::class);
	}

	public function batches()
	{
		return $this->hasMany(Batch::class);
	}

	public function getTrackersAttribute($value)
	{
		// There two calls return collections
		// as defined in relations.
		$trackersFrom = $this->trackersFrom;
		$trackersTo = $this->trackersTo;

		// Merge collections and return single collection.
		return $trackersFrom->merge($trackersTo);
	}

	public function trackersFrom()
	{
		return $this->hasMany(Trackers::class, 'from_warehouse_id');
	}

	public function trackersTo()
	{
		return $this->hasMany(Trackers::class, 'from_warehouse_id');
	}
}
