<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class BatchesTrackers extends Pivot
{
	protected $fillable = [
		'user_id',
		'batch_id',
		'tracker_id',
		'warehouse_id',

		'created_at',
		'updated_at',
	];

	public function batches()
	{
		return $this->belongsToMany(Batch::class, 'batches_trackers', 'batch_id', 'tracker_id');
	}

	public function trackers()
	{
		return $this->belongsToMany(\App\Trackers::class, 'batches_trackers', 'tracker_id', 'batch_id');
	}
}
