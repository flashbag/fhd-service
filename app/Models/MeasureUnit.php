<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeasureUnit extends Model
{
    protected $fillable = [
		'type',
		'text',
		'multiplier_to_base',

		'created_at',
		'updated_at'
	];

    public function scopeOnlyLength($query)
	{
		return $query->where('type','length');
	}

	public function scopeOnlyWeight($query)
	{
		return $query->where('type','weight');
	}
}
