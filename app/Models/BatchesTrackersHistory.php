<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BatchesTrackersHistory extends Model
{
	protected $fillable = [
		'user_id',
		'batch_id',
		'tracker_id',
		'warehouse_id',

		'created_at',
		'updated_at',
	];
}
