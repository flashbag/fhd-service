<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calculator extends Model
{
    protected $fillable = [
		'country_from',
		'country_to',
		'is_imperial',
		'with_dimensions',
		'with_custom_duty',
		'actual_weight',
		'width',
		'length',
		'height',
		'assessed_price',
		'currency',
		'results',

		'created_at',
		'updated_at'
	];
}
