<?php

namespace App\Models;

use App\Trackers;
use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
	const CARGO_NAME_LIMIT = 40;

	protected $fillable = [
        //'is_used',
		'tracker_id',
		'receptacle_id',

		'quantity',
		'description'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tracker()
	{
		return $this->belongsTo(Trackers::class);
	}

	public function receptacle()
	{
		return $this->belongsTo(Receptacle::class);
	}

	public static function processSingleCargo(Trackers $tracker, Receptacle $receptacle, $cargo)
	{
		if (isset($cargo['id'])) {

			// find with tracker id and receptacle id to avoid removing another tracker cargos!
			$cargoModel = self::where([
				'id' => $cargo['id'],
				'tracker_id' => $tracker->id,
				'receptacle_id' => $receptacle->id
			])->first();


			if ($cargoModel) {

				if (isset($cargo['delete'])) {
					$cargoModel->delete();
				} else {
					$cargoModel->update($cargo);
				}
			}


		} else {

			self::create([
				//'is_used' => $cargo['is_used'],
				'tracker_id' => $tracker->id,
				'receptacle_id' => $receptacle->id,
				'quantity' => $cargo['quantity'],
				'description' => $cargo['description'],
			]);

		}
	}
}
