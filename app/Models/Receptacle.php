<?php

namespace App\Models;

use App\Trackers;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Receptacle extends Model
{
    protected $fillable = [

    	'tracker_id',

		'type_inventory_id',

		'width',
		'length',
		'height',

		'actual_weight',
		'volume_weight',
		'volume_weight_ft',

		'unit_of_length_id',
		'unit_of_weight_id',
		'assessed_price',
		'currency_type'

	];

    public function tracker()
	{
		return $this->belongsTo(Trackers::class);
	}

    public function cargos()
	{
		return $this->hasMany(Cargo::class);
	}

	public function lengthUnit()
	{
		return $this->hasOne(MeasureUnit::class, 'id','unit_of_length_id');
	}

	public function weightUnit()
	{
		return $this->hasOne(MeasureUnit::class, 'id', 'unit_of_weight_id');
	}

	public function currency()
	{
		return $this->hasOne(Currency::class, 'id', 'currency_type');
	}

	public function documents()
	{
		return $this->hasMany(Document::class, 'receptacle_id');
	}

	public static function processSingleReceptacle(Trackers $tracker, $receptacleData)
	{
		if (isset($receptacleData['id'])) {

			// find with tracker id to avoid removing another tracker cargos!
			$receptacleModel = self::where([
				'id' => $receptacleData['id'],
				'tracker_id' => $tracker->id,
			])->first();


			if (!$receptacleModel) {
				return false;
			}

			if (isset($receptacleData['delete'])) {
				$receptacleModel->delete();
				return false;
			}

			$receptacleModel->update($receptacleData);

			return $receptacleModel;

		} else {

			$receptacleData['tracker_id'] = $tracker->id;
			$receptacleModel = self::create($receptacleData);

			return $receptacleModel;

		}
	}

	public function processReceptacleDocuments(Request $request, $receptacleIndex)
	{
		// different methods to get data and files
		$documentsFiles = $request->file('receptacles.' . $receptacleIndex . '.documents');
		$documentsFields = $request->input('receptacles.' . $receptacleIndex . '.documents');

		// looping fields because it is length will be always equal file inputs
		// event no file selected in any file input, field input will be just beacon
		if (!empty($documentsFields)) {

			foreach ($documentsFields as $index => $fields) {
				$documents[$index] = $fields;

				if (isset($documentsFiles[$index])) {
					$documents[$index] = array_merge($fields, $documentsFiles[$index]);
				}
			}

			if (!empty($documents)) {
				foreach ($documents as $file) {
					Document::processReceptacleFile($this, $file, Document::RECEPTACLE);
				}
			}
		}

	}
}
