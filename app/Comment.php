<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'ticket_id',
        'user_id',
        'date_read',
        'text',
        'link',
        'status',
    ];

    public function tickets()
    {
        return $this->BelongsTo(Ticket::class, 'ticket_id');
    }

    public function user()
    {
        return $this->BelongsTo(User::class, 'user_id');
    }

    public function file()
    {
        return $this->hasMany(FileComment::class, 'comment_id');
    }
}
