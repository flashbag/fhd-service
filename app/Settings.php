<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'title_site',
        'short_description',
        'email',
        'count_news_admin',
        'count_news_blog',
        'count_trackers_admin',
        'count_result_trackers_admin',
        'is_offline',
        'desc_offline_site'
    ];
}
