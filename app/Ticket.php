<?php

namespace App;

use App\Models\Cargo;
use App\Models\Receptacle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Ticket extends Model
{
    protected $fillable = [
        'tracker_id',
        'status',
    ];

    public function trackers()
    {
        return $this->BelongsTo(Trackers::class, 'tracker_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'ticket_id', 'id');
    }

    public function cargos()
    {
        return $this->hasManyThrough(Cargo::class, Trackers::class, 'id', 'tracker_id', 'tracker_id');
    }

    public function scopeApplyFilters($query, Request $request)
    {
        $query->select(['tickets.*']);

        $query->leftJoin('comments', 'comments.ticket_id', '=', 'tickets.id')
            ->leftJoin('trackers', 'trackers.id', '=', 'tickets.tracker_id')
            ->leftJoin('users', 'users.id', '=', 'comments.user_id');

        $date = $request->get('search_date');

        if (!empty($date)) {
            $query->where('created_at', '=', $date );
        }

        $fieldByLikeMappings = [
            'search_id' => 'trackers.id',
            'search_name' => 'users.name',
        ];

        foreach ($fieldByLikeMappings as $fieldInRequest => $fieldInDb) {
            $fieldInRequesValue = $request->get($fieldInRequest);

            if (!empty($fieldInRequesValue)) {
                $query->where($fieldInDb,  'like', '%'.$fieldInRequesValue. '%');
            }
        }
        $query->groupBy('tickets.id');

        return $query;
    }
}
