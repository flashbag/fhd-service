<?php
/**
 * Created by PhpStorm.
 * User: flashbag
 * Date: 31.01.19
 * Time: 10:19
 */

namespace App\Pivots;

use App\User;
use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Relations\Pivot;

class WarehouseUser extends Pivot {

	protected $table = 'warehouses_users';

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function warehouse()
	{
		return $this->belongsTo(Warehouse::class);
	}

}
