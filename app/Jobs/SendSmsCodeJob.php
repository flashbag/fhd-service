<?php

namespace App\Jobs;

use App\Models\SmsCode;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsCodeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 5;

	public $smsCode;

	/**
	 * Determine the time at which the job should timeout.
	 *
	 * @return \DateTime
	 */
	public function retryUntil()
	{
		return now()->addSeconds(5);
	}

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SmsCode $smsCode)
    {
        $this->smsCode = $smsCode;
    }

	/**
	 * Execute the job.
	 *
	 * @return void
	 * @throws \Exception
	 */
    public function handle()
    {
    	try {

//			$this->realSmsSend();
			$this->smsCode->update(['is_sent' => 1]);

		} catch (\Exception $e) {
    		throw $e;
		}

    }

	/**
	 * @throws \Nexmo\Client\Exception\Exception
	 * @throws \Nexmo\Client\Exception\Request
	 * @throws \Nexmo\Client\Exception\Server
	 */
	private function realSmsSend()
	{
		$key = env('NEXMO_KEY');
		$secret = env('NEXMO_SECRET');

		$credentials = new \Nexmo\Client\Credentials\Basic($key, $secret);
		$client = new \Nexmo\Client($credentials);

		$client->message()->send([
			'to' => '380681402868',
			'from' => "FHD",
			'text' => 'Confirmation code:' . $this->smsCode->code
		]);
	}
}
