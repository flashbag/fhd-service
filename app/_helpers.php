<?php
/**
 * Created by PhpStorm.
 * User: flashbag
 * Date: 25.11.18
 * Time: 13:49
 */


if (! function_exists('uniqIdReal')) {

	/**
	 * Real uniqid generation
	 *
	 * http://php.net/manual/en/function.uniqid.php#120123
	 *
	 * @param int $lenght
	 * @return bool|string
	 * @throws Exception
	 */
	function uniqIdReal($lenght = 10) {
		// uniqid gives 13 chars, but you could adjust it to your needs.
		if (function_exists("random_bytes")) {
			$bytes = random_bytes(ceil($lenght / 2));
		} elseif (function_exists("openssl_random_pseudo_bytes")) {
			$bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
		} else {
			throw new Exception("no cryptographically secure random function available");
		}
		return substr(bin2hex($bytes), 0, $lenght);
	}
}


if (! function_exists('filterNumbers')) {
	function filterNumbers($str) {
		return (int) filter_var($str, FILTER_SANITIZE_NUMBER_INT);
	}
}
