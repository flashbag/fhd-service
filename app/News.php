<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'id_category',
        'image',
        'short_description',
        'description',
        'is_active'
    ];

    public static function getLastThreeNews()
    {
        return News::latest()->limit(3)->get();
    }
}
