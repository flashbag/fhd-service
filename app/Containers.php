<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Containers extends Model
{
    protected $fillable = ['number_container', 'id_type_container'];
}
