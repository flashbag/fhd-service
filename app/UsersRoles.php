<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRoles extends Model
{
    protected $fillable = [
        'user_id',
        'role_id'
    ];

    public static function createUserRole($user_id, $role_id)
    {
        $userRole = null;
        if ((!is_null($user_id) && is_int($user_id)) && (!is_null($role_id) && is_int($role_id)))
        {
            $userRole = new self();
            $userRole->create([
                'user_id' => $user_id,
                'role_id' => $role_id
            ]);
        }

        return $userRole;
    }
}
