<?php

use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

$whitelist = [
	'127.0.0.1', '::1'
];

// make test route available only for localhost
if(php_sapi_name() != 'cli' && in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
	Route::get('/test/stuff', 'TestController@testStuff');
	Route::get('/test/email-view', 'TestController@testEmailView');
}

//Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
//    return $captcha->src($config);
//});

Route::get('/', 'PagesController@index')->name('index');

Route::get('setlocale/{locale}', function ($locale) {

	if (in_array($locale, \Config::get('app.locales'))) {
		\Session::put('locale', $locale);
	}

	return redirect()->back();

})->name('set-locale');

Route::post('/register/simple', 'Auth\RegisterAjaxController@registerSimple')->name('register-simple');
Route::post('/register/initialize', 'Auth\RegisterAjaxController@registerInitialize')->name('register-initialize');
Route::post('/register/verify', 'Auth\RegisterAjaxController@registerVerify')->name('register-verify');
Route::get('/verification/{emailVerifyHash}', 'Auth\RegisterAjaxController@verifiedEmail')->name('verified-email');
Route::get('/sendEmail', 'TrackerController@sendEmail')->name('send-email');
Route::post('/custom-email', 'Auth\PasswordResetCustomController@email')->name('custom-email');
Route::get('/passwords/{token}', 'Auth\PasswordResetCustomController@passwords')->name('passwords');
Route::post('/changePass/{token}', 'Auth\PasswordResetCustomController@verifiedPass')->name('changePassword');

Route::get('/template/receptacle/new', 'TemplateController@getNewReceptacleTemplate')->name('template-get-receptacle');
Route::get('/template/receptacle/cargo/new', 'TemplateController@getNewReceptacleCargoTemplate')->name('template-get-receptacle-cargo');
Route::get('/template/file/new', 'TemplateController@getNewFileTemplate')->name('template-get-file');

Route::get('/country/{id}/warehouses', 'CountryController@getCountryWarehouses')->name('get-country-warehouses');

Route::get('/agreement/legal', 'PagesController@agreement_legal')->name('agreement-legal');
Route::get('/agreement/individual', 'PagesController@agreement_individual')->name('agreement-individual');

Route::post('/feedback', 'PagesController@feedback')->name('feedback');
Route::get('/address-warehouse', 'PagesController@address')->name('address-warehouse');
//Route::group(['prefix' => 'print', 'middleware' => 'whitelist:local'], function(){
//	Route::get('/tracker/{id}', 'PrintController@printTracker');
//});

Route::get('/print/tracker/{id}', 'PrintController@printTracker');

Route::get('/download/tracker/{id_tracker}/pdf', 'DownloadController@downloadTrackerPdf')->name('download-tracker-pdf');
Route::get('/download/tracker/pdf/empty', 'DownloadController@downloadEmptyTrackerPdf')->name('download-tracker-empty-pdf');

Route::get('/custom', 'CustomController@index')->name('custom');
Route::post('/custom', 'CustomController@customFilter')->name('custom-filter');

Route::get('/google/place-details/{placeid}', 'GoogleController@getPlaceDetails')->name('google-getPlaceDetails');

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

// Public tracker routes
Route::get('/tracker/search', 'TrackerController@searchTracker')->name('search-tracker');
Route::get('/tracker/{id}', 'TrackerController@showTracker')->name('show-tracker');
Route::get('/tracker/full/{id}', 'TrackerController@showTrackerWithUser')->name('show-full-tracker');
Route::get('/trackers/create', 'TrackerController@createTrackers')->name('create-trackers');
Route::post('/trackers/store', 'TrackerController@store')->name('store-trackers');

// User tracker routes, need to check tracker affiliation to user!
Route::get('/trackers/history', 'TrackerController@history')->name('history-trackers');
Route::get('/trackers/edit/{id}', 'TrackerController@editTrackers')->name('edit-trackers');
Route::post('/trackers/update/{id}', 'TrackerController@updateTrackers')->name('update-trackers');
Route::get('/trackers/user/delete/{id}', 'TrackerController@deleteTrackers')->name('delete-trackers-user');
Route::any('/trackers/user/reset/{id}', 'TrackerController@resetTrackers')->name('reset-trackers-user');

//TODO move this two route to ProfileController
Route::get('/profile/edit', 'TrackerController@editProfile')->name('edit-profile');
Route::put('/profile/edit', 'TrackerController@updateProfile')->name('update-profile');

// Public pages
Route::get('/documents', 'PagesController@documents')->name('documents');
Route::get('/news', 'PagesController@news')->name('news');
Route::get('/faq', 'PagesController@faq')->name('faq');
Route::get('/about-us', 'PagesController@aboutUs')->name('about-us');
Route::get('/contacts', 'PagesController@contacts')->name('contacts');
Route::get('/rules', 'PagesController@rules')->name('rules');
Route::post('/calculator', 'PagesController@calculatorStatistic')->name('calculator');

//Route::get('/new_ticket', 'TicketsController@create')->name('new_ticket');
Route::any('/new_user_ticket/{tracker_id}', 'TicketsController@createTickets')->name('create-ticket');
Route::post('/new_ticket', 'TicketsController@store')->name('new_ticket-store');
Route::get('/my_tickets', 'TicketsController@userTickets')->name('my_tickets');
Route::get('/my_tickets_warehouse', 'TicketsController@warehouseTickets')->name('my_tickets_warehouse');
Route::get('/tickets/{id_tracker}', 'TicketsController@show')->name('tickets');
Route::get('/warehouse/tickets/{id_tracker}', 'TicketsController@warehouseShow')->name('tickets_warehouse');
Route::post('/comment', 'TicketsController@postComment')->name('comment');
Route::any('/download/file/{file}', 'TicketsController@download')->name('download');
Route::any('/send-ticket/{id_tracker}', 'TicketsController@sendMessage')->name('send-ticket');

Route::get('/tickets', 'TicketsController@warehouseIndex')->name('warehouse.tickets.index');
//Route::post('/warehouse/tickets', 'TicketsController@warehouseIndexFilter')->name('warehouse.tickets.index.filter');
Route::get('/tickets/single/{id_tracker}/show', 'TicketsController@warehouseTicketShow')->name('warehouse.tickets.show');

Route::group(['prefix' => 'admin', ['middleware' => 'checkDashboard']], function() {

    Route::get('/', 'AdminController@index')->name('dashboard-home');

    //AJAX
    Route::get('/get/status-trackers', 'AdminController@getStatusTrackers');

    Route::get('/get/currencies', 'AdminController@getCurrencies');

    Route::get('/get/additional/{id}', 'AdminController@getAdditional');


	Route::group(['prefix' => 'trackers'], function() {

		// Listing methods
		Route::get('/', 'AdminController@trackers')->name('trackers');
		Route::get('/unprocessed', 'AdminController@trackersUnprocessed')->name('trackers-unprocessed');
		Route::get('/processed', 'AdminController@trackersProcessed')->name('trackers-processed');
		Route::get('/ready', 'AdminController@trackersReady')->name('trackers-ready');
		Route::get('/trash', 'AdminController@trackersTrash')->name('trackers-trash');

		Route::get('/from-warehouse', 'AdminController@trackersFromWarehouse')->name('trackers-from-warehouse');
		Route::get('/to-warehouse', 'AdminController@trackersToWarehouse')->name('trackers-to-warehouse');


		// CRUD Methods
		Route::get('/create', 'AdminController@createTrackers')->name('admin-create-trackers');/*   view - admin/create-trackers   */
		Route::post('/store', 'AdminController@storeTrackers')->name('admin-store-trackers'); /*   view - admin/trackers  */
		Route::get('/edit/{id}', 'AdminController@editTrackers')->name('admin-edit-trackers'); /*   view - admin/edit-trackers  */
		Route::put('/update/{id}', 'AdminController@updateLevelTrackers')->name('update-lvl-trackers'); /*   view - admin/edit-trackers */
		Route::put('/edit/{id}', 'AdminController@updateTrackers')->name('admin-update-trackers'); /*   view - admin/trackers  */
		Route::delete('/force-delete/{id}', 'AdminController@forceDeleteTrackers')->name('force-delete-trackers');
		Route::any('/delete/{id}', 'AdminController@deleteTrackers')->name('delete-trackers');
        Route::put('/save/{id}/tracker', 'AdminController@saveTracker')->name('save-tracker');

		Route::post('/recovery/{id}', 'AdminController@recoveryTrackers')->name('recovery-trackers');
		Route::get('/search', 'AdminController@searchTrackers')->name('search-trackers');
		Route::get('/trash/search', 'AdminController@trashSearchTrackers')->name('trash-search-trackers');
		Route::put('/update/status/new', 'AdminController@updateStatusTrackers')->name('update-status-trackers');

	});

    Route::get('/containers/search/{id}', 'AdminController@searchContainers');
    Route::get('/containers/type/search/{id}', 'AdminController@getContainerType');
    Route::get('/containers/types', 'AdminController@getTypeContainers');

    Route::get('/history-clients', 'AdminController@historyClients')->name('history-clients');
    Route::get('/get/history/client/{id}', 'AdminController@getHistoryClient')->name('get-history-client');
    Route::post('/get/history/client/{id}', 'AdminController@appendAdminNote')->name('append-admin-note');



	Route::group(['namespace' => 'Admin', 'prefix' => 'warehouse'],function () {
		Route::get('/', 'WarehouseController@warehouse')->name('warehouse');
		Route::get('/edit/{id}', 'WarehouseController@editWarehouse')->name('edit-warehouse');
		Route::put('/edit/{id}', 'WarehouseController@updateWarehouse')->name('update-warehouse');
		Route::get('/tracker/{id}', 'WarehouseController@trackerWarehouse')->name('tracker-warehouse');

		Route::get('/admins/free', 'WarehouseController@getFreeWarehouseAdmins')->name('get-free-warehouse-admins');
		Route::get('/operators/free', 'WarehouseController@getFreeWarehouseOperators')->name('get-free-warehouse-operators');
		Route::get('/couriers/free', 'WarehouseController@getFreeWarehouseCouriers')->name('get-free-warehouse-couriers');
	});

	Route::group(['namespace' => 'Admin', 'prefix' => 'batch'],function () {
		Route::get('/', 'BatchController@index')->name('admin.batch.index');
		Route::get('/create', 'BatchController@create')->name('admin.batch.create');
		Route::post('/store', 'BatchController@store')->name('admin.batch.store');
		Route::put('/edit/{id}', 'BatchController@update')->name('admin.batch.update');
		Route::get('/edit/{id}', 'BatchController@edit')->name('admin.batch.edit');
		Route::delete('/delete/{id}', 'BatchController@destroySoft')->name('admin.batch.delete');

		Route::get('/show/{id}/trackers', 'BatchController@batchTrackers')->name('admin.batch.trackers');
	});

	Route::group(['namespace' => 'Admin', 'prefix' => 'news'],function () {
		Route::get('/', 'NewsController@news')->name('admin-news');
		Route::get('/create', 'NewsController@newsCreate')->name('create-news');
		Route::post('/store', 'NewsController@newsStore')->name('store-news');
		Route::get('/edit/{id}', 'NewsController@editNews')->name('edit-news');
		Route::put('/edit/{id}', 'NewsController@updateNews')->name('update-news');

		Route::get('/categories', 'NewsController@newsCategories')->name('news-categories');
		Route::get('/categories/create', 'NewsController@newsCategoriesCreate')->name('create-news-category');
		Route::post('/categories/store', 'NewsController@newsCategoriesStore')->name('store-news-category');
		Route::get('/categories/edit/{id}', 'NewsController@newsCategoriesEdit')->name('edit-news-category');
		Route::put('/categories/update/{id}', 'NewsController@newsCategoriesUpdate')->name('update-news-category');

		Route::get('/get/categories', 'NewsController@getNewsCategories');
	});

	Route::group(['namespace' => 'Admin', 'prefix' => 'users'],function () {
		Route::get('/', 'UsersController@users')->name('users');
		Route::get('/create', 'UsersController@createUsers')->name('create-users');
		Route::post('/store', 'UsersController@storeUsers')->name('store-users');
		Route::get('/edit/{id}', 'UsersController@editUsers')->name('edit-users');
		Route::put('/edit/{id}', 'UsersController@updateUser')->name('update-users');

		Route::get('/get/roles', 'UsersController@getRoles');

		Route::get('/search/{number}', 'UsersController@searchUserByNumber');
	});

    Route::get('/settings', 'AdminController@settings')->name('settings');
    Route::put('/settings/update', 'AdminController@updateSettings')->name('update-settings');
});



