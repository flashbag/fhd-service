let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.webpackConfig({});

// JavaScript

mix.js('resources/assets/js/vendor/jquery.js', 'public/js/vendor/jquery.js');
mix.js('resources/assets/js/vendor/bootstrap.js', 'public/js/vendor/bootstrap.js');

mix.babel([
	'resources/assets/js/vendor/bootstrap-select.min.js',
	'resources/assets/js/vendor/intlTelInput.min.js',
	'resources/assets/js/vendor/summernote.min.js',
	'resources/assets/js/vendor/summernote-ru-RU.js',
	'resources/assets/js/vendor/jquery.datepicker.min.js',
	'resources/assets/js/vendor/jquery.steps.js',
], 'public/js/vendor/other.js');


mix.babel([
	'resources/assets/js/swal.js',
	'resources/assets/js/general.js',
	'resources/assets/js/filters.js',
	'resources/assets/js/contacts.js',
	'resources/assets/js/calculator.js',
	'resources/assets/js/google-address.js',
	'resources/assets/js/typehead-general.js',
	'resources/assets/js/phone-intl-tel-inputs.js',
], 'public/js/bundle.js');


mix.babel(['resources/assets/js/tracker/*.js'], 'public/js/tracker.js');
mix.babel(['resources/assets/js/tracker/receptacles/*.js'], 'public/js/tracker-receptacles.js');

mix.js('resources/assets/js/print.js', 'public/js');

//frontend js
mix.babel(['resources/assets/js/frontend/ticket-single.js'], 'public/js/frontend/ticket-single.js');
mix.babel(['resources/assets/js/frontend/tickets.js'], 'public/js/frontend/tickets.js');

// Images
mix.copyDirectory('resources/img', 'public/img');
mix.copyDirectory('resources/fonts', 'public/fonts');
mix.copyDirectory('resources/docs', 'public/docs');

// SCSS
mix.sass('resources/assets/sass/print/a4.scss', 'public/css/print/a4.css');
mix.sass('resources/assets/sass/print/a5.scss', 'public/css/print/a5.css');
mix.sass('resources/assets/sass/print/print.scss', 'public/css/print/print.css');

mix.sass('resources/assets/sass/app.scss', 'public/css/app.css');

mix.sass('resources/assets/sass/vendor/bootstrap-select.scss', 'public/css/vendor/bootstrap-select.css');
mix.sass('resources/assets/sass/vendor/font-awesome.scss', 'public/css/vendor/font-awesome.css');
mix.sass('resources/assets/sass/vendor/intlTelInput.scss', 'public/css/vendor/intlTelInput.css');
mix.sass('resources/assets/sass/vendor/jquery-dataTables.scss', 'public/css/vendor/jquery-dataTables.css');
mix.sass('resources/assets/sass/vendor/jquery-ui.scss', 'public/css/vendor/jquery-ui.css');
mix.sass('resources/assets/sass/vendor/jquery-steps.scss', 'public/css/vendor/jquery-steps.css');
mix.sass('resources/assets/sass/vendor/summernote.scss', 'public/css/vendor/summernote.css');
mix.sass('resources/assets/sass/vendor/typehead.scss', 'public/css/vendor/typehead.css');


mix.styles([
	'public/css/vendor/bootstrap-select.css',
	'public/css/vendor/font-awesome.css',
	'public/css/vendor/intlTelInput.css',
	'public/css/vendor/jquery-ui.css',
	'public/css/vendor/jquery-dataTables.css',
	'public/css/vendor/jquery-steps.css',
	'public/css/vendor/summernote.css',
	'public/css/vendor/typehead.css'
], 'public/css/vendor.css');


// mix.sass('resources/assets/sass/*.scss', 'public/css/combined/style.css');
// mix.sass('resources/assets/sass/example/*.scss', 'public/css/combined/example.css');
// mix.combine(['resources/assets/sass/example/*.scss'], 'public/css/combined/example.css');
// mix.combine(['resources/assets/sass/*.scss'], 'public/css/combined/style.css');


mix.version();
