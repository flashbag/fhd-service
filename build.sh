#!/usr/bin/env bash

success_echo() {
   echo -e "\x1b[1;32m$MESSAGE\e[0m"
}

error_echo() {
   echo -e "\x1b[1;31m$MESSAGE\e[0m"
}

warning_echo() {
   echo -e "\x1b[0;33m$MESSAGE\e[0m"
}

read_dot_env() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

SELF_NAME=$(basename $0)
PHP=""
COMPOSER=""
NODE_PKG_MANAGER=""

# INIT FHD_ENV=APP_ENV IF `.env` FILE EXIST..
if [ -f .env ] && [ -h .env ]; then
   FHD_ENV=$(read_dot_env APP_ENV .env)
fi

# Detect build mode || print usage instruction
if [ $# -eq 0 ] && [ -z "$FHD_ENV" ] ; then
   MESSAGE="\nUsage: \n \
   ~$> $SELF_NAME <environment>\n \
   ~$> export FHD_ENV=<environment> \n \
to one of possible value: localhost|staging|prodaction"; warning_echo
   echo
   exit 0
else
   FHD_ENV="${1:-$FHD_ENV}"
fi

if [ -f .env ] && [ -h .env ]; then
   rm -f .env
fi

# Find & Link `.env` file...
if [ -f ".env.$FHD_ENV" ]; then
   ln -s .env.$FHD_ENV .env
else
   MESSAGE="\nError: environment file not found... expected: .env.$FHD_ENV"; error_echo
   exit 1;
fi


# TEST `yarn` || `npm` if exist...
if hash yarn 2>/dev/null; then
   NODE_PKG_MANAGER="yarn"
else
   MESSAGE="\n\nWARNING: not found 'yarn'...."; error_echo
   echo
   if hash npm 2>/dev/null; then
      NODE_PKG_MANAGER="npm"
   else
      MESSAGE="\n\nERROR: fatal not found 'npm'...."; error_echo
      echo
      exit 1
   fi
fi

# TEST `php` if exist...
if hash php 2>/dev/null; then
   PHP="php"
else
s  MESSAGE="\n\nERROR: fatal not found 'php'...."; error_echo
   echo
   exit 1
fi

# TEST `composer` if exist...
if hash composer 2>/dev/null; then
   COMPOSER="yarn"
else
s  MESSAGE="\n\nERROR: fatal not found 'composer'...."; error_echo
   echo
   exit 1
fi


MESSAGE="\n\nCOMPOSER: update"; success_echo
echo

$COMPOSER update 

MESSAGE="\n\nPHP: artisan... cache+config+view:clear..."; success_echo

php artisan cache:clear \
&& php artisan config:clear \
&& php artisan view:clear 


MESSAGE="\n\nYARN: install; run dev; run prod"; success_echo

$NODE_PKG_MANAGER install && $NODE_PKG_MANAGER run prod 

MESSAGE="\n\nCOMPOSER: dump-autoload"; success_echo

$COMPOSER dump-autoload \

MESSAGE="\n\nPHP: artisan migrate"; success_echo
php artisan migrate

MESSAGE="\n\nPHP: artisan db:seed"; success_echo
php artisan db:seed 

MESSAGE="\n\nCHMOD: ./storage/*"; success_echo
{
 chmod -R ug+rw ./storage &&
} || {
 MESSAGE="\n\nWARNING: failed CHMOD: ./storage/*"; warning_echo
 echo
}

MESSAGE="\n\nCHOWN: www-data: ./storage/*"; success_echo
{
 chown -R www-data ./storage
} || {
 MESSAGE="\n\nWARNING: failed CHOWN: ./storage/*"; warning_echo
 echo
}

MESSAGE="\n\n$SELF_NAME: all done..."; success_echo
