<?php
$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">

    <!-- Useful meta tags -->
    <meta name="viewport" content="width=device-width">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
    <meta name="description" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="index, follow, noodp">
    <meta name="googlebot" content="index, follow">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">

    <link href="{{ mix('css/print/print.css') }}" rel="stylesheet"/>
    <link id="a4" href="{{ mix('css/print/a4.css') }}" rel="stylesheet"/>

</head>
<body>

<div class="start-work">
    <div class="card-buttons">
        <div class="card-header dark-blue_text">
            <h2>Settings</h2>
        </div>
        <div class="card-body">
            <button class="button toggle">
                <div class="checkbox"></div>
                Clear document
            </button>
            <div class="document-type">
                    <span class="title">
                        * Choose file type:
                    </span>
                <div class="buttons">
                    <button class="button mr10 type-a4 types">A4</button>
                    <button class="button type-a5 types">A5</button>
                </div>
            </div>
            <div class="document-type">
                    <span class="title">
                        * Choose color:
                    </span>
                <select id="colors" class="select">
                    <option value="0">Default</option>
					<option value="1">Grey</option>
                    <option value="2">Blue</option>
                    <option value="3">Orange</option>
                </select>
            </div>
            <div class="document-submit">
                <button class="button button-submit">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">

    <div class="header">
        <div class="container">

            <div class="content">
                <div class="logo">
                    <img src="{{ asset('img/blank/logo.png') }}" alt="">
                </div>
                <div class="info">
                    <div class="top-info">
                        <div class="type blue_text"><span class="dark-blue_text">International</span> Air / Shipment
                            Waybill
                        </div>
                        <div>
                            <span class="phone dark-blue_text">С: +38 099 440 8001</span>
                            <span class="site dark-blue_text">www.fhd.com.ua</span>
                        </div>
                    </div>
					@if(isset($tracker))
                    <div class="bottom-info">
						<span class="id">
							{{ $tracker->id_tracker }}
						</span>
                    </div>
					@else
					<div class="bottom-info-empty">
						<span class="empty-tracker-recommendation">
							Please fill the form in english language
						</span>
					</div>
					@endif
                </div>
            </div>

            <div class="sidebar">
                <div class="code">
					@if(isset($tracker))
                    	<?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($tracker->id_tracker, $generator::TYPE_CODE_93)) . '">'; ?>
					@endif
                </div>
            </div>

        </div>
    </div>
    <div class="section">
        <div class="container">

            <div class="content">
                <div class="blank-header">
                    <div class="type-name">
                        FHD Service:
                    </div>
                    <div class="types">
                        <div class="type dark-blue_text">
                            FHD<span>by</span>AIR
                            @if ($typeTransport)
                                <div class="checkbox {{ $typeTransport->type == 'air' ? 'checked' : '' }}"></div>
                            @else
                                <div class="checkbox"></div>
                            @endif
                        </div>
                        <div class="type dark-blue_text">
                            FHD<span>by</span>SEA
                            @if ($typeTransport)
                                <div class="checkbox {{ $typeTransport->type == 'sea' ? 'checked' : '' }}"></div>
                            @else
                                <div class="checkbox"></div>
                            @endif
                        </div>
                        <div class="type dark-blue_text">
                            FHD<span>by</span>ROAD
                            @if ($typeTransport)
                                <div class="checkbox {{ $typeTransport->type == 'road' ? 'checked' : '' }}"></div>
                            @else
                                <div class="checkbox"></div>
                            @endif
                        </div>
                        <div class="type dark-blue_text">
                            FHD<span>by</span>TRAIN
                            @if ($typeTransport)
                                <div class="checkbox {{ $typeTransport->type == 'train' ? 'checked': '' }}"></div>
                            @else
                                <div class="checkbox"></div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="blank-body">
                    <div class="blank-block">
                        <div class="blank-title">
                            <div class="title-number"><span>1</span></div>
                            <div class="title-text dark-blue_text"><span>From (Sender) </span> / Грузоотправитель</div>
                        </div>

                        <div class="blank-inputs">

                            <div class="blank-row triangle-effect">

								{{--.blank-row width = 1230--}}

								{{--1230 / 2 = .blank-input.w50 width 615--}}

								{{--615 - 23.5 margin left = 591.5 --}}
								{{--591.5
									- .placeholders width 130
									- .blank-input margin rigth 15
									- .blank-input padding left 13
									- some padding for emphasis 5
									= .text max-width 428.5
								--}}

								<div class="blank-input w50 wrapoff" style="max-width: 591.5px">
									<div class="placeholders min-w-130">
										<span class="en dark-blue_text fs14">Sender’s name *</span>
										<span class="ru blue_text fs12">Ф.И.О. отправителя</span>
									</div>
									<div class="text fs14" style="max-width: 428.5px">
										@if (isset($tracker->sender->full_name))
											{{ $tracker->sender->full_name }}
										@endif
									</div>
								</div>
                                <div class="blank-input w50 wrapoff" style="max-width: 591.5px">
                                    <div class="placeholders min-w-130">
                                        <span class="en dark-blue_text fs14">Company name</span>
                                        <span class="ru blue_text fs12">Название компании</span>
                                    </div>
                                    <div class="text fs14" style="max-width: 428.5px">
                                        @if (isset($tracker->sender->company_name))
                                            {{ $tracker->sender->company_name }}
                                        @endif
                                    </div>
                                </div>
							</div>

                            <div class="blank-row">
                                <div class="blank-input w75">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Phone, fax or e-mail (if applicable) *</span>
                                        <span class="ru blue_text fs12">Телефон, Факс или e-mail (если применимо)</span>
                                    </div>

                                    @if ($tracker && $tracker->sender && isset($tracker->sender->number))
                                        <div class="text fs14">{{ $tracker->sender->number }}</div>
                                    @endif
                                    @if ($tracker && $tracker->sender && !isset($tracker->sender->number))
                                        <div class="text fs14">
											@if (!isset($tracker->sender->email))
											{{ $tracker->sender->email }}
											@endif
										</div>
                                    @endif
                                </div>
                                <div class="blank-input w25">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">FHD account number</span>
                                        <span class="ru blue_text fs12">FHD номер аккаунта</span>
                                    </div>
                                    <div class="text fs14">
										@if ($tracker && $tracker->sender && isset($tracker->sender->number_account))
											{{ $tracker->sender->number_account }}
										@endif
									</div>
                                </div>
                            </div>

                            <div class="blank-row">
								{{--.blank-row width = 1230--}}

								{{--.blank-input width = 1230 --}}
								{{--.blank-input real width 1215 + pading-left 15 --}}
								{{-- .placeholders width 65 --}}
								{{-- .text margin right 13 --}}
								{{-- .text max-width = 1215 - 15 - 65 - 13 = 1122 --}}

                                <div class="blank-input w100 wrapoff">
                                    <div class="placeholders min-w-65">
                                        <span class="en dark-blue_text fs14">Address *</span>
                                        <span class="ru blue_text fs12">Адрес</span>
                                    </div>
                                    <div class="text fs14" style="max-width: 1122px">
										@if ($tracker && isset($tracker->sender->address))
											{{ $tracker->sender->address }}
										@endif
									</div>
									<style type="text/css">

									</style>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input w33 wrapoff">
                                    <div class="placeholders min-w-35">
                                        <span class="en dark-blue_text fs14">City *</span>
                                        <span class="ru blue_text fs12">Город</span>
                                    </div>
                                    <div class="text fs14" style="max-width: 335.33px;">
										@if ($tracker && $tracker->sender && isset($tracker->sender->city))
											{{ $tracker->sender->city }}
										@endif
									</div>
                                </div>
                                <div class="blank-input w33 wrapoff">
                                    <div class="placeholders min-w-110">
                                        <span class="en dark-blue_text fs14">State / Province</span>
                                        <span class="ru blue_text fs12">Штат / Провинция</span>
                                    </div>
                                    <div class="text fs14" style="max-width: 335.33px;">
										@if ($tracker && $tracker->sender && isset($tracker->sender->region))
											{{ $tracker->sender->region }}
										@endif
									</div>
                                </div>
                                <div class="blank-input w33">
                                    <div class="placeholders min-w-65">
                                        <span class="en dark-blue_text fs14">Country *</span>
                                        <span class="ru blue_text fs12">Страна</span>
                                    </div>
                                    <div class="text fs14" style="max-width: 335.33px;">
										@if ($tracker && $tracker->sender && isset($tracker->sender->country))
											{{ $tracker->sender->country }}
										@endif
									</div>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input w80">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Sender’s VAT / TURN number (if required)</span>
                                        <span class="ru blue_text fs12">Номер плательщика НДС (если необходимо)</span>
                                    </div>
                                    <div class="text fs14">
										@if ($tracker && $tracker->sender && isset($tracker->sender->vat))
											{{ $tracker->sender->vat }}
										@endif
									</div>
                                </div>
                                <div class="blank-input w20 wrapoff">
                                    <div class="placeholders min-w-110">
                                        <span class="en dark-blue_text fs14">ZIP / Post code *</span>
                                        <span class="ru blue_text fs12">Почтовый индекс</span>
                                    </div>
                                    <div class="text fs14">
										@if ($tracker && $tracker->sender && isset($tracker->sender->index))
											{{ $tracker->sender->index }}
										@endif
									</div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="blank-block">
                        <div class="blank-title">
                            <div class="title-number">2</div>
                            <div class="title-text dark-blue_text"><span>To (Receiver) </span> / Грузополучатель</div>
                        </div>

                        <div class="blank-inputs">

                            <div class="blank-row triangle-effect">
                                <div class="blank-input w50 wrapoff">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Recipient’s name *</span>
                                        <span class="ru blue_text fs12">Ф.И.О. получателя</span>
                                    </div>
                                    <div class="text fs14" style="max-width: 428.5px">
										@if ($tracker && $tracker->recipient && isset($tracker->recipient->full_name))
											{{ $tracker->recipient->full_name }}
										@endif
									</div>
                                </div>
                                <div class="blank-input w50 wrapoff">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Company name</span>
                                        <span class="ru blue_text fs12">Название компании</span>
                                    </div>
                                    <div class="text fs14" style="max-width: 428.5px">
										@if ($tracker && $tracker->recipient && isset($tracker->recipient->company_name))
											{{ $tracker->recipient->company_name }}
										@endif
									</div>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input w75">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Phone, fax or e-mail (if applicable) *</span>
                                        <span class="ru blue_text fs12">Телефон, Факс или e-mail (если применимо)</span>
                                    </div>
                                    @if ($tracker && $tracker->recipient && isset($tracker->recipient->number))
                                        <div class="text fs14">
											@if ($tracker->recipient && isset($tracker->recipient->number))
												{{ $tracker->recipient->number }}
											@endif
										</div>
                                    @endif
                                    @if ($tracker &&  $tracker->recipient && !isset($tracker->recipient->number))
                                        <div class="text fs14">{{ $tracker->recipient->email }}
											@if (isset($tracker->recipient->email))
												{{ $tracker->recipient->email }}
											@endif
										</div>
                                    @endif
                                </div>
                                <div class="blank-input w25">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">FHD account number</span>
                                        <span class="ru blue_text fs12">FHD номер аккаунта</span>
                                    </div>
                                    <div class="text fs14">
										@if ($tracker &&  isset($tracker->recipient->number_account))
											{{ $tracker->recipient->number_account }}
										@endif
									</div>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input w100 wrapoff">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Address *</span>
                                        <span class="ru blue_text fs12">Адрес</span>
                                    </div>
									<div class="text fs14" style="max-width: 1122px">
										@if ($tracker &&  isset($tracker->recipient->address))
											{{ $tracker->recipient->address }}
										@endif
									</div>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input w33 wrapoff">
                                    <div class="placeholders min-w-35">
                                        <span class="en dark-blue_text fs14">City *</span>
                                        <span class="ru blue_text fs12">Город</span>
                                    </div>
									<div class="text fs14" style="max-width: 335.33px;">
										@if ($tracker &&  isset($tracker->recipient->city))
											{{ $tracker->recipient->city }}
										@endif
									</div>
                                </div>
                                <div class="blank-input w33">
                                    <div class="placeholders min-w-110">
                                        <span class="en dark-blue_text fs14">State / Province</span>
                                        <span class="ru blue_text fs12">Штат / Провинция</span>
                                    </div>
									<div class="text fs14" style="max-width: 335.33px;">
										@if ($tracker &&  $tracker->recipient && isset($tracker->recipient->region))
											{{ $tracker->recipient->region }}
										@endif
									</div>
                                </div>
                                <div class="blank-input w33">
                                    <div class="placeholders min-w-65">
                                        <span class="en dark-blue_text fs14">Country *</span>
                                        <span class="ru blue_text fs12">Страна</span>
                                    </div>
									<div class="text fs14" style="max-width: 335.33px;">
										@if ($tracker && isset($tracker->recipient->country))
											{{ $tracker->recipient->country }}
										@endif
									</div>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input w80">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Recipient’s Tax ID number for customs purpose</span>
                                        <span class="ru blue_text fs12">Для получателя, налоговый идентификатор для таможенных целей </span>
                                    </div>
									<div class="text fs14">
										@if ($tracker && isset($tracker->recipient->custom_identify))
											{{ $tracker->recipient->custom_identify }}
										@endif
									</div>
                                </div>
                                <div class="blank-input w20 wrapoff">
                                    <div class="placeholders min-w-110">
                                        <span class="en dark-blue_text fs14">ZIP / Post code *</span>
                                        <span class="ru blue_text fs12">Почтовый индекс</span>
                                    </div>
									<div class="text fs14">
										@if ($tracker &&  isset($tracker->recipient->index))
											{{ $tracker->recipient->index }}
										@endif
									</div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="blank-block">
                        <div class="blank-title">
                            <div class="title-number">3</div>
                            <div class="title-text dark-blue_text"><span>Shipment Information </span> / Информация о
                                грузе
                            </div>
                        </div>
                        <div class="blank-inputs">

                            <div class="blank-row">
                                <div class="blank-input w33">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Chargeable weight, Kg</span>
                                        <span class="ru blue_text fs12">Оплачиваемый вес, Кг</span>
                                    </div>
									@php
										$sumActualWeight = 0;
										$sumVolumeWeight = 0;

										if ($tracker) {

											foreach ($tracker->receptacles as $receptacle) {

												if ($receptacle->weightUnit && (int)$receptacle->weightUnit->multiplier_to_base != 1) {

													$multiplier = $receptacle->weightUnit->multiplier_to_base;

													$sumActualWeight = $sumActualWeight + ($receptacle->actual_weight / $multiplier);
													$sumVolumeWeight = $sumVolumeWeight + ($receptacle->volume_weight / $multiplier);

												} else {

													$sumActualWeight += $receptacle->actual_weight;
													$sumVolumeWeight += $receptacle->volume_weight;
												}

											}


											$sumActualWeight = ceil($sumActualWeight);
											$sumVolumeWeight = ceil($sumVolumeWeight);
										}


									@endphp

                                    <div class="text fs14">
										@if ($tracker)
											@if ($sumActualWeight  > $sumVolumeWeight)
												{{ $sumActualWeight }}
											@else
												{{ $sumVolumeWeight }}
											@endif
										@endif
									</div>
                                </div>
								<div class="blank-input w33">
									<div class="placeholders">
										<span class="en dark-blue_text fs14">Total weight, Kg</span>
										<span class="ru blue_text fs12">Общий вес, Кг</span>
									</div>
									<div class="text fs14">
										@if ($tracker && isset($tracker->receptacles))
											{{ $sumActualWeight }}
										@endif
									</div>
								</div>
                                <div class="blank-input w33 measures">
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">Volume weight</span>
                                        <span class="ru blue_text fs12">Объемный вес</span>
                                    </div>
                                    <div class="text fs14">
										@if ($tracker && isset($tracker->receptacles))
											{{ $sumVolumeWeight }}
										@else
											---
										@endif
									</div>
                                    <div class="placeholders">
                                        <span class="en dark-blue_text fs14">(L*H*W/5000), Kg</span>
                                    </div>
                                </div>
                            </div>

                            <div class="blank-row mt10">
                                <div class="blank-input blank-inner-title w33">
                                    <div class="placeholders">
                                        <span class="en fs14">Dimensions <span class="fs12"> / Габариты</span></span>
                                    </div>
                                </div>
                                <div class="blank-input blank-inner-title w77">
                                    <div class="placeholders">
                                        <span class="en fs14">Full description of goods<span class="fs12"> / Описание содержимого</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="blank-row">
                                <div class="blank-table w33">
									<div class="blank-input w10 p0 fjcenter">
										<span class="en dark-blue_text fs14">№</span>
									</div>
                                    <div class="blank-input w30">
                                        <span class="en dark-blue_text fs14">L, cm</span>
                                    </div>
                                    <div class="blank-input w30">
                                        <span class="en dark-blue_text fs14">W, cm</span>
                                    </div>
                                    <div class="blank-input w30">
                                        <span class="en dark-blue_text fs14">H, cm</span>
                                    </div>
                                </div>
                                <div class="blank-table w77">
                                    <div class="blank-input w35">
                                        <span class="en dark-blue_text fs14">Description</span>
                                        <span class="ru blue_text fs12">/ Описание</span>
                                    </div>
                                    <div class="blank-input w15">
                                        <span class="en dark-blue_text fs14">Qty</span>
                                        <span class="ru blue_text fs12">/ Кол-во</span>
                                    </div>
									<div class="blank-input w35">
										<span class="en dark-blue_text fs14">Description</span>
										<span class="ru blue_text fs12">/ Описание</span>
									</div>
									<div class="blank-input w15">
										<span class="en dark-blue_text fs14">Qty</span>
										<span class="ru blue_text fs12">/ Кол-во</span>
									</div>
                                </div>
                            </div>
                            <div class="blank-row">
                                <div class="blank-table w33">
									<div class="many-rows">
										@if ($tracker && isset($tracker->receptacles))
											@php
												$counter = 0;
											@endphp

											@foreach($tracker->receptacles as $receptacleKey => $receptacle)
												@if ($receptacleKey > 5)
													continue;
												@endif
												<div class="blank-row">
													<div class="blank-input dark-blue_text number-title w10">
														@php
															$counter = $receptacleKey + 1;
														@endphp
														{{ $counter }}
													</div>

													<div class="blank-input w30">
														<div class="text fs14">
															{{ $receptacle->length * $receptacle->lengthUnit->multiplier_to_base }}
														</div>
													</div>
													<div class="blank-input w30">
														<div class="text fs14">
															{{ $receptacle->width * $receptacle->lengthUnit->multiplier_to_base }}
														</div>
													</div>
													<div class="blank-input w30">
														<div class="text fs14">
															{{ $receptacle->height * $receptacle->lengthUnit->multiplier_to_base }}
														</div>
													</div>

												</div>
											@endforeach

											@if (count($tracker->receptacles) < 6)
												@for ($i = 0; $i < 5 - count($tracker->receptacles); $i++)
													<div class="blank-row">
														<div class="blank-input dark-blue_text number-title w10">
															@php
																$counter = $counter + 1;
															@endphp
															{{ $counter }}
														</div>
														<div class="blank-input w30">
															<div class="text fs14"></div>
														</div>
														<div class="blank-input w30">
															<div class="text fs14"></div>
														</div>
														<div class="blank-input w30">
															<div class="text fs14"></div>
														</div>
													</div>
												@endfor
											@endif

										@else

											@for($i = 1; $i <= 5; $i++)

											<div class="blank-row">
												<div class="blank-input dark-blue_text number-title w10">
													{{ $i }}
												</div>

												<div class="blank-input w30">
													<div class="text fs14"></div>
												</div>
												<div class="blank-input w30">
													<div class="text fs14"></div>
												</div>
												<div class="blank-input w30">
													<div class="text fs14"></div>
												</div>

											</div>

											@endfor

										@endif
									</div>
                                </div>
                                <div class="blank-table w77">
                                    <div class="many-rows">
										@php
										$counter = 0;
										@endphp

										@for($i = 0; $i < 5; $i++)
											<div class="blank-row">
												<div class="blank-input w35">
													<div class="text fs14">
														@if (isset($tracker->allCargos[$i]['description']))
															{{ $tracker->allCargos[$i]['description'] }}
														@endif
													</div>
												</div>
												<div class="blank-input w15">
													<div1 class="text fs14">
														@if (isset($tracker->allCargos[$i]['quantity']))
															{{ $tracker->allCargos[$i]['quantity'] }}
														@endif
													</div1>
												</div>

												<div class="blank-input w35">
													<div class="text fs14">
														@if (isset($tracker->allCargos[$i + 5]))
														{{ $tracker->allCargos[$i + 5]['description'] }}
														@endif
													</div>
												</div>
												<div class="blank-input w15">
													<div class="text fs14">
														@if (isset($tracker->allCargos[$i + 5]))
															{{ $tracker->allCargos[$i + 5]['quantity'] }}
														@endif
													</div>
												</div>
											</div>
										@endfor

                                    </div>
                                </div>
                            </div>


                            <div class="blank-row mt10">
                                <div class="blank-input blank-inner-title w54-s">
                                    <div class="placeholders">
                                        <span class="en fs14">Non-document shipments<span
                                                    class="ru fs12"> / Для посылок, облагаемых пошлиной (таможенные требования)</span></span>
                                    </div>
                                </div>
                                <div class="blank-input blank-inner-title w45-s">
                                    <div class="placeholders">
                                        <span class="en fs14">Bill Duties and Taxes to <span class="ru fs12"> / Кто оплачивает таможенные затраты</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="blank-row">
                                <div class="blank-table w54-s">
                                    <div class="blank-input blank-input-big w40 h140">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14">Declared Value for Customs *</span>
                                            <span class="ru blue_text fs12"> Таможенная стоимость</span>
                                        </div>
                                        <div class="text fs14">
											@php

												if (isset($tracker)) {

													if ($isChinaTracker) {

														$sumAssessedPriceInCNY = 0;

														foreach ($tracker->receptacles as $receptacle) {

															$multiplier = 1;

															if ($receptacle->currency) {
																$multiplier = $receptacle->currency->cny;
															}

															$sumAssessedPriceInCNY = $sumAssessedPriceInCNY + ($receptacle->assessed_price * $multiplier);
														}

														echo money_format('%i', $sumAssessedPriceInCNY);


													} else {

														$sumAssessedPriceInUSD = 0;

														foreach ($tracker->receptacles as $receptacle) {

															$multiplier = 1;

															if ($receptacle->currency) {
																$multiplier = $receptacle->currency->usd;
															}

															$sumAssessedPriceInUSD = $sumAssessedPriceInUSD + ($receptacle->assessed_price * $multiplier);
														}

														echo money_format('%i', $sumAssessedPriceInUSD);
													}
												}

											@endphp

										</div>
                                    </div>
                                    <div class="blank-input blank-input-big w20 h140">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14">Currency *</span>
                                            <span class="ru blue_text fs12">Валюта</span>
                                        </div>
                                        <div class="text fs14">
											@if(isset($tracker) && $tracker && $isChinaTracker)
												CNY
											@else
												USD
											@endif
										</div>
                                    </div>
                                    <div class="blank-input blank-input-big w40 h140">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14">Harmonized Commodity Code</span>
                                            <span class="ru blue_text fs12"> Код товара</span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>

                                <div class="blank-table w45-s">
                                    <div class="many-inner-inputs w100">
                                        <div class="blank-row">
                                            <div class="blank-input w100 mb20">
                                                <div class="blank-type h30">
                                                    <div class="checkbox @if($tracker) {{ $tracker->bill_duty_id == 1 ? 'checked' : '' }} @endif"></div>
                                                    <div class="dflex">
                                                        <span class="en dark-blue_text fs13">Sender</span>
                                                        <span class="ru blue_text fs12"> / Грузоотправитель</span>
                                                    </div>
                                                </div>
                                                <div class="blank-type">
                                                    <div class="checkbox @if($tracker) {{ $tracker->bill_duty_id == 2 ? 'checked' : '' }} @endif"></div>
                                                    <div class="dflex">
                                                        <span class="en dark-blue_text fs13">Recipient</span>
                                                        <span class="ru blue_text fs12"> / Грузополучатель</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blank-row">
                                            <div class="blank-input w50 mr0">
                                                <div class="blank-type w90">
                                                    <div class="checkbox @if($tracker) {{ $tracker->bill_duty_id == 3 ? 'checked' : '' }} @endif"></div>
                                                    <div class="bg-white">
                                                        <div class="placeholders">
                                                            <span class="en dark-blue_text fs14">Third party</span>
                                                            <span class="ru blue_text fs12">Третье лицо</span>
                                                        </div>
                                                        <div class="text fs14">
															@if($tracker)
																{{ $tracker->billDutyThridPart }}
															@endif
														</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="blank-input w50">
                                                <div class="blank-type w90">
                                                    <div class="checkbox @if($tracker) {{ $tracker->bill_duty_id == 4 ? 'checked' : '' }} @endif"></div>
                                                    <div class="bg-white">
                                                        <div class="placeholders">
                                                            <span class="en dark-blue_text fs14">FHD Acct. №</span>
                                                            <span class="ru blue_text fs12">FHD акк. №</span>
                                                        </div>
                                                        <div class="text fs14">
															@if($tracker)
																{{ $tracker->billDutyFhdAccount }}
															@endif
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sidebar">
                <div class="blank-body">
                    <div class="blank-block">
                        <div class="blank-title">
                            <div class="title-number">4</div>
                            <div class="title-text dark-blue_text"><span>Express Freight Service </span> / Доставка
                                грузов
                            </div>
                        </div>
                        <div class="blank-inputs">

                            <div class="blank-row mt0">
                                <div class="blank-input">
                                    <div class="checkbox @if($tracker) {{ $tracker->freight_service_id == 1 ? 'checked' : '' }} @endif"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">FHD Intl. </span>
                                        <span class="blue_text fs12"> / Priority Freight</span>
                                    </div>
                                </div>

                                <div class="blank-input">
									<div class="checkbox @if($tracker) {{ $tracker->freight_service_id == 1 ? 'checked' : '' }} @endif"></div>
									<div class="dflex fcol farifgr mt15">
										<span class="dark-blue_text fs12">Packages over 150 lbs /60 kg. </span>
										<span class="blue_text fs12"> Груз свыше 60 кг.</span>
									</div>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input">
                                    <div class="checkbox @if($tracker) {{ $tracker->freight_service_id == 2 ? 'checked' : '' }} @endif"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">FHD Intl.</span>
                                        <span class="blue_text fs12"> / Economy Freight</span>
                                    </div>
                                </div>
                            </div>

                            <div class="blank-row inputs-with-bg mt10">
                                <div class="blank-input w100">
                                    <div class="blank-type">
                                        <div class="checkbox @if($tracker) {{ $tracker->freight_service_id == 3 ? 'checked' : '' }} @endif"></div>
                                        <div class="bg-white">
                                            <div class="placeholders">
                                                <span class="en dark-blue_text fs14 pr5">Booking Number</span>
                                                <span class="ru blue_text fs12"> Номер бронирования</span>
                                            </div>
                                            <div class="text fs14">
												@if($tracker)
													{{ $tracker->freightServiceBookingNumber }}
												@endif
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blank-row only-text">
                                <div class="blank-input blank-text w100">
                                    <span class="en dark-blue_text fs12">Please contact FHD office to book shipments </span>
                                    <span class="ru blue_text fs12">Свяжитесь с офисом FHD для бронирования.</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="blank-block">
                        <div class="blank-title">
                            <div class="title-number">5</div>
                            <div class="title-text dark-blue_text"><span>Special Handling  </span> / Особые инструкции
                            </div>
                        </div>
                        <div class="blank-inputs">
                            <div class="blank-row mt0">
                                <div class="blank-input">
                                    <div class="checkbox @if($tracker) {{ $tracker->special_handling_id == 1 ? 'checked' : '' }} @endif"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">HOLD at FHD warehouse</span>
                                        <span class="blue_text fs12"> / Доставка до cклада FHD </span>
                                    </div>
                                </div>
                            </div>
                            <div class="blank-row mt0">
                                <div class="blank-input">
                                    <div class="checkbox @if($tracker) {{ $tracker->special_handling_id == 2 ? 'checked' : '' }} @endif"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">SATURDAY delivery</span>
                                        <span class="blue_text fs12"> / Доставка в СУББОТУ</span>
                                    </div>
                                </div>
                            </div>
							<div class="blank-row mt0">
                                <div class="blank-input">
                                    <div class="checkbox @if($tracker)  {{ $tracker->special_handling_id == 2 ? 'checked' : '' }} @endif"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">ADDRESS delivery</span>
                                        <span class="blue_text fs12"> / Адресная доставка</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="blank-block long-title">
                        <div class="blank-title">
                            <div class="title-number">6</div>
                            <div class="title-text dark-blue_text"><span>Bill Transportation Charges to: </span> Кто
                                оплачивает транспортные затраты:
                            </div>
                        </div>

                        <div class="blank-inputs">

                            <div class="blank-row mt0">
                                <div class="blank-input">
                                    <div class="checkbox @if($tracker)  {{ $tracker->bill_transportation_id == 1 ? 'checked' : '' }} @endif"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">Sender </span>
                                        <span class="blue_text fs12"> / Грузоотправитель</span>
                                    </div>
                                </div>

                                <div class="blank-input">
                                    <div class="checkbox @if($tracker)  {{ $tracker->bill_transportation_id == 2 ? 'checked' : '' }} @endif"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">Recipient</span>
                                        <span class="blue_text fs12"> / Получатель</span>
                                    </div>
                                </div>
                            </div>

                            <div class="blank-row inputs-with-bg mt10">
                                <div class="blank-input w100">
                                    <div class="blank-type">
                                        <div class="checkbox @if($tracker) {{ $tracker->bill_transportation_id == 3 ? 'checked' : '' }} @endif"></div>
                                        <div class="bg-white">
                                            <div class="placeholders">
                                                <span class="en dark-blue_text fs14 pr5">Third party</span>
                                                <span class="ru blue_text fs12"> Третье лицо </span>
                                            </div>
                                            <div class="text fs14">
												@if($tracker)
													{{ $tracker->billTransportationThirdParty }}
												@endif
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="blank-row mt10">
                                <div class="blank-input blank-inner-title w100">
                                    <div class="placeholders">
                                        <span class="en fs14">Payment method <span class="fs12"> / Способ оплаты</span></span>
                                    </div>
                                </div>
                            </div>

                            <div class="blank-row">
                                <div class="blank-input">
                                    <div class="checkbox"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">Credit card </span>
                                        <span class="blue_text fs12"> / Кредитная карта </span>
                                    </div>
                                </div>

                                <div class="blank-input">
                                    <div class="checkbox"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">Cash </span>
                                        <span class="blue_text fs12"> / Наличные</span>
                                    </div>
                                </div>
                            </div>

                            <div class="blank-row mt0">
                                <div class="blank-input">
                                    <div class="checkbox"></div>
                                    <div class="dflex">
                                        <span class="dark-blue_text fs14 pr5">Cheque </span>
                                        <span class="blue_text fs12"> / Чек</span>
                                    </div>
                                </div>
                            </div>

                            <div class="blank-row inputs-with-bg mt10">
                                <div class="blank-input w100">
                                    <div class="blank-type">
                                        <div class="checkbox"></div>
                                        <div class="bg-white">
                                            <div class="placeholders">
                                                <span class="en dark-blue_text fs14 pr5">Bank Transfer</span>
                                                <span class="ru blue_text fs12"> Банковский перевод</span>
                                            </div>
                                            <div class="text fs14"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="blank-block">
                        <div class="blank-title">
                            <div class="title-number">7</div>
                            <div class="title-text dark-blue_text"><span>Charges </span> / Начисления</div>
                        </div>

                        <div class="blank-inputs">
                            <div class="blank-row inputs-white inputs-big mt0">
                                <div class="blank-input w33">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Service Charges</span>
                                            <span class="ru blue_text fs12"> Стоимость сервиса</span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
                                <div class="blank-input w33">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Insurance</span>
                                            <span class="ru blue_text fs12"> Страховой тариф </span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
								<div class="blank-input w33">
									<div class="bg-white">
										<div class="placeholders">
											<span class="en dark-blue_text fs14 pr5">Currency</span>
											<span class="ru blue_text fs12"> Валюта </span>
										</div>
										<div class="text fs14"></div>
									</div>
								</div>
                            </div>
                            <div class="blank-row inputs-white inputs-big">
                                <div class="blank-input w100">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Total Amount</span>
                                            <span class="ru blue_text fs12"> Cумма к оплате</span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="blank-block">
                        <div class="blank-title">
                            <div class="title-number">8</div>
                            <div class="title-text dark-blue_text"><span>Courier Signature </span> / Подпись курьера
                            </div>
                        </div>
                        <div class="blank-inputs bg-section">
                            <div class="blank-row inputs-white inputs-big">
                                <div class="blank-input w100">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Picked up by:</span>
                                            <span class="ru blue_text fs12"> Получил:</span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="blank-row inputs-white">
                                <div class="blank-input w50">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Date:</span>
                                            <span class="ru blue_text fs12"> Дата:</span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
                                <div class="blank-input w50">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Time:</span>
                                            <span class="ru blue_text fs12">Время:</span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="border-dashed"></div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="content">
                <div class="footer-block">
                    <div class="footer-text w90">
                        <p class="fs12 blue_text"><span class="dark-blue_text">Sender’s agreement</span> / Согласие
                            отправителя</p>
                        <p class="fs11 blue_text">Sender’s signature on this International Air/Shipment Waybill confirms
                            its knowledge of the Terms of Services and the Public Agreement posted on the site <span
                                    class="dark-blue_text">www.fhd.com.ua</span>, and is a confirmation of the
                            undertaken obligations, as well as the acceptance of all the proposed conditions. The Sender
                            also confirms that there are no prohibited items in parcel.</p>
                        <p class="fs11 blue_text">Подписание отправителем данной экспресс-накладной подтверждает его
                            осведомленность об Условиях предоставления услуг и Публичном договоре, размещенных на сайте
                            <span class="dark-blue_text">www.fhd.com.ua</span>, и является подтверждением взятых на себя
                            обязательств, а также акцептом всех предложенных условий. Отправитель также подтверждает
                            отсутствие в отпралении запрещенных к перевозке предметов.</p>
                    </div>
                    <div class="footer-icon w10">
                        <img class="arrow" src="{{ asset('img/blank/arrow.png') }}" alt="">
                    </div>
                </div>
            </div>

            <div class="sidebar">
                <div class="blank-body">
                    <div class="blank-block">
                        <div class="blank-inputs bg-footer mt10">
                            <div class="blank-row inputs-white mt0">
                                <div class="blank-input w40">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Date :</span>
                                            <span class="ru blue_text fs12"> Дата :</span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
                                <div class="blank-input w60">
                                    <div class="bg-white">
                                        <div class="placeholders">
                                            <span class="en dark-blue_text fs14 pr5">Recipient signature</span>
                                            <span class="ru blue_text fs12"> Подпись получателя: </span>
                                        </div>
                                        <div class="text fs14"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>
</div>

<div class="current-type" title="Current doc type"></div>


<script src="{{ mix('js/vendor/jquery.js') }}"></script>
<script src="{{ mix('js/print.js') }}"></script>

</body>
</html>
