@extends('layouts.app')

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active"
                     style="background: url('http://cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/train_bckg.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                                <div class="pull-left" style="width: 65%;">
                                    <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.trackers.p') }}</p>
                                    <h1>{{ trans('main.trackers.h1') }}</h1>
                                    <h5 class="subtitle">{{ trans('main.trackers.h5') }}</h5>
                                    <div class="btn-group-block">
                                        <a href="/about-us"
                                           class="btn btn-default-new">{{ trans('main.trackers.link2') }}</a>
                                    </div>
                                </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('content')

	<div class="container">

	@include('filters.filters-trackers')

	<div id="filters-container">
	@include('custom-table-ajax')
	</div>

	</div>

@endsection
