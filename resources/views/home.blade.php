@extends('layouts.app')

@section('content')

    <section class="main-section">
        <div class="container">
            <div class="search-block">
                <h1 class="title">{{ trans('main.main_page.track_shipment') }}</h1>
                <div class="subtitle"></div>

                @include('include/hero-block')

                <a href="{{ route('create-trackers') }}" class="btn btn-secondary btn-create">{{ trans('main.main_page.create_tracker') }}</a>
            </div>
        </div>
    </section>

    <section class="services-section">
        <div class="container">
            <div class="title-block">
{{--                <div class="title">{{ trans('main.main_page.our_services') }}</div>--}}
                <h2 class="title">{{ trans('main.main_page.core_values') }}</h2>
            </div>
            <div class="services-block">
                <div class="service-block_flex">
                    <div class="service-block__col">
                            <div class="service-block service-block_padding-l service-block_margin">
                                    <div class="icon">
                                        <svg><use xlink:href="#profitable_SVG"></use></svg>
                                    </div>
                                    <div class="info-block">
                                        <div class="info-block-title">
                                            <span>{{ trans('main.main_page.tariffs') }}</span>
                                        </div>
                                        <div class="info-block-text">
                                            {{ trans('main.main_page.tariffs_value') }}
                                        </div>
                                    </div>
                            </div>
                        </div>
                    <div class="service-block__col">
                        <div class="service-block service-block_padding-l service-block_margin">
                            <div class="icon">
                                <svg><use xlink:href="#composition"></use></svg>
                            </div>
                            <div class="info-block">
                                <div class="info-block-title">
                                    <span> {{ trans('main.main_page.custom_w') }}</span>
                                </div>
                                <div class="info-block-text">
                                    {{ trans('main.main_page.custom_w_value') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="service-block__col">
                        <div class="service-block service-block_margin">
                            <div class="icon">
                                <svg><use xlink:href="#authenticity_SVG"></use></svg>
                            </div>
                            <div class="info-block">
                                <div class="info-block-title">
                                    <span>{{ trans('main.main_page.online_tracking') }}</span>
                                </div>
                                <div class="info-block-text">
                                    {{ trans('main.main_page.online_tracking_value') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="service-block__col">
                        <div class="service-block service-block_padding-l service-block_margin">
                            <div class="icon">
                                <svg><use xlink:href="#timer"></use></svg>
                            </div>
                            <div class="info-block">
                                <div class="info-block-title">
                                    <span> {{ trans('main.main_page.time') }}</span>
                                </div>
                                <div class="info-block-text">
                                    {{ trans('main.main_page.time_value') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="service-block__col">
                        <div class="service-block service-block_padding-l service-block_margin">
                            <div class="icon">
                                <svg><use xlink:href="#protection_SVG"></use></svg>
                            </div>
                            <div class="info-block">
                                <div class="info-block-title">
                                    <span> {{ trans('main.main_page.safety') }}</span>
                                </div>
                                <div class="info-block-text">
                                    {{ trans('main.main_page.safety_value') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="service-block__col">
                        <div class="service-block service-block_margin">
                            <div class="icon">
                                <svg><use xlink:href="#tarif_SVG"></use></svg>
                            </div>
                            <div class="info-block">
                                <div class="info-block-title" >
                                    <span> {{ trans('main.main_page.buy_online') }}</span>
                                </div>
                                <div class="info-block-text">
                                    {{ trans('main.main_page.buy_online_value') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--<section class="documents-section">--}}
        {{--<div class="container">--}}
            {{--<div class="title-block">--}}
                {{--<div class="subtitle">{{ trans('main.main_page.documents') }}</div>--}}
                {{--<h2 class="title">{{ trans('main.main_page.documents_for') }}</h2>--}}
            {{--</div>--}}
            {{--<div class="documents-block">--}}
                {{--<div class="document-block">--}}
                    {{--<div class="document-title">{{ trans('main.main_page.legal') }}</div>--}}
                    {{--<div class="document-list">--}}
                        {{--<ul>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.cargoes<150') }}</a></li>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.cargoes>150') }}</a></li>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.contract_custom') }}</a></li>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.details_custom') }}</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="document-block">--}}
                    {{--<div class="document-title">{{ trans('main.main_page.natural') }}</div>--}}
                    {{--<div class="document-list">--}}
                        {{--<ul>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.cargoes>150') }}</a></li>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.list_prohibited') }}</a></li>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.details_custom') }}</a></li>--}}
                            {{--<li><a href="#!">{{ trans('main.main_page.terms') }}</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

{{--    <section class="contact-section">--}}
{{--        <div class="container">--}}
{{--            <div class="contact-block">--}}
{{--                <div class="title-block">--}}
{{--                    <div class="subtitle">{{ trans('main.main_page.contact') }}</div>--}}
{{--                    <h2 class="title">{{ trans('main.main_page.anytime') }}</h2>--}}
{{--                </div>--}}
{{--                <div class="contact-form">--}}
{{--                    <form action="#!">--}}
{{--                        <input type="text" name="name" placeholder="{{ trans('main.main_page.name') }}" required>--}}
{{--                        <input type="text" name="email" placeholder="{{ trans('main.main_page.e-mail') }}" required>--}}
{{--                        <input type="text" name="subject" placeholder="{{ trans('main.main_page.subject') }}">--}}
{{--                        <textarea name="message" cols="30" rows="10" placeholder="{{ trans('main.main_page.message') }}" required></textarea>--}}
{{--                        <button type="submit" class="btn btn-main btn-send">{{ trans('main.main_page.submit') }}</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}


	@include('calculator')

    <section class="services-section">
        <div class="container">
            <div class="title-block">
                <h2 class="title">{{ trans('main.main_page.add_info') }}</h2>
            </div>

            <div class="add-info">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <a href="/agreement/individual" class="add-info__link">
                            <div class="icon">
                                <svg style="width:39px; height:47px;" ><use xlink:href="#regulations_SVG"></use></svg>
                            </div>
                            {{ trans('main.main_page.agreement_physical') }}
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <a href="/agreement/legal" class="add-info__link">
                            <div class="icon">
                                <svg style="width:39px; height:39px;" ><use xlink:href="#del_restriction_SVG"></use></svg>
                            </div>
                            {{ trans('main.main_page.agreement_legal') }}
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <a href="#" class="add-info__link">
                            <div class="icon">
                                <svg style="width:39px; height:40px;" ><use xlink:href="#conditions_SVG"></use></svg>
                            </div>
                            {{ trans('main.main_page.terms_of_service') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--@include('include/after-hero-block')--}}
	{{--@include('include/search-block')--}}
    {{--@include('include/our-services-block')--}}
    {{--@include('include/our-customers-block')--}}
    {{--@include('include/stats-block')--}}
    {{--@include('include/create-tracker-block')--}}
    {{--@include('include/we-top-block')--}}

@endsection

