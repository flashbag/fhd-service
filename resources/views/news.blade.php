@extends('layouts.app')

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                            <div class="pull-left" style="width: 65%;">
                                <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.news.p') }}</p>
                                <h1>{{ trans('main.news.h1') }}</h1>
                                <h5 class="subtitle">{{ trans('main.news.h5') }}</h5>
                                <div class="btn-group-block">
                                    <a href="/about-us" class="btn btn-default-new">{{ trans('main.news.link2') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('content')

    <div class="wrap">
        <div class="contacts-page">
            <div class="container">
                <div class="row">

                    <div class="col-sm-9">

                        @if(isset($articles) && count($articles) > 0)

                            @foreach($articles as $article)

                                <div class="thumbnail">
                                    <img src="..." alt="...">
                                    <div class="caption">
                                        <a href="#"><h3>{{ $article->title }}</h3></a>
                                        {!! $article->short_description !!}
                                        <p><a href="#" class="btn btn-default-new">Читать далее</a></p>
                                    </div>
                                </div>

                            @endforeach

                            {{ $articles->links() }}

                        @else
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">{{ trans('main.news.inform') }}</div>
                                            <div class="panel-body">
                                                {{ trans('main.news.inform_message') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endif

                    </div>
                    <div class="col-sm-3">
                        @if(isset($newsCategories) && count($newsCategories) > 0)
                            <div class="page-header-block">
                                <p>Категории новостей</p>
                                <div class="list-group">
                                    @foreach($newsCategories as $category)
                                        <a href="#" class="list-group-item">{{ $category->title }}</a>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
