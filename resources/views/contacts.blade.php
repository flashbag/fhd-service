@extends('layouts.app')
@section('content')
        <section class="services-section section-address">
            <div class="container">
                <div class="title-block">
                    <h2 class="title title_mb">{{ trans('main.contacts.our_address') }}</h2>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="section-address__inner">
                            <div class="section-address__map">
                                <svg class="section-address__marker" style="width: 45px; height: 61px;"><use xlink:href="#Marker_SVG"></use></svg>
                                <img src="/img/contact/map_ukraine.jpg" alt="map_location">
                            </div>
                            <ul class="section-address__list pt-5">
                                <li><p>{{ trans('main.contacts.country') }}</p><span>{{ trans('main.contacts.ukraine_country') }}</span></li>
                                <li><p>{{ trans('main.contacts.region') }}</p><span>{{ trans('main.contacts.ukraine_region') }}</span></li>
                                <li><p>{{ trans('main.contacts.city') }}</p><span>{{ trans('main.contacts.ukraine_city') }}</span></li>
                                <li><p>{{ trans('main.contacts.street') }}</p><span>{{ trans('main.contacts.ukraine_street') }}</span></li>
                            </ul>
                            <ul class="section-address__list pb-5">
                                <li><p>{{ trans('main.contacts.index') }}</p><span>{{ trans('main.contacts.ukraine_postcode') }}</span></li>
                                <li><p>{{ trans('main.contacts.telephone') }}</p><span>{{ trans('main.contacts.ukraine_phone') }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="section-address__inner">
                            <div class="section-address__map">
                                <svg class="section-address__marker" style="width: 45px; height: 61px;"><use xlink:href="#Marker_SVG"></use></svg>
                                <img src="/img/contact/map_usa.jpg" alt="map_location">
                            </div>
                            <ul class="section-address__list pt-5">
                                <li><p>{{ trans('main.contacts.country') }}</p><span>{{ trans('main.contacts.usa_country') }}</span></li>
                                <li><p>{{ trans('main.contacts.state') }}</p><span>{{ trans('main.contacts.usa_region') }}</span></li>
                                <li><p>{{ trans('main.contacts.city') }}</p><span>{{ trans('main.contacts.usa_city') }}</span></li>
                                <li><p>{{ trans('main.contacts.street') }}</p><span>{{ trans('main.contacts.usa_street') }}</span></li>
                            </ul>
                            <ul class="section-address__list pb-5">
                                <li><p>{{ trans('main.contacts.index') }}</p><span>{{ trans('main.contacts.usa_postcode') }}</span></li>
                                <li><p>{{ trans('main.contacts.telephone') }}</p><span>{{ trans('main.contacts.usa_phone') }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="section-address__inner">
                            <div class="section-address__map">
                                <svg class="section-address__marker" style="width: 45px; height: 61px;"><use xlink:href="#Marker_SVG"></use></svg>
                                <img src="/img/contact/map_china.jpg" alt="map_location">
                            </div>
                            <ul class="section-address__list pt-5">
                                <li><p>{{ trans('main.contacts.country') }}</p><span>{{ trans('main.contacts.china_country') }}</span></li>
                                <li><p>{{ trans('main.contacts.provinces') }}</p><span>{{ trans('main.contacts.china_region') }}</span></li>
                                <li><p>{{ trans('main.contacts.city') }}</p><span>{{ trans('main.contacts.china_city') }}</span></li>
                                <li><p>{{ trans('main.contacts.street') }}</p><span>{{ trans('main.contacts.china_street') }}</span></li>
                            </ul>
                            <ul class="section-address__list pb-5">
                                <li><p>{{ trans('main.contacts.index') }}</p><span>{{ trans('main.contacts.china_postcode') }}</span></li>
                                <li><p>{{ trans('main.contacts.telephone') }}</p><span>{{ trans('main.contacts.china_phone') }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="services-section section-contactUs">
            <div class="container">
                <div class="title-block">
                    <h2 class="title title_mb">{{ trans('main.contacts.contact_us') }}</h2>
                </div>
                <form method="POST" action="{{ route('feedback') }}"
                     class="section-contactUs__form" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <input type="text" id="username" name="username" class="form-control input__custom" placeholder="{{ trans('main.contacts.name') }}" data-placement="bottom" data-toggle="popover" data-trigger="hover" data-content="Input name">
                                @if ($errors->has('username'))
                                    <p class="messages">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="email" id="email" name="email" class="form-control input__custom" placeholder="{{ trans('main.contacts.email') }}" data-placement="bottom" data-toggle="popover" data-trigger="hover" data-content="Input email">
                                @if ($errors->has('email'))
                                    <p class="messages">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" id="theme" name="theme" class="form-control input__custom" placeholder="{{ trans('main.contacts.subject') }}" data-placement="bottom" data-toggle="popover" data-trigger="hover" data-content="Input theme">
                                @if ($errors->has('theme'))
                                    <p class="messages">
                                        <strong>{{ $errors->first('theme') }}</strong>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <textarea id="textarea" name="textarea" class="form-control textarea__custom" placeholder="{{ trans('main.contacts.message') }}"></textarea>
                                <div class="popover fade bottom in" id="" style="top: 124px; left: 15px; display: none;">
                                    <div class="arrow" style="left: 50%;"></div>
                                    <span class="popover-icon-textarea"></span>
                                    <p class="popover-content"><b>90</b> {{ trans('main.contacts.comment1') }}<br><b class="js-textarea-count">0</b>  {{ trans('main.contacts.comment2') }}</p>
                                </div>
                                @if ($errors->has('textarea'))
                                    <p class="messages">
                                        <strong>{{ $errors->first('textarea') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <label for=""></label>
                                    <img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
                                    {{--                                <a href="#" id="refresh"><span class="glyphicon glyphicon-refresh"></span></a></p>--}}
                                </div>
                                <div class="col-sm-8 form-group">
                                    <input class="form-control input__custom" type="text" name="captcha"/>
                                    @if ($errors->has('captcha'))
                                        <p class="messages">
                                            <strong>{{ $errors->first('captcha') }}</strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <button type="submit" class="btn btn-main section-contactUs__btn pull-right">{{ trans('main.contacts.send') }}</button>
                        </div>


{{--                        @if($errors->count())--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-12">--}}
{{--                                    <div class="panel panel-danger">--}}
{{--                                        <div class="panel-heading">{{ trans('main.custom.error') }}</div>--}}
{{--                                        <div class="panel-body">--}}
{{--                                            @foreach ($errors->all() as $key => $value)--}}
{{--                                                {{ $value }}<br>--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endif--}}
                    </div>
                </form>
            </div>
        </section>
        <script>
            $('#refresh').on('click',function(){
                var captcha = $('img.captcha-img');
                var config = captcha.data('refresh-config');
                $.ajax({
                    method: 'GET',
                    url: '/get_captcha/' + config,
                }).done(function (response) {
                    captcha.prop('src', response);
                });
            });
        </script>
@endsection
