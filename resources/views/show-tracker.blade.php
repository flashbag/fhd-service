@extends('layouts.app')

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active"
                     style="background: url('http://cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                            <div class="pull-left" style="width: 65%;">
                                <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.trackers.p') }}</p>
                                <h1>{{ trans('main.trackers.h1') }}</h1>
                                <h5 class="subtitle">{{ trans('main.trackers.h5') }}</h5>
                                <div class="btn-group-block">
                                    <a href="/about-us"
                                       class="btn btn-default-new">{{ trans('main.trackers.link2') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('content')

    {{--@include('include.after-hero-block')--}}
    <div class="container wrap">

		<div class="show-tracker show-tracker_wrapp">
			@include('trackers-search-form-result')

			@if(isset($tracker) && $tracker !== null && $tracker->is_active == 1)

				@if ($errors->any())
					<div>
						<span>{{ $errors->first() }}</span>
					</div>
				@endif
		</div>

		<div class="show-tracker__state">

			<div class="show-tracker__state-head mb-5">
				{{--<p>{{ trans('main.trackers.content.p') }}</p>--}}
				<h3 class="show-tracker__text">{{ trans('main.trackers.content.h3') }}</h3>
				<p class="show-tracker__text">Номер трекера: <strong>{{ $tracker->id_tracker }}</strong></p>
	            {{--todo if isset $tracker--}}
			</div>

		</div>

			@if(isset($number))
			@foreach($statusTrackers as $key => $status)
				<div class="row stepper">
					@if (isset($historyStatuses))
						@if($historyStatuses->status_id == $status->id)
							<div>
								<div class="col-sm-1">
									<div class="circle-step step-success">
										<div class="step-counter step-counter_icon">
											<img style="width: 54px;" src="{{ asset('img/status-icons/' . ($key + 1) . '.png') }}">
										</div>
									</div>
								</div>
								<div class="col-sm-5">
									<div style="opacity: 0; height: 14px;">{{ trans('main.trackers.content.title') }}</div> {{ $status->title }}
								</div>
								<div class="col-sm-2 col-sm-offset-2">
									<br>{{ \Carbon\Carbon::parse($historyStatuses->created_at)->format('d-m-Y') }}
								</div>
								<div class="col-sm-2">
									<div class="circle-status-step pull-right">
										<div class="status-icon"><i class="fa fa-check" aria-hidden="true"></i>
										</div>
									</div>
								</div>
							</div>
						@else
							<div>
								<div class="col-sm-1">
									<div class="circle-step @if($historyStatuses->status_id > $status->id) step-success @else step-info step-default @endif">
										<div class="step-counter step-counter_icon"><img style="width: 54px;"  src="{{ asset('img/status-icons/' . ($key +1) . '.png') }}">
										</div>
									</div>
								</div>
								<div class="col-sm-5">
									<div style="opacity: 0; height: 14px;">{{ trans('main.trackers.content.title') }}</div> {{ $status->title }}
								</div>
								@if($historyStatuses->status_id > $status->id)
									<div class="col-sm-2 col-sm-offset-2">
										<br>Завершено
									</div>
								@else
									<div class="col-sm-2 col-sm-offset-2">
										<br>{{ trans('main.trackers.content.date_title') }}</div>
								@endif
								<div class="col-sm-2">
									<div class="circle-status-step pull-right">
										<div class="status-icon"><i
													class="fa @if($historyStatuses->status_id > $status->id) fa-check @else fa-clock-o @endif"
													aria-hidden="true"></i>
										</div>
									</div>
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					@endif
				</div>
			@endforeach
			@else
            @if(isset($historyStatuses))
				@if ($historyStatuses->status_id)
					<div class="show-tracker__state-item mb-2">

						<div class="show-tracker__state-content">
							<h4 class="show-tracker__state-text">Статус: {{ $historyStatuses->status->title }}</h4>
							<span>23.04.2019</span>
						</div>
					</div>
				@endif
				Введите номер телефона чтобы узнать подробную информацию о трекере
			@endif
        @endif


		@elseif ($tracker->is_active == 0)
			<div>Данный трекер на стадии расмотрения</div>
		@endif

    </div>

@endsection
