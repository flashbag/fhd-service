@extends('layouts.app')

@section('content')
    <div class="simple-page reset-password-page section_padding">
        9559955
        <div class="container">
            <div class="card position-center">
                <div class="card-header">
                    <h5>{{ trans('main.login.login_form.forgotten') }}</h5>
                    54545454
                </div>
                <div class="card-body">

                    @if (session('success-message'))
                        <div class="alert alert-success">
                            {{ session('success-message') }}
                        </div>
                    @endif

                        <form method="POST" action="{{route('changePassword', ['token' => $token])}}">
                            {{ csrf_field() }}
                                <input type="password" name="password1" class="form-control input__custom" placeholder="{{ trans('main.custom.resPas') }}" required="" />
                                <input type="password" name="password2" class="form-control input__custom" placeholder="{{ trans('main.custom.resPasconf') }}" required="" />
                            <br>
                            <div class="create-acc">
                                <button type="submit" class="btn btn-success">
                                    {{ trans('main.custom.resPasres') }}
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection