@extends('layouts.app')

@section('slider')

	<section class="hero-carousel" style="height: 85%;">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active"
					 style="background: url('//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
					<div class="container">
						<div class="carousel-caption-new">

							<div class="pull-left" style="width: 65%;">
								<p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.login.p') }}</p>
								<h1>{{ trans('main.login.h1') }}</h1>
								<h5 class="subtitle">{{ trans('main.login.h5') }}</h5>
								<div class="btn-group-block">
									<a href="/about-us" class="btn btn-default-new">{{ trans('main.login.link2') }}</a>
								</div>
							</div>

						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('content')

	<div class="simple-page sing-page section_padding">
		<div class="container">
			<div class="card sing-page__wrapp position-center">
                @include('flash-messages')
				<div class="card-header">
					<h5 class="sing-page__title">{{ trans('main.login.login_form.forgotten') }}</h5>
				</div>
				<div class="card-body">

					<form class="form-horizontal" method="POST" action="{{ route('custom-email') }}">
						{{ csrf_field() }}

						<div>
							<p class="input-title">E-Mail</p>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} mx-0">
								<input id="email" type="email" class="input__custom input__custom_bg" name="email"
									   value="{{ old('email') }}" required>
							</div>
							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>

						<div class="d-flex justify-content-end mt-5">
							<button type="submit" class="btn-custom btn-custom_green-bg">
								{{ trans('main.login.login_form.forgotten') }}
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
