@extends('layouts.app')
{{--

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                            <div class="pull-left" style="width: 65%;">
                                <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.login.p') }}</p>
                                <h1>{{ trans('main.login.h1') }}</h1>
                                <h5 class="subtitle">{{ trans('main.login.h5') }}</h5>
                                <div class="btn-group-block">
                                    <a href="/about-us" class="btn btn-default-new">{{ trans('main.login.link2') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection
--}}

@section('content')

	<div class="simple-page sing-page login-page section_padding">
		<div class="container">
			<div class="card sing-page__wrapp position-center">
				<div class="card-header">
					<h5 class="sing-page__title">{{ trans('main.login.login_form.text_1') }}</h5>
				</div>
				<div class="card-body">
					<form class="form-auth" method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}

						<div class="mb-5">
							<p class="input-title">{{ trans('main.login.login_form.text_2') }}</p>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<input id="email" type="text" class="input__custom input__custom_bg" name="email" value="{{ old('email') }}" autofocus>
							</div>
							@if ($errors->has('email'))
								<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
							@endif
						</div>

						<div class="mb-5">
							<p class="input-title">{{ trans('main.login.login_form.text_3') }}</p>
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<input id="password" type="password" class="input__custom input__custom_bg" name="password">
							</div>
							@if ($errors->has('password'))
								<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
							@endif
						</div>

						<div class="form-group pt-2 mt-4">
							<div class="checkbox">
								<label class="fake-checkbox">
									<input type="checkbox"
										   name="remember" {{ old('remember') ? 'checked' : '' }}> {{ trans('main.login.login_form.text_4') }}
								</label>
							</div>
						</div>

						<div class="login-page__btn pt-4">

							<a class="link-reset" href="/password/reset" class="reset-link">
								{{ trans('main.login.login_form.forgotten') }}
							</a>

							<button class="btn-custom btn-custom_yellow-bg"
									type="submit">{{ trans('main.login.login_form.text_5') }}</button>
						</div>

						{{--<div class="form-group">
							<a href=" {{ url('auth/google') }}" class="btn btn-google"><i
										class="fa fa-google"></i> {{ trans('main.login.login_form.text_6') }}</a>
						</div>
						<div class="form-group">
							<a href=" {{ url('auth/facebook') }}" class="btn btn-facebook"><i
										class="fa fa-facebook"></i> {{ trans('main.login.login_form.text_7') }}</a>
						</div>
						<div class="form-group">
							<a href=" {{ url('auth/twitter') }}" class="btn btn-twitter"><i
										class="fa fa-twitter"></i> {{ trans('main.login.login_form.text_9') }}</a>
						</div>
						<div class="form-group">
							<a href=" {{ url('auth/instagram') }}" class="btn btn-instagram"><i
										class="fa fa-instagram"></i> {{ trans('main.login.login_form.text_8') }}</a>
						</div>--}}
					</form>
				</div>
				<div class="card-footer login-page__footer">
					<p class="input-title text-center">{{ trans('main.login.login_form.text_10') }} <a href="{{ route('register') }}" class="link-reset">{{ trans('main.login.login_form.text_11') }}</a></p>
				</div>
			</div>
		</div>
	</div>
@endsection

