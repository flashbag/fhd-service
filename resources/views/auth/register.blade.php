@extends('layouts.app')

@section('content')
	<div class="simple-page sing-page register-page section_margin">
		<div class="container">
			<div class="card sing-page__wrapp position-center">
				<div class="card-header">
					<h5 class="sing-page__title">{{ trans('main.login.reg_form.text_1') }}</h5>
				</div>
				<div class="card-body">
					<form id="" class="form-auth" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}

						<div class="mb-5">
							<label for="name" class="input-title">{{ trans('main.login.reg_form.text_2') }}</label>
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<input id="name" type="text" class="input__custom input__custom_bg" name="name" value="{{ old('name') }}" >
							</div>
							@if ($errors->has('name'))
								<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
							@endif
						</div>

						<div class="mb-5">
							<label for="email" class="input-title">{{ trans('main.login.reg_form.text_3') }}</label>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<input id="email" type="text" class="input__custom input__custom_bg" name="email" value="{{ old('email') }}" >
                            </div>
							@if ($errors->has('email'))
								<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
							@endif
						</div>

						<div class="mb-5">
							<label for="number" class="input-title">{{ trans('main.login.reg_form.text_4') }}</label>
							<div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
								<input id="numberSender" type="text" class="input__custom input__custom_bg" name="number" value="{{ old('number') }}" >
							</div>
							@if ($errors->has('number'))
								<span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
							@endif
						</div>

						<div class="mb-5">
							<label for="password" class="input-title">{{ trans('main.login.reg_form.text_5') }}</label>
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<input id="password" type="password" class="input__custom input__custom_bg" name="password" >
							</div>
							@if ($errors->has('password'))
								<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
							@endif
						</div>

						<div class="mb-5">
							<label for="password-confirm" class="input-title">{{ trans('main.login.reg_form.text_6') }}</label>
							<div class="form-group">
								<input id="password-confirm" type="password" class="input__custom input__custom_bg" name="password_confirmation">
							</div>
						</div>

						<div class="d-flex justify-content-center mt-4 mb-5">
							<button class="btn-custom btn-custom_width btn-custom_yellow-bg" type="submit">
								{{ trans('main.login.reg_form.text_7') }}
							</button>
						</div>

					</form>
				</div>
				<div class="card-footer">
					<p class="input-title text-center">{{ trans('main.login.p') }}&nbsp;
						<a href="{{ route('login') }}" class="link-reset">{{ trans('main.main_page.sign_in') }}</a>
					</p>
				</div>
			</div>
		</div>
	</div>
@endsection

{{--


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
