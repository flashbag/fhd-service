<br>


@if (count($trackers))
	<table class="table table-bordered table-tracker">
		<thead>
		<tr>
			<th>{{ trans('main.custom_department.number_tracker') }}</th>

			<th>{{ trans('main.custom_department.status') }}</th>

			<th>{{ trans('main.custom.send_country') }}</th>
			<th>{{ trans('main.custom.send_city') }}</th>
			<th>{{ trans('main.custom.send_name') }}</th>


            <th>{{ trans('main.custom.rec_country') }}</th>
            <th>{{ trans('main.custom.rec_city') }}</th>
            <th>{{ trans('main.custom.rec_name') }}</th>

{{--            <th>{{ trans('main.custom.view') }}</th>--}}
		</tr>
		</thead>
		<tbody>

		@foreach($trackers as $tracker)
			<tr class="normal-tracker">

				<td>{{ $tracker->id_tracker }}</td>
				<td>{{ $tracker->status ? $tracker->status->title  : '-' }}</td>

				<td>{{ $tracker->sender && $tracker->sender->country ? $tracker->sender->country : '-' }}</td>
				<td>{{ $tracker->sender && $tracker->sender->city ? $tracker->sender->city : '-' }}</td>
				<td>{{ $tracker->sender && $tracker->sender->full_name ? $tracker->sender->full_name : '-' }}</td>

				<td>{{ $tracker->recipient && $tracker->recipient->country ? $tracker->recipient->country : '-' }}</td>
				<td>{{ $tracker->recipient && $tracker->recipient->city ? $tracker->recipient->city : '-' }}</td>
				<td>{{ $tracker->recipient && $tracker->recipient->full_name ? $tracker->recipient->full_name : '-' }}</td>
{{--                <td><a href="{{ route('edit-trackers', $tracker->id_tracker) }}">{{ trans('main.custom.view') }}</a></td>--}}
			</tr>
		@endforeach


		</tbody>
	</table>

@else

	<br>
	<br>
	<h3>Нету результатов. Попробуйте поменять фильтры.</h3>
	<br>
	<br>

@endif

{!! $trackers->appends(\Request::except('page'))->render() !!}

