<div class="row file-block">
	<div class="form-group col-sm-8">
		<label class="fake-input-file form-control">
			<span class="placeholder" data-default-value="{{ __('trackers.parts.documents.file_not_selected') }}">
				{{ __('trackers.parts.documents.file_not_selected') }}
			</span>
			<input type="hidden" name="{{ $input_name }}[{{ $new_block_index }}][beacon]" value="beacon">
			<input type="file" name="{{ $input_name }}[{{ $new_block_index }}][file]">
			<span class="select-file">
				{{ __('trackers.parts.documents.select_file') }}
			</span>
		</label>
	</div>
	<div class="form-group col-sm-1">
		<a class="remove-file-input remove-one-more" data-input-name="{{ $input_name }}" data-container-selector="{{ $container_selector }}">
		</a>
	</div>
</div>
