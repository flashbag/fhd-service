<div style="width: 500px;">
    <div style="background: #016d84; padding: 25px; text-align: center;">
        <img src="http://fhd.com.ua/img/logo.png" style="max-width: 120px;" alt="FHD"/>
    </div>
    <div style="background: #fff; padding: 20px; text-align: center; font-size: 16px;">       
        <p style="color: #555555;">{{ $message_text }}</p>
        @isset($message_link)
            <a href="{{ $message_link }}" style="color: #555555; color: #555555;"> {{ $message_link_text }}</a>  
        @endisset($message_link)     
    </div>    
</div>