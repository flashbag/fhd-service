<div style="width: 500px;">
    <div style="background: #016d84; padding: 25px; text-align: center;">
        <img src="http://fhd.com.ua/img/logo.png" style="max-width: 120px;" alt="FHD"/>
    </div>
    <div style="background: #fff; padding: 20px; text-align: center; font-size: 16px;">
        <h3 style="color: #016d84; font-size: 24px;"> {{ $header }}</h3>
        <p style="color: #555555;">{{ $number_trecker }}</p>
        <p style="color: #555555;">{{ $description }}</p>               
    </div>   
    <div style="background: #f5f5f5; padding: 20px; text-align: left; font-size: 16px; color: #555555;">
        <div style="margin-bottom: 2rem; color: #555555;">
            <p>{{ $manadger_name }}</p>
            <p>{{ $data_update_tracker }}</p>            
            <a href="{{ $link_tracker }}" target="_blank">{{ $link_text }}</a>
        </div>                   
    </div>    
</div>