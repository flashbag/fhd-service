<div style="width: 500px;">
    <div style="background: #016d84; padding: 25px; text-align: center;">
        <img src="//fhd.com.ua/img/logo.png" style="max-width: 120px;" alt="FHD"/>
    </div>
    <div style="background: #fff; padding: 20px; text-align: center; font-size: 16px;">
        <h3 style="color: #016d84; font-size: 24px;"> {{ $header }} <br> - // {{$full_name}} // -!</h3>
        <p style="color: #555555;">{{ $thanks_text }}</p>
        <p style="color: #555555;">{{ $description }}</p>               
    </div>   
    <div style="background: #f5f5f5; padding: 20px; text-align: left; font-size: 16px; color: #555555;">
        <div style="margin-bottom: 2rem; color: #555555;">
            <p>{{ $description_action }}</p>
            <p style="margin-bottom: 1rem; color: #555555;">{{ $number_tracker }}</p>            
            @include('beautymail::templates.minty.button', ['text' => $text_button_tracker, 'link' => $tracker_link, 'color' => '#016d84'])
        </div>
        <div style="margin-bottom: 2rem; color: #555555;">
            <p>{{ $description_login }}</p>
            <p>{{ $user_login }}</p>
            <p>{{ $user_pass }}</p>
        </div>
        <div>
            <p style="margin-bottom: 1rem; color: #555555;" >{{ $description_dashbord }}</p>            
            @include('beautymail::templates.minty.button', ['text' => $text_button_dashbord, 'link' => $dashbord_link, 'color' => '#016d84'])
        </div>            
    </div>    
</div>