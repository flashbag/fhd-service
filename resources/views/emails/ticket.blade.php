<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Support Ticket</title>
</head>
<body>

<p>Tracker: {{ $id_tracker }}</p>

<p>
    You can view the ticket at any time at {{ url('tickets/'. $id_tracker) }}
</p>

</body>
</html>