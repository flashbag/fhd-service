<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<style>

    html, body{
        width: 100%;
        height: 100%;
    }

    .navbar-brand{
        margin-top: 8px;
    }

    .btn-info-new{
        background: #016d84;
        border-radius: 2px;
        text-transform: uppercase;
        color: #fff;
        font-weight: bold;
        font-size: 12px;
        padding: 6px 18px;
    }

    .btn-info-new:hover{
        background: #08487e;
        color: #fff;
    }

    .btn-default-new{
        background: #686d7a;
        border-radius: 2px;
        text-transform: uppercase;
        color: #fff;
        font-weight: bold;
        padding: 6px 18px;
        font-size: 12px;
    }

    .btn-default-new:hover{
        background: #535761;
        color: #fff;
    }

    .btn-inline-new{
        background: none;
        border-radius: 2px;
        text-transform: uppercase;
        color: #fff;
        font-weight: bold;
        padding: 10px 22px;
        font-size: 12px;
        border: 2px solid #fff;
    }

    .btn-inline-new:hover{
        background: #fff;
        color: rgb(11, 96, 169);
    }

    .btn-group-block .btn{
        margin-top: 20px;
        margin-right: 25px;
    }

    .wrap{
        padding: 80px 0;
    }

    .page-header-block{
        text-align: right;
    }

    .page-header-block p{
        text-transform: uppercase;
        font-size: 14px;
        font-weight: 500;
    }

    .page-header-block h3{
        text-transform: uppercase;
        margin-top: 0;
        line-height: 32px;
    }

    .default-form label{
        font-weight: 400;
    }

    .default-form input, .default-form textarea{
        border-radius: 2px;
        box-shadow: none;
        border: 1px solid rgba(0, 0, 0, .1);
        color: #333;
    }

    .default-form textarea{
        max-width: 100%;
    }

    .header-top-navbar{
        background: rgba(0, 0, 0, 0.66);
        position: absolute;
        top:0;
        width: 100%;
        z-index: 9999;
    }

    .header-top-navbar ul{
        padding-left: 0;
    }

    .header-top-navbar li{
        list-style-type: none;
        float: left;
        padding: 5px;
        margin-right: 25px;
    }

    .header-top-navbar li a{
        color: #fff;
        font-size: 13px;
    }

    .header-top-navbar .pull-right ul li:last-of-type{
        margin-right: 0;
    }

    #accordion .panel-heading{
        border: none !important;
    }


    .section-header{
        position: absolute;
        left: 0;
        top: 40px;
        width: 100%;
        z-index: 1000;
    }

    .section-header.affix{
        position: fixed;
        top:0;
        background: #016d84;
        transition: .12s;
    }

    .section-header .navbar{
        border-radius: 0;
        background: none;
        border: none;
    }

    .section-header .navbar-nav{
        padding-top: 20px;
    }

    .section-header .navbar li:hover a{
        color: #fff;
    }

    .section-header .navbar li.active a{
        color: #fff;
        background: none;
    }

    .section-header .navbar li.active:hover a{
        background: none;
        color: #fff;
    }

    .section-header .navbar li a{
        text-transform: uppercase;
        color: rgba(255, 255, 255, 0.5);
        font-weight: 500;
    }

    .section-header .navbar-brand img{
        width: 100px;
    }

    .hero-carousel {
        min-height: 550px;
    }

    .hero-carousel .container{
        position: relative;
        height: 100%;
    }

    .hero-carousel, .carousel, .carousel-inner, .carousel-inner .item{
        height: 100%;
    }

    .hero-carousel .carousel-control{
        width: 7%;
    }

    .hero-carousel .carousel-caption-new{
        position: absolute;
        top: 50%;
        width: 100%;
        transform: translateY(-50%);
        bottom: initial !important;
        color: #fff;
    }

    .hero-carousel .carousel-caption-new h1{
        font-size: 50px;
        line-height: 70px;
        margin-top: 0;
        letter-spacing: 1px;
        text-transform: uppercase;
    }

    .hero-carousel .carousel-caption-new h1:after{
        width: 85px;
        margin-top: 10px;
        margin-bottom: 10px;
        content: " ";
        display: block;
        border-top: 3px solid #016d84;
    }

    .hero-carousel .carousel-caption-new .pull-left{
        text-align: left;
        width: 40%;
    }

    .hero-carousel .carousel-caption-new .pull-left .subtitle{
        font-size: 19px;
        padding: 3px 0;
        line-height: 35px
    }

    .hero-carousel .carousel-caption-new .pull-right{
        position: absolute;
        width: 40%;
        height: 100%;
        right: 30px;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker{
        background-color: rgba(0, 0, 0, .4);
        border-radius: 2px;
        padding: 5px 15px 15px 15px;
        position: absolute;
        right: 0;
        width: 100%;
        top:50%;
        transform: translateY(-50%);
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker h4{
        margin-bottom: 5px;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker span{
        font-size: 12px;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker .form-group:first-of-type{
        margin-top: 15px;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker .form-group label{
        font-weight: 300;
        text-transform: uppercase;
        margin-top: 15px;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker .form-group input{
        border-radius: 2px;
        border: 1px solid rgba(255, 255, 255, .1);
        background: rgba(255, 255, 255, 0.2);
        padding: 10px 8px 10px 50px;
        color: #fff;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker .form-group .intl-tel-input .selected-flag .iti-arrow{
        border-top-color: #fff !important;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker .form-group .country-name{
        color: #000;
    }

    .hero-carousel .carousel-caption-new .pull-right .search-slider-tracker .form-group input:focus{
        -webkit-box-shadow: inset 5px 0px 0px 0px rgba(19, 96, 169, 1);
        -moz-box-shadow:    inset 5px 0px 0px 0px rgba(19, 96, 169, 1);
        box-shadow:         inset 5px 0px 0px 0px rgba(19, 96, 169, 1);
    }

    .hero-carousel .carousel-indicators{
        bottom: 0 !important;
    }

    .hero-carousel .carousel-indicators li{
        width: 90px;
        height: 1px;
        border: none;
        background: rgba(255, 255, 255, 0.5);
        margin: 0;
    }

    .hero-carousel .carousel-indicators li.active{
        background: #016d84;
        height: 3px;
    }




    .after-hero-block{
        background: #016d84;
        padding: 20px 0;
        color: #fff;
    }

    .after-hero-block .left-border{
        border-left: 1px solid rgba(255, 255, 255, .1);
    }

    .after-hero-block .icon{
        width: 64px;
        font-size: 30px;
        padding-left: 15px;
    }

    .after-hero-block .content p{
        font-size: 12px;
        margin-bottom: 5px;
    }




    .our-services-block{
        padding: 65px 0;
    }

    .our-services-block{
        position: relative;
    }

    .our-services-block .bg-car{
        position: absolute;
        max-width: 300px;
        bottom: -50px;
    }

    .our-services-block .desc-block{
        text-align: right;
    }

    .our-services-block .desc-block h4{
        margin-top: -2px;
        font-size: 15px;
    }

    .our-services-block .desc-block h3{
        margin: 15px 0 30px 0;
    }

    .our-services-block .service-block{
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .25), 0 -1px 1px rgba(0, 0, 0, .125);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .25), 0 -1px 1px rgba(0, 0, 0, .125);
        padding: 35px 15px 35px 30px;
        margin-bottom: 25px;
    }

    .our-services-block .service-block:last-child{
        margin-bottom: 0;
    }

    .our-services-block .service-block .icon{
        width: 55px;
        font-size: 30px;
        color: #016d84;
    }

    .our-services-block .service-block .content{
        width: calc(100% - 55px);
    }

    .our-services-block .service-block .content h4{
        font-size: 16px;
        margin-bottom: 15px;
    }

    .our-services-block .service-block .content p{
        font-size: 13px;
        line-height: 25px;
        margin-bottom: 0;
    }



    .our-customers-block{
        background-image: url({{ asset('/img/parallax.jpg') }});
        background-position: 25% 50%;
        padding: 120px 0;
    }

    .our-customers-block .desc-block{
        color: #fff;
        text-align: right;
    }

    .our-customers-block .desc-block h4{
        font-size: 15px;
        margin-top: 2px;
    }

    .our-customers-block .desc-block h3{
        margin: 15px 0 30px 0;
    }

    .our-customers-block .testimonial-block{
        background-color: rgba(255, 255, 255, .07);
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .25), 0 -1px 1px rgba(0, 0, 0, .125);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .25), 0 -1px 1px rgba(0, 0, 0, .125);
        padding: 35px 30px 35px 30px;
        color: #fff;
    }

    .our-customers-block .testimonial-block .content p{
        font-size: 14px;
        line-height: 25px;
    }

    .our-customers-block .testimonial-block .icon{
        width: 55px;
        font-size: 30px;
        color: #fff;
    }

    .our-customers-block .testimonial-block .content{
        width: calc(100% - 55px);
    }

    .our-customers-block .author-testimonial{
        margin-top: 35px;
        text-align: right;
        display: none;
    }

    .our-customers-block .author-testimonial p{
        margin-top: 0;
        margin-bottom: 0;
    }

    .our-customers-block .author-testimonial p:last-child{
        font-size: 11px;
    }

    @media (max-width:767px) {

        .our-customers-block .customers-logotypes .col-sm-2:first-child{
            margin-top: 75px;
        }

        .our-customers-block .customers-logotypes .left-border{
            border-left: 1px solid rgba(255, 255, 255, .1);
            margin-top: 100px;
            margin-bottom: 80px;
        }

    }

    .our-customers-block .customers-logotypes .col-sm-2:last-child{
        margin-bottom: 25px;
    }

    .our-customers-block .customers-logotypes img{
        width: 100%;
    }




    .stats-block{
        position: relative;
        padding-top: 75px;
    }

    .stats-block .container{
        position: relative;
    }

    .stats-block .stats-bg{
        max-width: 300px;
        top: -130px;
        right: 0;
        position: absolute;
    }

    .stats-block .desc-block{
        text-align: right;
    }

    .stats-block .desc-block h4{
        margin-top: 3px;
    }

    .stats-block .desc-block h3{
        margin: 15px 0 30px 0;
    }

    .stats-block .stats-desc{
        font-size: 14px;
        line-height: 25px;
        margin-top: 0;
    }

    .stats-block .stats-counters{
        padding: 75px 0 115px 0;
        text-align: center;
    }

    .stats-block .stats-counters h3{
        color: #016d84;
        font-size: 50px;
        font-weight: 400;
    }

    .stats-block .stats-counters h4{
        color: #000;
        font-size: 16px;
    }



    .we-top-block{
        background-image: url(//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/10/shutterstock_147692165-tinted.jpg);
        background-position: 50% 50%;
        padding: 120px 0;
    }

    .we-top-block .desc-block{
        color: #fff;
        text-align: right;
    }

    .we-top-block .desc-block h4{
        font-size: 15px;
        margin-top: 2px;
    }

    .we-top-block .desc-block h3{
        margin: 15px 0 30px 0;
    }

    .we-top-block .desc-block p{
        line-height: 30px;
    }

    .we-top-block .progress-block{
        color: #fff;
    }

    .we-top-block .progress-block label{
        text-transform: uppercase;
        font-weight: 500;
    }

    .we-top-block .progress-block .progress{
        border-radius: 0;
        height: 24px;
        background-color: rgba(255, 255, 255, .07);
    }

    .we-top-block .progress-block .progress:last-child{
        margin-bottom: 0;
    }

    .we-top-block .progress-block .progress .progress-bar{
        box-shadow: none;
        background: #016d84;
        text-align: right;
        padding-right: 10px;
        padding-top: 2px;
    }



    .create-tracker-block{
        background: #016074;
        padding: 40px 0;
        color: #fff;
    }

    .create-tracker-block h3, .create-tracker-block h5{
        margin-bottom: 0;
        margin-top: 0;
    }

    .create-tracker-block h3{
        font-size: 20px;
        margin-top: 5px;
    }

    .create-tracker-block h5{
        font-size: 13px;
    }


    .before-footer-block{
        background: #016d84;
        padding: 40px 0;
        color: #fff;
    }

    .before-footer-block .footer-block h4{
        text-transform: uppercase;
        color: #02a7ca;
    }

    .before-footer-block .footer-block h4:after{
        display: block;
        content: " ";
        border-top: #fff 3px solid;
        width: 50px;
        margin-top: 10px
    }

    .before-footer-block .footer-block .media .media-left .media-object{
        width: 64px;
    }

    .before-footer-block .footer-block .media .media-body h3{
        color: #fff;
        font-size: 13px;
        margin-top: 0;
        line-height: 18px;
    }

    .before-footer-block .footer-block .media .media-body p{
        color: #fff;
        font-size: 12px;
    }

    .before-footer-block .footer-block .footer-nav{
        padding-left: 0;
    }

    .before-footer-block .footer-block .footer-nav li{
        list-style-type: none;
        width: 50%;
        float: left;
        line-height: 30px;
    }

    .before-footer-block .footer-block .footer-nav li a{
        color: #fff;
        font-size: 13px;
    }

    footer{
        border-top: 1px solid rgba(128, 128, 128, .1);
        background: #016d84;
        color: #fff;
        padding: 15px;
        font-size: 12px;
    }

    .circle-step{
        width: 65px;
        height: 65px;
        border-radius: 50%;
        position: relative;
        margin-top: -10px;
    }

    .step-counter{
        position: absolute;
        top:50%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 25px;
        font-weight: 500;
        color: #fff;
    }

    .circle-step.step-success{
        background: #4CAF50;
    }

    .circle-step.step-default{
        background: #607d8b;
    }

    .circle-step.step-info{
        background: #016d84;
    }

    .stepper{
        margin-top: 35px;
    }

    .circle-status-step{
        width: 50px;
        height: 50px;
        background: #fff;
        border: 2px solid #efefef;
        border-radius: 50%;
        position: relative;
        margin-top: 5px;
    }

    .status-icon{
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        font-size: 25px;
    }

    .status-icon .fa-check{
        color: #4CAF50;
    }

    .form-group.required label:after {
        content:"*";
        color:red;
        font-weight: bold;
    }

    .set-locale{
        margin-top: 8px;
    }

    .set-locale .btn{
        background: none;
        border: none;
        text-transform: uppercase;
        color: rgba(255, 255, 255, 0.5);
        font-weight: 500;
    }

    .set-locale .dropdown-menu{
        background: #016d84 !important;
        border-color: #016d84;
        color: #fff;
    }

    .set-locale-links li a:hover{
        color: #000 !important;
    }








    .before-footer-block .col-sm-3, .testimonial-block{
        margin-bottom: 25px;
    }

    @media (max-width: 767px) {

        .navbar-brand{
            margin-top: 0 !important;
            padding-top: 20px !important;
            padding-left: 20px !important;
        }

        .navbar-toggle{
            margin-top: 25px;
        }

        div#bs-example-navbar-collapse-1.navbar-collapse{
            margin-top: 30px !important;
        }

        .hero-carousel .carousel-caption-new .pull-left{
            width: 80% !important;
            margin-left: 10% !important;
        }
        .hero-carousel .carousel-caption-new h1{
            font-size: 28px;
            line-height: 34px;
        }
        .hero-carousel .carousel-caption-new .pull-left .subtitle{
            font-size: 14px;
            line-height: 20px;
        }
        .hero-carousel .carousel-caption-new .pull-left p{
            font-size: 15px !important;
        }
        .hero-carousel .carousel-indicators li{
            width: 45px;
        }
        .we-top-block .desc-block, .stats-block .desc-block, .our-customers-block .desc-block, .our-services-block .desc-block, .page-header-block{
            text-align: left;
        }
        .create-tracker-block .btn-inline-new{
            width: 100%;
            margin-top: 20px;
        }
        div#bs-example-navbar-collapse-1.navbar-collapse{
            background: #016d84;
            margin-top: 50px;
            border-top: none;
        }
        .our-services-block .col-sm-4{
            margin-bottom: 25px;
        }
        .customers-logotypes img{
            width: 70% !important;
            margin-left: 15%;
        }
        .set-locale .btn, .set-locale .dropdown-menu{
            background: none !important;
            border: none !important;
            text-transform: uppercase;
        }
        .set-locale-links a{
            color: #fff !important;
        }
    }

    @media (max-width: 991px) and (min-width: 768px) {
        .hero-carousel .carousel-caption-new .pull-left{
            width: 80% !important;
            margin-left: 10% !important;
        }
        .our-services-block .col-sm-8{
            margin-bottom: 25px;
        }
    }

    .profile-icon:hover .fa{
        color: rgba(255, 255, 255, 0.5) !important;
    }

    .mini-profile-link{
        margin-top: 8px;
        margin-right: 10px;
    }

    .mini-profile-link .dropdown .btn-default{
        background: #016d84 !important;
        border: none;
    }

    .mini-profile-link .dropdown-menu{
        background: #016d84 !important;
    }

    .mini-profile-link .dropdown-menu li{
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .mini-profile-links li{
        padding: 0 !important;
    }

    .mini-profile-links li a{
        padding: 10px 15px !important;
        display: block;
    }

    .mini-profile-links li:hover{
        background: #016d84 !important;
    }

    .mini-profile-links h4{
        color: #016d84 !important;
    }

    @media (max-width: 767px) and (max-height: 600px) {
        #bs-example-navbar-collapse-1  {
            overflow-y: scroll !important;
            max-height: 250px !important;
        }
    }

    .btn-secondary-with-border{
        border-color: #67696d;
        padding: 0 5px;
    }


</style>

<style>

    .header {
        background: #016d84;
        min-height: 90px;
    }

    .navbar-brand {
        float: none;
        height: 50px;
        padding: 15px 15px;
        font-size: 18px;
        line-height: 20px;
        margin: 0 auto;
    }

    .logo {
        float: none;
        margin: 0 auto;
    }

</style>
<body>

<div class="container">

    <div class="header clearfix">
        <a class="navbar-brand" href="{{ route('index') }}">
            <img src="{{ asset('img/logo.png') }}" class="logo" width="100">
        </a>
    </div>

    <br><br>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="text-center" style="background: #fff; padding: 20px; font-size: 16px;">
                {{ trans('auth.hello') }}, {{$user->name}}
            </h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <p class="text-center" style="background: #f5f5f5; padding: 45px; font-size: 16px; color: #555555">
                {{ trans('auth.thanks') }}
                {{ trans('auth.ForVerifyGoTo') }} <a href="{{url('/verification', $user->email_verify_hash)}}">{{ trans('auth.link') }}</a>
                {{ trans('auth.message1') }}
            </p>
        </div>
    </div>


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        </div>
    </div>

    <footer class="footer text-center">
        <p>&copy; <?= date('Y') ?> FHD</p>
    </footer>

</div> <!-- /container -->
</body>
</html>
