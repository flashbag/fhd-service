<div style="width: 500px;">
    <div style="background: #016d84; padding: 25px; text-align: center;">
        <img src="//fhd.com.ua/img/logo.png" style="max-width: 120px;" alt="FHD"/>
    </div>
    <div style="background: #fff; padding: 45px; text-align: center; font-size: 16px;">
        {{ $info }}
    </div>
    @if(!empty($dopInfo))
    <div style="background: #f5f5f5; padding: 45px; text-align: center; font-size: 16px;">
        {{ $dopInfo }}
    </div>
    @endif
</div>