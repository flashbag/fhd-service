<div style="width: 500px;">
    <div style="background: #016d84; padding: 25px; text-align: center;">
        <img src="http://fhd.com.ua/img/logo.png" style="max-width: 120px;" alt="FHD"/>
    </div>
    <div style="background: #fff; padding: 20px; text-align: center; font-size: 16px;">
        <h3 style="color: #016d84; font-size: 24px;"> {{ $header }} <br> - // {{$full_name}} // -!</h3>
        <p style="color: #555555;">{{ $thanks_text }}</p>       
    </div>
    @isset($description)
    <div style="background: #f5f5f5; padding: 45px; text-align: center; font-size: 16px; color: #555555">
        {{ $description }}
    </div>
    @endisset
</div>