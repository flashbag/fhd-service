<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Support Ticket</title>
</head>
<body>
<p>
    {{ $ticket->text }}
</p>

---
<p>Replied by: {{ $user->name }}</p>

<p>Tracker: {{ $ticket->tracker_id }}</p>
<p>Title: {{ $ticket->ticket_id }}</p>

<p>
    You can view the ticket at any time at {{ url('tickets/'. $ticket->id) }}
</p>

</body>
</html>