<h2>
	<span class="step-icon">
		2
	</span>
	<span class="step-text">
		{{ __('trackers.parts.receptacles.title_main') }}
	</span>
</h2>

@section('receptacle-weight-help-block')
	<span class="help-block help-block-static">
		{{ __('trackers.parts.receptacles.weight_help_block')  }}
	</span>
@endsection

@section('receptacle-cargos-block')
	<span class="help-block help-block-static">
		{{ __('trackers.parts.receptacles.cargos_help_block_box')  }}
	</span>
@endsection

<section class="shipping-information step" data-id=2>

	<div class="panel-group">
		<div class="panel panel-default">

			<div class="panel-heading clearfix" role="tab">
				<h5 class="pull-left panel-title pull-left">
					{{ __('trackers.parts.receptacles.title_main') }}
				</h5>
				<div class="pull-right departures" style="display: none">
					<div class="departure-item opened">
						<div class="text">{{ __('trackers.parts.receptacles.departure') }}</div>
						<div class="number">#1</div>
						<a href="#" class="close-trigger"></a>
					</div>
					<div class="departure-item">
						<div class="text">{{ __('trackers.parts.receptacles.departure') }}</div>
						<div class="number">#2</div>
						<a href="#" class="close-trigger"></a>
					</div>
				</div>

			</div>
			<div class="panel-collapse collapse in" role="tabpanel">
				<div class="panel-body">
					<div class="receptacle-container">

						@if (isset($tracker) && $tracker->receptacles->count())
							@foreach($tracker->receptacles as $index => $receptacle)
								@include('tracker-parts.receptacles.receptacle-existing')
							@endforeach
						@else
							@include('tracker-parts.receptacles.receptacle-new')
						@endif
					</div>

					<br>

					<div class="row">
						<div class="col-lg-12 text-center">
							<a class="btn add-one-more-receptacle"
							   data-get-template-url="{{ route('template-get-receptacle') }}">
								{{ __('trackers.parts.receptacles.add_position') }}
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
