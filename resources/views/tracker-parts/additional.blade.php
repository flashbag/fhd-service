<h2>
	<span class="step-icon">
		5
	</span>
	<span class="step-text">
		{{ trans('trackers.parts.additional.title') }}
	</span>
</h2>

<section class="step additional" data-id=5>

	<div class="step-inner panel panel-default">
		<div class="panel-heading">
			<h5>{{ __('trackers.parts.additional.title') }}</h5>
		</div>
		<div class="panel-body">

			<div class="row">
				<div class="form-group col-sm-6 {{ $errors->has('bill_duty_id') ? ' has-error' : '' }}">
					<label for="customTax">
						{{ __('trackers.parts.bill_duties.title' ) }}
					</label>

					@if(isset($tracker) && isset($tracker->bill_duty_id))
						<input type="hidden" name="bill_duty_type" value="{{ $tracker->billDuty->type }}">
					@else
						<input type="hidden" name="bill_duty_type" value="">
					@endif

					<select class="form-control selectpicker billDuties" id="customTax" name="bill_duty_id">

						{{--@if(!isset($tracker))--}}
							{{--<option value=null selected>--</option>--}}
						{{--@endif--}}

						@foreach($bill_duties as $bill_duty)

							@if(isset($tracker))
								<option data-type="{{$bill_duty->type}}" value="{{ $bill_duty->id }}"
										{{ old('bill_duty_id') != $bill_duty->id ?: 'selected' }} @if($tracker->bill_duty_id == $bill_duty->id)selected @endif
                                        @if($bill_duty->disabled) disabled @endif>

							@else
								<option data-type="{{$bill_duty->type}}"
										value="{{ $bill_duty->id }}" {{ old('bill_duty_id') != $bill_duty->id ?: 'selected' }}
                                @if($bill_duty->disabled) disabled @endif>
									@endif

									{{ __('trackers.parts.bill_duties.type_' .  $bill_duty->type ) }}

								</option>
						@endforeach

					</select>

					@if ($errors->has('bill_duty_id'))
						<span class="help-block">
							<strong>{{ $errors->first('bill_duty_id') }}</strong>
						</span>
					@endif

				</div>

				{{--<div class="form-group col-sm-6 billDutyFhdAccount {{ $errors->has('bill_duty_fhd_account') ? ' has-error' : '' }}">--}}
					{{--<label for="fhdAccount" >--}}
						{{--{{ __('trackers.parts.bill_duties.type_fhd_account' ) }}--}}
					{{--</label>--}}

					{{--@if(isset($tracker))--}}
						{{--<input type="text" class="form-control only-numeric" name="bill_duty_fhd_account"--}}
							   {{--value="{{ old('bill_duty_fhd_account', $tracker->billDutyFHDAccount) }}">--}}
					{{--@else--}}
						{{--<input type="text" class="form-control only-numeric" name="bill_duty_fhd_account"--}}
							   {{--value="{{ old('bill_duty_fhd_account') }}">--}}
					{{--@endif--}}

					{{--@if ($errors->has('bill_duty_fhd_account'))--}}
						{{--<span class="help-block">--}}
							{{--<strong>{{ $errors->first('bill_duty_fhd_account') }}</strong>--}}
						{{--</span>--}}
					{{--@endif--}}
				{{--</div>--}}
			</div>

			<div class="row">
				<div class="form-group col-sm-6 {{ $errors->has('bill_transportation_id') ? ' has-error' : '' }}">
					<label for="transportTax">
						{{ __('trackers.parts.bill_transportations.title' ) }}
					</label>

					@if(isset($tracker) && isset($tracker->bill_transportation_id))
						<input type="hidden" name="bill_transportation_type"
							   value="{{ $tracker->billTransportation->type }}">
					@else
						<input type="hidden" name="bill_transportation_type" value="">
					@endif

					<select class="form-control selectpicker transportTax" id="transportTax"
							name="bill_transportation_id">

						{{--@if(!isset($tracker))--}}
							{{--<option value=null selected>--</option>--}}
						{{--@endif--}}

						@foreach($bill_transportations as $bill_transportation)

							@if(isset($tracker))
								<option data-type="{{$bill_transportation->type}}"
										value="{{ $bill_transportation->id }}"
										{{ old('bill_transportation_id') != $bill_transportation->id ?: 'selected' }} @if($tracker->bill_transportation_id == $bill_transportation->id)selected @endif
                                        @if($bill_transportation->disabled) disabled @endif>

							@else
								<option data-type="{{$bill_transportation->type}}"
										value="{{ $bill_transportation->id }}" {{ old('bill_transportation') != $bill_transportation->id ?: 'selected' }}
                                        @if($bill_transportation->disabled) disabled @endif>
							@endif

							{{ __('trackers.parts.bill_transportations.type_' .  $bill_transportation->type ) }}
						@endforeach

					</select>

					@if ($errors->has('bill_transportation_id'))
						<span class="help-block">
							<strong>{{ $errors->first('bill_transportation_id') }}</strong>
						</span>
					@endif

				</div>
				{{--<div class="form-group col-sm-6 billDutyThirdParty {{ $errors->has('bill_duty_third_party') ? ' has-error' : '' }}">--}}
					{{--<label for="thridParty">--}}
						{{--{{ __('trackers.parts.bill_duties.type_third_party' ) }}--}}
					{{--</label>--}}

					{{--@if(isset($tracker))--}}
						{{--<input type="text" class="form-control" name="bill_duty_third_party"--}}
							   {{--value="{{ old('bill_duty_third_party', $tracker->billDutyThirdParty)  }}">--}}
					{{--@else--}}
						{{--<input type="text" class="form-control" name="bill_duty_third_party"--}}
							   {{--value="{{ old('bill_duty_third_party') }}">--}}
					{{--@endif--}}


					{{--@if ($errors->has('bill_duty_third_party'))--}}
						{{--<span class="help-block">--}}
							{{--<strong>{{ $errors->first('bill_duty_third_party') }}</strong>--}}
						{{--</span>--}}
					{{--@endif--}}
				{{--</div>--}}
			</div>


			<div class="row">
				<div class="form-group col-sm-6 {{ $errors->has('freight_service_id') ? ' has-error' : '' }}">
					<label for="expressFreight">
						{{ __('trackers.parts.freight_services.title' ) }}
					</label>

					@if(isset($tracker) && isset($tracker->freight_service_id))
						<input type="hidden" name="freight_service_type" value="{{ $tracker->freightService->type }}">
					@else
						<input type="hidden" name="freight_service_type" value="">
					@endif


					<select class="form-control selectpicker" id="expressFreight" name="freight_service_id">

						@foreach($freight_services as $freight_service)

							@if(isset($tracker))

								<option data-type="{{$freight_service->type}}" value="{{ $freight_service->id }}"
										{{ old('freight_service_id') != $freight_service->id ?: 'selected' }} @if($tracker->freight_service_id == $freight_service->id)selected @endif
                                        @if($freight_service->disabled) disabled @endif>

							@else
								<option data-type="{{$freight_service->type}}"
										value="{{ $freight_service->id }}" {{ old('freight_service_id') != $freight_service->id ?: 'selected' }}
                                        @if($freight_service->disabled) disabled @endif>
							@endif

							{{ __('trackers.parts.freight_services.type_' .  $freight_service->type ) }}
						@endforeach

					</select>

					@if ($errors->has('freight_service_id'))
						<span class="help-block">
							<strong>{{ $errors->first('freight_service_id') }}</strong>
						</span>
					@endif

				</div>
				{{--<div class="form-group col-sm-6 freightServiceBookingNumber {{ $errors->has('freight_service_booking_number') ? ' has-error' : '' }}">--}}
					{{--<label for="bookingNumber">--}}
						{{--{{ __('trackers.parts.freight_services.type_booking_number' ) }}--}}
					{{--</label>--}}

					{{--<input type="text" class="form-control only-numeric" name="freight_service_booking_number"--}}
						   {{--@if(isset($tracker))--}}
						   {{--value="{{ old('freight_service_booking_number', $tracker->freightServiceBookingNumber) }}"--}}
						   {{--@else--}}
						   {{--value="{{ old('freight_service_booking_number') }}"--}}
							{{--@endif--}}
					{{-->--}}

					{{--@if ($errors->has('freight_service_booking_number'))--}}
						{{--<span class="help-block">--}}
							{{--<strong>{{ $errors->first('freight_service_booking_number') }}</strong>--}}
						{{--</span>--}}
					{{--@endif--}}

				{{--</div>--}}
			</div>

			<div class="row">
				<div class="form-group required col-sm-6 {{ $errors->has('special_handling_id') ? ' has-error' : '' }}">
					<label for="specialHandling">
						{{ __('trackers.parts.special_handlings.title' ) }}
					</label>

					@if(isset($tracker) && isset($tracker->special_handling_id))
						<input type="hidden" name="special_handling_type" value="{{ $tracker->specialHandling->type }}">
					@else
						<input type="hidden" name="special_handling_type" value="">
					@endif


					<select class="form-control selectpicker" id="specialHandling" name="special_handling_id">

						@foreach($special_handlings as $special_handling)

							@if(isset($tracker))

								<option data-type="{{$special_handling->type}}" value="{{ $special_handling->id }}"
										{{ old('special_handling_id') != $special_handling->id ?: 'selected' }} @if($special_handling->disabled) disabled @endif @if($tracker->special_handling_id == $special_handling->id)selected @endif
                                        @if($special_handling->disabled) disabled @endif>

							@else
								<option data-type="{{$special_handling->type}}"
										value="{{ $special_handling->id }}"
										{{ old('special_handling_id') != $special_handling->id ?: 'selected' }}
										@if($special_handling->type =='address_delivery') selected @endif
                                        @if($special_handling->disabled) disabled @endif
								>
							@endif

							{{ __('trackers.parts.special_handlings.type_' .  $special_handling->type ) }}
						@endforeach

					</select>

					@if ($errors->has('special_handling_id'))
						<span class="help-block">
							<strong>{{ $errors->first('special_handling_id') }}</strong>
						</span>
					@endif

				</div>
			</div>

			<div class="required-info">
				<div class="text">
					<span class="color-red">*</span> {{ __('trackers.tracker_required_fields' ) }}
				</div>
			</div>

			<div class="separator"></div>


		</div>
	</div>

</section>



