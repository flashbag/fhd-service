<h2>
	<span class="step-icon">
		1
	</span>
	<span class="step-text">
		{{ __('trackers.parts.transport.type_transport') }}
	</span>
</h2>

<section class="step fields-to-fill" data-id=1>

	<div class="step-inner panel panel-default">
		<div class="panel-heading">
			<h5>
				{{ trans('trackers.parts.transport.type_transport') }}
			</h5>
		</div>
		<div class="panel-body">

			<div class="row">

				<div class="typeTransport form-group {{ $errors->has('type_transport_id') ? ' has-error' : '' }}">
					@foreach($type_transports as $type_transport)
						<label class="type-item fake-radio card-transport-{{ $type_transport->id }}" @if($type_transport->disabled)style="opacity: 0.4;" @endif>
							<input type="radio" name="type_transport_id" value="{{ $type_transport->id }}"
								   @if(isset($tracker) && $tracker->type_transport_id == $type_transport->id) checked @endif
							@if($type_transport->disabled) disabled  @endif
                            >
							<div class="text">
								{{ __('trackers.parts.transport.type_transport_' .  $type_transport->type ) }}
							</div>
						</label>
					@endforeach
					@if ($errors->has('type_transport_id'))
						<span class="help-block">
							<strong style="margin-left: 15px">{{ $errors->first('type_transport_id') }}</strong>
						</span>
					@endif
				</div>

			</div>


			<br><br>

			<div class="row">
				<div class="form-group col-sm-3 required">
					<label for="fromCountryId">
						{{ __('trackers.parts.countries.country_departure') }}
						<span class="color-red">*</span>
					</label>

					<select class="form-control selectpicker" name="from_country"
							data-warehouse-selector="#select_warehouse_from">
						<option value="">--</option>
						@foreach($countries as $country)
							<option value="{{ $country->id }}"
									data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2, [], 'en') }}"
									@if(isset($tracker) && $tracker->warehouseFrom && $tracker->warehouseFrom->country->id == $country->id) selected @endif>
								{{ __('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2) }}
							</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-sm-3 required">
					<label for="fromCountryId">
						{{ __('trackers.parts.warehouse.country_departure_warehouse') }}
						<span class="color-red">*</span>
					</label>

					<select class="form-control selectpicker" id="select_warehouse_from" name="from_warehouse_id" readonly>
						@if( isset($tracker) && $tracker->warehouseFrom)
							<option value="{{ $tracker->warehouseFrom->id }}">{{ $tracker->warehouseFrom->name }}</option>
						@endif
					</select>
				</div>

				<div class="form-group col-sm-3 required">
					<label for="fromCountryId">
						{{ __('trackers.parts.countries.country_arrival') }}
						<span class="color-red">*</span>
					</label>

					<select class="form-control selectpicker" name="to_country"
							data-warehouse-selector="#select_warehouse_to">
						<option value="">--</option>
						@foreach($countries as $country)

							<option value="{{ $country->id }}"
									data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2, [], 'en') }}"
									@if(isset($tracker) && $tracker->warehouseTo && $tracker->warehouseTo->country->id == $country->id) selected @endif>
								{{ __('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2) }}
							</option>

						@endforeach
					</select>
				</div>

				<div class="form-group col-sm-3 required">
					<label for="fromCountryId">
						{{ __('trackers.parts.warehouse.country_arrival_warehouse') }}
						<span class="color-red">*</span>
					</label>

					<select class="form-control selectpicker" id="select_warehouse_to" name="to_warehouse_id" readonly>
						@if( isset($tracker) && $tracker->warehouseTo)
							<option value="{{ $tracker->warehouseTo->id }}">{{ $tracker->warehouseTo->name }}</option>
						@endif
					</select>
				</div>
			</div>

		</div>


	</div>

</section>
