@if (!isset($tracker))

	<div class="row">
		<div class="col-sm-6">

			<label>{{ trans('main.tracker_create.content.label_33') }}</label>
			<div class="form-group" id="toggler">
				<label class="radio-inline fake-radio default">
					<input id="inlineR1-O1" type="radio" checked name="inlineRadioOptions1" value="option1">
					{{ trans('main.tracker_create.content.label_34') }}
				</label>
				<label class="radio-inline fake-radio default">
					<input id="inlineR1-O2" type="radio" name="inlineRadioOptions1" value="option2">
					{{ trans('main.tracker_create.content.label_35') }}
				</label>
				<br>
				<br>
				<div id="inlineRODanger" class="panel panel-danger" style="display: none;">
					<div class="panel-heading">{{ trans('main.tracker_create.content.label_36') }}</div>
					<div class="panel-body">
						<a href="{{ route('contacts') }}">{{ trans('main.tracker_create.content.label_37') }}</a>{{ trans('main.tracker_create.content.label_38') }}
					</div>
				</div>
			</div>

		</div>

	</div>

	<div class="row">
		<div class="col-sm-6">
			<label>
				{!!   __('trackers.rules_agreement', [
                        'link_start' => '<a target="_blank" href="' . url('/documents') . '">',
                        'link_end' => '</a>'])
                !!}
			</label>

			<div class="form-group">
				<label class="radio-inline fake-radio default">
					<input id="inlineR2-O1" type="radio" checked name="inlineRadioOptions2" value="option1">
					{{ trans('main.tracker_create.content.label_34') }}
				</label>
				<label class="radio-inline fake-radio default">
					<input id="inlineR2-O2" type="radio" name="inlineRadioOptions2" value="option2">
					{{ trans('main.tracker_create.content.label_35') }}
				</label>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-sm-6">
			<div id="inlineRODanger" class="panel panel-danger" style="display: none;">
				<div class="panel-heading">{{ trans('main.tracker_create.content.label_36') }}</div>
				<div class="panel-body">
					<a href="{{ route('contacts') }}">{{ trans('main.tracker_create.content.label_37') }}</a>{{ trans('main.tracker_create.content.label_38') }}
				</div>
			</div>
		</div>
	</div>

@endif
