<div class="row">
	<div class="col-xs-4 col-sm-3 pull-left text-left mb-4" >
		@if(Auth::user() && !Auth::user()->isCustom())
			<button onclick="location.href='{{ url()->route('history-trackers') }}'"  class="btn btn-info-new check-default-btn" style="width: 90px;">{{ trans('main.tracker_create.content.back') }}</button>
		@endif
			@if(Auth::user() && Auth::user()->isCustom())
                <button onclick="location.href='{{ url()->route('custom') }}'"  class="btn btn-info-new check-default-btn" style="width: 90px;">{{ trans('main.tracker_create.content.back') }}</button>
        @endif
	</div>

        @if(Auth::user())
            @if(!Auth::user()->isCustom())
                <div class="col-xs-8 col-sm-3 text-right mb-4">
                    <button id="save-as-draft" class="btn btn-info-new check-default-btn" style="width: 190px;">
                        {{ trans('main.tracker_create.content.label_42') }}
                    </button>
                </div>
                <div class="col-xs-12 col-sm-3 pull-right text-right mb-4">
                    <button type="submit" id="save-tracker" @if (!isset($tracker)) disabled="disabled" @endif class="btn btn-info-new check-submit-btn">
                        @if (!isset($tracker))
                            {{ trans('main.tracker_create.content.label_39') }}
                        @else
                            {{ trans('main.tracker_create.content.label_390') }}
                        @endif
                    </button>
                </div>
            @endif
        @else
            <div class="col-xs-12 col-sm-3 pull-right text-right mb-4">
                <button type="submit" id="save-tracker" @if (!isset($tracker)) disabled="disabled" @endif class="btn btn-info-new check-submit-btn">
                    @if (!isset($tracker))
                        {{ trans('main.tracker_create.content.label_39') }}
                    @else
                        {{ trans('main.tracker_create.content.label_390') }}
                    @endif
                </button>
            </div>
        @endif

	<input type="hidden" name="is_draft" value="0">

</div>

@push('script')
	<script type="text/javascript">
		$(document).ready(function(){

			var $trackerForm = $('#tracker-form');
			var $isDraftInput = $('input[name="is_draft"]');

			$('#save-as-draft').on('click', function(e){

				e.preventDefault();

				$isDraftInput.val('1');
				$trackerForm.submit();
			});

			$('#save-tracker').on('click', function (e) {

				e.preventDefault();

				$isDraftInput.val('0');
				$trackerForm.submit();
			});

		});
	</script>
@endpush
