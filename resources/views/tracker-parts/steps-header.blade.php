<div class="steps clearfix">
	<ul role="tablist">
		<li role="tab" aria-disabled="false" class="first current" aria-selected="true"><a id="tracker-form-total-t-0" href="#tracker-form-total-h-0" aria-controls="tracker-form-total-p-0"><span class="current-info audible"> </span><div class="title">
	<span class="step-icon">
		1
	</span>
					<span class="step-text">
		Тип транспорта
	</span>
				</div></a></li>
		<li role="tab" aria-disabled="false">
			<a id="tracker-form-total-t-1" href="#tracker-form-total-h-1" aria-controls="tracker-form-total-p-1">
				<div class="title">
                    <span class="step-icon">
		2
	</span>
					<span class="step-text">
		Информация о позиции
	</span>
				</div>
			</a>
		</li>
		<li role="tab" aria-disabled="false">
			<a id="tracker-form-total-t-2" href="#tracker-form-total-h-2" aria-controls="tracker-form-total-p-2">
				<div class="title">
                    <span class="step-icon">
		3
	</span>
					<span class="step-text">
		Отправитель
	</span>
				</div>
			</a>
		</li>
		<li role="tab" aria-disabled="false">
			<a id="tracker-form-total-t-3" href="#tracker-form-total-h-3" aria-controls="tracker-form-total-p-3">
				<div class="title">
                    <span class="step-icon">
		4
	</span>
					<span class="step-text">
		Получатель
	</span>
				</div>
			</a>
		</li>
		<li role="tab" aria-disabled="false" class="last">
			<a id="tracker-form-total-t-4" href="#tracker-form-total-h-4" aria-controls="tracker-form-total-p-4">
				<div class="title">
                    <span class="step-icon">
		5
	</span>
					<span class="step-text">
		Дополнительная информация
	</span>
				</div>
			</a>
		</li>
	</ul>
</div>
