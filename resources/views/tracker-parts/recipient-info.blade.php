<h2>
	<span class="step-icon">
		4
	</span>
	<span class="step-text">
		{{ __('trackers.parts.recipient.title') }}
	</span>
</h2>


<section class="recipient-info step" data-id=4>
	<div class="panel panel-default">
		<div class="panel-heading clearfix">
			<h5 class="pull-left panel-title pull-left">
				{{ __('trackers.parts.recipient.title') }}
			</h5>
		</div>
		@if(isset($tracker))
			<input type="hidden" name="recipient_id" value="{{ $tracker->sender_id }}">
		@endif
		<div class="panel-body">

			<div class="row">
				<div class="form-group required col-sm-4 {{ $errors->has('number_recipient') ? ' has-error' : '' }}">
					<label for="numberRecipient">
						{{ __('trackers.parts.recipient.number') }}
						<span class="color-red">*</span>
					</label>
					<div class="">
						<input type="text" class="form-control input__custom" id="numberRecipient" name="number_recipient"
							   @if (isset($tracker) && $tracker->recipient)
							   value="{{ old('number_recipient', $tracker->recipient->number) }}"
							   @else
							   value="{{ old('number_recipient') }}"
								@endif
						>
						<span id="numberRecipient-valid-msg" class="hide phone-valid-msg">✓ Valid</span>
						<span id="numberRecipient-error-msg" class="hide phone-error-msg"></span>
					</div>
					@if ($errors->has('number_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('number_recipient') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-sm-4 {{ $errors->has('email_recipient') ? ' has-error' : '' }}">
					<label for="inputEmailRecipient">
						{{ __('trackers.parts.recipient.email') }}
						{{--<span class="color-red">*</span>--}}
					</label>
					<input type="text" class="form-control input__custom" id="inputEmailRecipient" name="email_recipient"
						   data-error-msg="Некорректный Email"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('email_recipient', $tracker->recipient->email) }}"
						   @else
						   value="{{ old('email_recipient') }}"
							@endif
					>
					@if ($errors->has('email_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('email_recipient') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-sm-4 {{ $errors->has('recipient_person_type') ? ' has-error' : '' }}">

					<label for="recipientPersonType">
						{{ __('trackers.parts.recipient.person_type') }}
					</label>

					<select class="form-control selectpicker select-person-type" id="recipientPersonType"
							name="recipient_person_type">

						@foreach(\App\Models\ClientInfo::getPersonTypes() as $personType)

							@if (isset($tracker) && $tracker->recipient)

								<option value={{ $personType }}
								{{ old('recipient_person_type') != $tracker->recipient->person_type ?: 'selected' }} @if($tracker->recipient->person_type == $personType)selected @endif>

							@else
								<option value={{ $personType }} {{ old('recipient_person_type') != $personType ?: 'selected' }}>
							@endif


							{{ __('trackers.parts.person_types.' . $personType) }}
						@endforeach

					</select>

					@if ($errors->has('recipient_person_type'))
						<span class="help-block">
							<strong>{{ $errors->first('recipient_person_type') }}</strong>
						</span>
					@endif

				</div>
			</div>

			<div class="row">

				<div class="form-group required col-sm-4 {{ $errors->has('full_name_recipient') ? ' has-error' : '' }}">
					<label for="inputRecipientFullName">
						{{ __('trackers.parts.recipient.full_name') }}
					</label>
					<input type="text" class="form-control input__custom" id="inputRecipientFullName" name="full_name_recipient"
						   data-without-digits="true"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('full_name_recipient', $tracker->recipient->full_name) }}"
						   @else
						   value="{{ old('full_name_recipient') }}"
							@endif
					>
					@if ($errors->has('full_name_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('full_name_recipient') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group col-sm-4 {{ $errors->has('recipient_company_name') ? ' has-error' : '' }}">
					<label for="inputRecipientCompanyName">
						{{ __('trackers.parts.recipient.company_name') }}
					</label>
					<input type="text" class="form-control input__custom" id="inputRecipientCompanyName" name="recipient_company_name"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('recipient_company_name', $tracker->recipient->company_name) }}"
						   @else
						   value="{{ old('recipient_company_name') }}"
							@endif
					>
					@if ($errors->has('recipient_company_name'))
						<span class="help-block">
							<strong>{{ $errors->first('recipient_company_name') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group col-sm-4 {{ $errors->has('recipient_number_account') ? ' has-error' : '' }}">
					<label for="recipientAccountNumber">
						{{ __('trackers.parts.recipient.number_account') }}
					</label>
					<input type="text" class="form-control only-numeric input__custom" id="recipientAccountNumber"
						   name="recipient_number_account"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('recipient_number_account', $tracker->recipient->number_account) }}"
						   @else
						   value="{{ old('recipient_number_account') }}"
							@endif
					>
					@if ($errors->has('recipient_number_account'))
						<span class="help-block">
							<strong>{{ $errors->first('recipient_number_account') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="form-group required col-sm-4 {{ $errors->has('city_id_recipient') ? ' has-error' : '' }}">
					<label for="inputCityRecipientId">
						{{ __('trackers.parts.address_components.city') }}
						<span class="color-red">*</span>
					</label>

					<div class="typehead typehead-google-address typehead-google-address-city">
						<input type="text" class="form-control inputCityRecipient input__custom"
							   data-without-digits="true"
							   name="city_id_recipient"
							   id="inputCityRecipientId"
							   data-country-selector="#inputCountryRecipientId"
							   data-region-selector="#inputRecipientRegion"
							   data-address-selector="#inputAddressRecipientId"
							   @if (isset($tracker) && $tracker->recipient)
							   value="{{ old('city_id_recipient', $tracker->recipient->city) }}"
							   @else
							   value="{{ old('city_id_recipient') }}"
								@endif
						>
						<i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
						<ul class="typehead-list"></ul>
					</div>

					@if ($errors->has('city_id_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('city_id_recipient') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-sm-4 {{ $errors->has('region_recipient') ? ' has-error' : '' }}">
					<label for="inputRecipientRegion">
						{{ __('trackers.parts.address_components.region') }}
						{{--<span class="color-red">*</span>--}}
					</label>

					<div class="typehead typehead-google-address typehead-google-address-region">
						<input type="text" class="form-control input__custom"
							   data-without-digits="true"
							   name="region_recipient"
							   id="inputRecipientRegion"
							   data-country-selector="#inputCountryRecipientId"
							   data-city-selector="#inputCityRecipientId"
							   data-address-selector="#inputAddressRecipientId"
							   @if (isset($tracker) && $tracker->recipient)
							   value="{{ old('region_recipient', $tracker->recipient->region) }}"
							   @else
							   value="{{ old('region_recipient') }}"
								@endif
						>
						<i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
						<ul class="typehead-list"></ul>
					</div>

					@if ($errors->has('region_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('region_recipient') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group required col-sm-4 {{ $errors->has('country_id_recipient') ? ' has-error' : '' }}">
					<label for="inputCountryRecipientId">
						{{ __('trackers.parts.address_components.country') }}
						<span class="color-red">*</span>
					</label>

					<div class="typehead typehead-google-address typehead-google-address-country">
						<input type="text" class="form-control input__custom"
							   name="country_id_recipient"
							   readonly="readonly"
							   id="inputCountryRecipientId"
							   data-region-selector="#inputRecipientRegion"
							   data-city-selector=".inputCityRecipient"
							   data-address-selector=".inputAddressRecipient"
							   @if (isset($tracker) && $tracker->recipient)
							   value="{{ old('country_id_recipient', $tracker->recipient->country) }}"
							   @else
							   value="{{ old('country_id_recipient') }}"
								@endif
						>
						<i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
						<ul class="typehead-list"></ul>
					</div>

					@if ($errors->has('country_id_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('country_id_recipient') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="form-group required col-sm-4 {{ $errors->has('address_recipient') ? ' has-error' : '' }}">
					<label for="inputAddressRecipientId">
						{{ __('trackers.parts.address_components.address') }}
						<span class="color-red">*</span>
					</label>

					<div class="typehead typehead-google-address typehead-google-address-address">
						<input type="text" class="form-control inputAddressRecipient input__custom"
							   id="inputAddressRecipientId"
							   name="address_recipient"
							   data-country-selector="#inputCountryRecipientId"
							   data-region-selector="#inputRecipientRegion"
							   data-city-selector="#inputCityRecipientId"
							   data-index-selector="#inputRecipientIndex"
							   @if (isset($tracker) && $tracker->recipient)
							   value="{{ old('address_recipient', $tracker->recipient->address) }}"
							   @else
							   value="{{ old('address_recipient') }}"
								@endif
						>
						<i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
						<ul class="typehead-list"></ul>
					</div>
					<span class="help-block-static">
						{{ __('trackers.parts.address_components.address_help_block') }}
					</span>
					@if ($errors->has('address_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('address_recipient') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group required col-sm-4 {{ $errors->has('index_recipient') ? ' has-error' : '' }}">
					<label for="inputRecipientIndex">
						{{ __('trackers.parts.address_components.index') }}
						<span class="color-red">*</span>
					</label>
					<input type="text" class="form-control input__custom" id="inputRecipientIndex" name="index_recipient"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('index_recipient', $tracker->recipient->index) }}"
						   @else
						   value="{{ old('index_recipient') }}"
							@endif
					>
					@if ($errors->has('index_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('index_recipient') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-sm-4 {{ $errors->has('inn_recipient') ? ' has-error' : '' }}">
					<label for="recipientInputInn">
						{{ __('trackers.parts.recipient.inn') }}
					</label>
					<input type="text" class="form-control only-numeric input__custom" id="recipientInputInn" name="inn_recipient"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('inn_recipient', $tracker->recipient->inn) }}"
						   @else
						   value="{{ old('inn_recipient') }}"
							@endif
					>
					@if ($errors->has('inn_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('inn_recipient') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="form-group col-sm-4 {{ $errors->has('passport_data_recipient') ? ' has-error' : '' }}">
					<label for="recipientInputPassportData">
						{{ __('trackers.parts.recipient.passport_data') }}
					</label>
					<input type="text" class="form-control input__custom" id="recipientInputPassportData"
						   name="passport_data_recipient"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('passport_data_recipient', $tracker->recipient->passport_data) }}"
						   @else
						   value="{{ old('passport_data_recipient') }}"
							@endif
					>
					@if ($errors->has('passport_data_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('passport_data_recipient') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-sm-4 {{ $errors->has('recipient_number_service') ? ' has-error' : '' }}">
					<label for="inputRecipientNumberService">
						{{ __('trackers.parts.recipient.number_service') }}
					</label>
					<input type="text" class="form-control only-numeric input__custom" id="inputRecipientNumberService"
						   name="recipient_number_service"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('recipient_number_service', $tracker->recipient->number_service) }}"
						   @else
						   value="{{ old('recipient_number_service') }}"
							@endif
					>
					@if ($errors->has('recipient_number_service'))
						<span class="help-block">
							<strong>{{ $errors->first('recipient_number_service') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-sm-4 {{ $errors->has('recipient_service_delivery_id') ? ' has-error' : '' }}">
					<label for="recipientServiceDeliverySelect">
						{{ __('trackers.parts.recipient.service_delivery') }}
					</label>

					<select class="form-control selectpicker" id="recipientServiceDeliverySelect"
							name="recipient_service_delivery_id">

						<option selected value="null">--</option>

						@foreach($service_deliveries as $service_delivery)

							@if (isset($tracker) && $tracker->recipient)

								<option value="{{ $service_delivery->id }}"
										{{ old('recipient_service_delivery_id') != $service_delivery->id ?: 'selected' }} @if($tracker->recipient->service_delivery_id == $service_delivery->id)selected @endif>

							@else
								<option value="{{ $service_delivery->id }}" {{ old('recipient_service_delivery_id') != $service_delivery->id ?: 'selected' }}>
							@endif

							{{ $service_delivery->name }}
						@endforeach
						<option value=""
								@if (isset($tracker) && $tracker->recipient  && $tracker->recipient->service_delivery_other) selected
								@endif }}>другое
						</option>

					</select>

					@if ($errors->has('recipient_service_delivery_id'))
						<span class="help-block">
							<strong>{{ $errors->first('recipient_service_delivery_id') }}</strong>
						</span>
					@endif

				</div>
			</div>

			<div class="row">
				<div class="form-group col-sm-4 recipientCustomIdentifyBlock {{ $errors->has('custom_id') ? ' has-error' : '' }}">
					<label for="customId">
						{{ __('trackers.parts.recipient.custom_id') }}
						<span class="color-red">*</span>
					</label>
					<input type="text" class="form-control input__custom" id="customId" name="custom_id"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('custom_id', $tracker->recipient->custom_identify) }}"
						   @else
						   value="{{ old('custom_id') }}"
							@endif
					>
					@if ($errors->has('custom_id'))
						<span class="help-block">
							<strong>{{ $errors->first('custom_id') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="row" style="display: none;">
				{{-- Без понятия зачем эти поля, но в дизайне их нет --}}
				{{-- Будем уточнять --}}
				<div class="form-group col-sm-3 recipientServiceDeliveryOther {{ $errors->has('recipient_service_delivery_other') ? ' has-error' : '' }}"
					 style="display: none">
					<label for="bookingNumber">
						{{ __('trackers.parts.recipient.service_delivery_other') }}
					</label>

					<input type="text" disabled="disabled" class="form-control input__custom" name="recipient_service_delivery_other"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('recipient_service_delivery_other', $tracker->recipient->service_delivery_other) }}"
						   @else
						   value="{{ old('recipient_service_delivery_other') }}"
							@endif
					>

					@if ($errors->has('recipient_service_delivery_other'))
						<span class="help-block">
							<strong>{{ $errors->first('recipient_service_delivery_other') }}</strong>
						</span>
					@endif

				</div>
				<div class="form-group col-sm-3 {{ $errors->has('representative_recipient') ? ' has-error' : '' }}">
					<label for="inputRepresentativeRecipient">
						{{ __('trackers.parts.recipient.contact_name') }}
					</label>
					<input type="text" class="form-control input__custom" id="inputRepresentativeRecipient"
						   name="representative_recipient"
						   @if (isset($tracker) && $tracker->recipient)
						   value="{{ old('representative_recipient', $tracker->recipient->representative_recipient) }}"
						   @else
						   value="{{ old('representative_recipient') }}"
							@endif
					>
					@if ($errors->has('representative_recipient'))
						<span class="help-block">
							<strong>{{ $errors->first('representative_recipient') }}</strong>
						</span>
					@endif
				</div>
			</div>


			<div class="row recipient-files-container">
				<div class="form-group col-sm-8">
					<label for="inputDocumentId">
						{{ __('trackers.parts.documents.title_recipient') }}
					</label>
					@if (isset($tracker) && $tracker->recipientDocuments->count())
						@foreach($tracker->recipientDocuments as $index => $recipientDocument)
							<div class="form-group file-block">
								<input type="hidden" name="recipient_documents[{{$index}}][id]"
									   value="{{ $recipientDocument->id }}">
								<input type="file" name="recipient_documents[{{$index}}][file]"
									   style="display: inline-block">
								<a href="{{ $recipientDocument->getDocumentUrl() }}" style="margin-right: 15px;"
								   target="_blank">
									{{ __('trackers.parts.documents.view_document') }} #{{ $recipientDocument->id }}
								</a>
								<a class="remove-file-input is-existing-file"
								   data-input-name="recipient_documents"
								   data-container-selector=".recipient-files-container"
								   data-index="{{ $index }}" data-id="{{ $recipientDocument->id }}"
								>
									<i class="fa fa-2x fa-minus-square"></i>
								</a>
							</div>
						@endforeach
					@else
						<div class="form-group file-block">
							<!-- This hidden field is needed! -->
							<input type="hidden" name="recipient_documents[1][beacon]" value="beacon">
							<label class="fake-input-file form-control">
								<span class="placeholder" data-default-value="{{ __('trackers.parts.documents.file_not_selected') }}">
									{{ __('trackers.parts.documents.file_not_selected') }}
								</span>
								<input type="file" name="recipient_documents[1][file]">
								<span class="select-file">
									{{ __('trackers.parts.documents.select_file') }}
								</span>
							</label>
						</div>
					@endif
				</div>

				<div class="form-group col-sm-1">
					<label>&nbsp;</label>
					<a data-input-name="recipient_documents" data-container-selector=".recipient-files-container"
					   data-get-template-url="{{ route('template-get-file') }}"
					   class="add-one-more add-one-more-file">
					</a>
				</div>
			</div>

			<div class="required-info">
				<div class="text">
					<span class="color-red">*</span> {{ __('trackers.tracker_required_fields') }}
				</div>
			</div>
		</div>

	</div>
</section>
