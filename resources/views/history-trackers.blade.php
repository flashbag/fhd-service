@extends('layouts.app')

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                            <div class="pull-left" style="width: 65%;">
                                <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.history.p') }}</p>
                                <h1>{{ trans('main.history.h1') }}</h1>
                                <h5 class="subtitle">{{ trans('main.history.h5') }}</h5>
                                <div class="btn-group-block">
                                    <a href="/about-us" class="btn btn-default-new">{{ trans('main.history.link2') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('content')
    <div class="wrap">
        <div class="trackers-history-page">
            <div class="container-fluid">

                @include('flash-messages')

                <div class="row h-100">

                    @include('tickets.warehouse.left-menu-tickets')

                    <div id="google-map" class="trackers-history-page__col-6 map-section"></div>

                    <div class="trackers-history-page__col-4 history-section">

                        <div class="history-section__tab">
                            <div class="history-section__header" role="tab" id="headingHistory">
                                <a class="history-section_list" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseHistory" aria-expanded="false" aria-controls="collapseHistory">
                                    {{ trans('main.mini_nav.trackers') }}
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div id="collapseHistory" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHistory">

                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link" id="current-orders-tab" data-toggle="tab"
                                           href="#current-orders" role="tab" aria-controls="current-orders"
                                           aria-selected="true">{{ trans('main.custom.actualOrders') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="draft-tab" data-toggle="tab" href="#draft" role="tab"
                                           aria-controls="draft" aria-selected="false">{{ trans('main.custom.draft') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="archive-tab" data-toggle="tab" href="#archive" role="tab"
                                           aria-controls="archive" aria-selected="false">{{ trans('main.custom.archive') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="trash-tab" data-toggle="tab" href="#trash" role="tab"
                                           aria-controls="trash" aria-selected="false">{{ trans('main.custom.trash') }}</a>
                                    </li>
                                </ul>

                            </div>

                        </div>


                        {{--						<ul class="nav nav-tabs nav-justified" role="tablist">
                                                    <li class="nav-item active">
                                                        <a class="nav-link" id="current-orders-tab" data-toggle="tab"
                                                           href="#current-orders" role="tab" aria-controls="current-orders"
                                                           aria-selected="true">{{ trans('main.custom.actualOrders') }}</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="draft-tab" data-toggle="tab" href="#draft" role="tab"
                                                           aria-controls="draft" aria-selected="false">{{ trans('main.custom.draft') }}</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="archive-tab" data-toggle="tab" href="#archive" role="tab"
                                                           aria-controls="archive" aria-selected="false">{{ trans('main.custom.archive') }}</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="trash-tab" data-toggle="tab" href="#trash" role="tab"
                                                           aria-controls="trash" aria-selected="false">{{ trans('main.custom.trash') }}</a>
                                                    </li>
                                                </ul>
                        --}}

                        <div class="tab-content tab-content_scroll">
                            <div class="tab-pane fade active in" id="current-orders" role="tabpanel"
                                 aria-labelledby="current-orders-tab">
                                <div class="caption">
                                    <button type="button" class="map-trigger show-map-trigger btn btn-default">
                                        <img src="/img/icons/map-icon.png">
                                        {{ trans('main.custom.showMap') }}
                                    </button>
                                    {{ trans('main.custom.purchaseHistory') }}
                                </div>
                                <button type="button" class="map-trigger btn btn-default">
                                    <img src="/img/icons/arrow-gray-right-icon.png">
                                    {{ trans('main.custom.hideMap') }}
                                </button>
                                <ul class="nav nav-tabs nav-justified" role="tablist" style="display: none">
                                    <li class="nav-item active">
                                        <a class="nav-link" id="to-departure-tab" data-toggle="tab"
                                           href="#to-departure" role="tab" aria-controls="to-departure"
                                           aria-selected="true">{{ trans('main.custom.toDeparture') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="to-receive-tab" data-toggle="tab" href="#to-receive" role="tab"
                                           aria-controls="to-receive" aria-selected="false">{{ trans('main.custom.toReceive') }}</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="to-departure tab-pane fade active in" id="to-departure" role="tabpanel"
                                         aria-labelledby="to-departure-tab">

                                        <table class="table">
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>{{ trans('main.custom.№invoice') }}</th>
                                                <th>{{ trans('main.custom.date') }}</th>
                                                <th>{{ trans('main.custom.status') }}</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            @if(isset($trackers) && count($trackers) > 0)
                                                @foreach($trackers as $tracker)

                                                    <tr class="show-block">
                                                        @if(\App\Ticket::where('tracker_id', $tracker->id)->first()['status'] === 0)
                                                            <td><a href="{{  route('warehouse.tickets.show', $tracker->id_tracker ) }}"><svg style="width:39px; height:40px;" ><use xlink:href="#message"></use></svg></a></td>
                                                        @elseif(\App\Ticket::where('tracker_id', $tracker->id)->first()['status'] === 1)
                                                                <td><a href="{{  route('warehouse.tickets.show', $tracker->id_tracker ) }}"><svg style="width:39px; height:40px;" ><use xlink:href="#message_read"></use></svg></a></td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                        <td><a href="{{ route('edit-trackers', $tracker->id_tracker) }}">{{ $tracker->id_tracker }}</a></td>
                                                        <td>{{ $tracker->created_at }}</td>

                                                        @if($tracker->is_unprocessed == 1)
                                                            <td class="status-moderation"><i class="circle"></i>&nbsp;
                                                                {{ trans('main.history.content.on_mod') }}.</td>
                                                        @else
                                                            <td class="status-checked"><i class="circle"></i>&nbsp;
                                                                {{ trans('main.history.content.off_mod') }}.</td>
                                                        @endif

                                                        <td class="pointed-menu-wrapper">
                                                            <div class="dropdown">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                    <img src="/img/icons/pointed-menu-vertical-icon.png">
                                                                </a>
                                                                <ul class="dropdown-menu dropdown-menu-right arrow_box">
                                                                    <li class="item edit">
                                                                        <a href="{{ route('edit-trackers', $tracker->id_tracker) }}">{{ trans('main.custom.edit') }}</a>
                                                                    </li>

                                                                    <li class="item print">
                                                                        <a href="#" class="btn-print" data-toggle="modal" data-id-tracker="{{ $tracker->id_tracker }}">
                                                                            {{ trans('main.custom.downloadPDF') }}
                                                                        </a>
                                                                    </li>

                                                                    @if($tracker->status_id == NULL || $tracker->status_id == 11)
                                                                        <li class="item remove">
                                                                            <a href="{{ route('delete-trackers-user', $tracker->id_tracker) }}">{{ trans('main.custom.delete') }}</a>
                                                                        </li>
                                                                    @endif

                                                                    @if(\App\Ticket::where('tracker_id', $tracker->id)->first() != null)
                                                                        <li class="item print">
                                                                            <a href="{{ route('create-ticket', $tracker->id) }}">{{ trans('main.custom.ticket') }}</a>
                                                                        </li>
                                                                    @else
                                                                        <li class="item print">
                                                                            <a href="{{ route('create-ticket', $tracker->id) }}">{{ trans('main.custom.new_ticket') }}</a>
                                                                        </li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </table>

                                    </div>
                                    <div class="tab-pane fade" id="to-receive" role="tabpanel"
                                         aria-labelledby="to-receive-tab">
                                        {{ trans('main.custom.toReceive') }}
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="draft" role="tabpanel" aria-labelledby="draft-tab">
                                {{--<div class="caption">--}}
                                {{--<button type="button" class="map-trigger show-map-trigger btn btn-default">--}}
                                {{--<img src="/img/icons/map-icon.png">--}}
                                {{--{{ trans('main.custom.showMap') }}--}}
                                {{--</button>--}}
                                {{--{{ trans('main.custom.draft') }}--}}
                                {{--</div>--}}
                                {{--<div class="tab-content">--}}
                                {{--<div class="tab-pane fade active in" id="to-departure" role="tabpanel"--}}
                                {{--aria-labelledby="to-departure-tab">--}}

                                <table class="table">
                                    <tr>
                                        <th>{{ trans('main.custom.№invoice') }}</th>
                                        <th>{{ trans('main.custom.date') }}</th>
                                        <th>{{ trans('main.custom.status') }}</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    @if(isset($trackersDraft) && count($trackersDraft) > 0)
                                        @foreach($trackersDraft as $tracker)
                                            <tr class="show-block">
                                                <td><a href="{{ route('edit-trackers', $tracker->id_tracker) }}">{{ $tracker->id_tracker }}</a></td>
                                                <td>{{ $tracker->created_at }}</td>
                                                @if($tracker->is_unprocessed == 1)
                                                    <td class="status-moderation"><i class="circle"></i>&nbsp;
                                                        {{ trans('main.history.content.on_mod') }}.</td>
                                                @else
                                                    <td class="status-checked"><i class="circle"></i>&nbsp;
                                                        {{ trans('main.history.content.off_mod') }}.</td>
                                                @endif

                                                <td class="pointed-menu-wrapper">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            <img src="/img/icons/pointed-menu-vertical-icon.png">
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-right arrow_box">
                                                            <li class="item edit">
                                                                <a href="{{ route('edit-trackers', $tracker->id_tracker) }}">{{ trans('main.custom.edit') }}</a>
                                                            </li>
                                                            <li class="item print">
                                                                <a href="{{ route('download-tracker-pdf', $tracker->id_tracker) }}">{{ trans('main.custom.downloadPDF') }}</a>
                                                            </li>
                                                            @if($tracker->status_id == NULL)
                                                                <li class="item remove">
                                                                    <a href="{{ route('delete-trackers-user', $tracker->id_tracker) }}">{{ trans('main.custom.delete') }}</a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>

                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="tab-pane fade" id="archive" role="tabpanel" aria-labelledby="archive-tab">
                                <div class="col-sm-12 py-4"><span class="caption">{{ trans('main.custom.archive') }}</span></div>
                            </div>
                            <div class="tab-pane fade" id="trash" role="tabpanel" aria-labelledby="trash-tab">
                                <table class="table">
                                    <tr>
                                        <th>{{ trans('main.custom.№invoice') }}</th>
                                        <th>{{ trans('main.custom.date') }}</th>
                                        <th>{{ trans('main.custom.status') }}</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    @if(isset($trackersTrash) && count($trackersTrash) > 0)
                                        @foreach($trackersTrash as $trash)
                                            <tr class="show-block">
                                                <td><a href="{{ route('edit-trackers', $trash->id_tracker) }}">{{ $trash->id_tracker }}</a></td>
                                                <td>{{ $trash->created_at }}</td>
                                                @if($trash->is_unprocessed == 1)
                                                    <td class="status-moderation"><i class="circle"></i>&nbsp;
                                                        {{ trans('main.history.content.on_mod') }}.</td>
                                                @else
                                                    <td class="status-checked"><i class="circle"></i>&nbsp;
                                                        {{ trans('main.history.content.off_mod') }}.</td>
                                                @endif

                                                <td class="pointed-menu-wrapper">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            <img src="/img/icons/pointed-menu-vertical-icon.png">
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-right arrow_box">
                                                            <li class="item edit">
                                                                <a href="{{ route('edit-trackers', $trash->id_tracker) }}">{{ trans('main.custom.show') }}</a>
                                                            </li>
                                                            <li class="item reset">
                                                                <a href="{{ route('reset-trackers-user', $trash->id_tracker) }}">{{ trans('main.custom.reset') }}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;visibility: hidden;">
        <symbol id="arrowDownSVG" iewBox="0 0 12 8" fill="none">
            <rect x="9.89941" y="7.07129" width="8" height="2" transform="rotate(-135 9.89941 7.07129)" fill="#828282"></rect>
            <rect y="5.65723" width="8" height="2" transform="rotate(-45 0 5.65723)" fill="#828282"></rect>
        </symbol>

        <symbol  id="message" viewBox="0 0 49 57" >
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 455 455" style="enable-background:new 0 0 455 455;" xml:space="preserve">
                <path d="M287.5,320.572h-260c-6.893,0-12.5-5.607-12.5-12.5V81.928c0-6.893,5.607-12.5,12.5-12.5h335c6.893,0,12.5,5.607,12.5,12.5
        v151.144c0,4.142,3.357,7.5,7.5,7.5s7.5-3.358,7.5-7.5V81.928c0-15.164-12.337-27.5-27.5-27.5h-335
        c-15.163,0-27.5,12.336-27.5,27.5v226.144c0,15.164,12.337,27.5,27.5,27.5h260c4.143,0,7.5-3.358,7.5-7.5
        S291.643,320.572,287.5,320.572z" fill="#828282"></path>
                <path d="M357.674,86.499c-2.999-2.857-7.746-2.745-10.604,0.256L203.368,237.551c-2.24,2.352-5.213,3.647-8.368,3.647
        s-6.128-1.295-8.368-3.646L42.93,86.754c-2.859-2.999-7.604-3.112-10.604-0.256c-2.999,2.858-3.113,7.605-0.256,10.604
        l93.078,97.674l-93.09,98.134c-2.851,3.005-2.726,7.752,0.28,10.603c1.45,1.376,3.307,2.059,5.16,2.059
        c1.985,0,3.968-0.784,5.442-2.338l92.568-97.585l40.263,42.251c5.1,5.351,11.929,8.298,19.228,8.298s14.128-2.947,19.228-8.299
        l40.262-42.25l56.417,59.474c1.475,1.555,3.457,2.338,5.442,2.338c1.854,0,3.71-0.683,5.16-2.059
        c3.006-2.851,3.131-7.598,0.28-10.603l-56.939-60.024l93.078-97.674C360.787,94.104,360.673,89.356,357.674,86.499z" fill="#828282"/>
                <path d="M382.5,255.572c-39.977,0-72.5,32.523-72.5,72.5s32.523,72.5,72.5,72.5s72.5-32.523,72.5-72.5
        S422.477,255.572,382.5,255.572z M382.5,385.572c-31.706,0-57.5-25.794-57.5-57.5s25.794-57.5,57.5-57.5s57.5,25.794,57.5,57.5
        S414.206,385.572,382.5,385.572z" fill="#EB5757"></path>
                <path d="M385.37,296.143c-2.802-1.162-6.028-0.52-8.174,1.626l-10,10c-2.929,2.929-2.929,7.678,0,10.606
        c2.111,2.111,5.166,2.699,7.804,1.77v32.927c0,4.142,3.357,7.5,7.5,7.5s7.5-3.358,7.5-7.5v-50
        C390,300.038,388.173,297.304,385.37,296.143z " fill="#EB5757"></path>

            </svg>
        </symbol>

        <symbol  id="message_read" viewBox="0 0 49 57">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 31.012 31.012" style="enable-background:new 0 0 31.012 31.012;" xml:space="preserve">
                <path d="M25.109,21.51c-0.123,0-0.246-0.045-0.342-0.136l-5.754-5.398c-0.201-0.188-0.211-0.505-0.022-0.706
            c0.189-0.203,0.504-0.212,0.707-0.022l5.754,5.398c0.201,0.188,0.211,0.505,0.022,0.706C25.375,21.457,25.243,21.51,25.109,21.51z
            " fill="#828282"></path>
                <path d="M5.902,21.51c-0.133,0-0.266-0.053-0.365-0.158c-0.189-0.201-0.179-0.518,0.022-0.706l5.756-5.398
            c0.202-0.188,0.519-0.18,0.707,0.022c0.189,0.201,0.179,0.518-0.022,0.706l-5.756,5.398C6.148,21.465,6.025,21.51,5.902,21.51z" fill="#828282"/>
                <path d="M28.512,26.529H2.5c-1.378,0-2.5-1.121-2.5-2.5V6.982c0-1.379,1.122-2.5,2.5-2.5h26.012c1.378,0,2.5,1.121,2.5,2.5v17.047
        C31.012,25.408,29.89,26.529,28.512,26.529z M2.5,5.482c-0.827,0-1.5,0.673-1.5,1.5v17.047c0,0.827,0.673,1.5,1.5,1.5h26.012
        c0.827,0,1.5-0.673,1.5-1.5V6.982c0-0.827-0.673-1.5-1.5-1.5H2.5z" fill="#828282"></path>
                <path d="M15.506,18.018c-0.665,0-1.33-0.221-1.836-0.662L0.83,6.155C0.622,5.974,0.6,5.658,0.781,5.449
        c0.183-0.208,0.498-0.227,0.706-0.048l12.84,11.2c0.639,0.557,1.719,0.557,2.357,0L29.508,5.419
        c0.207-0.181,0.522-0.161,0.706,0.048c0.181,0.209,0.16,0.524-0.048,0.706L17.342,17.355
        C16.835,17.797,16.171,18.018,15.506,18.018z" fill="#828282"></path>
            </svg>
        </symbol>
    </svg>

@endsection

@include('modals.modal-print')
