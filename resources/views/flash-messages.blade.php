@if(Session::has('success-message'))

    <div class="row">
        <div class="col-md-12">

            <div class="alert alert-dismissible alert-success alert__custom alert-success__custom mt-4">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true"></i></button>
                <p class="alert__header"><svg style="width:28px; height:28px;"><use xlink:href="#success_SVG"></use></svg>{{trans('main.custom.inform')}}</p>
                <p class="alert__text">{{ Session::get('success-message') }}</p>
            </div>

        </div>
    </div>
@endif
@if(Session::has('danger-message'))
    <div class="row">
        <div class="col-md-12">

            <div class="alert alert-dismissible alert-warning alert__custom alert-warning__custom mt-4">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true"></i></button>
                <p class="alert__header"><svg style="width:26px; height:26px;"><use xlink:href="#warning_SVG"></use></svg>{{trans('main.custom.inform')}}</p>
                <p class="alert__text">{!! Session::get('danger-message') !!}</p>
            </div>

        </div>
    </div>
@endif

@if($errors->count())
    <div class="row">
        <div class="col-md-12">

            <div class="alert alert-dismissible alert-danger alert__custom alert-danger__custom mt-4">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true"></i></button>
                <p class="alert__header"><svg style="width:26px; height:26px;"><use xlink:href="#error_SVG"></use></svg>{{trans('main.custom.error')}}</p>
                <p class="alert__text">
                    @foreach ($errors->all() as $key => $value)
                        {{ $value }}
                    @endforeach
                </p>
            </div>

        </div>
    </div>

@endif
