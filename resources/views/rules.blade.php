@extends('layouts.app')

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/train_bckg.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                            <div class="pull-left" style="width: 65%;">
                                <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.rules.p') }}</p>
                                <h1>{{ trans('main.rules.h1') }}</h1>
                                <h5 class="subtitle">{{ trans('main.rules.h5') }}</h5>
                                <div class="btn-group-block">
                                    <a href="/about-us" class="btn btn-default-new">{{ trans('main.rules.link2') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('content')

    {{--@include('include.after-hero-block')--}}

    <div class="wrap">
        <div class="contacts-page">
            <div class="container">
                <div class="col-sm-4">
                    <div class="page-header-block">
                        <p>{{ trans('main.rules.content.p') }}</p>
                        <h3>{{ trans('main.rules.content.h3') }}</h3>
                    </div>
                </div>
                <div class="col-sm-8">

                    Грузоотправитель согласен с Общими условиями перевозки-- //ссылка//-- FHD. Если в накладной
                    не указано Объявленная стоимость для перевозки, то будут применяться ограничения
                    ответственности, определенные Варшавской Конвенцией или КДПГ (если применимо) или
                    указанные в п-- //пункт//-- . Сроки для предоставления любых претензий (исков) определяются в п-
                    -//пункт//- - Условий перевозки. Грузоотправитель уполномочивает FHD действовать в качестве
                    экспедитора для экспортного контроля и таможенных целей. Грузоотправитель дает согласие ООО
                    «Ф. Х. Д. Сервис» на обработку своих персональных данных и предоставление доступа к
                    персональным данным третьим особам с целью предоставления мне ООО «Ф. Х. Д. Сервис» услуг
                    и подтверждает, что получил уведомление про включение информации о грузоотправителе в базу
                    данных и получил разъяснение про свои права, как субъекта персональных данных согласно ст. 8
                    Закона Украины «Про защиту персональных данных». Под ООО «Ф. Х. Д. Сервис» имеется в виду
                    общество с ограниченной ответственностью «Ф. Х. Д. Сервис» и/или его партнеры (агенты,
                    представители и так далее), которые имеют право использовать знак для товаров и услуг,
                    регистрация которого подтверждается свидетелем Государственной службы интеллектуальной
                    собственности на знак для товаров и услуг -- //номер знака//-- , зарегистрированного в
                    Государственном реестре свидетельств Украины на знаки для товаров и услуг от -- //дата
                    регистрации//-- .

                </div>
            </div>
        </div>
    </div>

@endsection
