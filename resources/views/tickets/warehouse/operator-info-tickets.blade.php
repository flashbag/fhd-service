<div class="operator-info operator-info_padding">
    <div class="operator-info__head">
        <div class="operator-info__image">
            <img src="https://ichef.bbci.co.uk/news/912/mcs/media/images/80258000/jpg/_80258798_avatar_getty.jpg" alt="operator-avatar">
        </div>
        <div>
            <p class="operator-info__name">{{Auth::user()->name}}</p>
            <p class="operator-info__id">id <span>{{Auth::user()->id_client}}</span></p>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <p class="operator-info__title">{{Auth::user()->printRole()->name}}</p>
    </div>
    @if(!Auth::user()->isUser())
    <ul class="operator-info__list">
        <li class="operator-info__list_gray"><span>Не отобрано</span><span class="counter">23</span></li>
        <li class="operator-info__list_green"><span>Отобрано</span><span class="counter">11</span></li>
        <li class="operator-info__list_orange"><span>Оплачено</span><span class="counter">34</span></li>
    </ul>
    @endif
    <div class="d-flex justify-content-center hidden">
        <p class="operator-info__time">
            <span>Время сесии</span><span>11:11</span>
        </p>
    </div>
</div>