@extends('layouts.app')

@section('content')

    <div class="trackers-history-page">
        <div class="container-fluid">

            @include('flash-messages')

            <div class="row position-relative z-index-0 h-100">

                @include('tickets.warehouse.left-menu-tickets')

                <div class="col-xs-12 col-sm-12 col-lg-10 h-100">
                    @if(!Auth::user()->isUser())
                        <div class="row pt-5 px-4 pb-1">
                            <div class="col-xs-12 col-sm-12 col-md-4 mb-md-4">
                                <div class="input__custom_search">
                                    <input type="search" name="search_name" class="form-control input__custom" placeholder="ФИО">
                                    <button class="btn-search btn-search_send"></button>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 mb-md-4">
                                <div class="input__custom_search">
                                    <input type="search" name="search_id" class="form-control input__custom" placeholder="id">
                                    <button class="btn-search btn-search_send"></button>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 mb-md-4">
                                <div class="form-group">
                                    <input type="text" id="js-custom-input-date" name="search_date" class="form-control input__custom input__custom-date" placeholder="dd.mm.yyyy">
                                    <p class="messages"></p>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div>

                        <!-- Nav tabs -->

                        <div class="history-section__tab">

                            <div class="history-section__header" role="tab" id="collapseMessage">
                                <a class="history-section_list" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTabs" aria-expanded="false" aria-controls="collapseTabs">
                                    {{ trans('main.ticket.new') }}
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                            </div>

                            <div id="collapseTabs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseMessage">
                                <ul class="row nav nav-tabs__custom">
                                    <li class="col-xs-12 col-lg-4 nav-tabs__item nav-tabs__item_border">
                                        <a class="nav-tabs__tab" href="#new" data-toggle="tab">{{ trans('main.ticket.new') }}</a>
                                    </li>
                                    <li class="col-xs-12 col-lg-4 nav-tabs__item nav-tabs__item_border-y active">
                                        <a class="nav-tabs__tab" href="#messages" data-toggle="tab">{{ trans('main.ticket.all') }}</a>
                                    </li>
                                    <li class="col-xs-12 col-lg-4 nav-tabs__item nav-tabs__item_border">
                                        <a class="nav-tabs__tab" href="#archive" data-toggle="tab">{{ trans('main.ticket.archive') }}</a>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <!-- tabs Content -->
                        <div class="tab-content">
                            <div class="tab-pane" id="new">
                                <div class="row py-5 px-4">
                                    @foreach($new_tickets as $ticket)
                                        @if(\App\Comment::where('ticket_id', $ticket->id)->latest()->first()->user->id != Auth::user()->id)
                                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-20 mb-5">
                                                <a class="tickets__message answer" href="{{ route('warehouse.tickets.show', $ticket->id_tracker) }}">
                                                    <p class="tickets__date">{{$ticket->updated_at}}</p>
                                                    <div class="tickets__message_flex">
                                                        <p class="tickets__message_font">
                                                            @if(isset($ticket->cargos[0]->description))
                                                                <span class="js-ToCutOff">{{$ticket->cargos[0]->description}}</span>
                                                            @else
                                                                <span class="js-ToCutOff"></span>
                                                            @endif
                                                            <span>id {{$ticket->id_tracker}}</span>
                                                        </p>
                                                        <span class="counter counter_red">{{count(\App\Comment::where('ticket_id', $ticket->id)->get())}}</span>
                                                    </div>
                                                </a>
                                            </div>
                                            @else
                                                {{ trans('main.ticket.not_new') }}
                                                @break
                                            @endif
                                    @endforeach
                                    {!! $new_tickets->appends(\Request::except('page'))->render() !!}
                                </div>
                            </div>
                            <div class="tab-pane fade active in" id="messages">
                                <div class="row py-5 px-4">
                                    @if(isset($all_tickets))
                                    @foreach($all_tickets as $ticket)
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-20 mb-5">
                                            <a class="tickets__message answer"  href="{{ route('warehouse.tickets.show', $ticket->id_tracker) }}">
                                                <p class="tickets__date">{{$ticket->created_at}}</p>
                                                <div class="tickets__message_flex">
                                                    <p class="tickets__message_font">
                                                        @if(isset($ticket->cargos[0]->description))
                                                            <span class="js-ToCutOff">{{$ticket->cargos[0]->description}}</span>
                                                        @else
                                                            <span class="js-ToCutOff"></span>
                                                        @endif
                                                        <span>id {{$ticket->id_tracker}}</span>
                                                    </p>
                                                    @if($ticket->status == 0)
                                                        @if(\App\Comment::where('ticket_id', $ticket->id)->latest()->first()->user->id == Auth::user()->id)
                                                            <span class="counter counter_white">{{count(\App\Comment::where('ticket_id', $ticket->id)->get())}}</span>
                                                        @else
                                                            <span class="counter counter_red">{{count(\App\Comment::where('ticket_id', $ticket->id)->get())}}</span>
                                                        @endif
                                                    @else
                                                        <span class="counter counter_grey">{{count(\App\Comment::where('ticket_id', $ticket->id)->get())}}</span>
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                    @else
                                        {{ trans('main.ticket.not_all') }}
                                    @endif
                                    {{-- Pagination--}}
                                    {!! $all_tickets->appends(\Request::except('page'))->render() !!}
                                    {{-- Pagination--}}
                                </div>
                            </div>
                            <div class="tab-pane" id="archive">
                                <div class="row py-5 px-4">
                                    @foreach($all_tickets as $ticket)
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-20 mb-5">
                                            <a class="tickets__message answer" href="{{ route('warehouse.tickets.show', $ticket->id_tracker) }}">
                                                <p class="tickets__date">{{$ticket->created_at}}</p>
                                                <div class="tickets__message_flex">
                                                    <p class="tickets__message_font">
                                                        @if(isset($ticket->cargos[0]->description))
                                                            <span class="js-ToCutOff">{{$ticket->cargos[0]->description}}</span>
                                                        @else
                                                            <span class="js-ToCutOff"></span>
                                                        @endif
                                                        <span>id {{$ticket->id_tracker}}</span>
                                                    </p>
                                                    @if($ticket->status == 0)
                                                        @if(\App\Comment::where('ticket_id', $ticket->id)->latest()->first()->user->id == Auth::user()->id)
                                                            <span class="counter counter_white">{{count(\App\Comment::where('ticket_id', $ticket->id)->get())}}</span>
                                                        @else
                                                            <span class="counter counter_red">{{count(\App\Comment::where('ticket_id', $ticket->id)->get())}}</span>
                                                        @endif
                                                    @else
                                                        <span class="counter counter_grey">{{count(\App\Comment::where('ticket_id', $ticket->id)->get())}}</span>
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                    {{-- Pagination--}}
                                    {!! $all_tickets->appends(\Request::except('page'))->render() !!}
                                    {{-- Pagination--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="/js/frontend/tickets.js" ></script>
@endpush
@stack('script')
