@extends('layouts.app')


@section('content')

    <div class="trackers-history-page">
        <div class="container-fluid">
            <div class="row">
                @include('tickets.warehouse.left-menu-tickets')

                <div class="col-xs-12 col-sm-12 col-lg-10 px-0">
                    <div class="ticket-single ticket-single_flex">
                        <div class="ticket-single_col ticket-single__head">

                            <div class="ticket-single__head_col">
                                <div class="d-flex align-item-center">
                                    <a href="{{ route('warehouse.tickets.index') }}" class="btn-back ticket-single__back">
                                        <i class="fa fa-2x fa-angle-left" aria-hidden="true"></i>
                                        Назад
                                    </a>
                                    <span class="ticket-single__integer">{{$tracker->id_tracker}}</span>
                                </div>
                            </div>
                            <div class="ticket-single__head_col">
                                <div class="ticket-single__id text-right">
                                    @foreach($user_mess as $user)
                                        <span>{{ \App\User::where('id', $user)->first()->name }}</span>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div id="js-scroll-ticket-page" class="ticket-single_col ticket-single__body">
                            @foreach ($comments as $comment)
                                @if(Auth::user()->id == $comment->user_id)
                                    <div class="chat__row chat__row_right">
                                @else
                                    <div class="chat__row chat__row_left">
                                @endif
                                    <div class="chat__inner">
                                        @if(Auth::user()->id == $comment->user_id)
                                            <div class="chat__message chat__message_operator">
                                                <div class="chat__text chat__text_white">
                                        @else
                                            <div class="chat__message chat__message_client">
                                                <div class="chat__text chat__text_black">
                                        @endif
                                                @for($i = 0; $i < count($comment->file); $i++)
                                                    @if(pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) == "png" ||
                                                        pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) == "jpg" ||
                                                        pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) == "jpeg" ||
                                                        pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) =="gif")
                                                            <img src = " {{ asset('/storage/' . $comment->file[$i]->link ) }}" />
                                                            <a class="input-file__chat mb-2 js-typeFile" href="{{ asset('/storage/' . $comment->file[$i]->link ) }}" target="_blank"></a>
                                                    @else
{{--                                                        <img src=" {{asset('/img/icons/file.png')}}" height="20" weight="20">--}}
                                                            <a class="input-file__chat mb-2 js-typeFile" href="/download/file/{{$comment->file[$i]->link}}"> {{$comment->file[$i]->link}}</a>
                                                    @endif
                                                @endfor
                                                {!! $comment->text  !!}
                                            </div>
                                        </div>
                                        <div class="chat__client-info">
                                            <div class="inner">
                                                @if($comment->user->id != Auth::user()->id)
                                                <div class="chat__client-name">{{ $comment->user->name }}</div>
                                                @else

                                                @endif
                                            </div>
                                            @if(\Carbon\Carbon::now() > $comment->created_at->addHours(24))
                                                <div class="date">{{ $comment->created_at->format('m-d H:i') }}</div>
                                            @else
                                                <div class="date">{{ $comment->created_at->diffForHumans() }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="ticket-single_col ticket-single__footer">
                            <div class="comment-form">
                                <form enctype="multipart/form-data"  action="{{ url('comment') }}" method="POST" class="form">
                                    {!! csrf_field() !!}

                                    <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">

                                    <div class="comment-form__wrapp">
                                        <div class="comment-form__file">

                                            <div class="form-group form-group__input-file-chat">
                                                <label class="input-file-chat" for="input-file">
                                                    <svg viewBox="0 0 41 41" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M21.2132 4.43577C16.3479 4.43578 12.4031 8.3806 12.4031 13.2458L12.4031 31.6652C12.4031 35.3157 15.3605 38.2731 19.011 38.2732C22.0199 38.2732 24.5319 36.247 25.3293 33.4967C25.5053 33.3021 25.6174 33.0498 25.619 32.7678L25.619 14.3485C25.619 11.9159 23.6474 9.94429 21.2148 9.94429C18.7822 9.94429 16.8105 11.9159 16.8105 14.3485L16.8105 28.3636C16.8105 28.971 17.3042 29.4647 17.9115 29.4647C18.5189 29.4646 19.0126 28.971 19.0126 28.3636L19.0126 14.3485C19.0126 13.1307 19.9984 12.1449 21.2148 12.1464C22.4327 12.1464 23.4185 13.1322 23.4169 14.3485L23.4169 31.6683C23.4169 34.1009 21.4452 36.0726 19.0126 36.0726C16.58 36.0726 14.6084 34.1009 14.6084 31.6683L14.6052 13.2458C14.6052 9.59539 17.5627 6.63793 21.2132 6.63786C24.8637 6.63786 27.8211 9.59532 27.8212 13.2458L27.8212 29.4631C27.8212 30.0705 28.3149 30.5642 28.9222 30.5641C29.5297 30.5641 30.0233 30.0704 30.0233 29.4631L30.0233 13.2458C30.0233 8.3806 26.0785 4.43577 21.2132 4.43577Z"/>
                                                    </svg>
                                                </label>
                                                <input id="input-file" type="file" multiple name="file[]">
                                            </div>
                                        </div>

{{--                                        <input type="hidden" name="ticket_id" value="">--}}
                                        <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }} comment-form__note-editor">
                                            <textarea rows="10" id="summernote" class="summernote" name="comment"></textarea>

                                            @if ($errors->has('comment'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('comment') }}</strong>
                                                </span>
                                            @endif

                                            <button type="submit" class="btn-send btn-send_pr ticket-single__btn-send">
                                                Отправить
                                                <svg viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M17.9084 0.0681402C17.8282 -0.00357541 17.7142 -0.0202139 17.6176 0.0255282L0.155693 8.30478C0.0616842 8.34937 0.00114611 8.44508 1.60703e-05 8.55085C-0.00111397 8.65661 0.057433 8.75364 0.150527 8.80026L5.09221 11.2751C5.18283 11.3205 5.29088 11.3102 5.37181 11.2485L10.1765 7.58395L6.40462 11.5431C6.35081 11.5996 6.32336 11.677 6.32928 11.7555L6.70483 16.7465C6.71317 16.8571 6.78582 16.9516 6.88909 16.9863C6.91669 16.9955 6.94505 17 6.97308 17C7.04998 17 7.12478 16.9663 7.17682 16.9048L9.7997 13.8033L13.0421 15.3842C13.1125 15.4185 13.194 15.42 13.2655 15.3882C13.337 15.3564 13.3915 15.2946 13.4148 15.2187L17.9876 0.356815C18.0196 0.25303 17.9884 0.139856 17.9084 0.0681402Z" fill="white"/>
                                                </svg>
                                            </button>
                                        </div>

                                        {{-- mobile btn--}}
                                        <button type="submit" class="btn-send btn-send_pr ticket-single__btn-send-mobile">
                                            Отправить
                                            <svg viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M17.9084 0.0681402C17.8282 -0.00357541 17.7142 -0.0202139 17.6176 0.0255282L0.155693 8.30478C0.0616842 8.34937 0.00114611 8.44508 1.60703e-05 8.55085C-0.00111397 8.65661 0.057433 8.75364 0.150527 8.80026L5.09221 11.2751C5.18283 11.3205 5.29088 11.3102 5.37181 11.2485L10.1765 7.58395L6.40462 11.5431C6.35081 11.5996 6.32336 11.677 6.32928 11.7555L6.70483 16.7465C6.71317 16.8571 6.78582 16.9516 6.88909 16.9863C6.91669 16.9955 6.94505 17 6.97308 17C7.04998 17 7.12478 16.9663 7.17682 16.9048L9.7997 13.8033L13.0421 15.3842C13.1125 15.4185 13.194 15.42 13.2655 15.3882C13.337 15.3564 13.3915 15.2946 13.4148 15.2187L17.9876 0.356815C18.0196 0.25303 17.9884 0.139856 17.9084 0.0681402Z" fill="white"/>
                                            </svg>
                                        </button>
                                        {{-- mobile btn--}}

                                        <div class="form-group comment-form__nav">

                                            <div id="js-btn-send-to-email" class="btn-send-to-email" data-toggle="modal" data-target="#modalSendToEmail">
                                                <svg viewBox="0 0 30 23" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M29.6865 3.12944L28.4965 1.93945L17.4735 12.9624C16.0956 14.3403 13.7783 14.3403 12.4004 12.9624L1.37748 2.00209L0.1875 3.19206L8.39209 11.3967L0.1875 19.6013L1.37748 20.7912L9.58207 12.5866L11.2105 14.215C12.2126 15.2171 13.5278 15.7808 14.9057 15.7808C16.2835 15.7808 17.5988 15.2171 18.6009 14.215L20.2293 12.5866L28.4338 20.7912L29.6238 19.6013L21.4192 11.3967L29.6865 3.12944Z"></path>
                                                    <path d="M27.3695 22.9227H2.63048C1.18998 22.9227 0 21.7328 0 20.2923V2.63048C0 1.18998 1.18998 0 2.63048 0H27.3695C28.81 0 30 1.18998 30 2.63048V20.2923C30 21.7328 28.81 22.9227 27.3695 22.9227ZM2.56785 1.69102C2.0668 1.69102 1.69103 2.0668 1.69103 2.56784V20.2296C1.69103 20.7307 2.0668 21.1065 2.56785 21.1065H27.3069C27.8079 21.1065 28.1837 20.7307 28.1837 20.2296V2.56784C28.1837 2.0668 27.8079 1.69102 27.3069 1.69102H2.56785Z"></path>
                                                </svg>
                                            </div>
                                            <div id="js-btn-clear-editor" class="btn-clear-editor">
                                                <svg viewBox="0 0 32 27" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M27.2082 1.00078L4.79185 1C2.70105 1 1 2.70105 1 4.79185V19.6345C1 21.7253 2.70105 23.4263 4.79185 23.4263H6.53745L5.94297 25.7629C5.87159 26.0437 5.9681 26.3406 6.19105 26.5258C6.32851 26.6399 6.49881 26.6989 6.6708 26.6989C6.77793 26.6989 6.88565 26.6759 6.98657 26.6292L13.8928 23.4263L27.2082 23.4271C29.299 23.4271 31 21.726 31 19.6352V4.79263C31 2.70173 29.299 1.00078 27.2082 1.00078ZM29.4983 19.6352C29.4983 20.898 28.4709 21.9254 27.2082 21.9254L13.7271 21.9246C13.618 21.9246 13.5101 21.9484 13.4112 21.9943L7.78788 24.6022L8.23099 22.8606C8.28816 22.636 8.2383 22.3975 8.09604 22.2145C7.95377 22.0315 7.73502 21.9246 7.50326 21.9246H4.79185C3.5291 21.9246 2.50172 20.8972 2.50172 19.6345V4.79185C2.50172 3.5291 3.5291 2.50172 4.79185 2.50172L27.2082 2.5025C28.4709 2.5025 29.4983 3.52988 29.4983 4.79263V19.6352Z" stroke-width="0.4"></path>
                                                    <path class="stroke" d="M20.6016 7.77393L12.2682 16.1073" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <path class="stroke" d="M20.6016 16.0234L12.2682 7.6901" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                                </svg>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-send-to-email')

@endsection
@push('script')
    <script type="text/javascript" src="/js/frontend/ticket-single.js"></script>
@endpush
@stack('script')