@extends('layouts.app')

@section('title', $ticket->title)
@section('textarea')
    <!-- include summernote css/js-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $ticket->id_tracker }}
                </div>

                <div class="panel-body">

                    <div class="ticket-info">
                        <p>Трекер: {{ $tracker->id_tracker }}</p>
                        <p>
                            @if ($ticket->status === 1)
                                Статус: <span class="label label-success">Прочитано</span>
                            @else
                                Статус: <span class="label label-danger">Непрочитано</span>
                            @endif
                        </p>
                        <p>Создан: {{ $ticket->created_at->diffForHumans() }}</p>
                        <p>Сообщений: {{ count($comments) }}</p>
                        <p>Отправить переписку: </p>
                        <form id="send-ticket" action="{{ route('send-ticket', $ticket->trackers->id_tracker) }}"
                              method="POST" >
                            {{ csrf_field() }}
                            {{--<select>--}}
                                {{--<option name = "send_email" value="ayaroshchuk43@gmail.com">Отдел работы с клиентами</option>--}}
                                {{--<option name = "send_email" value="ayaroshchuk43@gmail.com">Юридический отдел</option>--}}
                                {{--<option name = "send_email" value="ayaroshchuk43@gmail.com">Технический отдел</option>--}}
                            {{--</select><br>--}}
                            <input type="text" name = "send_email" required><br>
                            <button type="submit" class="btn btn-info">Отправить</button>
                        </form>
                    </div>


                    <hr>
                    <div class="comments">
                        @foreach ($comments as $comment)
                            <div class="panel panel-@if($user === $comment->user_id) {{"default"}}@else{{"success"}}@endif">
                                <div class="panel panel-heading">
                                    {{ $comment->user->name }}
                                    <span class="pull-right">{{ $comment->created_at->format('Y-m-d H:i') }}</span>
                                </div>

                                <div class="panel panel-body">
                                    @for($i = 0; $i < count($comment->file); $i++)
                                        @if(pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) == "png" ||
                                            pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) == "jpg" ||
                                            pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) == "jpeg" ||
                                            pathinfo($comment->file[$i]->link, PATHINFO_EXTENSION) =="gif")
                                            <a href="{{ asset('/storage/' . $comment->file[$i]->link ) }}" target="_blank"> <img src = " {{ asset('/storage/' . $comment->file[$i]->link ) }}" height="200" weight="400" /></a>
                                        @else
                                            <img src=" {{asset('/img/icons/file.png')}}" height="20" weight="20"><a href="/download/file/{{$comment->file[$i]->link}}"> {{$comment->file[$i]->link}}</a>
                                        @endif
                                        <br>
                                        <br>
                                    @endfor
                                    <br>
                                        <br>
                                    {!! $comment->text  !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="comment-form">
                        <form enctype="multipart/form-data"  action="{{ url('comment') }}" method="POST" class="form">
                            {!! csrf_field() !!}

                            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">

                            <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                <textarea rows="10" id="summernote" class="summernote" name="comment"></textarea>

                                @if ($errors->has('comment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <p><input type="file" multiple name="file[]"></p>
                        <div class="form-group pull-right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="pull-left">
                        <a href="javascript:history.back()" class="btn btn-default">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            $('.summernote').summernote();
        });
    </script>
@endpush
