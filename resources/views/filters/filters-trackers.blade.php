<div>

    @if(!Auth::user()->isCustom())

    @else
{{--  Active Filters--}}
            <div class="py-4">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFilters" class="btn btn-success">
                    Фильтр
                </a>
            </div>
{{--  Active Filters--}}
    @endif


	<div class="panel position-relative z-index-0">
		<div id="collapseFilters"  class="panel-body collapse">
         @if(!Auth::user()->isCustom())
		    <form id="filters-form" class="form-inline" action="{{ url()->current() }}" method="POST">

            {{ csrf_field() }}

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.tracker_number') }}</span>
                        </div>
                        <input type="text" class="form-control" name="id_tracker" value="{{ old('id_tracker') }}">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.systemic') }} ID</span>
                        </div>
                        <input type="text" class="form-control" name="id" value="{{ old('id') }}">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2 {{ $errors->has('date_registered') ? ' has-error' : '' }}">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.from_date') }}</span>
                        </div>
                        <input type="text" class="form-control" id="inputStartDateRegistered" name="start_date_registered" value="">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2 {{ $errors->has('date_registered') ? ' has-error' : '' }}">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon2">{{ trans('admin.to_date') }}</span>
                        </div>
                        <input type="text" class="form-control" id="inputEndDateRegistered" name="end_date_registered" value="">
                    </div>
                </div>

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.type_transport') }}</span>
                        </div>
                        <select class="custom-select form-control" name="type_transport" style="max-width: 250px">
                            <option>--</option>
                            @foreach ( $type_transport as $key => $type)
                                <option value="{{ $type->id }}" {{  old('type_transport', $type->id ) != $type->id ? 'selected' : '' }}>{{ trans('admin.transport.'. $type->type) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.status') }}</span>
                        </div>
                        <select class="custom-select form-control" name="statuses" style="max-width: 250px">
                            <option>--</option>
                            @foreach ($statuses as $key => $status)
                                <option value="{{ $status->id }}" {{  old('statuses', $status->id ) != $status->id ? 'selected' : '' }}>{{ $status->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.invoice_number') }}</span>
                        </div>
                        <input type="text" class="form-control" name="number_invoice" value="{{ old('number_invoice') }}">
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.carrier') }}</span>
                        </div>
                        <input type="text" class="form-control" name="carrier" value="{{ old('carrier') }}">
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.container_number') }}</span>
                        </div>
                        <input type="text" class="form-control" name="container_id" value="{{ old('container_id') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2 sender-info">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_country') }}</span>
                        </div>
                        <div class="typehead typehead-google-address typehead-google-address-country">
                            <input type="text" class="form-control"
                                   name="country_id_sender"
                                   id="inputCountrySenderId"
                                   data-city-selector="#inputCitySenderId"
                                   data-address-selector="#inputAddressSenderId"
                                   value="{{ old('country_id_sender') }}"
                            >
                            <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                            <ul class="typehead-list"></ul>
                        </div>
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2 sender-info">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_city') }}</span>
                        </div>
                        <div class="typehead typehead-google-address typehead-google-address-city">
                            <input type="text" class="form-control"
                                   name="city_id_sender"
                                   id="inputCitySenderId"
                                   data-country-selector="#inputCountrySenderId"
                                   data-address-selector="#inputAddressSenderId"
                                   value="{{ old('city_id_sender') }}"
                            >
                            <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                            <ul class="typehead-list"></ul>
                        </div>
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_fullname') }}</span>
                        </div>
                        <input class="form-control" id="inputSenderFullName" name="sender_full_name"
                               type="text" value="{{ old('country_id_recipient') }}">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">Email отправителя</span>
                        </div>
                        <input type="text" class="form-control"  name="sender_email" value="{{ old('sender_email') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_phone') }}</span>
                        </div>
                        <input type="text" class="form-control"  name="sender_number" value="{{ old('sender_number') }}">
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_phone') }}</span>
                        </div>
                        <input type="text" class="form-control" name="sender_company_name" value="{{ old('sender_company_name') }}">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2 recipient-info">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.recipient_country') }}</span>
                        </div>
                        <div class="typehead typehead-google-address typehead-google-address-country">
                            <input type="text" class="form-control"
                                   name="country_id_recipient"
                                   id="inputCountryRecipientId"
                                   data-city-selector="#inputCityRecipientId"
                                   data-address-selector=".inputAddressRecipient"
                                   value="{{ old('country_id_recipient') }}">
                            <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                            <ul class="typehead-list"></ul>
                        </div>
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2 recipient-info">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.recipient_city') }}</span>
                        </div>
                        <div class="typehead typehead-google-address typehead-google-address-city">
                            <input type="text" class="form-control inputCityRecipient"
                                   name="city_id_recipient"
                                   id="inputCityRecipientId"
                                   data-country-selector="#inputCountryRecipientId"
                                   data-address-selector="#inputAddressRecipientId"
                                   value="{{ old('city_id_recipient') }}"
                            >
                            <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                            <ul class="typehead-list"></ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.recipient_fullname') }}</span>
                        </div>
                        <input class="form-control" id="inputRecipientFullName" name="recipient_full_name" type="text">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">Email получателя</span>
                        </div>
                        <input type="text" class="form-control"  name="recipient_email" value="{{ old('recipient_email') }}">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.recipient_phone') }}</span>
                        </div>
                        <input type="text" class="form-control" name="recipient_number" value="{{ old('recipient_number') }}">
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.recipient_company') }}</span>
                        </div>
                        <input type="text" class="form-control" name="recipient_company_name" value="{{ old('recipient_company_name') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2" style="display: none">

                        <button type="submit" class="btn btn-success">{{ trans('admin.filtrate') }}</button>
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">

                        <button type="reset" class="btn">{{ trans('admin.clear_filter') }}</button>
                    </div>
                </div>

        </form>
         @else
            <form id="filters-form" class="form-inline" action="{{ url()->current() }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.tracker_number') }}</span>
                        </div>
                        <input type="text" class="form-control" name="id_tracker" value="{{ old('id_tracker') }}">
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2 {{ $errors->has('date_registered') ? ' has-error' : '' }}">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.from_date') }}</span>
                        </div>
                        <input type="text" class="form-control" id="inputStartDateRegistered" name="start_date_registered" value="">
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2 {{ $errors->has('date_registered') ? ' has-error' : '' }}">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon2">{{ trans('admin.to_date') }}</span>
                        </div>
                        <input type="text" class="form-control" id="inputEndDateRegistered" name="end_date_registered" value="">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.status') }}</span>
                        </div>
                        <select class="custom-select form-control" name="statuses" style="max-width: 250px">
                            <option>--</option>
                            @foreach ($statuses as $key => $status)
                                <option value="{{ $status->id }}" {{  old('statuses', $status->id ) != $status->id ? 'selected' : '' }}>{{ $status->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.invoice_number') }}</span>
                        </div>
                        <input type="text" class="form-control" name="number_invoice" value="{{ old('number_invoice') }}">
                    </div>

                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_country') }}</span>
                        </div>
{{--                        <div class="typehead typehead-google-address typehead-google-address-country">--}}
                            <input type="text" class="form-control"
                                   name="country_id_sender"
{{--                                   id="inputCountrySenderId"--}}
{{--                                   data-city-selector="#inputCitySenderId"--}}
{{--                                   data-address-selector="#inputAddressSenderId"--}}
                                   value="{{ old('country_id_sender') }}"
                            >
                            <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                            <ul class="typehead-list"></ul>
{{--                        </div>--}}
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_city') }}</span>
                        </div>
{{--                        <div class="typehead typehead-google-address typehead-google-address-city">--}}
                            <input type="text" class="form-control"
                                   name="city_id_sender"
{{--                                   readonly="readonly"--}}
{{--                                   id="inputCitySenderId"--}}
{{--                                   data-country-selector="#inputCountrySenderId"--}}
{{--                                   data-address-selector="#inputAddressSenderId"--}}
                                   value="{{ old('city_id_sender') }}"
                            >
                            <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                            <ul class="typehead-list"></ul>
{{--                        </div>--}}
                    </div>
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.sender_fullname') }}</span>
                        </div>
                        <input class="form-control" id="inputSenderFullName" name="sender_full_name"
                               type="text" value="{{ old('country_id_recipient') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="input-group input-group_width mb-2 mr-2">
                        <div class="input-group-prepend mb-1">
                            <span class="input-group-text" id="basic-addon1">{{ trans('admin.recipient_fullname') }}</span>
                        </div>
                        <input class="form-control" id="inputRecipientFullName" name="recipient_full_name" type="text">
                    </div>
                </div>

                <div class="row">
{{--                    <div class="input-group input-group_width mb-2 mr-2">--}}

{{--                        <button type="submit" class="btn btn-success">{{ trans('admin.filtrate') }}</button>--}}
{{--                    </div>--}}
                    <div class="input-group mb-2 mr-2">

                        <button type="reset" class="btn">{{ trans('admin.clear_filter') }}</button>
                    </div>
                </div>

            </form>
         @endif
	</div>
	</div>

</div>
