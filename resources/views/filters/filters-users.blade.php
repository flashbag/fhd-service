<div class="collapse" id="filters">
	<br>
	<br>
	<br>

	<form id="filters-form" class="form-horizontal" action="{{ url()->current() }}" method="POST">

		{{ csrf_field() }}

		<div class="row">
			<div class="col-md-2 form-group">
				<label> ID </label>
				<input type="text" class="form-control" name="user_id" value="">
			</div>
			<div class="col-md-2 form-group">
				<label> Телефон </label>
				<input type="text" class="form-control" name="user_number" value="">
			</div>
			<div class="col-md-2 form-group">
				<label> Email </label>
				<input type="text" class="form-control" name="user_email" value="">
			</div>

			<div class="col-md-2 form-group">
				<label> ФИО </label>
				<input type="text" class="form-control" name="user_name" value="">
			</div>

			<div class="col-md-2 form-group">
				<label for="roleId">Роль пользователя</label>
				<select id="roleId" class="form-control selectpicker role_id" name="role_id">
					<option value=""> - </option>
					@foreach($roles as $role)
						<option value="{{ $role->id }}">{{ $role->name }}</option>
					@endforeach
				</select>
			</div>

			<div class="col-md-2 form-group">
				<button type="reset" class="btn">Очистить фильтр</button>
			</div>
		</div>


	</form>


</div>
