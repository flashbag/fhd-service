@extends('layouts.app')

@section('content')

	<div class="wrap simple-page create-trackers-wrapper card position-center create-trakers-card">
		<div class="card-header">
            {{ trans('trackers.edit_tracker') }}

		</div>
        <span class="color-red">*</span> <span>  {{ trans('trackers.fill_form_lang') }}</span>
        <br>
        <br>
		<div class="card-body">

			<form method="post" id="tracker-form"
				  class="default-form form-with-steps ajax-form"
				  data-ukraine-value="{{ __('main.ukraine') }}"
				  action="{{ route('update-trackers', $tracker->id_tracker) }}"
				  enctype="multipart/form-data">

				<div id="tracker-form-total">

{{--					@include('tracker-parts.steps-header')--}}

					{{ csrf_field() }}

					@include('tracker-parts.fields-to-fill')

					@if(count($historyStatuses) > 0 && $historyStatuses !== null)
						<br>
						<div class="panel panel-default">
							<div class="panel-heading">История изменения статусов</div>
							<table class="table table-bordered table-tracker">
								<tr>
                                    <th>{{ trans('admin.status') }}</th>
                                    <th>{{ trans('admin.who_installed') }}</th>
                                    <th>{{ trans('admin.date') }}</th>
								</tr>
								@foreach($historyStatuses as $status)
									<tr>
										<td>{{ trans('admin.status') }}: {{ $status->status ? $status->status->title : '' }}</td>
										<td>{{ App\User::find($status->id_user)->name }}</td>
										<td>{{ $status->created_at }}</td>
									</tr>
								@endforeach
							</table>
						</div>
					@endif

					@include('tracker-parts.receptacles-blocks')

					@include('tracker-parts.sender-info')

					@include('tracker-parts.recipient-info')

					@include('tracker-parts.additional')

					@include('tracker-parts.submit-buttons')

				</div>
			</form>

		</div>

	</div>

@endsection