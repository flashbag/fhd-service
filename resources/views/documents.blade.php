@extends('layouts.app')

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('http://cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                            <div class="pull-left" style="width: 65%;">
                                <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.documents.p') }}</p>
                                <h1>{{ trans('main.documents.h1') }}</h1>
                                <h5 class="subtitle">{{ trans('main.documents.h5') }}</h5>
                                <div class="btn-group-block">
                                    <a href="/about-us" class="btn btn-default-new">{{ trans('main.documents.link2') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('content')

    {{--@include('include.after-hero-block')--}}

    <div class="wrap">
        <div class="contacts-page">
            <div class="container">
                <div class="col-sm-4">
                    <div class="page-header-block">
                        <p>{{ trans('main.documents.content.p') }}</p>
                        <h3>{{ trans('main.documents.content.h3') }}</h3>
                    </div>
                </div>
                <div class="col-sm-8">

                    <h4>{{ trans('main.documents.content.h4_1') }}</h4>
                    <br>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        {{ trans('main.documents.content.panel1_1.a') }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">{{ trans('main.documents.content.panel1_1.desc') }}</div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        {{ trans('main.documents.content.panel2_1.a') }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">{{ trans('main.documents.content.panel2_1.desc') }}</div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        {{ trans('main.documents.content.panel3_1.a') }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    {{ trans('main.documents.content.panel3_1.desc') }}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        {{ trans('main.documents.content.panel4_1.a') }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    {{ trans('main.documents.content.panel4_1.desc') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <h4>{{ trans('main.documents.content.h4_2') }}</h4>
                    <br>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                        {{ trans('main.documents.content.panel2_1.a') }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">{{ trans('main.documents.content.panel2_1.desc') }}</div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        {{ trans('main.documents.content.panel2_2.a') }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body">
                                    {{ trans('main.documents.content.panel2_2.desc') }}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        {{ trans('main.documents.content.panel3_2.a') }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    {{ trans('main.documents.content.panel3_2.desc') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#treaty" aria-expanded="false" aria-controls="collapseSeven">
                                    {{ trans('main.documents.description') }}
                                </a>
                            </h4>
                        </div>
                        <div id="treaty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                            <div class="panel-body">
                                <a href="{{ asset('files/document/Treaty.pdf') }}">{{ trans('main.documents.treaty') }}</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
