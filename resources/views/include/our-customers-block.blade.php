<section class="our-customers-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <div class="desc-block">
                    <h4>{{ trans('main.our_customers_block.h4') }}</h4>
                    <h3>{{ trans('main.our_customers_block.h3') }}</h3>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="testimonial-block">
                    <div class="icon pull-left">
                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-left">
                        <p>{{ trans('main.our_customers_block.testimonials.testimonial_desc1') }}</p>
                        <div class="author-testimonial">
                            <p>JOHN SMITH</p>
                            <p>CTO of KLM</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="testimonial-block">
                    <div class="icon pull-left">
                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-left">
                        <p>{{ trans('main.our_customers_block.testimonials.testimonial_desc2') }}</p>
                        <div class="author-testimonial">
                            <p>JOHN SMITH</p>
                            <p>CTO of KLM</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row customers-logotypes">
            <div class="col-sm-2">
                <img style="margin-top: 30px;" src="{{ asset('img/partners1.png') }}">
            </div>
            <div class="col-sm-2 left-border">
                <img src="{{ asset('img/partners2.png') }}">
            </div>
            <div class="col-sm-2">
                <img style="margin-top: 30px;" src="{{ asset('img/partners1.png') }}">
            </div>
            <div class="col-sm-2 left-border">
                <img src="{{ asset('img/partners2.png') }}">
            </div>
            <div class="col-sm-2">
                <img style="margin-top: 30px;" src="{{ asset('img/partners1.png') }}">
            </div>
            <div class="col-sm-2 left-border">
                <img src="{{ asset('img/partners2.png') }}">
            </div>
        </div>
    </div>
</section>