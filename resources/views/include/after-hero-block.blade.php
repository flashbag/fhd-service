<section class="after-hero-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <div class="icon pull-left">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </div>
                <div class="content pull-left">
                    <h4>{{ trans('main.after_hero_block.col_1.h4') }}</h4>
                    {{--<p>{{ trans('main.after_hero_block.col_1.p1') }}</p>--}}
                    <p><a href="tel:+380994408001" style="color: #fff !important;">+380994408001</a></p>
                </div>
            </div>
            <div class="col-sm-4 left-border col-xs-12">
                <div class="icon pull-left">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                </div>
                <div class="content pull-left">
                    <h4>{{ trans('main.after_hero_block.col_2.h4') }}</h4>
                    <p>{{ trans('main.after_hero_block.col_2.p1') }}</p>
                    <p>{{ trans('main.after_hero_block.col_2.p2') }}</p>
                </div>
            </div>
            <div class="col-sm-4 left-border col-xs-12">
                <div class="icon pull-left">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
                <div class="content pull-left">
                    <h4>{{ trans('main.after_hero_block.col_3.h4') }}</h4>
                    <p>{{ trans('main.after_hero_block.col_3.p1') }}</p>
                    <p>{{ trans('main.after_hero_block.col_3.p2') }}</p>
                </div>
            </div>
        </div>
    </div>
</section>