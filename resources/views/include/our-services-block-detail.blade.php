<section class="our-services-block">
    <div class="container">

        <div class="row">
            <div class="col-sm-4">
                <div class="desc-block">
                    <h4>{{ trans('main.our_services_block_detail.h4') }}</h4>
                    <h3>{{ trans('main.our_services_block_detail.h3') }}</h3>
                </div>
            </div>
            <div class="col-sm-8 col-md-4">
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-ship" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-right">
                        <h4>{{ trans('main.our_services_block_detail.service_block1.h4') }}</h4>
                        <p>{{ trans('main.our_services_block_detail.service_block1.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-8 col-md-4 col-sm-offset-4 col-md-offset-0">
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-plane" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-left">
                        <h4>{{ trans('main.our_services_block_detail.service_block2.h4') }}</h4>
                        <p>{{ trans('main.our_services_block_detail.service_block2.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-sm-8 col-md-4 col-sm-offset-4 col-md-offset-0">
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-ship" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-right">
                        <h4>{{ trans('main.our_services_block_detail.service_block3.h4') }}</h4>
                        <p>{{ trans('main.our_services_block_detail.service_block3.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-8 col-md-4 col-sm-offset-4 col-md-offset-0">
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-ship" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-right">
                        <h4>{{ trans('main.our_services_block_detail.service_block4.h4') }}</h4>
                        <p>{{ trans('main.our_services_block_detail.service_block4.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-8 col-md-4 col-sm-offset-4 col-md-offset-0">
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-plane" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-left">
                        <h4>{{ trans('main.our_services_block_detail.service_block5.h4') }}</h4>
                        <p>{{ trans('main.our_services_block_detail.service_block5.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</section>