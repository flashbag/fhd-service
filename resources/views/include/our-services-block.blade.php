<section class="our-services-block">
    <div class="container">

        <img class="bg-car hidden-xs" src="//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/10/truck_green.png">

        <div class="row">
            <div class="col-sm-4">
                <div class="desc-block">
                    <h4>{{ trans('main.our_services_block.h4') }}</h4>
                    <h3>{{ trans('main.our_services_block.h3') }}</h3>
                </div>
            </div>
            <div class="col-sm-8 col-md-4 col-lg-4">
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-ship" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-right">
                        <h4>{{ trans('main.our_services_block.service_block1.h4') }}</h4>
                        <p>{{ trans('main.our_services_block.service_block1.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-bus" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-left">
                        <h4>{{ trans('main.our_services_block.service_block2.h4') }}</h4>
                        <p>{{ trans('main.our_services_block.service_block2.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-8 col-sm-offset-4 col-md-offset-0 col-md-4 col-lg-4">
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-plane" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-left">
                        <h4>{{ trans('main.our_services_block.service_block3.h4') }}</h4>
                        <p>{{ trans('main.our_services_block.service_block3.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="service-block">
                    <div class="icon pull-left">
                        <i class="fa fa-ship" aria-hidden="true"></i>
                    </div>
                    <div class="content pull-left">
                        <h4>{{ trans('main.our_services_block.service_block4.h4') }}</h4>
                        <p>{{ trans('main.our_services_block.service_block4.p') }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>