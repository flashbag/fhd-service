<section class="stats-block">
    <div class="container">
        <img class="stats-bg hidden-xs" src="//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/10/forklift.png">
        <div class="row">
            <div class="col-sm-4">
                <div class="desc-block">
                    <h4>{{ trans('main.stats_block.h4') }}</h4>
                    <h3>{{ trans('main.stats_block.h3') }}</h3>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="stats-desc">{{ trans('main.stats_block.description') }}</p>
            </div>
        </div>
        <div class="row stats-counters">
            <div class="col-sm-3">
                <h3>1200</h3>
                <h4>{{ trans('main.stats_block.stats_counters.counter1.h4') }}</h4>
            </div>
            <div class="col-sm-3">
                <h3>438721</h3>
                <h4>{{ trans('main.stats_block.stats_counters.counter2.h4') }}</h4>
            </div>
            <div class="col-sm-3">
                <h3>8721</h3>
                <h4>{{ trans('main.stats_block.stats_counters.counter3.h4') }}</h4>
            </div>
            <div class="col-sm-3">
                <h3>1221</h3>
                <h4>{{ trans('main.stats_block.stats_counters.counter4.h4') }}</h4>
            </div>
        </div>
    </div>
</section>