<section class="create-tracker-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-xs-12">
                <h5>{{ trans('main.create_tracker_block.h5') }}</h5>
                <h3>{{ trans('main.create_tracker_block.h3') }}</h3>
            </div>
            <div class="col-sm-3 col-xs-12">
                <a href="{{ route('create-trackers') }}" class="btn btn-inline-new pull-right">{{ trans('main.create_tracker_block.link') }}</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>