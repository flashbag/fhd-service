@if ($errors->any())
    <div class="search-info">
        <span style="color: #EB5757;">{{ $errors->first() }}</span>
    </div>
@endif
@include('trackers-search-form')
