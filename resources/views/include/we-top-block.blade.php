<section class="we-top-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="desc-block">
                    <h4>{{ trans('main.we_top.h4') }}</h4>
                    <h3>{{ trans('main.we_top.h3') }}</h3>
                    <p>{{ trans('main.we_top.description') }}</p>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="progress-block">

                    <label>{{ trans('main.we_top.progress_block.label1') }}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                            75%
                        </div>
                    </div>

                    <label>{{ trans('main.we_top.progress_block.label2') }}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="61" aria-valuemin="0" aria-valuemax="100" style="width: 61%;">
                            61%
                        </div>
                    </div>

                    <label>{{ trans('main.we_top.progress_block.label3') }}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="54" aria-valuemin="0" aria-valuemax="100" style="width: 54%;">
                            54%
                        </div>
                    </div>

                    <label>{{ trans('main.we_top.progress_block.label4') }}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                            100%
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>