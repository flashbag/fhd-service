@extends('layouts.app')
@section('content')
    <div class="trackers-history-page">
        <div class="container-fluid">

            @include('flash-messages')

            <div class="row position-relative z-index-0">

                @include('tickets.warehouse.left-menu-tickets')
                    <section class="section-address">
                        <div class="container">
                            <div class="title-block">
                                <h2 class="title py-5 text-center">{{ trans('main.contacts.our_address') }}</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="section-address__inner">
                                        <ul class="section-address__list pt-5">
                                            <li><span>{{ Auth::user()->name }}</span></li>
                                            <li><p>{{ trans('main.contacts.ukraine_street') }}, {{ trans('main.contacts.suite') }}: {{Auth::user()->id_client}}</p></li>
                                            <li><p>{{ trans('main.contacts.ukraine_city') }}, {{ trans('main.contacts.ukraine_postcode') }} </p></li>
                                            <li><p>{{ trans('main.contacts.ukraine_phone') }}</p></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="section-address__inner">
                                        <ul class="section-address__list pt-5">
                                            <li><span>{{ Auth::user()->name }}</span></li>
                                            <li><p>{{ trans('main.contacts.usa_street') }}, {{ trans('main.contacts.suite') }}: {{Auth::user()->id_client}}</p></li>
                                            <li><p>{{ trans('main.contacts.usa_city') }}, {{ trans('main.contacts.usa_postcode') }}</p></li>
                                            <li><p>{{ trans('main.contacts.usa_phone') }}</p></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="section-address__inner">
                                        <ul class="section-address__list pt-5">
                                            <li><span>{{ Auth::user()->name }}</span></li>
                                            <li><p>{{ trans('main.contacts.china_street') }},  {{ trans('main.contacts.suite') }}: {{Auth::user()->id_client}}</p></li>
                                            <li><p>{{ trans('main.contacts.china_city') }}, {{ trans('main.contacts.china_postcode')}}</p></li>
                                            <li><p>{{ trans('main.contacts.china_phone') }}</p></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

            </div>
        </div>
    </div>
@endsection
