<form id="search-tracker-form" class="search-tracker-form main-form py-5" method="get" action="{{ route('search-tracker') }}">

	<div class="form_group mb-2">
		{{--<input type="text" name="tracker" id="id_tracker" class="form_control" placeholder="{{ trans('main.trackers.label2') }}" value={{ old('id_tracker') }} >--}}
		<input type="text" name="id_tracker" id="tracker" class="form-control input__custom" placeholder="{{ trans('main.trackers.label2') }}" value="@if(isset($id_tracker) && $id_tracker != null){{ old('id_tracker', $id_tracker) }}@else{{ old('id_tracker') }}@endif">

		@if ($errors->has('id_tracker'))
			<span class="help-block">
				<span style="color: #EB5757;">{{ trans('main.main_page.tracking_number') }}</span>
			</span>
		@endif
	</div>
	<div class="form_group">
		<input type="text" name="number" id="inputNumber" class="form_control input__custom" placeholder="{{ trans('main.trackers.label1') }}" value="@if(isset($number) && $number != null){{ old('number', $number) }}@else{{ old('number') }}@endif">
		<span id="numberSender-valid-msg" class="hide phone-valid-msg">✓ Valid</span>
		<span id="numberSender-error-msg" class="hide phone-error-msg"></span>
		@if ($errors->has('number'))
			<span class="help-block">
				<span style="color: #EB5757;">{{ trans('main.main_page.phone_number') }}</span>
			</span>
		@endif
		@if (session('error2'))
			<span class="help-block">
				<span>{{ trans ('main.custom.errorNoTrecking')  }}</span>
			</span>
		@endif
	</div>
	<button class="btn btn-main btn-search btn-search_static" type="submit">{{ trans('main.main_page.track') }}</button>
</form>
