<!-- Modal -->
<div class="modal fade" id="modalRegisterByPhone" tabindex="-1" role="dialog" aria-labelledby="modalRegisterByPhone">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Подтверждение телефона</h4>
			</div>
			<div class="modal-body">
				Подтвердите пожалуйста номер телефона отправителя для продолжения.
				<br><br>
				Для этого вам нужно ввести код из СМС.
				<br><br>

				<div class="block-register-initialize" style="display: block">

					<form class="ajax-form"
						  method="POST" action="{{ route('register-initialize') }}"
						  data-success-callback="registerInitializeSuccessCallbackPrimary">

						{{ csrf_field() }}

						<div class="row">
							<div class="form-group col-md-2 col-md-offset-2">
								<label for="name" class="col-md-4 control-label">Телефон</label>
							</div>

							<div class="form-group col-md-6">
								<input type="text" id="registerInitializeNumber" class="form-control" name="phone_number" value="" readonly="readonly">
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-block btn-primary pull-right">Получить код</button>
							</div>
						</div>
					</form>

				</div>

				<div class="block-register-verify" style="display: none">

					<form class="ajax-form"
						  id="register-initialize-secondary"
						  method="POST" action="{{ route('register-initialize') }}"
						  data-success-callback="registerInitializeSuccessCallbackSecondary"
						  data-error-callback="registerInitializeErrorCallbackSecondary">

						{{ csrf_field() }}

						<div class="row">
							<div class="form-group col-md-4 text-right">
								<label for="name" class="control-label">Телефон</label>
							</div>

							<div class="form-group col-md-6">
								<input type="text" class="form-control" name="phone_number" value="" readonly="readonly">
							</div>
						</div>

					</form>

					<form class="ajax-form"
						  id="register-verify-form"
						  method="POST" action="{{ route('register-verify') }}"
						  data-success-callback="registerVerifySuccessCallback">

						{{ csrf_field() }}

						<input type="hidden" id="registerVerifyIsExisting" name="is_existing" value="0">

						<input type="hidden" id="registerVerifyNumber" name="phone_number">
						<input type="hidden" id="registerVerifyEmail" name="email">
						<input type="hidden" id="registerVerifyFullName" name="full_name">

						<div class="row">
							<div class="form-group col-md-4 text-right">
								<label for="name" class="control-label">Код из СМС</label>
							</div>

							<div class="form-group col-md-6">
								<input id="register-verify-code" type="text" class="form-control" name="code" value="">
							</div>
						</div>

						<div class="row row-register-verify-password">
							<div class="form-group col-md-4 text-right">
								<label for="name" class="control-label">Пароль</label>
							</div>

							<div class="form-group col-md-6">
								<input id="password" type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="row row-register-verify-password">
							<div class="form-group col-md-4 text-right">
								<label for="name" class="control-label">Пароль (подтверждение)</label>
							</div>

							<div class="form-group col-md-6">
								<input id="password" type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-6 col-md-offset-4">
								<button type="submit" id="btn-get-code" class="btn btn-block btn-primary pull-right">Подтвердить код</button>
							</div>
						</div>


					</form>

					<div class="form-horizontal">
						<div class="form-group ">
							<label for="name" class="col-md-4 control-label"></label>

							<div class="col-md-6">
								<button type="submit" id="btn-get-one-more-code"
										class="btn btn-block btn-default pull-right">Запросить код ещё раз</button>
							</div>
						</div>

						<div class="form-group get-code-info-block">
							<label for="name" class="col-md-4 control-label"></label>
							<div class="col-md-6">
								<span class="help-block">
									Вы можете запросить код повторно через <span id="code-countdown">10</span> секунд
								</span>
							</div>
						</div>

					</div>


				</div>


			</div>

		</div>
	</div>
</div>


@push('script')

	<script type="text/javascript">

		function codeCountdown() {

			let codeCountdownSeconds = 300;
			let $codeCountDown = $('#code-countdown');
			let $getOneMoreCode = $('#btn-get-one-more-code');
			let $infoBlock = $('.get-code-info-block');

			$infoBlock.show();
			$getOneMoreCode.attr('disabled','disabled');

			let countdownInterval = setInterval(function(){
				codeCountdownSeconds = codeCountdownSeconds - 1;
				$codeCountDown.text(codeCountdownSeconds);
			}, 1000);

			setTimeout(function(){
				clearInterval(countdownInterval);
				$getOneMoreCode.removeAttr('disabled');
				$infoBlock.hide();
				$codeCountDown.text(codeCountdownSeconds);
			}, 1000 * codeCountdownSeconds);
		}

		function fillCode(data) {
			if (data.hasOwnProperty('code')) {
				$('input[name="code"]').val(data.code);
			}
		}

		function fillUserData (data) {

			console.log(data);

			if (!data.hasOwnProperty('user') || data.user === null) {
				return false;
			}

			let user = data.user;

			$('#registerVerifyIsExisting').val(1);

			$('.row-register-verify-password').hide();

			if (user.hasOwnProperty('email')) {
				$('#registerVerifyEmail').val(user.email);
			}

			if (user.hasOwnProperty('full_name')) {
				$('#registerVerifyFullName').val(user.full_name);
			}
		}

		window.registerInitializeSuccessCallbackPrimary = function(data) {
			$('.block-register-initialize').hide();
			$('.block-register-verify').show();

			codeCountdown();
			fillUserData(data);
			fillCode(data);
		};

		window.registerInitializeSuccessCallbackSecondary = function(data) {
			codeCountdown();
			fillUserData(data);
		};

		window.registerVerifySuccessCallback = function(data) {

			let $codeFormGroup = $('#register-verify-code').closest('.form-group');

			let message = 'Код успешно подтверждён!';

			if (data.hasOwnProperty('need_to_login') && data.need_to_login) {
				message = message + ' Войдите пожалуйста для продолжения';
			} else {
				message = message + ' Через 5 секунд вы будете перенаправлены в личный кабинет';
			}

			let $successSpan = $('<span/>', { 'class': 'help-block'}).text(message);

			$codeFormGroup.addClass('has-success');
			$codeFormGroup.append($successSpan);

			setTimeout(function(){
				$('#modalRegisterByPhone').modal('hide');
				$('#tracker-form').submit();
			}, 5000);

			$codeFormGroup.append();
		};

		$(document).ready(function(){

			let loggedUserNumber = $('#loggedUserNumber').val();
			let numberSenderInitial = $('#numberSender').val();
			let emailSenderInitial = $('#inputEmailSender').val();
			let fullNameSenderInitial = $('#inputSenderFullName').val();

			let actualNumber = numberSenderInitial;
			if (loggedUserNumber) {
				actualNumber = loggedUserNumber;
			}

			$('input[name="phone_number"]').val(actualNumber);
			$('#registerInitializeNumber').val(actualNumber);

			$('#registerVerifyNumber').val(actualNumber);
			$('#registerVerifyEmail').val(emailSenderInitial);
			$('#registerVerifyFullName').val(fullNameSenderInitial);

			$('#numberSender').on('change', function(){
				if (!loggedUserNumber) {
					$('input[name="phone_number"]').val($(this).val());
					$('#registerInitializeNumber').val($(this).val());
					$('#registerVerifyNumber').val($(this).val());
				}
			});

			$('#inputEmailSender').on('change', function(){
				$('#registerVerifyEmail').val($(this).val());
			});

			$('#inputSenderFullName').on('change', function(){
				$('#registerVerifyFullName').val($(this).val());
			});

			$('#btn-get-one-more-code').on('click', function(){
				$('#register-initialize-secondary').submit();
			});

		});
	</script>

@endpush
