<!-- Modal -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Вход</h4>
			</div>

			<div class="modal-body">
				Войдите пожалуйста для продолжения.
				<br><br>

				<form class="form-auth ajax-form" method="POST" action="{{ route('login') }}"  data-success-callback="modalLoginSuccessCallback">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email">{{ trans('main.login.login_form.text_2') }}</label>
						<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password">{{ trans('main.login.login_form.text_3') }}</label>
						<input id="password" type="password" class="form-control" name="password" required>

						@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>

					<div class="clearfix"></div>

					<div class="form-group">
						<div>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ trans('main.login.login_form.text_4') }}
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<button class="btn btn-info-new" type="submit">{{ trans('main.login.login_form.text_5') }}</button>
						<a href="/password/reset" class="btn btn-default-new" type="submit">{{ trans('main.login.login_form.forgotten') }}</a>
					</div>

				</form>

			</div>

		</div>
	</div>
</div>


@push('script')

	<script type="text/javascript">

		window.modalLoginSuccessCallback = function(data) {
			setTimeout(function(){
				$('#tracker-form').submit();
			}, 1000);
		};


		$(document).ready(function(){

			let numberSenderInitial = $('#numberSender').val();

			$('#modalLogin').find('input[name="email"]').val(numberSenderInitial);

			$('#numberSender').on('change', function(){
				$('#modalLogin').find('input[name="email"]').val($(this).val());
			});

		});
	</script>

@endpush
