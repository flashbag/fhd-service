<!-- Modal  -->
<div class="modal fade" id="modalRegisterSimple" tabindex="-1" role="dialog" aria-labelledby="modalRegisterSimple">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">{{ trans('main.login.reg_form.text_1') }}</h4>
			</div>
			<div class="modal-body">

				<form class="form-auth ajax-form" method="POST" action="{{ route('register-simple') }}">
					{{ csrf_field() }}

					<label for="name" class="control-label">{{ trans('main.login.reg_form.text_2') }}</label>
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

						<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" >

						@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email">{{ trans('main.login.reg_form.text_3') }}</label>
						<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" >

						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
						<label for="number">{{ trans('main.login.reg_form.text_4') }}</label>
						<input id="numberReg" type="text" class="form-control" name="number" value="{{ old('number') }}" >

						@if ($errors->has('number'))
							<span class="help-block">
								<strong>{{ $errors->first('number') }}</strong>
							</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password">{{ trans('main.login.reg_form.text_5') }}</label>
						<input id="password" type="password" class="form-control" name="password" >

						@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>

					<div class="clearfix"></div>

					<div class="form-group">
						<label for="password-confirm" class="control-label">{{ trans('main.login.reg_form.text_6') }}</label>
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
					</div>

					<div class="clearfix"></div>
					<br>

					<div class="form-group">
						<button class="btn btn-main btn-block" type="submit">
							{{ trans('main.login.reg_form.text_7') }}
						</button>
					</div>

					<br>

					<div class="form-group text-center">
						<span> {{ trans('main.login.p') }}</span>&nbsp;
						<a href="{{ route('login') }}">{{ trans('main.mini_nav.log_in') }}</a>
					</div>



				</form>

			</div>

		</div>
	</div>
</div>


@push('script')

	<script type="text/javascript">

		$(document).ready(function(){

			// let numberSenderInitial = $('#numberSender').val();
			//
			// $('#modalRegisterSimple').find('input[name="email"]').val(numberSenderInitial);
			//
			// $('#numberSender').on('change', function(){
			// 	$('#modalRegisterSimple').find('input[name="number"]').val($(this).val());
			// });

		});
	</script>

@endpush
