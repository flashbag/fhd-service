<!-- Modal -->
<div class="modal fade" id="modalSendToEmail" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-fhd__dialog" role="document">
        <div class="modal-fhd modal-fhd__content">
            <div class="modal-fhd__header">
                <button type="button" class="modal-fhd__close" data-dismiss="modal" aria-label="Close">
                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="16" cy="16" r="16"></circle>
                        <rect x="10.8818" y="12.229" width="1.77778" height="12.4444" transform="rotate(-45 10.8818 12.229)"></rect>
                        <rect x="19.6826" y="10.9712" width="1.77778" height="12.4444" transform="rotate(45 19.6826 10.9712)"></rect>
                    </svg>
                </button>

                <h4 class="title-modal title-modal_svg my-0">
                    <svg class="mr-2" viewBox="0 0 30 23" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.6865 3.12944L28.4965 1.93945L17.4735 12.9624C16.0956 14.3403 13.7783 14.3403 12.4004 12.9624L1.37748 2.00209L0.1875 3.19206L8.39209 11.3967L0.1875 19.6013L1.37748 20.7912L9.58207 12.5866L11.2105 14.215C12.2126 15.2171 13.5278 15.7808 14.9057 15.7808C16.2835 15.7808 17.5988 15.2171 18.6009 14.215L20.2293 12.5866L28.4338 20.7912L29.6238 19.6013L21.4192 11.3967L29.6865 3.12944Z"></path>
                        <path d="M27.3695 22.9227H2.63048C1.18998 22.9227 0 21.7328 0 20.2923V2.63048C0 1.18998 1.18998 0 2.63048 0H27.3695C28.81 0 30 1.18998 30 2.63048V20.2923C30 21.7328 28.81 22.9227 27.3695 22.9227ZM2.56785 1.69102C2.0668 1.69102 1.69103 2.0668 1.69103 2.56784V20.2296C1.69103 20.7307 2.0668 21.1065 2.56785 21.1065H27.3069C27.8079 21.1065 28.1837 20.7307 28.1837 20.2296V2.56784C28.1837 2.0668 27.8079 1.69102 27.3069 1.69102H2.56785Z"></path>
                    </svg>
                    <span>Отправить переписку на почту</span></h4>
            </div>
            <div class="modal-fhd__body">
                <form id="send-ticket" action="{{ route('send-ticket', $ticket->trackers->id_tracker) }}"
                      method="POST" >
                    {{ csrf_field() }}
                    <div class="d-flex w-100">
                        <div class="form-group my-0 w-100">
                            <input type="email" id="email" name="send_email" class="form-control input__custom" placeholder="Электронная почта">
                            <p class="messages"></p>
                        </div>
                        <button type="submit" class="btn-send modal-fhd_btn-height ml-2">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>