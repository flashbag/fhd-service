<!-- Modal -->
<div class="modal fade" id="modalCheckEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Подтверждение почты</h4>
			</div>
			<div class="modal-body">
				У вас уже существует аккаунт или вы новый пользователь?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default-new pull-left" data-dismiss="modal">Новый клиент
				</button>
				<a href="/login" class="btn btn-info-new pull-right">Войти в аккаунт</a>
			</div>
		</div>
	</div>
</div>
