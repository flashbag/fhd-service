<!-- Modal -->
<form id="delete-tracker-form" action="{{ route('delete-trackers', $tracker->id) }}"
      method="POST" >
    {{ csrf_field() }}
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content_warning">
                <div class="modal-header modal-header_warning">
                    <button type="button" class="close modal-custom_close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <svg width="55" height="50" viewBox="0 0 55 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.7907 38.0025L29.7901 38.0157L29.7908 38.0289C29.8495 39.3025 28.6664 40.4585 27.347 40.4585C25.9836 40.4585 24.8361 39.311 24.8361 37.9476C24.8361 36.5842 25.9836 35.4367 27.347 35.4367C28.7193 35.4367 29.8509 36.5677 29.7907 38.0025Z" fill="#F1774F" stroke="#FEFAED" stroke-width="0.6"/>
                        <path d="M53.1051 44.9474L53.105 44.9475C51.3816 47.9253 48.3206 49.7 44.9095 49.7H9.73977C6.3386 49.7 3.30039 47.9466 1.60012 45.0256L1.60001 45.0254C-0.122139 42.0716 -0.133466 38.5321 1.56723 35.5554C1.56735 35.5552 1.56747 35.5549 1.56759 35.5547L19.1968 5.02973L19.1974 5.02864C20.8867 2.07502 23.9356 0.3 27.3584 0.3C30.7816 0.3 33.8298 2.06405 35.5189 5.03906L35.52 5.04095L53.1274 35.5334L53.1274 35.5335C54.8272 38.4755 54.817 41.9926 53.1051 44.9474ZM49.8014 43.0446L49.8017 43.0441C50.8209 41.2783 50.8347 39.1853 49.8356 37.4488L49.8354 37.4484L32.2176 6.93477C32.2174 6.93449 32.2172 6.9342 32.2171 6.93391C31.2081 5.16588 29.3888 4.10747 27.3584 4.10747C25.3176 4.10747 23.5091 5.15387 22.4997 6.92266C22.4995 6.92295 22.4994 6.92324 22.4992 6.92353L4.87015 37.4371L4.86966 37.4379C3.8492 39.2178 3.86135 41.3331 4.88133 43.1003L4.88145 43.1005C5.89158 44.8474 7.71166 45.8813 9.72852 45.8813H44.8983C46.94 45.8813 48.77 44.8229 49.8014 43.0446Z" fill="#F1774F" stroke="#FEFAED" stroke-width="0.6"/>
                        <path d="M24.9711 21.1316L24.9712 21.1316L24.9707 21.1258C24.9371 20.6829 24.9063 20.2369 24.8753 19.7898L24.8753 19.7893C24.8447 19.3466 24.814 18.9027 24.7805 18.4614C24.7846 17.1123 25.5405 16.0355 26.7261 15.6935C27.9234 15.415 29.1122 15.9868 29.6252 17.1093C29.7946 17.5093 29.8578 17.9138 29.8584 18.3868C29.8249 19.0836 29.7775 19.7809 29.7298 20.4817L29.7297 20.4832C29.6821 21.1845 29.6341 21.8892 29.6003 22.5945C29.5385 23.6786 29.4739 24.7627 29.4092 25.8474L29.4092 25.8477C29.3447 26.9306 29.2801 28.014 29.2184 29.0975C29.1502 29.8172 29.1501 30.4681 29.1501 31.159C29.098 32.1609 28.325 32.9128 27.3476 32.9128C26.3588 32.9128 25.6028 32.2039 25.5445 31.2167C25.449 29.5247 25.3534 27.8495 25.2578 26.1745L25.2578 26.1742C25.1623 24.4989 25.0667 22.8237 24.9711 21.1316Z" fill="#F1774F" stroke="#FEFAED" stroke-width="0.6"/>
                    </svg>
                    <h4 class="modal-title modal-title_warning text-center">{{ trans('admin.specify_reason') }}</h4>
                </div>
                <div class="modal-body modal-body_warning">
                    <p>{{ trans('admin.attention_title') }}</p>
                    <div class="modal-body_warning_select">
                        <select class="form-control selectpicker" name="reason">
                            <option value="" selected>--</option>
                            <option value="{{ trans('admin.reason.1') }}">{{ trans('admin.reason.1') }}</option>
                            <option value="{{ trans('admin.reason.2') }}">{{ trans('admin.reason.2') }}</option>
                            <option value="{{ trans('admin.reason.3') }}">{{ trans('admin.reason.3') }}</option>
                            <option value="{{ trans('admin.reason.4') }}">{{ trans('admin.reason.4') }}</option>
                        </select>
                    </div>
                    <div class="modal-body__warning_textarea">
                        <div class="form-group">
                            <p class="form-subtitle">{{ trans('admin.variant') }}:</p>
                            <textarea id="textarea" name="reason_second" class="form-control textarea__custom"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer modal-footer__custom modal-footer_warning">
                    <button type="button" class="btn-custom btn-custom_orange pull-left"
                            data-dismiss="modal" aria-label="Close">{{ trans('admin.cancel') }}</button>
                    <button id="rules-agreed" type="submit"
                            class="btn-custom btn-custom_red pull-right">{{ trans('admin.delete') }}</button>
                    {{--                        <a href="{{ route('delete-trackers', $tracker->id) }}" class="btn btn-danger delete-tracker">{{ trans('admin.delete') }}</a>--}}
                </div>
            </div>
        </div>
    </div>
</form>