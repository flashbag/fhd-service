<div class="modal fade" id="modalPrint" tabindex="-1" role="dialog" aria-labelledby="modalPrint" style="z-index: 99999;">

	<div class="modal-dialog" role="document">
		<div class="modal-content" style="margin-top: 150px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span  aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					{{ __('trackers.print_options.settingsTitle' ) }}
				</h4>
			</div>
			<div class="modal-body">

				<form class="form-auth" id="form-print-settings">
					{{ csrf_field() }}

					<input type="hidden" name="print-id-tracker" id="modalPrintIdTracker">

					<div class="row row-browser-warning" style="display: none">
						<div class="form-group col-xs-12">
							<div class="alert alert-dismissible alert-warning alert__custom alert-warning__custom">
								<p class="alert__text">
									{{ __('trackers.print_options.browser_warning' ) }}
								</p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-2 col-md-offset-2">
							<label for="color">
								{{ __('trackers.print_options.colorLabel' ) }}
							</label>
						</div>

						<div class="form-group col-md-6">
							<select id="colors-select" class="form-control selectpicker">
								<option value="default">{{ __('trackers.print_options.colors.default' ) }}</option>
								<option value="grey">{{ __('trackers.print_options.colors.grey' ) }}</option>
								<option value="blue">{{ __('trackers.print_options.colors.blue' ) }}</option>
								<option value="orange">{{ __('trackers.print_options.colors.orange' ) }}</option>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-2 col-md-offset-2">
						</div>
						<div class="form-group col-md-3">
							<button class="btn btn-primary" type="submit">
								{{ __('trackers.print_options.download' ) }}
							</button>
						</div>
						<div class="form-group col-md-3">
							<a class="btn btn-primary pull-left" target="_blank" id="show-pdf" href="#">
								{{ __('trackers.print_options.show' ) }}
							</a>
						</div>
					</div>

				</form>

			</div>

		</div>
	</div>

</div>


@push('script')

	<script>

		function printBrowserCheck() {

			if (!isChromeBrowser()) {
                $('.row-browser-warning').show();
			}

		}

		$(document).ready(function(){

			printBrowserCheck();

			window.printTrackerId = null;

			$(".btn-print").on('click', function () {

				$("#modalPrint").modal('show');

				window.printTrackerId = $(this).data('id-tracker');

				$("#modalPrintIdTracker").val(window.printTrackerId);

				changeShowTrackerLink();
			});

			$('#colors-select').on('change', function(){
				changeShowTrackerLink();
			});

			function changeShowTrackerLink() {

				let showHref;

				if (window.printTrackerId) {
					showHref = '/print/tracker/' + window.printTrackerId;
				} else {
					showHref = '/print/tracker/0';
				}

				showHref = attachColorToUrl(showHref);

				$('#show-pdf').attr('href', showHref);

			}

			function attachColorToUrl(url) {

				let color = $('#colors-select').val();

				return url + '?color=' + color;
			}

			$("#form-print-settings").on('submit', function (e) {

				e.preventDefault();

				let url;

				if (window.printTrackerId) {
					url = '/download/tracker/' + window.printTrackerId + '/pdf';
				} else {
					url = '/download/tracker/pdf/empty';
				}

				url = attachColorToUrl(url);

				$.ajax({
					url: url,
					type: 'GET',
					success: function() {
						window.location = url;
					}
				});
			});

		});

	</script>

@endpush
