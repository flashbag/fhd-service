<!-- Modal -->
<div class="modal fade" id="modalRules" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">{{ trans('main.custom.acceptZak') }}</h4>
			</div>
			<div class="modal-body">{{ trans('main.custom.acceptText') }}
				<div class="checkbox">
					<label>
						<input type="checkbox" id="rules-check">{{trans('main.custom.IacceptZak') }}<a
								href="{{ route('rules') }}" target="_blank">{{trans('main.custom.rulesAnd') }}</a>
					</label>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default-new pull-left"
						data-dismiss="modal">{{ trans('main.custom.rulesCancel') }}</button>
				<button id="rules-agreed" type="button"
						class="btn btn-info-new pull-right disabled">{{ trans('main.custom.rulesAccept') }}</button>
			</div>
		</div>
	</div>
</div>
