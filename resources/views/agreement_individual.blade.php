@extends('layouts.app')

@section('content')
{{--    {!! trans('agreement.agreement_individual')  !!}--}}

<div class="container">
    <div class="title-block">
        <h1 class="dogovor-section__title text-center">{{trans('agreement.label11')}}</h1>
    </div>
    <div class="dogovor-section__body">
        {!! trans('agreement.fiz') !!}
    </div>
    <section class="dogovor-section">
        <div class="container">
            <div class="dogovor-section__download">
                <div class="col-sm-12 col-md-4">
                    <a href="/docs/ind_RUS.pdf" class="add-info__link"  download>
                        <div class="icon">
                            <svg style="width:39px; height:40px;" ><use xlink:href="#pdf"></use></svg>
                        </div>
                        {{trans('agreement.button1')}}
                    </a>
                </div>
                <div class="col-sm-12 col-md-4">
                    <a href="/docs/ind_UKR.pdf" class="add-info__link" download>
                        <div class="icon">
                            <svg style="width:39px; height:40px;" ><use xlink:href="#pdf"></use></svg>
                        </div>
                        {{trans('agreement.button2')}}
                    </a>
                </div>
                <div class="col-sm-12 col-md-4">
                    <a href="/docs/ind_ENG.pdf" class="add-info__link" download>
                        <div class="icon">
                            <svg style="width:39px; height:40px;" ><use xlink:href="#pdf"></use></svg>
                        </div>
                        {{trans('agreement.button3')}}
                    </a>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

