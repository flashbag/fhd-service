@extends('layouts.app')

@section('slider')

	<section class="hero-carousel" style="height: 85%;">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active"
					 style="background: url('//cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
					<div class="container">
						<div class="carousel-caption-new">

							<div class="pull-left" style="width: 65%;">
								<p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.tracker_create.p') }}</p>
								<h1>{{ trans('main.tracker_create.h1') }}</h1>
								<h5 class="subtitle">{{ trans('main.tracker_create.h5') }}</h5>
								<div class="btn-group-block">
									<a href="/about-us"
									   class="btn btn-default-new">{{ trans('main.tracker_create.link2') }}</a>
								</div>
							</div>

						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('content')

	<div class="container">

		@include('flash-messages')

		<div class="wrap simple-page create-trackers-wrapper card position-center create-trakers-card">
			<div class="card-header">
				{{ __('trackers.add_tracker') }}
			</div>
			<span class="color-red">*</span> <span>{{ trans('trackers.fill_form_lang') }}</span>
			<div class="card-body">
				<form method="post" id="tracker-form"
					  class="default-form form-with-steps ajax-form"
					  data-ukraine-value="{{ __('main.ukraine') }}"
					  action="{{ route('store-trackers') }}"
					  data-success-callback="storeTrackerSuccessCallback"
					  data-error-callback="storeTrackerErrorCallback"
					  enctype="multipart/form-data">

					<div id="tracker-form-total">

{{--						@include('tracker-parts.steps-header')--}}

						{{ csrf_field() }}

						@if(Auth::check())
							<input type="hidden" name="user_number" id="loggedUserNumber"
								   value="{{ Auth::user()->number }}">
						@endif

						@include('tracker-parts.fields-to-fill')

						@include('tracker-parts.receptacles-blocks')

						@include('tracker-parts.sender-info')

						@include('tracker-parts.recipient-info')

						@include('tracker-parts.additional')

						@include('tracker-parts.radio-buttons')

						@include('tracker-parts.submit-buttons')
					</div>
				</form>
			</div>

		</div>

		@include('modals.modal-rules')

		@include('modals.check-email')

		{{--@include('modals.modal-register-by-phone')--}}

		@include('modals.modal-register-simple')

		@include('modals.modal-login')
	</div>
@endsection

@push('script')

	<script>

		$(document).ready(function () {

			let statusRadio1 = true;
			let statusRadio2 = false;

			let $submitBtn = $('.check-submit-btn');

			function checkSubmitBtn() {
				if (statusRadio1 && statusRadio2) {
					$submitBtn.removeAttr('disabled');
				} else {
					$submitBtn.attr('disabled', 'true');
				}
			}

			$('#inlineR1-O1').click(function () {
				statusRadio1 = true;
				$('#inlineRODanger').fadeOut(500);
				checkSubmitBtn();
			});

			$('#inlineR1-O2').click(function () {
				statusRadio1 = false;
				$('#inlineRODanger').fadeIn(500);
				checkSubmitBtn();
			});

			$('#inlineR2-O1').click(function () {
				statusRadio2 = false;
				checkSubmitBtn();
			});

			$('#inlineR2-O2').click(function () {
				statusRadio2 = true;
				checkSubmitBtn();
			});

			// $('.check-submit-btn').click(function (e) {
			//     e.preventDefault();
			//     $('#modalRules').modal();
			// });

			// $('#rules-check').click(function () {
			//
			//     if ($('#rules-agreed').hasClass('disabled')) {
			//         $('#rules-agreed').removeClass('disabled')
			//     } else {
			//         $('#rules-agreed').addClass('disabled');
			//     }
			//
			// });
			//
			// $('#rules-agreed').click(function () {
			//     if (!$(this).hasClass('disabled')) {
			//         $('.default-form').submit();
			//     }
			// });
		});
	</script>
@endpush
