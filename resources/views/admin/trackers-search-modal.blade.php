<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px;">
	<div class="modal-dialog" role="document">
		<form action="{{ route('search-trackers') }}">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Поиск трекера</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label>По номеру трекера</label>
						<input type="text" class="form-control input-sm" name="id_tracker">
					</div>

					<div class="form-group">
						<label>По номеру инвойса</label>
						<input type="text" class="form-control input-sm" name="number_invoice">
					</div>

					<div class="form-group">
						<label>По дате оформления</label>
						<input type="text" class="form-control input-sm" name="date_registered" placeholder="Формат: ДД-ММ-ГГ">
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label>Дата доставки</label>
								<input type="text" class="form-control input-sm" name="min_date_delivery" placeholder="Формат: ДД-ММ-ГГ">
							</div>
							<div class="col-sm-6">
								<label>По дату доставки</label>
								<input type="text" class="form-control input-sm" name="max_date_delivery" placeholder="Формат: ДД-ММ-ГГ">
							</div>
						</div>
					</div>


					<div class="form-group">
						<label>По перевозчику</label>
						<input type="text" class="form-control input-sm" name="carrier">
					</div>

					{{--<div class="form-group">--}}
					{{--<label>По номеру контейнера</label>--}}
					{{--<input type="text" class="form-control input-sm" name="number_container">--}}
					{{--</div>--}}

					<div class="form-group">
						<label>По типу контейнера</label>
						<select class="form-control input-sm selectpicker" name="container_id">
							<option selected style="display: none;" value="">--</option>
						</select>
					</div>

					{{--<div class="form-group">--}}
					{{--<div class="row">--}}
					{{--<div class="col-sm-6">--}}
					{{--<label>Оценочная стоимость от</label>--}}
					{{--<input type="text" class="form-control input-sm" name="min_assessed_price">--}}
					{{--</div>--}}
					{{--<div class="col-sm-6">--}}
					{{--<label>Оценочная стоимость до</label>--}}
					{{--<input type="text" class="form-control input-sm" name="max_assessed_price">--}}
					{{--</div>--}}
					{{--</div>--}}
					{{--</div>--}}

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label>Размер пошлины от</label>
								<input type="text" class="form-control input-sm" name="min_duty_price">
							</div>
							<div class="col-sm-6">
								<label>Размер пошлины до</label>
								<input type="text" class="form-control input-sm" name="max_duty_price">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label>Контрольная стоимость от</label>
								<input type="text" class="form-control input-sm" name="min_total_price">
							</div>
							<div class="col-sm-6">
								<label>Контрольная стоимость до</label>
								<input type="text" class="form-control input-sm" name="max_total_price">
							</div>
						</div>
					</div>

					<hr />

					<div class="form-group">
						<label>По ID отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_id">
					</div>

					<div class="form-group">
						<label>По номеру отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_number">
					</div>

					<div class="form-group">
						<label>По эл. почте отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_email">
					</div>

					<div class="form-group">
						<label>По ФИО отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_full_name">
					</div>

					<div class="form-group">
						<label>По ИНН отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_inn">
					</div>

					<div class="form-group">
						<label>По паспорту отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_passport_data">
					</div>

					<div class="form-group">
						<label for="inputCountrySenderId">По стране отправителя</label>
						<div class="ui-widget">
							<input id="inputCountrySenderId" autocomplete="off" type="text" class="form-control input-sm" name="sender_country">
						</div>
					</div>

					<div class="form-group">
						<label>По городу отправителя</label>
						<div class="ui-widget">
							<input id="inputCitySenderId" autocomplete="off" type="text" class="form-control input-sm" name="sender_city" disabled>
						</div>
					</div>

					<div class="form-group">
						<label>По адресу отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_address">
					</div>

					<div class="form-group">
						<label>По индексу отправителя</label>
						<input type="text" class="form-control input-sm" name="sender_index">
					</div>

					<hr />

					<div class="form-group">
						<label>По ID получателя</label>
						<input type="text" class="form-control input-sm" name="recipient_id">
					</div>

					<div class="form-group">
						<label>По номеру получателя</label>
						<input type="text" class="form-control input-sm" name="recipient_number">
					</div>

					<div class="form-group">
						<label>По эл. почте получателя</label>
						<input type="text" class="form-control input-sm" name="recipient_email">
					</div>

					<div class="form-group">
						<label>По ФИО получателя</label>
						<input type="text" class="form-control input-sm" name="recipient_full_name">
					</div>

					<div class="form-group">
						<label>По стране отправителя</label>
						<div class="ui-widget">
							<input id="inputCountryRecipientId" autocomplete="off" type="text" class="form-control input-sm" name="recipient_country">
						</div>
					</div>

					<div class="form-group">
						<label>По городу отправителя</label>
						<div class="ui-widget">
							<input id="inputCityRecipientId" type="text" class="form-control input-sm" name="recipient_city" disabled>
						</div>
					</div>

					<div class="form-group">
						<label>По адресу отправителя</label>
						<input type="text" class="form-control input-sm" name="recipient_address">
					</div>

					<div class="form-group">
						<label>По индексу отправителя</label>
						<input type="text" class="form-control input-sm" name="recipient_index">
					</div>

					<hr />

					<div class="form-group">
						<label>По ФИО работника принявшего МЭО</label>
						<input type="text" class="form-control input-sm" name="name_employee_accepted">
					</div>

					<div class="form-group">
						<label>По ФИО работника выдавшего МЭО</label>
						<input type="text" class="form-control input-sm" name="name_employee_issued">
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
					<button type="submit" class="btn btn-primary pull-right">Найти трекер</button>
				</div>
			</div>
		</form>
	</div>
</div>
