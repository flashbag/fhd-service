@extends('admin.layouts.app')

@section('search-help-block')
	<span class="help-block help-block-static">
			{{  __('warehouses.search_help_block') }}
		</span>
@endsection

@section('content')

	<div class="row">
		<div class="col-md-9">
			<h1>Редактирование/просмотр склада</h1>
		</div>

		<div class="col-md-3">
			<div class="pull-right">
				<a href="{{ route('admin.batch.trackers', $batch->id) }}" class="btn btn-success">Список трекеров данной партии</a>
			</div>
		</div>
	</div>

	<br>
	<br>

	<div class="panel panel-default">
		<div class="panel-heading">Информация о складе</div>
		<div class="panel-body">

			<div class="row">
				<div class="form-group col-sm-3">
					<label for="inputNameUser">Наименование склада</label>
					<input type="text" class="form-control" name="name"
						   id="inputNameUser"  value="{{ $batch->warehouse->name }}" readonly="readonly">
				</div>

				<div class="form-group col-sm-3">
					<label for="inputProvince">Регион/Штат</label>
					<input type="text" class="form-control" name="region"
						   id="inputSenderRegion" value="{{ $batch->warehouse->region }}" readonly="readonly">
				</div>

				<div class="form-group col-sm-3">
					<label for="inputCitySenderId">Город</label>
					<input type="text" class="form-control" name="city"
						   id="inputCitySenderId" value="{{ $batch->warehouse->city }}" readonly="readonly">

				</div>

				<div class="form-group col-sm-3">
					<label for="inputAddress">Адрес</label>
					<input type="text" class="form-control" name="address"
						   id="inputAddressSenderId" value="{{ $batch->warehouse->address }}" readonly="readonly">
				</div>

			</div>

		</div>
	</div>

	<form action="{{ route('admin.batch.update', $batch->id) }}" method="post">

		<input name="_method" type="hidden" value="PUT">

		{{ csrf_field() }}

		<div class="panel panel-default">
			<div class="panel-heading">
				Информация об партии
			</div>
			<div class="panel-body">

				<div class="row">
					<div class="col-md-4 form-group">
						<label>{{ trans('admin.select_warehouse') }}</label>
						<select class="form-control selectpicker" readonly="readonly" disabled="disabled">
							<option value=""> </option>
							@foreach($warehouses as $warehouse)
								<option value="{{ $warehouse->id }}" @if(old('warehouse_id') == $warehouse->id || $batch->warehouse_id == $warehouse->id) selected @endif>{{ $warehouse->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-4 form-group">
						<label>Название</label>
						<input type="text" class="form-control" name="name" value="{{ old('name', $batch->name) }}" >
					</div>

					<div class="col-md-4 form-group">
						<label>Уникальный номер</label>
						<input type="text" class="form-control" value="{{ $batch->number }}" disabled="disabled">
					</div>
				</div>

			</div>
			<div class="panel-footer">
				<div class="pull-left">
					<a href="{{ route('warehouse') }}" class="btn btn-default">Назад к скаладам</a>
				</div>
				<div class="pull-right">
					<button type="submit" class="btn btn-success">Сохранить</button>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

	</form>


@endsection

@push('script')
	<script type="text/javascript">

		$(document).ready(function() {

		});

	</script>
@endpush
