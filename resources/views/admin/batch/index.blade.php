@extends('admin.layouts.app')

@section('content')

    <h1 class="pull-left">Список партий отправок</h1>

	<a href="{{ route('admin.batch.create') }}" class="btn btn-success pull-right">
		{{ __('admin.batches.create') }}
	</a>

    <div class="clearfix"></div>

    <table id="myTable"	class="table table-bordered  table-tracker">
        <thead>
        <tr>
            <th>ID</th>
			<th>Дата создания</th>
			<th>Дата обновления</th>
			<th>Склад</th>
			<th>Уникальный номер</th>
            <th>Название</th>
            <th>Кол-во трекеров</th>

            <th colspan="3">Действия</th>
        </tr>
        </thead>
            @foreach($batches as $batch)
            <tr>
                <td>{{ $batch->id }}</td>
				<td>{{ $batch->created_at }}</td>
				<td>{{ $batch->updated_at }}</td>
				<td>{{ $batch->warehouse->name }}</td>
				<td>{{ $batch->number }}</td>
                <td>{{ $batch->name }}</td>
				<td>{{ $batch->trackers->count() }}</td>

				<td><a href="{{ route('admin.batch.trackers', $batch->id) }}">Трекеры</a></td>
                <td><a href="{{ route('admin.batch.edit', $batch->id) }}">Изменить</a></td>
				<td><a href="{{ route('admin.batch.delete', $batch->id) }}" class="remove-item">Удалить</a></td>

            </tr>
        @endforeach
    </table>

	<br>
	<br>
	<br>

	{!! $batches->appends(\Request::except('page'))->render() !!}

	<br>
	<br>
	<br>

@endsection



@push('script')
	<script>

		$(function(){

			function removeItem(url) {



				$.ajax({
					url: url,
					method: 'POST',
					data: {
						"_token": '{{ csrf_token() }}',
						"_method": 'DELETE'
					},
					success: function (response) {
						location.reload();
					},
					error: function (response) {
						alert('Ошибка!')
					}
				});

			}

			$('.remove-item').on('click', function (e) {

				e.preventDefault();

				var url = $(e.target).attr('href');

				var isConfirmed = confirm('Вы уверены что хотите удалить партию?');

				if (!isConfirmed) {
					return false;
				}

				// alert(isConfirmed);
				removeItem(url);

			});

		});

	</script>
@endpush
