@extends('admin.layouts.app')

@section('content')

	<div class="row">
		<div class="col-md-9">
			<h1>Создание партии</h1>
		</div>


	</div>

	<br>
	<br>

	<form action="{{ route('admin.batch.store') }}" method="post">

		{{--<input name="_method" type="hidden" value="PUT">--}}

		{{ csrf_field() }}

		<div class="panel panel-default">
			<div class="panel-heading">
				Информация об партии отправки
			</div>
			<div class="panel-body">

				<div class="row">
					<div class="col-md-4 form-group">
						<label>{{ trans('admin.select_warehouse') }}</label>
						<select class="form-control selectpicker" name="warehouse_id">
							<option value=""> </option>
							@foreach($warehouses as $warehouse)
								<option value="{{ $warehouse->id }}" @if(Request::get('warehouse_id') == $warehouse->id) selected @endif>{{ $warehouse->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-4 form-group">
						<label>Название</label>
						<input type="text" class="form-control" name="name" value="{{ old('name') }}" >
					</div>

				</div>

			</div>
			<div class="panel-footer">
				<div class="pull-left">
					<a href="{{ route('admin.batch.index') }}" class="btn btn-default">Назад к списку партий</a>
				</div>
				<div class="pull-right">
					<button type="submit" class="btn btn-success">Сохранить</button>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

	</form>


@endsection

@push('script')
	<script type="text/javascript">

		function warehouseUsersAjaxSelect($select, route) {

			let users_ids = [];

			$select.find('option').each(function(){
				users_ids.push($(this).val());
			});

			$select.on('select2:select', function (e) {
				var data = e.params.data;
				users_ids.push(data.id);
			});

			// console.log('existing users ids:', users_ids);

			$select.select2({
				placeholder: "Выберите пользователей",
				minimumInputLength: 2,
				ajax: {
					url: route,
					dataType: 'json',
					data: function (params) {
						return {
							query: params.term,
							page: params.page,
							users_ids: users_ids,
							_token: '{{ csrf_token() }}'
						};
					},
					processResults: function (data, params) {
						return {
							results: data.results,
							pagination: {
								more: data.more
							}
						};
					},
					cache: false
				}
			});

		}

		$(document).ready(function() {

			warehouseUsersAjaxSelect($('#warehouse_admins'), '{{ route('get-free-warehouse-admins') }}');
			warehouseUsersAjaxSelect($('#warehouse_operators'), '{{ route('get-free-warehouse-operators') }}');
			warehouseUsersAjaxSelect($('#warehouse_couriers'), '{{ route('get-free-warehouse-couriers') }}');

		});

	</script>
@endpush
