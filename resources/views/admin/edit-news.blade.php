@extends('admin.layouts.app')

@section('content')

    <h1>Изменение новости</h1>

    <form action="{{ route('update-news', $news->id) }}" method="post">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">Поля для заполнения</div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-8 {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="inputTitle">Название статьи</label>
                        <input type="text" class="form-control" id="inputTitle" name="title" value="{{ old('title', $news->title) }}">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('id_category') ? ' has-error' : '' }}">
                        <label for="imputIdCategory">Категория</label>
                        <select id="imputIdCategory" class="form-control selectpicker id_category" name="id_category">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if($news->id_category == $category->id) selected @endif>{{ $category->title }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('id_category'))
                            <span class="help-block">
                                <strong>{{ $errors->first('id_category') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-12 {{ $errors->has('short_description') ? ' has-error' : '' }}">
                        <label for="textareaShortDescription">Краткое описание</label>
                        <textarea rows="5" id="textareaShortDescription" name="short_description" class="form-control summernote-medium" style="opacity: 0;">{{ old('short_description', $news->short_description) }}</textarea>
                        @if ($errors->has('short_description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('short_description') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="textareaDescription">Полное описание</label>
                        <textarea rows="10" id="textareaDescription" name="description" class="form-control summernote-medium" style="opacity: 0;">{{ old('description', $news->description) }}</textarea>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" @if($news->is_active == 1)checked value="1" @endif name="is_active"> Активировать новость
                    </label>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <a href="{{ route('news') }}" class="btn btn-default">Назад к новостям</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Изменить новость</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </form>

@endsection

@push('script')

    <script>

        $(document).ready(function () {

            $('.summernote-medium').summernote({
                height: 200,
                lang: 'ru-RU',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']],
                    ['link', ['linkDialogShow', 'unlink']],
                    ['color', ['color']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['para', ['ul', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture']]
                ],
            });

        });

    </script>

@endpush
