@extends('admin.layouts.app')

@section('content')

    <h1>Изменение пользователя</h1>

    <form action="{{ route('update-users', $user->id) }}" method="post">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">Поля для заполнения</div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('name_user') ? ' has-error' : '' }}">
                        <label for="inputNameUser">ФИО пользователя</label>
                        <input type="text" class="form-control" id="inputNameUser" name="name_user" value="{{ old('name_user', $user->name) }}">
                        @if ($errors->has('name_user'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name_user') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="inputEmail">Email</label>
                        <input type="text" class="form-control" id="inputEmail" name="email" value="{{ old('email', $user->email) }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('role_id') ? ' has-error' : '' }}">
                        <label for="roleId">Роль пользователя</label>
                        <select id="roleId" class="form-control selectpicker role_id" name="role_id">
                            @foreach($roles as $role)
                                <option @if($userRole == $role->id) selected @endif value="{{ $role->id }}">{{ old('role_id', $role->name) }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('role_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role_id') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <a href="{{ route('users') }}" class="btn btn-default">Назад к пользователям</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Изменить пользователя</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </form>

@endsection