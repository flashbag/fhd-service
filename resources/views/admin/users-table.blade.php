@if(isset($users) && $users->count() < 1)

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Информация</div>

				<div class="panel-body">
					Пользователей не найдено
					@if (request()->ajax())
						Попробуйте отменить или изменить фильтры.
					@endif
				</div>
			</div>
		</div>
	</div>

@else

	<table class="table table-bordered table-tracker">
		<tr>
			<th>ID</th>
			<th>Телефон</th>
			<th>Email</th>
			<th>ФИО</th>
			<th>Никнейм</th>
			<th>Дата регистрации</th>
			<th>Редактировать</th>
		</tr>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->id }}</td>
				<td>{{ $user->number }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->name }}</td>
				<td>{{ $user->nickname }}</td>
				<td>{{ $user->created_at }}</td>
				<td><a href="{{ route('edit-users', $user->id) }}">Изменить</a></td>
			</tr>
		@endforeach
	</table>

	<br>

	{!! $users->appends(\Request::except('page'))->render() !!}

@endif
