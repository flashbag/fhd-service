@extends('admin.layouts.app')

@section('content')

    <h1>Изменение категории</h1>

    <form action="{{ route('update-news-category', $category->id) }}" method="post">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">Поля для заполнения</div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-8 {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="inputTitle">Название категории</label>
                        <input type="text" class="form-control" id="inputTitle" name="title" value="{{ old('title', $category->title) }}">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('parent_id') ? ' has-error' : '' }}">
                        <label for="typeTracker">Родительская категория</label>
                        <select class="form-control selectpicker parent_id_category" name="parent_id">
                            @if($category->parent_id == null)
                                <option selected style="display: none;">--</option>
                            @endif
                            @foreach($categories as $cat)
                                @if($cat->id != $category->id)
                                    <option value="{{ $cat->id }}" @if($category->parent_id == $cat->id) selected @endif>{{ $cat->title }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_id') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="textareaDescription">Описание категории</label>
                        <textarea rows="5" id="textareaDescription" name="description" class="form-control summernote" style="opacity: 0;">{{ old('description', $category->description) }}</textarea>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" checked value="1" name="is_active"> Активировать категорию
                    </label>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <a href="{{ route('news-categories') }}" class="btn btn-default">Назад к категориям</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Изменить категорию</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </form>

@endsection