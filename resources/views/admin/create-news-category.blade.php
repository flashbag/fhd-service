@extends('admin.layouts.app')

@section('content')

    <h1>Создание категории</h1>

    <form action="{{ route('store-news-category') }}" method="post">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">Поля для заполнения</div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-8 {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="inputTitle">Название категории</label>
                        <input type="text" class="form-control" id="inputTitle" name="title" value="{{ old('title') }}">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('parent_id') ? ' has-error' : '' }}">
                        <label for="typeTracker">Родительская категория</label>
                        <select class="form-control selectpicker parent_id_category" name="parent_id">
                            <option selected style="display: none;" value="">--</option>
                        </select>
                        @if ($errors->has('parent_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent_id') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="textareaDescription">Описание категории</label>
                        <textarea rows="5" id="textareaDescription" name="description" class="form-control summernote" style="opacity: 0;">{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" checked value="1" name="is_active"> Активировать категорию
                    </label>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <a href="{{ route('news-categories') }}" class="btn btn-default">Назад к категориям</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Создать категорию</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </form>

@endsection

 @push('script')
     
     <script>
         
         $(document).ready(function () {
             
             $('.parent_id_category').click(function () {
                 var parent_id_category = $('select[name="parent_id"]');
                 if (!parent_id_category.hasClass('fetch-categories')) {
                     $.get('/admin/news/get/categories', function (data) {
                         if (data !== null) {
                             parent_id_category.addClass('fetch-categories');
                             parent_id_category.empty();
                             parent_id_category.append('<option selected style="display: none;" value="">--</option>');
                             data.forEach(function (item) {
                                 if (item.parent_id == null){
                                     parent_id_category.append('<option value="' + item.id + '">' + item.title + '</option>');
                                     data.forEach(function (childItem) {
                                         if(item.id == childItem.parent_id){
                                             parent_id_category.append('<option value="' + childItem.id + '" style="padding-left: 50px;"> — ' + childItem.title + '</option>');
                                         }
                                     })
                                 }
                             });
                             parent_id_category.selectpicker('refresh');
                         }
                     }, 'json');
                 }
             });
             
         });
         
     </script>

 @endpush
