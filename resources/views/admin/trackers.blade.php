@extends('admin.layouts.app')

@section('content')

	<div class="overflow-hidden">

    <h1>{{ $title }}</h1>

	<div class="flex-wrapp pt-4">
		<div class="col-flex col-flex_order-0 mr-2 mb-2">

{{--			@if(!Route::is('trackers'))--}}
{{--				<button type="button" class="btn btn-info search-modal" data-toggle="modal" data-target="#myModal"> {{ trans('admin.find_tracker') }}</button>--}}
{{--			@endif--}}

			<a class="btn btn-primary" data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseExample">
                {{ trans('admin.filters') }}
			</a>

		</div>

		@if (!Auth::user()->isWarehouseUser())
			<div class="col-flex col-flex_select">

				<select class="form-control selectpicker" name="warehouse_id">
					<option value=""> {{ trans('admin.select_warehouse') }}</option>
					@foreach($warehouses as $warehouse)
						<option value="{{ $warehouse->id }}" @if(Request::get('warehouse_id') == $warehouse->id) selected @endif>{{ $warehouse->name }}</option>
					@endforeach
				</select>

			</div>
		@endif

		<div class="col-flex col-flex_order-2 mr-2 mb-2">

			<a href="{{ route('admin-create-trackers') }}" class="btn btn-success pull-right">
                {{ trans('admin.add') }}
			</a>

		</div>


		<div class="col-flex col-flex_order-1 mr-2 mb-2">

			<a href="#" class="btn btn-info btn-print" data-toggle="modal">
				{{ __('trackers.print_options.downloadCleanPDFTitle' ) }}
			</a>

		</div>

	</div>
	
	<div class="clearfix"></div>


	@include('filters.filters-trackers')

	<br>
    <br>

	<div class="clearfix"></div>



	<form method="post" action="{{ route('update-status-trackers') }}">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT">

		<div id="filters-container">
		@include('admin/trackers-table')
		</div>

		<br>
		<hr>
		<br>

        @if(!Route::is('trackers-trash'))

		<div class="row panel-footer tracker-footer">

			<div class="col-xs-12 col-md-12">

				<label><small> {{ trans('admin.change_status') }}</small></label>

				<span class="fix-click-status">

					<select class="selectpicker" name="status_id">
						<option selected style="display: none;" value="">--</option>
					</select>
				</span>

				<button type="submit" class="form-group btn btn-success"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>

			{{--<div class="col-md-3">--}}
				{{--<label><small>Отправить в другой склад</small></label>--}}

				{{--<select class="selectpicker" name="tracker_to_warehouse_id">--}}
					{{--<option selected style="display: none;" value="">--</option>--}}
					{{--@foreach($warehouses as $warehouse)--}}
						{{--<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>--}}
					{{--@endforeach--}}
				{{--</select>--}}

				{{--<button type="submit" class="form-group btn btn-success"><i class="fa fa-check" aria-hidden="true"></i></button>--}}
			{{--</div>--}}

		</div>

        @endif
	</form>

	</div>

	@include('admin.trackers-search-modal')
	@include('modals.modal-print')

@endsection



@push('script')

    <script>

        $(document).ready(function () {

        	let dataTable;
        	let $selectWarehouse = $('select[name="warehouse_id"]');

			$selectWarehouse.on('change', function (){
				let warehouseId = $(this).val();
				let url = window.location.href + '?warehouse_id=' + warehouseId;

				$.get(url, function (data) {
					$('#filters-container').html(data);
				}).fail(function( jqXhr, textStatus, errorThrown ){
					console.error( errorThrown );
					alert('Что то пошло не так!');
				});
			});

        	function tableTrackerDataTable() {
				$('.table-tracker').DataTable({
					'paging': false,
					'info': false,
					'searching': false,
					"columnDefs": [
						{
							"targets": [0, -1],
							"orderable": false
						}
					]
				});
			}

			window.ajaxFiltersCompleteCallback = function(){
				tableTrackerDataTable()
			}

			tableTrackerDataTable();


			$(document).on('click', '.table-tracker td', function(){
               if ($(this).index() != 0) {
                   if ($(this).parent().attr('data-tracker-id')) {
                       var link = $(this).parent().find('td:last-child').find('a').attr('href');
                       window.location.assign(link);
                   }
               }
            });

			$(document).on('click', '#allInputCheked', function(){

				var checkboxes = $(this).closest('form').find(':checkbox');
				if($(this).is(':checked')) {
					checkboxes.attr('checked', 'checked');
				} else {
					checkboxes.removeAttr('checked');
				}

			});

			$('.fix-click-status').on('click', function(){
				let $element = $(this);
				let $select = $element.find('select[name="status_id"]');

				if (!$select.hasClass('fetch-statuses')) {
					$.get('/admin/get/status-trackers', function (data) {
						if (data !== null) {
							$select.addClass('fetch-statuses');
							data.forEach(function (item) {
								$select.append('<option value="' + item.id + '">' + item.title + '</option>');
							});
							$select.selectpicker('refresh');
						}
					}, 'json');
				}
			});

        });

    </script>

@endpush
