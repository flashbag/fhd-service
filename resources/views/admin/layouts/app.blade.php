<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>FHD | Dashboard</title>

    <!-- Styles -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
	<link href="{{ mix('css/vendor.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" integrity="sha256-xqxV4FDj5tslOz6MV13pdnXgf63lJwViadn//ciKmIs=" crossorigin="anonymous" />

	@stack('style')
    @yield('textarea')
</head>
<body>
{{--loader--}}
{{--	@include('preloader')--}}
{{--loader--}}

    <style>
        @import url('//fonts.googleapis.com/css?family=Open+Sans:400,600');

        body{
            font-family: 'Open Sans', sans-serif;
        }

        #app{
            padding-top: 105px;
        }



        .navbar{
            margin-bottom: 50px;
            z-index: 999;
        }

        .content{
            min-height: 550px;
        }

        .content h1{
            font-size: 16px;
            margin-bottom: 15px;
            text-transform: uppercase;
            font-weight: bold;
            color: #000;
        }

        .content .panel .panel-heading{
            font-weight: 600;
        }

        .content .panel label{
            font-size: 12px;
            font-weight: 600;
        }

        .content .panel .panel-body{
            border-bottom: 1px solid #d3e0e9;
        }

        .content .panel .panel-body:last-child{
            border-bottom: none;
        }

        .content .panel .panel-footer{
            background: #fff;
            border-top: none;
        }

        .content .table-tracker{
            background: #fff;
            border-color: #d3e0e9;
            margin-bottom: -1px;
        }

		.content .table-tracker th {
			background-color: #fff;
		}

		.content .table-tracker th,
		.content .table-tracker td{
            border-color: #d3e0e9;
            padding: 10px 15px;
            text-align: center;
            vertical-align: middle;
        }

        .content .pagination{
            margin: 0;
        }

        .content .dashboard-home .panel-body{
            padding: 12px 15px;
        }

        .tracker-footer{
            background: #fff;
            border: 1px solid #d3e0e9;
        }

        .normal-tracker{
            background: #e7ffe4 !important;
        }

        .dataTables_wrapper tr:hover td{
            cursor: pointer;
        }

        .normal-tracker td {
            color: #000;
        }

        .normal-tracker td a{
            color: #000;
        }

        .tracker-unprocessed{
            background-color: rgb(255, 255, 231) !important;
        }

        .tracker-disabled{
            background-color: rgb(255, 234, 234) !important;
        }

        .tracker-disabled td {
            color: #000;
        }

        .tracker-disabled td a{
            color: #000;
        }

		.form-group. label:after {
            content:"*";
            color:red;
            font-weight: bold;
        }
        .ui-autocomplete{
            z-index: 99999 !important;
        }
    </style>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">

				<!-- Collapsed Hamburger -->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<!-- Branding Image -->
				<a class="navbar-brand" href="{{ route('dashboard-home') }}">
					<b>FHD</b> Dashboard
				</a>
			</div>

			<div class="collapse navbar-collapse" id="app-navbar-collapse">
				<!-- Left Side Of Navbar -->
				<ul class="nav navbar-nav">

					<li class="@if( Route::is('trackers') || Route::is('create-trackers') || Route::is('admin-edit-trackers')
					 || Route::is('trackers-ready') || Route::is('trackers-unprocessed') || Route::is('trackers-processed') || Route::is('trackers-trash') )active @endif dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ trans('admin.trackers') }} <span class="caret"></span>
						</a>

						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ route('trackers-unprocessed') }}"> {{ trans('admin.unprocessed') }}</a></li>
							<li><a href="{{ route('trackers-processed') }}">{{ trans('admin.processed') }}</a></li>

							@if(Auth::user()->isWarehouseOperator())
								<li><a href="{{ route('trackers-from-warehouse') }}">{{ trans('admin.departure') }}</a></li>
								<li><a href="{{ route('trackers-to-warehouse') }}">{{ trans('admin.receive') }}</a></li>
							@else
								<li><a href="{{ route('trackers-ready') }}">{{ trans('admin.ready_departure') }}</a></li>
							@endif
							<li><a href="{{ route('trackers-trash') }}">{{ trans('admin.trash') }}</a></li>
							<li><a href="{{ route('trackers') }}">{{ trans('admin.all') }}</a></li>
							<li><a href="{{ route('admin-create-trackers') }}">{{ trans('admin.add') }}</a></li>

						</ul>
					</li>
                    <li class="@if( Route::is('admin.batch.index') || Route::is('admin.batch.create') || Route::is('admin.batch.edit') )active @endif dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ __('admin.batches.title') }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('admin.batch.index') }}">{{ __('admin.batches.all') }}</a></li>
                            <li><a href="{{ route('admin.batch.create') }}">{{ __('admin.batches.create') }}</a></li>
                        </ul>
                    </li>
					@if(!Auth::user()->isWarehouseUser())
						<li @if( Route::is('users') || Route::is('create-users') || Route::is('edit-users') )class="active" @endif><a href="{{ route('users') }}">Пользователи</a></li>
						<li @if( Route::is('warehouse'))class="active" @endif><a href="{{ route('warehouse') }}">{{ trans('admin.warehouse') }}</a></li>
						<li @if( Route::is('settings'))class="active" @endif><a href="{{ route('settings') }}">Настройки сайта</a></li>
					@endif
                    @if(Auth::user()->hasRole('Warehouse'))
                        <li @if( Route::is('my_tickets_warehouse') )class="active" @endif><a href="{{ route('my_tickets_warehouse') }}">Тикеты</a></li>
                    @endif

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" style="text-transform: uppercase;" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Session::get('locale') ? Session::get('locale') : 'RU' }}
							<span class="caret"></span>
						</a>

						<ul class="dropdown-menu dropdown-menu__language" role="menu">
							<li @if(Session::get('locale') == 'en') class="active" @endif><a href="{{ route('set-locale', 'en') }}">EN</a></li>
							<li @if(Session::get('locale') == 'ru') class="active" @endif><a href="{{ route('set-locale', 'ru') }}">RU</a></li>
						</ul>
					</li>


				</ul>


				<!-- Right Side Of Navbar -->
				<ul class="nav navbar-nav navbar-right">
					<!-- Authentication Links -->

					<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								{{ Auth::user()->name }} <span class="caret"></span>
							</a>

							<ul class="dropdown-menu" role="menu">
								<li><a href="/" target="_blank">{{ trans('admin.site') }}</a></li>
								<li>
									<a href="{{ route('logout') }}"
										onclick="event.preventDefault();
												 document.getElementById('logout-form').submit();">
                                        {{ trans('admin.logout') }}
									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container-fluid" style="padding: 0 25px;">
		<div class="row">
			<div class="col-sm-12 content">

					@include('flash-messages')

				@yield('content')
			</div>
		</div>
	</div>

    <!-- Scripts -->
    <script src="{{ mix('js/vendor/jquery.js') }}"></script>
	<script src="{{ mix('js/vendor/bootstrap.js') }}"></script>
	<script src="{{ mix('js/vendor/other.js') }}"></script>

	<!-- Used jquery-ui.dataTables for two elements, and as dependency of summernote -->
	<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.12.0/validate.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

	<script src="{{ mix('js/bundle.js') }}"></script>
	<script src="{{ mix('js/tracker.js') }}"></script>
	<script src="{{ mix('js/tracker-receptacles.js') }}"></script>

	{{-- Move this script after bundle.js, because initMap is undefined --}}
	<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCFpcv67_0e4zDzXROMA-AVxc5sUJnCIyc&libraries=places&language=en&region=US" async defer></script>

    @stack('script')

	<svg xmlns="http://www.w3.org/2000/svg" style="display: none;visibility: hidden;">
		<symbol id="error_SVG" viewBox="0 0 26 26">
			<path d="M13 26C20.1601 26 26 20.1824 26 13C26 5.81756 20.1601 0 13 0C5.83993 0 0 5.83993 0 13C0 20.1601 5.83993 26 13 26ZM13 1.83477C19.1532 1.83477 24.1652 6.84682 24.1652 13C24.1652 19.1532 19.1532 24.1652 13 24.1652C6.84682 24.1652 1.83477 19.1532 1.83477 13C1.83477 6.84682 6.84682 1.83477 13 1.83477Z" fill="#EB5757"></path>
			<path d="M8.12202 17.7438C8.30102 17.9228 8.52477 18.0123 8.7709 18.0123C9.01703 18.0123 9.24078 17.9228 9.41978 17.7438L12.9998 14.1638L16.5798 17.7438C16.7589 17.9228 16.9826 18.0123 17.2287 18.0123C17.4749 18.0123 17.6986 17.9228 17.8776 17.7438C18.2356 17.3858 18.2356 16.8041 17.8776 16.4461L14.2976 12.866L17.8776 9.28599C18.2356 8.92799 18.2356 8.34623 17.8776 7.98823C17.5196 7.63023 16.9379 7.63023 16.5798 7.98823L12.9998 11.5683L9.41978 7.98823C9.06178 7.63023 8.48002 7.63023 8.12202 7.98823C7.76401 8.34623 7.76401 8.92799 8.12202 9.28599L11.7021 12.866L8.12202 16.4461C7.76401 16.8041 7.76401 17.3858 8.12202 17.7438Z" fill="#EB5757"></path>
		</symbol>
		<symbol id="info_SVG" viewBox="0 0 29 26">
			<path d="M14.2204 18.271C13.4253 18.271 12.7588 18.9375 12.7588 19.7327C12.7588 20.5278 13.4253 21.1943 14.2204 21.1943C14.9864 21.1943 15.6821 20.5278 15.647 19.7677C15.6821 18.9317 15.0214 18.271 14.2204 18.271Z" fill="#0A5E74"></path>
			<path d="M27.7496 23.4509C28.6676 21.8664 28.6734 19.978 27.7613 18.3994L18.6055 2.54329C17.6993 0.947156 16.0622 0 14.2264 0C12.3905 0 10.7535 0.953002 9.84723 2.53744L0.6797 18.4111C-0.232376 20.0072 -0.226529 21.9074 0.69724 23.4918C1.60932 25.0587 3.24053 26 5.06468 26H23.353C25.183 26 26.8259 25.047 27.7496 23.4509ZM25.7618 22.3049C25.2531 23.1819 24.3527 23.7023 23.3471 23.7023H5.05883C4.0649 23.7023 3.17037 23.1936 2.6734 22.3342C2.17059 21.463 2.16475 20.4223 2.66756 19.5453L11.8351 3.67754C12.332 2.80639 13.2207 2.29188 14.2264 2.29188C15.2261 2.29188 16.1207 2.81223 16.6176 3.68338L25.7793 19.5512C26.2704 20.4048 26.2646 21.4338 25.7618 22.3049Z" fill="#0A5E74"></path>
			<path d="M13.8579 8.00953C13.1621 8.20831 12.7295 8.83975 12.7295 9.60566C12.7646 10.0675 12.7938 10.5353 12.8289 10.9972C12.9283 12.757 13.0277 14.4818 13.1271 16.2416C13.1621 16.838 13.624 17.2706 14.2204 17.2706C14.8167 17.2706 15.2845 16.8087 15.3137 16.2065C15.3137 15.844 15.3137 15.5108 15.3488 15.1424C15.4131 14.014 15.4833 12.8856 15.5476 11.7572C15.5827 11.0264 15.647 10.2956 15.682 9.56473C15.682 9.30163 15.647 9.06777 15.5476 8.8339C15.2494 8.17908 14.5536 7.84582 13.8579 8.00953Z" fill="#0A5E74"></path>
		</symbol>
		<symbol id="warning_SVG" viewBox="0 0 29 26">
			<path d="M14.2204 18.271C13.4253 18.271 12.7588 18.9375 12.7588 19.7327C12.7588 20.5278 13.4253 21.1943 14.2204 21.1943C14.9864 21.1943 15.6821 20.5278 15.647 19.7677C15.6821 18.9317 15.0214 18.271 14.2204 18.271Z" fill="#F1774F"></path>
			<path d="M27.7496 23.4509C28.6676 21.8664 28.6734 19.978 27.7613 18.3994L18.6055 2.54329C17.6993 0.947156 16.0622 0 14.2264 0C12.3905 0 10.7535 0.953002 9.84723 2.53744L0.6797 18.4111C-0.232376 20.0072 -0.226529 21.9074 0.69724 23.4918C1.60932 25.0587 3.24053 26 5.06468 26H23.353C25.183 26 26.8259 25.047 27.7496 23.4509ZM25.7618 22.3049C25.2531 23.1819 24.3527 23.7023 23.3471 23.7023H5.05883C4.0649 23.7023 3.17037 23.1936 2.6734 22.3342C2.17059 21.463 2.16475 20.4223 2.66756 19.5453L11.8351 3.67754C12.332 2.80639 13.2207 2.29188 14.2264 2.29188C15.2261 2.29188 16.1207 2.81223 16.6176 3.68338L25.7793 19.5512C26.2704 20.4048 26.2646 21.4338 25.7618 22.3049Z" fill="#F1774F"></path>
			<path d="M13.8579 8.00953C13.1621 8.20831 12.7295 8.83975 12.7295 9.60566C12.7646 10.0675 12.7938 10.5353 12.8289 10.9972C12.9283 12.757 13.0277 14.4818 13.1271 16.2416C13.1621 16.838 13.624 17.2706 14.2204 17.2706C14.8167 17.2706 15.2845 16.8087 15.3137 16.2065C15.3137 15.844 15.3137 15.5108 15.3488 15.1424C15.4131 14.014 15.4833 12.8856 15.5476 11.7572C15.5827 11.0264 15.647 10.2956 15.682 9.56473C15.682 9.30163 15.647 9.06777 15.5476 8.8339C15.2494 8.17908 14.5536 7.84582 13.8579 8.00953Z" fill="#F1774F"></path>
		</symbol>
		<symbol id="success_SVG" viewBox="0 0 32 32">
			<path d="M16 1C7.72923 1 1 7.72877 1 16C1 24.2712 7.72923 31 16 31C24.2708 31 31 24.2712 31 16C31 7.72877 24.2708 1 16 1ZM16 29.1538C8.74692 29.1538 2.84615 23.2531 2.84615 16C2.84615 8.74692 8.74692 2.84615 16 2.84615C23.2531 2.84615 29.1538 8.74692 29.1538 16C29.1538 23.2531 23.2531 29.1538 16 29.1538Z" fill="#219653" stroke="#219653" stroke-width="0.5"></path>
			<path d="M15.9286 11.5628C16.7813 11.5628 17.4725 10.8716 17.4725 10.0189C17.4725 9.1663 16.7813 8.4751 15.9286 8.4751C15.076 8.4751 14.3848 9.1663 14.3848 10.0189C14.3848 10.8716 15.076 11.5628 15.9286 11.5628Z" fill="#219653" stroke="#219653" stroke-width="0.5"></path>
			<path d="M16 14C15.448 14 15 14.4267 15 14.9524V23.0476C15 23.5733 15.448 24 16 24C16.552 24 17 23.5733 17 23.0476V14.9524C17 14.4262 16.552 14 16 14Z" fill="#219653" stroke="#219653" stroke-width="0.5"></path>
		</symbol>
	</svg>
</body>
</html>