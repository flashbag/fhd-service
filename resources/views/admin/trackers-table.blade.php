@if(isset($trackers) && $trackers->count() < 1)

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">{{ trans('admin.info') }}</div>

				<div class="panel-body">
                    {{ trans('admin.not_found') }}
					@if (request()->ajax())
						Попробуйте отменить или изменить фильтры.
					@endif
				</div>
			</div>
		</div>
	</div>

	<br>

@else

	<p>{{ trans('admin.buttons') }} <b>{{ trans('admin.shift') }} </b> {{ trans('admin.shift_text') }}</p>
	<table id="myTable" class="table table-bordered table-tracker">
		<thead>
		<tr>

		<th><input type="checkbox" id="allInputCheked"></th>
		<th>ID</th>
        <th>{{ trans('admin.tracker_number') }}</th>
		<th>{{ trans('admin.created_at') }}</th>
		<th>{{ trans('admin.updated_at') }}</th>
        <th>{{ trans('admin.date_of_issue') }}</th>
		<th>{{ trans('admin.invoice_number') }}</th>
		<th>{{ trans('admin.delivery_date') }}</th>
		<th>{{ trans('admin.status') }}</th>
		<th>{{ trans('admin.carrier') }}</th>
		<th>{{ trans('admin.container_number') }}</th>

		<th>{{ trans('admin.total_weight') }}</th>
		<th>{{ trans('admin.volume_weight') }}</th>
		<th>{{ trans('admin.assessed_value') }}</th>
		<th>{{ trans('admin.duty_rate') }}</th>
		<th>{{ trans('admin.cost_reference') }}</th>

		<th>{{ trans('admin.shipping_rate') }}</th>
		<th>{{ trans('admin.shipping_currency') }}</th>
		<th>{{ trans('admin.total_shipping_cost') }}</th>
		<th>{{ trans('admin.shipping_currency') }}</th>

		<th>{{ trans('admin.sender_phone') }}</th>
		<th>{{ trans('admin.sender_email') }}</th>
		<th>{{ trans('admin.sender_company') }}</th>
		<th>{{ trans('admin.sender_country') }}</th>

		<th>{{ trans('admin.recipient_phone') }}</th>
		<th>{{ trans('admin.recipient_email') }}</th>
		<th>{{ trans('admin.recipient_company') }}</th>
		<th>{{ trans('admin.recipient_country') }}</th>

		<th>{{ trans('admin.accepted_employee') }}</th>
		<th>{{ trans('admin.issued_employee') }}</th>
		@if (Route::is('trackers-trash'))
            <th>{{ trans('admin.delete_reason') }}</th>
            <th>{{ trans('admin.delete_by') }}</th>
			<th>{{ trans('admin.trash_restore') }}</th>
			<th>{{ trans('admin.trash_delete') }}</th>
		@else
			<th>{{ trans('admin.edit') }}</th>
		@endif
	</tr>
	</thead>

	@foreach($trackers as $tracker)

		@if(Route::is('trackers-unprocessed'))
			<tr class="tracker-unprocessed" data-tracker-id="{{ $tracker->id }}">
		@elseif(Route::is('trackers-trash'))
			<tr>
		@else
			<tr data-tracker-id="{{ $tracker->id }}" class="normal-tracker
			@if($tracker->is_active == 0) tracker-disabled @endif
			@if($tracker->is_active == 0 && $tracker->is_unprocessed == 1) tracker-unprocessed @endif
			@if($tracker->is_active == 0 && $tracker->is_unprocessed == 0) tracker-disabled @endif
			">
		@endif

			<td><input type="checkbox" name="trackers[]" value="{{ $tracker->id }}"></td>

			<td>{{ $tracker->id }}</td>
            <td>{{ $tracker->id_tracker }}</td>


			<td>{{ $tracker->created_at }}</td>
			<td>{{ $tracker->updated_at }}</td>

			<td>{{ $tracker->date_registered }}</td>

			<td>{{ $tracker->number_invoice }}</td>
			<td>{{ $tracker->date_delivery ? $tracker->date_delivery : '-' }}</td>
			<td>{{ $tracker->status ? $tracker->status->title : $tracker->status_id }}</td>
			<td>{{ $tracker->carrier ? $tracker->carrier : '-' }}</td>
			<td>{{ $tracker->container_id ? $tracker->container_id : '-' }}</td>

			<td>{{ $tracker->receptacles->sum('actual_weight') }}</td>
			<td>{{ $tracker->receptacles->sum('volume_weight') }}</td>
			<td>{{ $tracker->receptacles->sum('assessed_price') }}</td>

			<td>
				@if($tracker->duty_price === null)
					-
				@endif
			</td>

			<td>{{ $tracker->total_price ? $tracker->total_price : '-' }}</td>

			<td>{{ $tracker->delivery_rate_kg ? $tracker->delivery_rate_kg : '-' }}</td>
			<td>{{ @$tracker->del_rate_currency->code ? $tracker->del_rate_currency->code : '-' }}</td>
			<td>{{ $tracker->total_ship_cost ? $tracker->total_ship_cost : '-' }}</td>
			<td>{{ @$tracker->ship_rate_currency->code ? $tracker->ship_rate_currency->code : '-' }}</td>

			<td>{{ $tracker->sender && $tracker->sender->number ? $tracker->sender->number : '-' }}</td>
			<td>{{ $tracker->sender && $tracker->sender->email ? $tracker->sender->email : '-' }}</td>
			<td>{{ $tracker->sender && $tracker->sender->full_name ? $tracker->sender->full_name : '-' }}</td>
			<td>{{ $tracker->sender && $tracker->sender->country ? $tracker->sender->country : '-' }}</td>

			<td>{{ $tracker->recipient && $tracker->recipient->number ? $tracker->recipient->number : '-' }}</td>
			<td>{{ $tracker->recipient && $tracker->recipient->email ? $tracker->recipient->email : '-' }}</td>
			<td>{{ $tracker->recipient && $tracker->recipient->full_name ? $tracker->recipient->full_name : '-' }}</td>
			<td>{{ $tracker->recipient &&  $tracker->recipient->country ? $tracker->recipient->country : '-' }}</td>

			<td>{{ $tracker->name_employee_accepted ? $tracker->name_employee_accepted : '-' }}</td>
			<td>{{ $tracker->name_employee_issued ? $tracker->name_employee_issued : '-' }}</td>

			@if(Route::is('trackers-trash'))
                <td>{{$tracker->delete_reason}}</td>
                <td>{{\App\User::where('id', $tracker->delete_by)->value('name')}}</td>
						<td>
					<form method="post" action="{{ route('recovery-trackers', $tracker->id) }}">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-default"><i class="fa fa-check" style="color: green;"></i></button>
					</form>
				</td>
				<td>
					<form method="post" action="{{ route('force-delete-trackers', $tracker->id) }}">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">
						<button type="submit" class="btn btn-default"><i class="fa fa-trash" style="color: red;"></i></button>
					</form>
				</td>

			@else
			<td><a href="{{ route('admin-edit-trackers', $tracker->id) }}">{{ trans('admin.change') }}</a></td>
			@endif
		</tr>
	@endforeach
</table>

<br>

{!! $trackers->appends(\Request::except('page'))->render() !!}
@endif
