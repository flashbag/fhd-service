@extends('admin.layouts.app')

@section('content')
    <div class="pull-left">
        <a href="{{ url()->previous() }}" class="btn btn-default">Назад к складу</a>
    </div>

	<br>
	<div class="clearfix"></div>
    <br>

	<div id="trackers-table">
		@include('admin.trackers-table')
	</div>

    <br>
    <div class="pull-left">
        <a href="{{  url()->previous()  }}" class="btn btn-default">Назад к складу</a>
    </div>
@endsection

