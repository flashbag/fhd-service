@extends('admin.layouts.app')

@section('search-help-block')
	<span class="help-block help-block-static">
			{{  __('warehouses.search_help_block') }}
		</span>
@endsection

@section('content')

	<div class="row">
		<div class="col-md-9">
			<h1>Редактирование/просмотр склада</h1>
		</div>

		<div class="col-md-3">
			<div class="pull-right">
				<a href="{{ route('tracker-warehouse', $warehouse->id) }}" class="btn btn-success">Список трекеров данного склада</a>
			</div>
		</div>
	</div>

	<br>
	<br>

	<div class="panel panel-default">
		<div class="panel-heading">
			Информация о стране &nbsp;&nbsp;&nbsp;
			<img src="/img/flags/{{ $warehouse->country->flag }}" alt="">
		</div>
		<div class="panel-body">

			<div class="row">
				<div class="col-md-3 form-group">
					<label>Название</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->name }}" disabled="disabled">
				</div>

				<div class="col-md-3 form-group">
					<label>Код ISO-3166 (2 letters)</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->iso_3166_2 }}" disabled="disabled">
				</div>

				<div class="col-md-3 form-group">
					<label>Код ISO-3166 (3 letters)</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->iso_3166_3 }}" disabled="disabled">
				</div>

				<div class="col-md-3 form-group">
					<label>Код страны</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->country_code }}" disabled="disabled">
				</div>

			</div>

			<div class="row">

				<div class="col-md-3 form-group">
					<label>Валюта</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->currency }}" disabled="disabled">
				</div>

				<div class="col-md-3 form-group">
					<label>Код валюты</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->currency_code }}" disabled="disabled">
				</div>

				<div class="col-md-3 form-group">
					<label>Символ валюты</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->currency_symbol }}" disabled="disabled">
				</div>

				<div class="col-md-3 form-group">
					<label>Телефонный код</label>
					<input type="text" class="form-control" value="{{ $warehouse->country->phone_code }}" disabled="disabled">
				</div>

			</div>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">Информация о складе</div>
		<div class="panel-body">

			<div class="row">
				<div class="form-group col-sm-3">
					<label for="inputNameUser">Наименование склада</label>
					<input type="text" class="form-control" name="name"
						   id="inputNameUser"  value="{{ $warehouse->name }}" readonly="readonly">
				</div>

				<div class="form-group col-sm-3">
					<label for="inputProvince">Регион/Штат</label>
					<input type="text" class="form-control" name="region"
						   id="inputSenderRegion" value="{{ $warehouse->region }}" readonly="readonly">
				</div>

				<div class="form-group col-sm-3">
					<label for="inputCitySenderId">Город</label>
					<input type="text" class="form-control" name="city"
						   id="inputCitySenderId" value="{{ $warehouse->city }}" readonly="readonly">

				</div>

				<div class="form-group col-sm-3">
					<label for="inputAddress">Адрес</label>
					<input type="text" class="form-control" name="address"
						   id="inputAddressSenderId" value="{{ $warehouse->address }}" readonly="readonly">
				</div>

			</div>

		</div>
	</div>


    <form action="{{ route('update-warehouse', $warehouse->id) }}" method="post">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-body">

				<h3>Здесь вы можете прикрепить админов склада, операторов и курьеров</h3>

				<br>
				<br>

				<div class="row">

					<div class="form-group col-sm-4">
						<label>
							<b>Администраторы склада</b>
						</label>
						<select id="warehouse_admins" name="warehouse_admins[]" class="form-control" multiple>
							@foreach($warehouse->admins as $user)
								<option selected="selected" value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
							@endforeach
						</select>
						@yield('search-help-block')
					</div>

					<div class="form-group col-sm-4">
						<label>
							<b>Работники/операторы склада</b>
						</label>
						<select id="warehouse_operators" name="warehouse_operators[]" class="form-control" multiple>
							@foreach($warehouse->operators as $user)
								<option selected="selected" value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
							@endforeach
						</select>
						@yield('search-help-block')
					</div>

					<div class="form-group col-sm-4">
						<label>
							<b>Курьеры склада</b>
						</label>
						<select id="warehouse_couriers" name="warehouse_couriers[]" class="form-control" multiple>
							@foreach($warehouse->couriers as $user)
								<option selected="selected" value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
							@endforeach
						</select>
						@yield('search-help-block')
					</div>
				</div>


                <div class="row">

                </div>
            </div>

            <div class="panel-footer">
                <div class="pull-left">
                    <a href="{{ route('warehouse') }}" class="btn btn-default">Назад к скаладам</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </form>


@endsection

@push('script')
	<script type="text/javascript">

		function warehouseUsersAjaxSelect($select, route) {

			let users_ids = [];

			$select.find('option').each(function(){
				users_ids.push($(this).val());
			});

			$select.on('select2:select', function (e) {
				var data = e.params.data;
				users_ids.push(data.id);
			});

			// console.log('existing users ids:', users_ids);

			$select.select2({
				placeholder: "Выберите пользователей",
				minimumInputLength: 2,
				ajax: {
					url: route,
					dataType: 'json',
					data: function (params) {
						return {
							query: params.term,
							page: params.page,
							users_ids: users_ids,
							_token: '{{ csrf_token() }}'
						};
					},
					processResults: function (data, params) {
						return {
							results: data.results,
							pagination: {
								more: data.more
							}
						};
					},
					cache: false
				}
			});

		}

		$(document).ready(function() {

			warehouseUsersAjaxSelect($('#warehouse_admins'), '{{ route('get-free-warehouse-admins') }}');
			warehouseUsersAjaxSelect($('#warehouse_operators'), '{{ route('get-free-warehouse-operators') }}');
			warehouseUsersAjaxSelect($('#warehouse_couriers'), '{{ route('get-free-warehouse-couriers') }}');

		});

	</script>
@endpush
