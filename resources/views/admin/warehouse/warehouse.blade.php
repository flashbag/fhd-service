@extends('admin.layouts.app')

@section('content')

    <h1 class="pull-left">Список складов</h1>

    <div class="clearfix"></div>

    <table id="myTable"	class="table table-bordered  table-tracker">
        <thead>
        <tr>
            <th>ID</th>
			<th>Страна</th>
			<th>Код страны</th>

            <th>Название склада</th>
            <th>Город</th>
            <th>Провинция</th>
            <th>Адрес</th>

            <th></th>
        </tr>
        </thead>
            @foreach($warehouses as $warehouse)
            <tr>
                <td>{{ $warehouse->id }}</td>
				<td>{{ $warehouse->country->name }}</td>
				<td>{{ $warehouse->country->iso_3166_2 }}</td>

                <td>{{ $warehouse->name }}</td>
                <td>{{ $warehouse->city }}</td>
                <td>{{ $warehouse->region }}</td>
                <td>{{ $warehouse->address }}</td>

                <td><a href="{{ route('edit-warehouse', $warehouse->id) }}">Изменить</a></td>
            </tr>
        @endforeach
    </table>
@endsection
