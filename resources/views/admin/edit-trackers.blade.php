@extends('admin.layouts.app')

@section('content')
	{{--loader--}}
		@include('preloader')
	{{--loader--}}
	<div class="container tracker-form_pt">

		<h1>{{ trans('admin.edit_trackers') }}</h1>
        <p class="mb-2">{{ trans('admin.attention_title') }}</p>

		<form class="ajax-form" id="tracker-form" data-ukraine-value="{{ __('main.ukraine') }}" action="{{ $formAction }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PUT">

			<input type="hidden" name="id_user" value="{{ $tracker->id_user }}">
			<input type="hidden" name="id" value="{{ $tracker->id }}">

			@include('admin.tracker-parts.fields-to-fill')

			@if(count($historyStatuses) > 0 && $historyStatuses !== null)
				<br>
				<div class="panel panel-default">
					<div class="panel-heading">{{ trans('admin.history_change_status') }}</div>
					<table class="table table-bordered table-tracker table-tracker__resize">
						<tr>
							<th>{{ trans('admin.status') }}</th>
							<th>{{ trans('admin.who_installed') }}</th>
							<th>{{ trans('admin.date') }}</th>
						</tr>
						@foreach($historyStatuses as $status)
							<tr>
								<td>{{ trans('admin.status') }}: {{ $status->status ? $status->status->title : '' }}</td>
								<td>{{ App\User::find($status->id_user)->name }}</td>
								<td>{{ $status->created_at }}</td>
							</tr>
						@endforeach
					</table>
				</div>
			@endif

			@include('admin.tracker-parts.receptacles-blocks')

			@include('admin.tracker-parts.price-delivery')

			@include('admin.tracker-parts.sender-info')

			@include('admin.tracker-parts.recipient-info')

			@include('admin.tracker-parts.additional')

			@include('admin.tracker-parts.employees-info')

		</form>
	</div>

	@include('modals.modal-print')
	@include('modals.modal-delete-trackers')
@endsection

@push('modal')
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			'use strict';

			var deleteBtn = document.querySelector('.modal-content_warning #rules-agreed');

			deleteBtn.addEventListener('click', function(e){
				var textareaDelete = document.querySelector('.modal-content_warning #textarea').value;
				var selectDelete = document.querySelector('.modal-content_warning select').value;

				if(textareaDelete.length < 1 && selectDelete.length < 1) {
					e.preventDefault();
					swal({
						className: "swal-modal_warningMini",
						title: "Вы не указали причину",
						text: "(You did not specify the reason)",
						icon: false,
						button: false
					});
				}else{
					return true;
				}
			});
		})
	</script>
@endpush
@stack('modal')
