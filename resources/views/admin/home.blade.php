@extends('admin.layouts.app')

@section('content')
    <h1>Админпанель</h1>

    <div class="row pt-4">
        <div class="col-sm-8 col-xs-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-map-signs fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $countTrackers }}</div>
                            <div>Кол-во трекеров</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="pull-left">За сегодня:</div>
                            <div class="pull-right"><b>{{ $countTrackersToday }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За неделю:</div>
                            <div class="pull-right"><b>{{ $countTrackersWeek }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За месяц:</div>
                            <div class="pull-right"><b>{{ $countTrackersMonth }}</b></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel-body">
                            <div class="pull-left">За 3 месяца:</div>
                            <div class="pull-right"><b>{{ $countTrackers3Months }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За 6 месяцев:</div>
                            <div class="pull-right"><b>{{ $countTrackers6Months }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За 1 год:</div>
                            <div class="pull-right"><b>{{ $countTrackers12Months }}</b></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-pencil fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $countNews }}</div>
                                    <div>Кол-во новостей</div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За сегодня:</div>
                            <div class="pull-right"><b>{{ $countNewsToday }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За неделю:</div>
                            <div class="pull-right"><b>{{ $countNewsWeek }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За месяц:</div>
                            <div class="pull-right"><b>{{ $countNewsMonth }}</b></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $countUsers }}</div>
                                    <div>Кол-во юзеров</div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За сегодня:</div>
                            <div class="pull-right"><b>{{ $countUsersToday }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За неделю:</div>
                            <div class="pull-right"><b>{{ $countUsersWeek }}</b></div>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left">За месяц:</div>
                            <div class="pull-right"><b>{{ $countUsersMonth }}</b></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-xs-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $countUserOnline }}</div>
                            <div>Администратор онлайн</div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if($users)
                        @for($a = 0, $j = 1; $a < $countUserOnline; $a++, $j++)
                            @if($users[$a]->isOnline())
                                @if($j < $countUserOnline)
                                    {{ $users[$a]->name }},
                                @else
                                    {{ $users[$a]->name }}
                                @endif
                            @endif
                        @endfor
                    @endif
                </div>
            </div>

        </div>
    </div>
    <br>
    <div class="row dashboard-home">

    </div>
@endsection
