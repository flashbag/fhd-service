@extends('admin.layouts.app')

@section('content')

    <h1>Настройки сайта</h1>

    <form action="{{ route('update-settings') }}" method="post">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">Поля для заполнения</div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-8 {{ $errors->has('title_site') ? ' has-error' : '' }}">
                        <label for="inputTitleSite">Название сайта</label>
                        <input type="text" class="form-control" id="inputTitleSite" name="title_site" value="{{ old('title_site', $settings->title_site) }}">
                        @if ($errors->has('title_site'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title_site') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-8 {{ $errors->has('short_description') ? ' has-error' : '' }}">
                        <label for="inputShortDescription">Краткое описание</label>
                        <input type="text" maxlength="128" class="form-control" id="inputShortDescription" name="short_description" value="{{ old('short_description', $settings->short_description) }}">
                        @if ($errors->has('short_description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('short_description') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="inputEmailSite">Email адрес</label>
                        <input type="text" class="form-control" id="inputEmailSite" name="email" value="{{ old('email', $settings->email) }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('count_news_admin') ? ' has-error' : '' }}">
                        <label for="inputCountNewsAdmin">Количество новостей на страницу админпанели</label>
                        <input type="text" max="20" class="form-control" id="inputCountNewsAdmin" name="count_news_admin" value="{{ old('count_news_admin', $settings->count_news_admin) }}">
                        @if ($errors->has('count_news_admin'))
                            <span class="help-block">
                                <strong>{{ $errors->first('count_news_admin') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('count_news_blog') ? ' has-error' : '' }}">
                        <label for="inputCountNewsBlog">Количество новостей на страницу блога</label>
                        <input type="text" max="20" class="form-control" id="inputCountNewsBlog" name="count_news_blog" value="{{ old('count_news_blog', $settings->count_news_blog) }}">
                        @if ($errors->has('count_news_blog'))
                            <span class="help-block">
                                <strong>{{ $errors->first('count_news_blog') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('count_trackers_admin') ? ' has-error' : '' }}">
                        <label for="inputCountTrackers">Количество трекеров на страницу админпанели</label>
                        <input type="text" max="20" class="form-control" id="inputCountTrackersAdmin" name="count_trackers_admin" value="{{ old('count_trackers_admin', $settings->count_trackers_admin) }}">
                        @if ($errors->has('count_trackers_admin'))
                            <span class="help-block">
                                <strong>{{ $errors->first('count_trackers_admin') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('count_result_trackers_admin') ? ' has-error' : '' }}">
                        <label for="inputCountResultTrackersAdmin">Количество результатов поиска на страницу админпанели</label>
                        <input type="text" max="20" class="form-control" id="inputCountResultTrackersAdmin" name="count_result_trackers_admin" value="{{ old('count_result_trackers_admin', $settings->count_result_trackers_admin) }}">
                        @if ($errors->has('count_result_trackers_admin'))
                            <span class="help-block">
                                <strong>{{ $errors->first('count_result_trackers_admin') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-8 {{ $errors->has('is_offline') ? ' has-error' : '' }}">
                        <label for="radioIsActive">Выключить сайт</label>
                        <div class="clearfix"></div>
                        <label class="radio-inline">
                            <input type="radio" name="is_offline" id="radioIsActive" value="0" @if($settings->is_offline == 0) checked @endif> Нет
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_offline" id="radioIsActive" value="1" @if($settings->is_offline == 1) checked @endif> Да
                        </label>
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-12 {{ $errors->has('desc_offline_site') ? ' has-error' : '' }}">
                        <label for="textareaDescOfflineSite">Сообщение при выключенном сайте</label>
                        <textarea rows="5" class="form-control summernote" style="opacity: 0;" id="textareaDescOfflineSite" name="desc_offline_site">{{ old('desc_offline_site', $settings->desc_offline_site) }}</textarea>
                        @if ($errors->has('desc_offline_site'))
                            <span class="help-block">
                                <strong>{{ $errors->first('desc_offline_site') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-8 {{ $errors->has('title_ru') ? ' has-error' : '' }}">
                        <label for="inputTitleCategoryRu">Очистить кеш сайта</label>
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-info">Очистить</button>
                    </div>

                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <a href="{{ route('dashboard-home') }}" class="btn btn-default">Назад на главную</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Сохранить настройки</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </form>

@endsection