@extends('admin.layouts.app')

@section('content')

	<h1>История клиента</h1>

	<div class="panel-footer" style="position: fixed; top: 50px; left: 0; width: 100%; z-index: 98; border-bottom: 1px solid #d3e0e9;">
		<div class="container">
			<div class="pull-left">
				<a href="{{ route('history-clients') }}" class="btn btn-default">Назад к истории</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>


	<table class="table table-bordered table-tracker">
		<tr>
			<th>Номер трекера</th>
			<th>Статус</th>
			<th>Последнее редактирование</th>
			<th>Примечание</th>
			<th>Перейти к трекеру</th>
		</tr>
		@foreach($trackers as $tracker)
			<tr>
				<td>{{ $tracker->id_tracker }}</td>
				<td>@if(isset($tracker->status)){{ $tracker->status->title }}@else - @endif</td>
				<td>@if(!empty($moderName = \App\User::getNameById($tracker->id_moderator))){{ $moderName }} @else - @endif</td>
				<td>@if( strlen($tracker->note_moder) > 0) {{ $tracker->note_moder }} <br> <button type="button" class="btn btn-xs btn-default addModerNote" data-id="{{ $tracker->id }}" data-toggle="modal" data-target="#myModal">Изменить</button> @else <button class="btn btn-success btn-xs addModerNote" data-id="{{ $tracker->id }}" data-toggle="modal" data-target="#myModal">Добавить примечание</button> @endif</td>
				<td><a href="{{ route('admin-edit-trackers', $tracker->id) }}">Просмотр трекера</a></td>
			</tr>
		@endforeach
	</table>

	{{ $trackers->links() }}


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-top: 12%;">
		<div class="modal-dialog" role="document">
			<form id="add-notes" method="post" action="#">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Примечание к заказу</h4>
					</div>
					<div class="modal-body">
                    <textarea style="width: 100%;" rows="7" name="note_moder">
                       @if( strlen($tracker->note_moder) > 0) {{ $tracker->note_moder }}@endif
                    </textarea>
					</div>
					<div class="modal-footer">
						<div class="pull-left"><button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button></div>
						<div class="pull-right"><button type="submit" class="btn btn-success">Сохранить</button></div>
					</div>
				</div>
			</form>
		</div>
	</div>


@endsection

@push('script')
	<script>

		$(document).ready(function() {

			$('.addModerNote').click(function () {

				$('#add-notes').prop('action', '/admin/get/history/client/' + $('.addModerNote').data('id'));

			});

		});

	</script>
@endpush
