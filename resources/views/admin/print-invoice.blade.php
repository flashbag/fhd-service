<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Трекер № {{ $tracker->id_tracker }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
	<link href="{{ mix('css/vendor.css') }}" rel="stylesheet">
</head>

<style>
    ul{
        padding-left: 0;
        width: 100%;
        display: block;
        height: 30px;
        border-bottom: 1px solid #ccc;
    }
    ul li{
        list-style-type: none;
        width: 267px;
        float: left;
    }
    .no-border{
        border-bottom: none;
    }
</style>
<body onload="window.print();">

<div style="padding: 15px; width: 1100px; border: 1px solid #ccc; margin-bottom: 30px;">
    <h4>Информация для заполнения</h4>
    <br>
    <ul>
        <li><b>Номер трекера</b>: {{ $tracker->id_tracker }}</li>
    </ul>
    <ul>
        <li><b>Дата оформления</b>: {{ $tracker->date_registered }}</li>
        <li><b>Количество мест</b>: {{ $tracker->number_seats }}</li>
    </ul>
    <ul>
        <li><b>Фактический вес кг</b>: {{ $tracker->receptacles->sum('actual_weight') }}</li>
        <li><b>Объемный вес кг</b>: {{ $tracker->receptacles->sum('volume_weight') }}</li>
    </ul>
    <ul class="no-border">
        <li><b>Контрольная стоимость</b>: {{ $tracker->receptacles->sum('assessed_price') }}</li>
        <li><b>Размер пошлины</b>: {{ $tracker->duty_price }}</li>
    </ul>
</div>


</body>
</html>
