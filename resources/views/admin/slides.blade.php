@extends('admin.layouts.app')

@section('content')

    <h1 class="pull-left">Список слайдов</h1>
    <a href="{{ route('create-slides') }}" class="btn btn-success pull-right">Добавить слайд</a>
    <div class="clearfix"></div>

    @if(isset($slides) && $slides->count() < 1)

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Информация</div>

                    <div class="panel-body">
                        Слайды не существуют!
                    </div>
                </div>
            </div>
        </div>

    @else

        <table class="table table-bordered table-tracker">
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Изображение</th>
                <th>Описание</th>
                <th>Статус</th>
                <th>Дата создания</th>
                <th>Редактировать</th>
            </tr>
            @foreach($slides as $slide)
                <tr>
                    <td>{{ $slide->id }}</td>
                    <td>{{ $slide->title }}</td>
                    <td><img width="100%" src="{{ $slide->image }}" alt="{{ $slide->title }}"></td>
                    <td>{{ str_limit($slide->description, 150, '...') }}</td>
                    <td>{{ $slide->is_active == '0' ? 'Не активно' : 'Активно' }}</td>
                    <td>{{ $slide->created_at }}</td>
                    <td><a href="{{ route('edit-slides', $slide->id) }}">Изменить</a></td>
                </tr>
            @endforeach
        </table>

        {{ $slides->links() }}

    @endif

@endsection