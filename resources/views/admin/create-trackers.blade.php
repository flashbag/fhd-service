@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <h1 class="header-title">{{ trans('admin.create_trackers') }}</h1>

        <span>{{ trans('admin.attention_title') }}</span>
        <br>
        <br>
        <form class="steps_form ajax-form" id="tracker-form" data-ukraine-value="{{ __('main.ukraine') }}" action="{{ route('admin-store-trackers') }}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

                @include('admin.tracker-parts.fields-to-fill')

                @include('admin.tracker-parts.receptacles-blocks')

                @include('admin.tracker-parts.price-delivery')

                @include('admin.tracker-parts.sender-info')

                @include('admin.tracker-parts.recipient-info')

                @include('admin.tracker-parts.additional')


                {{--<ul class="progressbar">--}}
                    {{--<li class="active" data-id=1>Транспорт</li>--}}
                    {{--<li data-id=2>Для Заполнения</li>--}}
                    {{--<li data-id=3>Информация</li>--}}
                    {{--<li data-id=4>Стоимость</li>--}}
                    {{--<li data-id=5>Описание</li>--}}
                    {{--<li data-id=6>Отправитель</li>--}}
                    {{--<li data-id=7>Получатель</li>--}}
                    {{--<li data-id=8>Дополнительно</li>--}}
                {{--</ul>--}}

                {{--<div class="steps">--}}


                {{--</div>--}}


            @if(isset($trackers))
                @include('admin.tracker-parts.buttons-confirm-change')
            @else
                @include('admin.tracker-parts.buttons-create-back')
            @endif


        </form>
    </div>
@endsection
