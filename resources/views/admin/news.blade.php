@extends('admin.layouts.app')

@section('content')

    <h1 class="pull-left">Список новостей</h1>
    <a href="{{ route('create-news') }}" class="btn btn-success pull-right">Добавить новость</a>
    <div class="clearfix"></div>

    @if(isset($news) && $news->count() < 1)

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Информация</div>

                    <div class="panel-body">
                        Новости не существуют!
                    </div>
                </div>
            </div>
        </div>

    @else

        <table class="table table-bordered table-tracker">
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Изображение</th>
                <th>Описание</th>
                <th>Статус</th>
                <th>Дата создания</th>
                <th>Редактировать</th>
            </tr>
            @foreach($news as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td>{{ $article->title }}</td>
                    <td><img width="100%" src="{{ $article->image }}" alt="{{ $article->title }}"></td>
                    <td>{{ str_limit($article->description, 150, '...') }}</td>
                    <td>{{ $article->is_active == '0' ? 'Не активно' : 'Активно' }}</td>
                    <td>{{ $article->created_at }}</td>
                    <td><a href="{{ route('edit-news', $article->id) }}">Изменить</a></td>
                </tr>
            @endforeach
        </table>

        {{ $news->links() }}

    @endif

@endsection