@extends('admin.layouts.app')

@section('content')

	<h1>История клиентов</h1>

	<br>
	<br>

	<div class="clearfix"></div>

	@if(isset($clients) && $clients->count() < 1)

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-info">
					<div class="panel-heading">Информация</div>

					<div class="panel-body">
						История не существует!
					</div>
				</div>
			</div>
		</div>

	@else

		<table class="table table-bordered table-tracker">
			<tr>
				<th>ID клиента</th>
				<th>ФИО</th>
				<th>Просмотр истории</th>
			</tr>
			@foreach($clients as $client)
				<tr>
					<td>{{ $client->client_id }}</td>
					<td>{{ $client->full_name }}</td>
					<td><a href="{{ route('get-history-client', $client->client_id) }}">Просмотр истории</a></td>
				</tr>
			@endforeach
		</table>

		{{ $clients->links() }}

	@endif

@endsection
