<section class="fields-to-fill step" data-id=2>
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('admin.fields_to_fill') }}</div>

        <div class="panel-body">
            <div class="row">

				<div class="form-group col-sm-3 {{ $errors->has('type_transport_id') ? ' has-error' : '' }}">
					<label for="typeTransport">
						{{ __('trackers.parts.transport.type_transport') }}
					</label>


					<select class="form-control selectpicker typeTransport" name="type_transport_id">

						<option value="" style="display: none;">--</option>
						@foreach($type_transports as $type_transport)

							@if(isset($tracker))
								<option value="{{ $type_transport->id }}"
										{{ old('type_transport_id') != $type_transport->id ?: 'selected' }} @if($tracker->type_transport_id == $type_transport->id)selected @endif
                                        @if($type_transport->disabled) disabled  @endif>

							@else
								<option value="{{ $type_transport->id }}" {{ old('type_transport_id') != $type_transport->id ?: 'selected' }}
                                @if($type_transport->disabled) disabled  @endif>
									@endif

									{{ __('trackers.parts.transport.type_transport_' .  $type_transport->type ) }}

								</option>
								@endforeach

					</select>

					@if ($errors->has('type_transport_id'))
						<span class="help-block">
                            <strong>{{ $errors->first('type_transport_id') }}</strong>
                        </span>
					@endif

				</div>

                <div class="form-group col-sm-3 {{ $errors->has('id_tracker') ? ' has-error' : '' }}">
                    <label for="idTracker">{{ trans('admin.tracker_number') }}</label>
                    @if (isset($tracker))
                        <input type="text" class="form-control input__custom" id="idTracker" name="id_tracker"
                               value="{{ old('id_tracker', $tracker->id_tracker) }}" readonly>
                    @else
                        <input type="text" class="form-control input__custom" id="idTracker" name="id_tracker"
                               value="{{ \Carbon\Carbon::now()->format('dmy') }}{{ mt_rand(10000, 99999) }}" readonly>
                    @endif
                    @if ($errors->has('id_tracker'))
                        <span class="help-block">
                                <strong>{{ $errors->first('id_tracker') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group col-sm-3 {{ $errors->has('number_invoice') ? ' has-error' : '' }}">
                    <label for="inputNumberInvoice">{{ trans('admin.invoice_number') }}</label>

                    @if (isset($tracker))
                        <input type="text" min="0" autocomplete="off" class="only-numeric form-control input__custom"
                               id="inputNumberInvoice" name="number_invoice" value="{{ old('number_invoice', $tracker->number_invoice) }}">
                    @else
                        <input type="text" min="0" autocomplete="off" class="only-numeric form-control input__custom"
                               id="inputNumberInvoice" name="number_invoice" value="{{ old('number_invoice') }}">
                    @endif

                    @if ($errors->has('number_invoice'))
                        <span class="help-block">
                            <strong>{{ $errors->first('number_invoice') }}</strong>
                        </span>
                    @endif
                </div>


				{{--<div class="form-group col-sm-3 {{ $errors->has('batch_id') ? ' has-error' : '' }}">--}}
					{{--<label for="inputNumberInvoice">{{ trans('admin.batch_id') }}</label>--}}

					{{--<select class="form-control selectpicker" name="batch_id">--}}

						{{--<option value="">--</option>--}}
						{{--@foreach($batches as $batch)--}}
							{{--<option value="{{ $batch->id }}"--}}
									{{--@if(isset($tracker) && $tracker->batch && $tracker->batch->id == $batch->id) selected @endif>--}}
								{{--{{ $batch->number }} - {{ $batch->name }}--}}
							{{--</option>--}}
						{{--@endforeach--}}

					{{--</select>--}}
				{{--</div>--}}

            </div>
        </div>

		<div class="panel-body">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="fromCountryId">
						{{ __('trackers.parts.countries.country_departure') }}
					</label>

					<select class="form-control selectpicker" name="from_country"
							data-warehouse-selector="#select_warehouse_from">
                        @if(isset($warehouse_country)){
                        <option value="{{ $warehouse_country->id }}"
                                data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.countries.by_iso_3166_2.' . $warehouse_country->iso_3166_2, [], 'en') }}"
                                @if(isset($tracker) && $tracker->warehouseFrom && $tracker->warehouseFrom->country->id == $warehouse_country->id) selected @endif>
                            {{ __('trackers.parts.countries.by_iso_3166_2.' . $warehouse_country->iso_3166_2) }}
                        </option>
                        }
                        @else{
                        <option value="">--</option>
                        @foreach($countries as $country)
                            <option value="{{ $country->id }}"
                                    data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2, [], 'en') }}"
                                    @if(isset($tracker) && $tracker->warehouseFrom && $tracker->warehouseFrom->country->id == $country->id) selected @endif>
                                {{ __('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2) }}
                            </option>
                        @endforeach
                        }
                        @endif
					</select>
				</div>

				<div class="form-group col-sm-3">
					<label for="fromCountryId">
						{{ __('trackers.parts.warehouse.country_departure_warehouse') }}
					</label>

                    @if(isset($warehouse_country))
                    <select class="form-control selectpicker" id="select_warehouse_from" name="from_warehouse_id" readonly>
                        @if( isset($tracker) && $tracker->warehouseFrom)
                            <option value="{{ $tracker->warehouseFrom->id }}">{{ $tracker->warehouseFrom->name }}</option>
						@else
							<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                        @endif

                    </select>
                    @else
                        <select class="form-control selectpicker" id="select_warehouse_from" name="from_warehouse_id" readonly>
                            @if( isset($tracker) && $tracker->warehouseFrom)
                                <option value="{{ $tracker->warehouseFrom->id }}">{{ $tracker->warehouseFrom->name }}</option>
                            @endif
                        </select>
                     @endif
				</div>

				<div class="form-group col-sm-3">
					<label for="fromCountryId">
						{{ __('trackers.parts.countries.country_arrival') }}
					</label>

					<select class="form-control selectpicker" name="to_country"
							data-warehouse-selector="#select_warehouse_to">
						<option value="">--</option>
						@foreach($countries as $country)
							<option value="{{ $country->id }}"
									data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2, [], 'en') }}"
									@if(isset($tracker) && $tracker->warehouseTo && $tracker->warehouseTo->country->id == $country->id) selected @endif>
								{{ __('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2) }}
							</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-sm-3">
					<label for="fromCountryId">
						{{ __('trackers.parts.warehouse.country_arrival_warehouse') }}
					</label>

					<select class="form-control selectpicker" id="select_warehouse_to" name="to_warehouse_id" readonly>
						@if( isset($tracker) && $tracker->warehouseTo)
							<option value="{{ $tracker->warehouseTo->id }}">{{ trans('trackers.parts.warehouses.'. $tracker->warehouseTo->unique_id) }}</option>
						@endif
					</select>
				</div>
			</div>
		</div>
        <div class="panel-body">
            <div class="row">

                <div class="form-group col-sm-3 {{ $errors->has('date_registered') ? ' has-error' : '' }}">
                    <label for="inputDateRegistered">{{ trans('admin.date_of_issue') }} <span class="color-red">*</span></label>
                    <input type="text" class="form-control" id="inputDateRegistered" name="date_registered"
                           value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"
						   readonly>
                    @if ($errors->has('date_registered'))
                        <span class="help-block">
                                <strong>{{ $errors->first('date_registered') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group col-sm-3 {{ $errors->has('date_delivery') ? ' has-error' : '' }}">
                    <label for="inputDateDelivery">{{ trans('admin.delivery_date') }}</label>
                    @if(isset($tracker))
                        <input type="text" class="form-control input__custom" id="inputDateDelivery" name="date_delivery"
                               value="{{ old('date_delivery', $tracker->date_delivery) }}"
							   readonly>
                    @else
                        <input type="text" class="form-control input__custom" id="inputDateDelivery" name="date_delivery"
                               value="{{ old('date_delivery') }}" readonly>
                    @endif
                    @if ($errors->has('date_delivery'))
                        <span class="help-block">
                                <strong>{{ $errors->first('date_delivery') }}</strong>
                            </span>
                    @endif
                </div>


                <div class="form-group col-sm-3 {{ $errors->has('status_id') ? ' has-error' : '' }}">
                    <label for="inputStatusTracker">{{ trans('admin.status') }}</label>
                    <select class="form-control selectpicker" name="status_id">
                        @if (isset($currentTypeStatuses))
                            <option value="" style="display: none;">--</option>
                            @foreach($currentTypeStatuses as $status)
                                    @if(isset($historyStatusTrackers) && $historyStatusTrackers->status !== null)
                                        @if($status->number >= $historyStatusTrackers->status->number)
                                                @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
                                                    <option value="{{ $status->id }}"
                                                        data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.status.' . $status->number, [], 'en') }}"
                                                        @if($status->number == $historyStatusTrackers->status->number)selected
                                                        @endif
                                                >
                                                    {{ __('trackers.parts.status.' .$status->number) }}
                                                </option>
                                                @else
                                                    <option value="{{ $status->id }}"
                                                            data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.status.' . $status->number, [], 'en') }}"
                                                            @if($status->number == $historyStatusTrackers->status->number)selected
                                                            @elseif($status->id-1 == $historyStatusTrackers->status_id)
                                                            @else disabled
                                                            @endif
                                                    >
                                                        {{ __('trackers.parts.status.' .$status->number) }}
                                                    </option>
                                                @endif
                                            @endif
                                    @else
                                        <option value="{{ $status->id }}" {{ old('status_id') != $status->id ?: 'selected' }}
                                        data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.status.' . $status->number, [], 'en') }}"
                                        > {{ __('trackers.parts.status.' .$status->number) }}</option>
                                    @endif
                            @endforeach
                        @else
                            @if($statusTrackers === null) disabled @endif>
                            @if($statusTrackers !== null)
                                @foreach($statusTrackers as $statusTracker)
                                    <option value="{{ $statusTracker->id }}"
                                            data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.status.' . $statusTracker->number, [], 'en') }}" >
                                         {{ __('trackers.parts.status.' .$statusTracker->number) }}
                                    </option>
                                @endforeach
                            @else
                                <option selected style="display: none;">--</option>
                            @endif
                        @endif
                    </select>
                    @if(isset($historyStatusTrackers))
                        <input type="hidden" name="old_status_id"
                               value="@if($historyStatusTrackers !== null){{ $historyStatusTrackers->status_id }}@endif">
                    @endif
                    @if ($errors->has('status_id'))
                        <span class="help-block">
							<strong>{{ $errors->first('status_id') }}</strong>
						</span>
                    @endif
                </div>
                <div class="form-group col-sm-3 {{ $errors->has('is_paid') ? ' has-error' : '' }}">
                    <label for="is_paid">{{ trans('admin.paid') }}</label>
                    <br>
	                <div class="d-flex">
		                @if (isset($tracker))
			                <input type="checkbox" name="is_paid" value="{{$tracker->id}}"
			                       @if($tracker->is_paid)
			                       checked/>
		                @else
		                @endif
		                @else
			                <input type="checkbox" name="is_paid"/>
		                @endif
		                <br>
		                <p class="ml-2"> {{ trans('admin.paid_label') }}</p>
	                </div>

                </div>
            </div>
        </div>



        <div class="panel-body">
            <div class="row">

                <div class="form-group col-sm-3 {{ $errors->has('carrier') ? ' has-error' : '' }}">
                    <label for="inputCarrier">{{ trans('admin.carrier') }}</label>
                    @if (isset($tracker))
                        <input type="text" class="form-control input__custom" id="inputCarrier" name="carrier"
                               value="{{ old('carrier', $tracker->carrier) }}">
                    @else
                        <input type="text" class="form-control input__custom" id="inputCarrier" name="carrier"
                               value="{{ old('carrier') }}">
                    @endif
                    @if ($errors->has('carrier'))
                        <span class="help-block">
                                <strong>{{ $errors->first('carrier') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group col-sm-3 {{ $errors->has('number_container') ? ' has-error' : '' }}">
                    <label for="inputContainerNumber">{{ trans('admin.container_number') }}</label>
                    <div class="typehead typehead-number-container">
						<input type="text" min="0" autocomplete="off" class="only-numeric form-control input__custom" id="inputContainerNumber"
							   name="number_container"
							   @if(isset($tracker))
							   	value="{{ old('number_container', $tracker->number_container) }}"
							   @else value="{{ old('number_container') }}" @endif
						>
                        <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                        <ul class="typehead-list"></ul>
                    </div>
                    @if ($errors->has('number_container'))
                        <span class="help-block">
                                <strong>{{ $errors->first('number_container') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group col-sm-3 {{ $errors->has('id_type_container') ? ' has-error' : '' }}">
                    <label for="selectContainerType">{{ trans('admin.container_type') }}</label>

                    <select class="form-control selectpicker" name="id_type_container" id="selectContainerType">
                        <option value="" style="display: none;">--</option>
                        @foreach($type_containers as $type_container)
{{--                            @if(isset($tracker))--}}
{{--                                <option value="{{ $type_container->id }}"--}}
{{--                                        {{ old('id_type_container') != $type_container->id ?: 'selected' }} @if($tracker->id_type_container == $type_container->id)selected @endif>{{ $type_container->title }}</option>--}}
{{--                            @else--}}
{{--                                <option value="{{ $type_container->id }}" {{ old('id_type_container') != $type_container->id ?: 'selected' }}>{{ $type_container->title }}</option>--}}
{{--                            @endif--}}
                                @if(isset($tracker))
                                    <option value="{{ $type_container->id }}"
                                            data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.type_container.' . $type_container->id, [], 'en') }}"
                                            {{ old('id_type_container') != $type_container->id ?: 'selected' }} @if($tracker->id_type_container == $type_container->id)selected @endif>
                                        {{ __('trackers.parts.type_container.' .$type_container->id) }}
                                    </option>
                                @else
                                    <option value="{{ $type_container->id }}" {{ old('id_type_container') != $type_container->id ?: 'selected' }}
                                    data-en-value="{{ \Illuminate\Support\Facades\Lang::getFacadeRoot()->get('trackers.parts.type_container.' . $type_container->id, [], 'en') }}"
                                    > {{ __('trackers.parts.type_container.' .$type_container->id) }}</option>
                                @endif
                        @endforeach
                    </select>
                    @if ($errors->has('id_type_container'))
                        <span class="help-block">
                                <strong>{{ $errors->first('id_type_container') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>


@push('script')

	<script>
		$(document).ready(function(){

			var $select = $('select[name="status_id"]');

			$select.on('change', function(){

				var selectedStatus = parseInt($(this).val());

				if (selectedStatus === 10) {
					$('input[name="name_employee_issued"]').val('{{ Auth::user()->name }}').removeAttr('disabled');
				}

				// console.log('selected status:', selectedStatus);

			});

		});
	</script>

@endpush
