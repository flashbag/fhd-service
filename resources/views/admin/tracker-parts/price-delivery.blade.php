<section class="price-delivery step" data-id=4>
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('admin.total_shipping_cost') }}</div>
        <div class="panel-body">
            <div class="row">

                <div class="form-group col-sm-3">
                    <label>{{ trans('admin.sender_country') }}</label>
                    @if(isset($tracker) && $tracker->sender)
                        <input type="text" class="form-control input__custom" value="{{ $tracker->sender->country }}" disabled>
                    @else
                        <input type="text" class="form-control disabled-country-sender input__custom" disabled>
                    @endif
					<span class="help-block">
						{{ trans('admin.validate_1') }}
					</span>
                </div>


                <div class="form-group col-sm-3 {{ $errors->has('delivery_rate_kg') ? ' has-error' : '' }}">
                    <label for="inputDeliveryRateKg">{{ trans('admin.shipping_rate') }} <span class="color-red">*</span></label>
					<input type="text" min="0" step="0.01" class="only-numeric form-control input__custom" name="delivery_rate_kg"
                    @if(isset($tracker))
						value="{{ old('delivery_rate_kg', $tracker->delivery_rate_kg) }}"
                    @else
						value="{{ old('delivery_rate_kg') }}"
                    @endif
					>
                    @if ($errors->has('delivery_rate_kg'))
                        <span class="help-block">
							<strong>{{ $errors->first('delivery_rate_kg') }}</strong>
						</span>
                    @endif
                </div>


                <div class="form-group col-sm-3 {{ $errors->has('delivery_rate_cur') ? ' has-error' : '' }}">
                    <label for="inputDeliveryRateCur">{{ trans('admin.shipping_currency') }} <span class="color-red">*</span></label>

                    <select class="form-control selectpicker" name="delivery_rate_cur" data-dropup-auto="false"
                            data-size="8">
                        @foreach($currencies as $currency)
							@if(isset($tracker))
								<option value="{{ $currency->id }}"
										{{ old('delivery_rate_cur') != $currency->id ?: 'selected' }} @if($tracker->delivery_rate_cur == $currency->id) selected @endif>{{ trans('admin.' . $currency->code) }}
									({{ $currency->code }})
								</option>
							@else
								<option value="{{ $currency->id }}" {{ old('delivery_rate_cur') != $currency->id ?: 'selected' }}>{{ trans('admin.' . $currency->code) }}
									({{ $currency->code }})
								</option>
							@endif
                        @endforeach
                    </select>
                    @if ($errors->has('delivery_rate_cur'))
                        <span class="help-block">
							<strong>{{ $errors->first('delivery_rate_cur') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group col-sm-3">
                    <label for="inputActualWeight">{{ trans('admin.total_weight') }}</label>
                    @if(isset($tracker))
                        <input type="text" disabled="disabled" class="only-numeric form-control weight_copy input__custom"
                               value="{{ $tracker->receptacles->sum('actual_weight') }}">
                    @else
                        <input type="text" disabled="disabled" class="only-numeric form-control weight_copy input__custom"
                               value="">
                    @endif
                </div>

            </div>
            <div class="row">

                <div class="form-group col-sm-3">
                    <label>{{ trans('admin.recipient_country') }}</label>
                    @if(isset($tracker) && $tracker->recipient)
                        <input type="text" class="form-control input__custom" value="{{ $tracker->recipient->country }}" disabled>
                    @else
                        <input type="text" class="form-control disabled-country-recipient input__custom" disabled>
                    @endif
					<span class="help-block">
						{{ trans('admin.validate_1') }}
					</span>
                </div>


                <div class="form-group col-sm-3 {{ $errors->has('total_ship_cost') ? ' has-error' : '' }}">
                    <label for="inputTotalShipCost">{{ trans('admin.total_shipping_cost') }} <span class="color-red">*</span></label>
					<input type="text" min="0" step="0.01" class="only-numeric form-control input__custom" name="total_ship_cost" readonly="readonly"
                    @if(isset($tracker))
                               value="{{ old('total_ship_cost', $tracker->total_ship_cost) }}"
                    @else
                               value="{{ old('total_ship_cost') }}"
                    @endif
					>
                    @if ($errors->has('total_ship_cost'))
                        <span class="help-block">
                        <strong>{{ $errors->first('total_ship_cost') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="form-group col-sm-3 {{ $errors->has('ship_rate_cur') ? ' has-error' : '' }}">
                    <label for="inputShipRateCur">{{ trans('admin.shipping_currency') }} <span class="color-red">*</span></label>

                    <select class="form-control selectpicker" name="ship_rate_cur" data-dropup-auto="false"
                            data-size="8">
                        @foreach($currencies as $currency)
							@if(isset($tracker))
								<option value="{{ $currency->id }}"
										{{ old('ship_rate_cur') != $currency->id ?: 'selected' }} @if($tracker->ship_rate_cur == $currency->id) selected @endif >{{ trans('admin.' . $currency->code) }}
									({{ $currency->code }})
								</option>
							@else
								<option value="{{ $currency->id }}" {{ old('ship_rate_cur') != $currency->id ?: 'selected' }}>{{ trans('admin.' . $currency->code) }}
									({{ $currency->code }})
								</option>
							@endif
                        @endforeach
                    </select>

                    @if ($errors->has('ship_rate_cur'))
                        <span class="help-block">
                                <strong>{{ $errors->first('ship_rate_cur') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group col-sm-3">
                    <label for="inputVolumeWeight">{{ trans('admin.volume_weight') }} </label>
                    @if(isset($tracker))
                        <input type="text" class="only-numeric form-control volume_copy input__custom" disabled
                               value="{{ $tracker->receptacles->sum('volume_weight') }}">
                    @else
                        <input type="text" class="only-numeric form-control volume_copy input__custom" disabled
                               value="">
                    @endif
                </div>

            </div>
        </div>
		<div class="panel-body">

			<div class="row">
				<div class="col-sm-12 form-group">
					<span class="help-block help-block-static">
						{{ __('trackers.parts.prices.help_block') }}
					</span>
				</div>
			</div>

			<div class="row">

				<div class="form-group col-sm-3 currency-picker {{ $errors->has('total_price') ? ' has-error' : '' }}">
					<label for="inputTotalPrice">{{ trans('admin.cost_reference') }} <span class="color-red"></span></label>
					<input type="text" min="0" autocomplete="off" class="only-numeric form-control input__custom" id="inputTotalPrice" name="total_price"

						   @if(Auth::user()->isWarehouseUser())
								   disabled="disabled"
						   @endif

						   @if(isset($tracker))
						   value="{{ old('total_price', $tracker->total_price) }}"
						   @else
						   value="{{ old('total_price') }}"
							@endif
					>
					@if ($errors->has('total_price'))
						<span class="help-block">
							<strong>{{ $errors->first('total_price') }}</strong>
						</span>
					@endif
					<br>
					<select class="form-control selectpicker" name="total_price_currency">
						<option selected>--</option>
						@foreach($currencies as $currency)
							<option value="{{ $currency->id }}"
									{{ old('total_price_currency') != $currency->id ?: 'selected' }}
									@if(isset($tracker) && $tracker->total_price_currency == $currency->id) selected @endif
							>
                                {{ trans('admin.' . $currency->code) }}
								({{ $currency->code }})
							</option>

						@endforeach
					</select>
				</div>

				<div class="form-group col-sm-3 currency-picker {{ $errors->has('duty_price') ? ' has-error' : '' }}">
					<label for="inputDutyPrice">{{ trans('admin.duty_rate') }} </label>
					@if(isset($tracker))
						<input type="text" min="0" autocomplete="off" class="only-numeric form-control input__custom" id="inputDutyPrice"
							   @if(Auth::user()->isWarehouseUser())
							   disabled="disabled"
							   @endif
							   name="duty_price" value="{{ old('duty_price', $tracker->duty_price) }}">
					@else
						<input type="text" min="0" autocomplete="off" class="only-numeric form-control input__custom" id="inputDutyPrice"
							   @if(Auth::user()->isWarehouseUser())
							   disabled="disabled"
							   @endif
							   name="duty_price" value="{{ old('duty_price') }}">
					@endif
					@if ($errors->has('duty_price'))
						<span class="help-block">
                                <strong>{{ $errors->first('duty_price') }}</strong>
                            </span>
					@endif
					<br>

					<select class="form-control selectpicker" name="duty_price_currency">
						<option selected>--</option>
						@foreach($currencies as $currency)

							<option value="{{ $currency->id }}"
									{{ old('duty_price_currency') != $currency->id ?: 'selected' }}
									@if(isset($tracker) && $tracker->duty_price_currency == $currency->id) selected @endif
							>
                                {{ trans('admin.' . $currency->code) }}
								({{ $currency->code }})
							</option>

						@endforeach
					</select>
				</div>

			</div>
		</div>
    </div>
</section>
