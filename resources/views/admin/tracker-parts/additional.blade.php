<section class="additional step" data-id=8>
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('admin.ct_title_3') }}</div>
        <div class="panel-body">
            <div class="row">


                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-sm-3 {{ $errors->has('bill_duty_id') ? ' has-error' : '' }}">
                            <label for="customTax">{{ trans('admin.customs_costs') }}</label>

                            @if(isset($tracker) && isset($tracker->bill_duty_id))
                            <input type="hidden" name="bill_duty_type" value="{{ $tracker->billDuty->type }}">
                            @else
                            <input type="hidden" name="bill_duty_type" value="">
                            @endif

                            <select class="form-control selectpicker billDuties" id="customTax" name="bill_duty_id">

								@if(!isset($tracker))
								@endif

                                @foreach($bill_duties as $bill_duty)

                                    @if(isset($tracker))
                                        <option data-type="{{$bill_duty->type}}" value="{{ $bill_duty->id }}"
                                        {{ old('bill_duty_id') != $bill_duty->id ?: 'selected' }} @if($tracker->bill_duty_id == $bill_duty->id)selected @endif
                                                @if($bill_duty->disabled) disabled @endif>
                
                                    @else
                                        <option data-type="{{$bill_duty->type}}" value="{{ $bill_duty->id }}" {{ old('bill_duty_id') != $bill_duty->id ?: 'selected' }}
                                        @if($bill_duty->disabled) disabled @endif>
                                    @endif

                                    {{ __('trackers.parts.bill_duties.type_' .  $bill_duty->type ) }}

                                    </option>
                                @endforeach
                            </select>

                            @if ($errors->has('bill_duty_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('bill_duty_id') }}</strong>
                                </span>
                            @endif

                        </div>

{{--                        <div class="form-group col-sm-3 billDutyThirdParty {{ $errors->has('bill_duty_third_party') ? ' has-error' : '' }}" style="display: none">--}}
{{--                            <label for="thridParty">Third party email / Email третьего лица</label>--}}

{{--                            @if(isset($tracker))--}}
{{--                            <input type="text" class="form-control" name="bill_duty_third_party" value="{{ old('bill_duty_third_party', $tracker->billDutyThirdParty)  }}">--}}
{{--                            @else--}}
{{--                            <input type="text" class="form-control" name="bill_duty_third_party" value="{{ old('bill_duty_third_party') }}">--}}
{{--                            @endif--}}


{{--                            @if ($errors->has('bill_duty_third_party'))--}}
{{--                                <span class="help-block">--}}
{{--                                    <strong>{{ $errors->first('bill_duty_third_party') }}</strong>--}}
{{--                                </span>--}}
{{--                            @endif--}}
{{--                        </div>--}}


{{--                        <div class="form-group col-sm-3 billDutyFhdAccount {{ $errors->has('bill_duty_fhd_account') ? ' has-error' : '' }}" style="display: none">--}}
{{--                            <label for="fhdAccount">FHD account / FHD аккаунт</label>--}}

{{--							@if(isset($tracker))--}}
{{--                            <input type="text" class="form-control only-numeric" name="bill_duty_fhd_account" value="{{ old('bill_duty_fhd_account', $tracker->billDutyFHDAccount) }}">--}}
{{--                            @else--}}
{{--                            <input type="text" class="form-control only-numeric" name="bill_duty_fhd_account" value="{{ old('bill_duty_fhd_account') }}">--}}
{{--                            @endif--}}

{{--                            @if ($errors->has('bill_duty_fhd_account'))--}}
{{--                                <span class="help-block">--}}
{{--                                    <strong>{{ $errors->first('bill_duty_fhd_account') }}</strong>--}}
{{--                                </span>--}}
{{--                            @endif--}}
{{--                        </div>--}}

                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-sm-3 {{ $errors->has('bill_transportation_id') ? ' has-error' : '' }}">
                            <label for="transportTax">{{ trans('admin.transportation_costs') }}</label>
                            
                            @if(isset($tracker) && isset($tracker->bill_transportation_id))
                            <input type="hidden" name="bill_transportation_type" value="{{ $tracker->billTransportation->type }}">
                            @else
                            <input type="hidden" name="bill_transportation_type" value="">
                            @endif

                            <select class="form-control selectpicker transportTax" id="transportTax" name="bill_transportation_id">

								@if(!isset($tracker))
								@endif

                            	@foreach($bill_transportations as $bill_transportation)

                            		@if(isset($tracker))
                                        <option data-type="{{$bill_transportation->type}}" value="{{ $bill_transportation->id }}"
                                        {{ old('bill_transportation_id') != $bill_transportation->id ?: 'selected' }} @if($tracker->bill_transportation_id == $bill_transportation->id)selected @endif
                                                @if($bill_transportation->disabled) disabled @endif>
                
                                    @else
                                        <option data-type="{{$bill_transportation->type}}" value="{{ $bill_transportation->id }}" {{ old('bill_transportation') != $bill_transportation->id ?: 'selected' }}
                                        @if($bill_transportation->disabled) disabled @endif>
                                    @endif

                                    {{ __('trackers.parts.bill_transportations.type_' .  $bill_transportation->type ) }}
                            	@endforeach

                            </select>

                            @if ($errors->has('bill_transportation_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('bill_transportation_id') }}</strong>
                                </span>
                            @endif

                        </div>

{{--                        <div class="form-group col-sm-3 billTransportationThirdParty {{ $errors->has('bill_transportation_third_party') ? ' has-error' : '' }}" style="display: none">--}}
{{--                            <label for="thridParty">Third party email / Email третьего лица</label>--}}

{{--                            @if(isset($tracker))--}}
{{--                            <input type="text" class="form-control" name="bill_transportation_third_party" value="{{ old('bill_transportation_third_party', $tracker->billTransportationThirdParty)  }}">--}}
{{--                            @else--}}
{{--                            <input type="text" class="form-control" name="bill_transportation_third_party" value="{{ old('bill_transportation_third_party') }}">--}}
{{--                            @endif--}}


{{--                            @if ($errors->has('bill_transportation_third_party'))--}}
{{--                                <span class="help-block">--}}
{{--                                    <strong>{{ $errors->first('bill_transportation_third_party') }}</strong>--}}
{{--                                </span>--}}
{{--                            @endif--}}
{{--                        </div>--}}

                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">

                        <div class="form-group col-sm-3 {{ $errors->has('freight_service_id') ? ' has-error' : '' }}">
                            <label for="expressFreight">{{ trans('admin.delivery_type') }}</label>

                            @if(isset($tracker) && isset($tracker->freight_service_id))
                            <input type="hidden" name="freight_service_type" value="{{ $tracker->freightService->type }}">
                            @else
                            <input type="hidden" name="freight_service_type" value="">
                            @endif


							<select class="form-control selectpicker" id="expressFreight" name="freight_service_id">

                            	@foreach($freight_services as $freight_service)

                            		@if(isset($tracker))

                                        <option data-type="{{$freight_service->type}}" value="{{ $freight_service->id }}"
                                        {{ old('freight_service_id') != $freight_service->id ?: 'selected' }} @if($tracker->freight_service_id == $freight_service->id)selected @endif
                                                @if($freight_service->disabled) disabled @endif>
                
                                    @else
                                        <option data-type="{{$freight_service->type}}" value="{{ $freight_service->id }}" {{ old('freight_service_id') != $freight_service->id ?: 'selected' }}
                                        @if($freight_service->disabled) disabled @endif>
                                    @endif

                                    {{ __('trackers.parts.freight_services.type_' .  $freight_service->type ) }}
                            	@endforeach

                            </select>

                            @if ($errors->has('freight_service_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('freight_service_id') }}</strong>
                                </span>
                            @endif

                        </div>

{{--                        <div class="form-group col-sm-3 freightServiceBookingNumber {{ $errors->has('freight_service_booking_number') ? ' has-error' : '' }}" style="display: none">--}}
{{--                            <label for="bookingNumber">Booking number / Номер букинга</label>--}}

{{--							<input type="text" class="form-control only-numeric" name="freight_service_booking_number"--}}
{{--                            @if(isset($tracker))--}}
{{--								value="{{ old('freight_service_booking_number', $tracker->freightServiceBookingNumber) }}"--}}
{{--                            @else--}}
{{--                            	value="{{ old('freight_service_booking_number') }}"--}}
{{--                            @endif--}}
{{--							>--}}

{{--                            @if ($errors->has('freight_service_booking_number'))--}}
{{--                                <span class="help-block">--}}
{{--                                    <strong>{{ $errors->first('freight_service_booking_number') }}</strong>--}}
{{--                                </span>--}}
{{--                            @endif--}}

{{--                        </div>--}}

                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">

						<div class="form-group col-sm-3 {{ $errors->has('special_handling_id') ? ' has-error' : '' }}">
                            <label for="specialHandling">{{ trans('admin.special_handlings') }}</label>

                            @if(isset($tracker) && isset($tracker->special_handling_id))
                            <input type="hidden" name="special_handling_type" value="{{ $tracker->specialHandling->type }}">
                            @else
                            <input type="hidden" name="special_handling_type" value="">
                            @endif


							<select class="form-control selectpicker" id="specialHandling" name="special_handling_id">

                            	@foreach($special_handlings as $special_handling)

                            		@if(isset($tracker))

                                        <option data-type="{{$special_handling->type}}" value="{{ $special_handling->id }}"
                                        {{ old('special_handling_id') != $special_handling->id ?: 'selected' }} @if($tracker->special_handling_id == $special_handling->id)selected @endif
                                                @if($special_handling->disabled) disabled @endif>
                
                                    @else
										<option data-type="{{$special_handling->type}}"
												value="{{ $special_handling->id }}"
												{{ old('special_handling_id') != $special_handling->id ?: 'selected' }}
												@if($special_handling->type =='address_delivery') selected @endif
                                                @if($special_handling->disabled) disabled @endif
										>
                                    @endif

                                    {{ __('trackers.parts.special_handlings.type_' .  $special_handling->type ) }}
                            	@endforeach

                            </select>

                            @if ($errors->has('special_handling_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('special_handling_id') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
