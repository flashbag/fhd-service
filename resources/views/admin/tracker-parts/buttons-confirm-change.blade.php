<section class="buttons-confirm-change">
    <div class="panel-footer pt-4 pb-2" style="position: fixed; top: 50px; left: 0; width: 100%; z-index: 98; border-bottom: 1px solid #d3e0e9;">
        <div class="container">
            <div class="btn-confirm-change">
                <div class="btn-confirm-change__col-2">
                    <a href="javascript:history.back()" id="btn-back-to-trackers" class="btn btn-default">{{ trans('admin.back_button') }}</a>
                </div>

                <div class="btn-confirm-change__col-10 btn-confirm-change_order">
{{--                    @if(!Auth::user()->isWarehouseUser())--}}
                        @if($tracker->is_unprocessed == 1 || $tracker->is_processed == 1)
                            <a class="btn btn-danger delete-tracker" data-toggle="modal" data-target="#modalDelete">{{ trans('admin.delete') }}</a>
                            <form id="delete-tracker-form" action="{{ route('delete-trackers', $tracker->id) }}"
                                  method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
{{--                            @include('modals.modal-rules')--}}
                        @else
{{--                        @endif--}}
                    @endif

                    <a href="#" class="btn btn-info btn-print" data-toggle="modal" data-id-tracker="{{ $tracker->id_tracker }}">
                        {{ __('trackers.print_options.downloadPDFTitle' ) }}
                    </a>
{{--                @if(Auth::user()->isAdmin() || $tracker->is_processed)--}}
                    <button id="save-only-tracker" type="submit" class="btn btn-success">{{ trans('admin.save') }}</button>
{{--                @endif--}}

                @if($tracker->is_unprocessed == 1)
                    <button type="submit" class="btn btn-success">{{ trans('admin.send_to_processed') }}</button>
                @elseif($tracker->is_processed == 1)
                    <button type="submit" class="btn btn-success">{{ trans('admin.confirm') }}</button>
                @endif


                    <a href="{{ route('create-ticket', $tracker->id) }}" class="btn btn-default ">{{ trans('admin.ticket') }}</a>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</section>

@push('script')
    <script>
        $(document).ready(function () {

            $('#btn-back-to-trackers').on('click', function () {
                var url = $(this).attr('href');
                window.history.pushState(url);
                window.location.href = url;
            });

            var isSaveButtonAlreadyClicked = false;

            $('#save-only-tracker').on('click', function(e){

                if (!isSaveButtonAlreadyClicked) {
                    e.preventDefault();
                    $('#tracker-form').attr('action', "{{ route('save-tracker', $tracker->id) }}");
                    isSaveButtonAlreadyClicked = true;
                    $('#save-only-tracker')[0].click();
                }

            })
        });
    </script>
@endpush
