<section class="employees-info">
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('admin.info_employee') }}</div>
        <div class="panel-body">
            <div class="row">

                <div class="form-group col-sm-3 {{ $errors->has('name_employee_accepted') ? ' has-error' : '' }}">
                    <label for="inputEmployeeAccepted">{{ trans('admin.accepted_employee') }}</label>
                    @if(isset($tracker))
                        <input disabled="disabled" type="text" class="form-control" id="inputEmployeeAccepted" name="name_employee_accepted"
                               value="{{ old('name_employee_accepted', $tracker->name_employee_accepted) }}">
                    @else
                        <input disabled="disabled" type="text" class="form-control" id="inputEmployeeAccepted" name="name_employee_accepted"
                               value="{{ old('name_employee_accepted') }}">
                    @endif
                    @if ($errors->has('name_employee_accepted'))
                        <span class="help-block">
                                <strong>{{ $errors->first('name_employee_accepted') }}</strong>
                            </span>
                    @endif
                </div>


                <div class="form-group col-sm-3 @if( old('status_id') == 10 || old('status_id') == 20 ) required @endif {{ $errors->has('name_employee_issued') ? ' has-error' : '' }}">
                    <label for="inputEmployeeIssued">{{ trans('admin.issued_employee') }}</label>
                    @if(isset($tracker))
                        <input disabled="disabled" type="text" class="form-control" id="inputEmployeeIssued" name="name_employee_issued"
                               value="{{ old('name_employee_issued', $tracker->name_employee_issued) }}"
                               @if( old('status_id') != 10 && old('status_id') != 20 ) readonly="readonly" @endif>
                    @else
                        <input disabled="disabled" type="text" class="form-control" id="inputEmployeeIssued" name="name_employee_issued"
                               value="{{ old('name_employee_issued') }}"
                               @if( old('status_id') != 10 && old('status_id') != 20 ) readonly="readonly" @endif>
                    @endif
                    @if ($errors->has('name_employee_issued'))
                        <span class="help-block">
                                <strong>{{ $errors->first('name_employee_issued') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
        </div>
        @if(isset($tracker))
            @include('admin.tracker-parts.buttons-confirm-change')
        @endif


    </div>
</section>
