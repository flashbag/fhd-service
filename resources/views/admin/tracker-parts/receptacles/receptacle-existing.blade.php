<div class="receptacle-block">

	<div class="panel panel-default ">
		<div class="panel-heading clearfix">
            {{ trans('admin.position') }} #<span class="receptacle-number">{{ $index + 1}}</span>
			@if ($index > 0)
			<div class="pull-right">
				<a class="btn btn-danger remove-receptacle-block is-existing-receptacle col-xs-12"
						data-cargos-container-selector=".receptacle-{{$index}}-cargos-container"
						data-receptacle-index="{{ $index }}"
						data-receptacle-id="{{ $receptacle->id }}">
					<i class="fa fa-plus"></i>  {{ trans('admin.del_position') }}
				</a>
			</div>
			@endif
		</div>
		<div class="panel-body panel-body-main">

			<div class="panel-body">
				<input type="hidden" name="receptacles[{{$index}}][id]" value="{{ $receptacle->id }}">
				<div class="row">
					@php
					$fieldRequestName = 'receptacles.' . $index . '.type_inventory_id';
					@endphp
					<div class="form-group col-sm-3">
						<label for="inputTypeInventories">{{ trans('admin.package_type') }}<span class="color-red">*</span></label>

						<select class="form-control selectpicker select-type-inventory" name="receptacles[{{ $index }}][type_inventory_id]">
							@foreach($type_inventories as $inventory)
								@if(isset($receptacle))
									<option value="{{ $inventory->id }}"
											@if($receptacle->type_inventory_id == $inventory->id)selected @endif>
										{{ __('trackers.parts.type_inventories.' . $inventory->id ) }}
									</option>
								@else
									<option value="{{ $inventory->id }}" {{ old($fieldRequestName) != $inventory->id ?: 'selected' }}>
										{{ __('trackers.parts.type_inventories.' . $inventory->id ) }}
									</option>
								@endif
							@endforeach
						</select>

					</div>



					@php
						$fieldRequestName = 'receptacles.' . $index . '.unit_of_length_id';
					@endphp

					<div class="form-group col-sm-3">
						<label for="selectUnitOfLength">{{ trans('admin.unit_length') }} <span class="color-red">*</span></label>

						<input type="hidden" class="length-multiplier-to-base" name="receptacles[{{ $index }}][unit_length_multiplier_to_base]" value="">
						<select class="form-control selectpicker select-unit-of-length" name="receptacles[{{ $index }}][unit_of_length_id]" id="selectUnitOfLength">
							@foreach($units_of_length as $unit_of_length)
								<option value="{{ $unit_of_length->id }}"
										data-is-metric="{{ $unit_of_length->is_metric }}"
										data-original-text="{{ $unit_of_length->text }}"
										data-multiplier-to-base="{{  $unit_of_length->multiplier_to_base }}"
										@if(isset($receptacle))
										@if($receptacle->unit_of_length_id == $unit_of_length->id)selected @endif>
									@else
										{{ old($fieldRequestName) != $unit_of_length->id ?: 'selected' }}>
									@endif
									{{ __('trackers.parts.shipping.unit_length.' .  $unit_of_length->text ) }}
								</option>
							@endforeach
						</select>
					</div>

					@php
						$fieldRequestName = 'receptacles.' . $index . '.unit_of_weight_id';
					@endphp

					<div class="form-group col-sm-3">
						<label for="selectUnitOfLength">{{ trans('admin.unit_weight') }} <span class="color-red">*</span></label>

						<input type="hidden" class="weight-multiplier-to-base" name="receptacles[{{ $index }}][unit_width_multiplier_to_base]" value="">

						<select class="form-control selectpicker select-unit-of-weight" name="receptacles[{{ $index }}][unit_of_weight_id]" >
							@foreach($units_of_weight as $unit_of_weight)
								<option value="{{ $unit_of_weight->id }}"
										data-is-metric="{{ $unit_of_length->is_metric }}"
										data-original-text="{{ $unit_of_weight->text }}"
										data-multiplier-to-base="{{  $unit_of_weight->multiplier_to_base }}"
										@if(isset($receptacle))
										@if($receptacle->unit_of_weight_id == $unit_of_weight->id)selected @endif>
									@else
										{{ old($fieldRequestName) != $unit_of_weight->id ?: 'selected' }}>
									@endif
									{{ __('trackers.parts.shipping.unit_weight.' .  $unit_of_weight->text ) }}
								</option>
							@endforeach
						</select>

					</div>

				</div>
			</div>

			<div class="panel-body">

				<div class="row">
					<div class="form-group col-sm-12">
						<span class="help-block help-block-static">
							{{ __('trackers.parts.receptacles.sizes_help_block') }}
						</span>
					</div>
				</div>

				<div class="row">

					@php
						$fieldRequestName = 'receptacles.' . $index . '.width';
					@endphp

					<div class="form-group col-sm-4">
						<label for="inputWidthCm">
                            {{ trans('admin.width') }}, <span class="length-measure-unit"></span>
							<span class="color-red">*</span>
						</label>
						<input name="receptacles[{{ $index }}][width]" type="number" min="0" step="0.01" autocomplete="off" class="only-numeric form-control input-width input__custom" id="inputWidthCm"
							   @if(isset($receptacle))
							   value="{{ old($fieldRequestName, $receptacle->width) }}"
							   @else
							   value="{{ old($fieldRequestName) }}"
								@endif
						>
					</div>
					@php
						$fieldRequestName = 'receptacles.' . $index . '.length';
					@endphp
					<div class="form-group col-sm-4">
						<label for="inputLengthCm">
                            {{ trans('admin.length') }}, <span class="length-measure-unit"></span>
							<span class="color-red">*</span>
						</label>
						<input name="receptacles[{{ $index }}][length]" type="number" min="0" step="0.01" autocomplete="off" class="only-numeric form-control input-length input__custom" id="inputLengthCm"
							   @if(isset($receptacle))
							   value="{{ old($fieldRequestName, $receptacle->length) }}"
							   @else
							   value="{{ old($fieldRequestName) }}"
								@endif
						>
					</div>


					@php
						$fieldRequestName = 'receptacles.' . $index . '.height';
					@endphp
					<div class="form-group col-sm-4 {{ $errors->has($fieldRequestName) ? ' has-error' : '' }}">
						<label for="inputHeightCm">
                            {{ trans('admin.height') }},  <span class="length-measure-unit"></span>
							<span class="color-red">*</span>
						</label>
						<input name="receptacles[{{ $index }}][height]" type="number" min="0" step="0.01" autocomplete="off" class="only-numeric form-control input-height input__custom" id="inputHeightCm"
							   @if(isset($receptacle))
							   value="{{ old($fieldRequestName, $receptacle->height) }}"
							   @else
							   value="{{ old($fieldRequestName) }}"
								@endif
						>
						@if ($errors->has($fieldRequestName))
							<span class="help-block">
								<strong>{{ $errors->first($fieldRequestName) }}</strong>
							</span>
						@endif
					</div>

				</div>
			</div>

			<div class="panel-body">
				<div class="row">

					@php
						$fieldRequestName = 'receptacles.' . $index . '.actual_weight';
					@endphp
					<div class="form-group col-sm-4 {{ $errors->has($fieldRequestName) ? ' has-error' : '' }}">
						<label for="inputActualWeight">{{ trans('admin.actual_weight') }}, <span class="weight-measure-unit"></span> <span class="color-red">*</span></label>
						<input type="text" min="0" autocomplete="off" class="only-numeric form-control input-actual-weight input__custom"
							   id="inputActualWeight" name="receptacles[{{ $index }}][actual_weight]"
							   @if(isset($receptacle))
							   value="{{ old($fieldRequestName, $receptacle->actual_weight) }}"
							   @else
							   value="{{ old($fieldRequestName) }}"
								@endif
						>
						@yield('receptacle-weight-help-block')
						@if ($errors->has($fieldRequestName))
							<span class="help-block">
								<strong>{{ $errors->first($fieldRequestName) }}</strong>
							</span>
						@endif
					</div>

					@php
						$fieldRequestName = 'receptacles.' . $index . '.volume_weight';
					@endphp
					<div class="form-group col-sm-4 {{ $errors->has($fieldRequestName) ? ' has-error' : '' }}">
						<label for="inputVolumeWeight"> {{ trans('admin.volume_weight') }}, <span class="weight-measure-unit"></span> </label>
						<input type="text" min="0" autocomplete="off" class="only-numeric form-control input-volume-weight input__custom"
							   id="inputVolumeWeight" name="receptacles[{{ $index }}][volume_weight]" readonly="readonly"
							   @if(isset($receptacle))
							   value="{{ old($fieldRequestName, $receptacle->volume_weight) }}"
							   @else
							   value="{{ old($fieldRequestName) }}"
								@endif
						>
						@yield('receptacle-weight-help-block')
						@if ($errors->has($fieldRequestName))
							<span class="help-block">
								<strong>{{ $errors->first($fieldRequestName) }}</strong>
							</span>
						@endif
					</div>

					@php
						$fieldRequestName = 'receptacles.' . $index . '.currency_type';
					@endphp
					<div class="form-group col-sm-2 currency-picker">
						<label for="inputAssessedPrice">{{ trans('admin.assessed_value') }}</label>
						<input type="text" min="0" autocomplete="off" class="only-numeric form-control input__custom"
							   id="inputAssessedPrice" name="receptacles[{{ $index }}][assessed_price]"
							   @if(isset($receptacle))
							   value="{{ old($fieldRequestName, $receptacle->assessed_price) }}"
							   @else
							   value="{{ old($fieldRequestName) }}"
								@endif
						>
						<br>
					</div>

					<div class="form-group col-sm-2">
						<label for="selectCurrencyType">{{ trans('main.tracker_create.content.label_11') }}</label>
						<select class="form-control selectpicker select-currency-type"
								data-size="10"
								name="receptacles[{{ $index }}][currency_type]">
							<option value="" style="display: none;">--</option>
							@foreach($currencies as $currency)
								<option value="{{ $currency->id }}"
										@if(isset($receptacle) && $receptacle->currency && $receptacle->currency->id == $currency->id) selected @endif>
                                    {{ trans('admin.' . $currency->code) }}
                                    ({{ $currency->code }})
								</option>
							@endforeach
						</select>

					</div>


				</div>
			</div>

			<div class="panel-body">
				<div class="row">

					<div class="form-group col-sm-12">
						<label for="textareaDescriptionItem">{{ trans('admin.description_in_position') }} <span class="color-red">*</span></label>

						@yield('receptacle-cargos-block')

						<div class="cargos-container receptacle-{{$index}}-cargos-container">

							@php
								$inputCargoNameLimit = \App\Models\Cargo::CARGO_NAME_LIMIT;
							@endphp

							@if (isset($receptacle) && $receptacle->cargos->count())
								@foreach($receptacle->cargos as $index2 => $cargo)
									<div class="row row-cargo">
										<input type="hidden" name="receptacles[{{$index}}][cargos][{{$index2}}][id]" value="{{ $cargo->id }}">
										<div class="form-group col-md-8">
											<input name="receptacles[{{$index}}][cargos][{{$index2}}][description]" type="text" class="form-control input-cargo-name input__custom" maxlength="{{ $inputCargoNameLimit }}" placeholder="{{ trans('admin.cargo_name') }}"  value="{{ $cargo->description }}" style="display: inline-block">
											<span class="cargo-chars-count">{{ trans('admin.characters_left') }}: <span>{{ intval($inputCargoNameLimit - strlen($cargo->description)) }}</span></span>
										</div>
										{{--<div class="form-group col-md-3">--}}
											{{--<select class="form-control selectpicker select-cargo-is-used"--}}
													{{--data-size="10"--}}
													{{--name="receptacles[{{ $index }}][cargos][{{$index2}}][is_used]">--}}

												{{--<option value="0" @if(isset($cargo) && $cargo->is_used == 0) selected @endif>--}}
													{{--{{ __('trackers.parts.receptacles.cargos.is_used_false') }}--}}
												{{--</option>--}}

												{{--<option value="1" @if(isset($cargo) && $cargo->is_used == 1) selected @endif>--}}
													{{--{{ __('trackers.parts.receptacles.cargos.is_used_true') }}--}}
												{{--</option>--}}

											{{--</select>--}}
										{{--</div>--}}
										<div class="form-group col-md-3">
											<input name="receptacles[{{$index}}][cargos][{{$index2}}][quantity]" type="number" class="form-control quantity-field input__custom" placeholder="{{ trans('admin.quantity') }}"  value="{{ $cargo->quantity }}" style="display: inline-block">
										</div>
										<div class="col-md-1">
											<a class="remove-cargo-block is-existing-cargo"
												data-cargos-container-selector=".receptacle-{{$index}}-cargos-container"
												data-index="{{ $index2 }}"
												data-cargo-id="{{ $cargo->id }}"
												data-receptacle-index="{{ $index }}">
												<i class="fa fa-2x fa-minus-square"></i>
											</a>
										</div>
									</div>
								@endforeach
							@else
								<div class="row row-cargo">
									<div class="form-group col-md-8">
										<input name="receptacle[{{$index}}][cargos][0][description]" type="text" class="form-control input-cargo-name input__custom" maxlength="{{ $inputCargoNameLimit }}" placeholder="{{ trans('admin.cargo_name') }}" style="display: inline-block">
										<span class="cargo-chars-count">{{ trans('admin.characters_left') }}: <span>{{ $inputCargoNameLimit }}</span></span>
									</div>
									<div class="form-group col-md-3">
										<input name="receptacle[{{$index}}][cargos][0][quantity]" min="1" type="number" class="form-control quantity-field" placeholder="{{ trans('admin.quantity') }}" style="display: inline-block">
									</div>
								</div>
							@endif

						</div>


						<a class="add-one-more-cargo"
						   data-receptacle-index="{{ $index }}"
						   data-get-template-url="{{ route('template-get-receptacle-cargo') }}"
						   data-cargos-container-selector=".receptacle-{{$index}}-cargos-container">
							<i class="fa fa-2x fa-plus-square"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
