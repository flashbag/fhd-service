@php

	$namedIndex = 1;

	if (isset($new_block_index)) {
		$namedIndex = $new_block_index;
	}

	$index = $namedIndex - 1;

@endphp

<div class="panel panel-default receptacle-block" id="receptacle-block-{{ $namedIndex }}">
	<div class="panel-heading clearfix">
        {{ trans('admin.position') }} #<span class="receptacle-number">{{ $namedIndex }}</span>
		@if ($index > 0)
			<div class="pull-right">
				<a class="btn btn-danger remove-receptacle-block col-xs-12">
					<i class="fa fa-plus"></i>  {{ trans('admin.del_position') }}
				</a>
			</div>
		@endif
	</div>
	<div class="panel-body panel-body-main">
		<div class="panel-body">
			<div class="row">
				<div class="form-group col-sm-3">
					<label for="inputTypeInventories">{{ trans('admin.package_type') }}<span class="color-red">*</span></label>
					<select class="form-control selectpicker select-type-inventory" name="receptacles[{{ $index }}][type_inventory_id]">
						@foreach($type_inventories   as $inventory)
							<option value="{{ $inventory->id }}" {{ old('receptacles.0.type_inventory_id') != $inventory->id ?: 'selected' }}>
								{{ __('trackers.parts.type_inventories.' . $inventory->id ) }}
							</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-sm-3">
					<label for="selectUnitOfLength">{{ trans('admin.unit_length') }}<span class="color-red">*</span></label>
					<input type="hidden" class="length-multiplier-to-base" name="receptacles[{{ $index }}][unit_length_multiplier_to_base]" value="">
					<select class="form-control selectpicker select-unit-of-length" name="receptacles[{{ $index }}][unit_of_length_id]" >
						@foreach($units_of_length as $unit_of_length)
							<option value="{{ $unit_of_length->id }}"
									data-is-metric="{{ $unit_of_length->is_metric }}"
									data-original-text="{{ $unit_of_length->text }}"
									data-multiplier-to-base="{{  $unit_of_length->multiplier_to_base }}"
									{{ old('receptacles.0.unit_of_length_id') != $unit_of_length->id ?: 'selected' }}>
								{{ __('trackers.parts.shipping.unit_length.' .  $unit_of_length->text ) }}
							</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="selectUnitOfWeight">{{ trans('admin.unit_weight') }}<span class="color-red">*</span></label>
					<input type="hidden" class="weight-multiplier-to-base" name="receptacles[{{ $index }}][unit_width_multiplier_to_base]" value="">
					<select class="form-control selectpicker select-unit-of-weight" name="receptacles[{{ $index }}][unit_of_weight_id]" >
						@foreach($units_of_weight as $unit_of_weight)
							<option value="{{ $unit_of_weight->id }}"
									data-is-metric="{{ $unit_of_length->is_metric }}"
									data-original-text="{{ $unit_of_weight->text }}"
									data-multiplier-to-base="{{  $unit_of_weight->multiplier_to_base }}"
									{{ old('receptacles.0.unit_of_weight_id') != $unit_of_weight->id ?: 'selected' }}>
								{{ __('trackers.parts.shipping.unit_weight.' .  $unit_of_weight->text ) }}
							</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<div class="panel-body">

			<div class="row">
				<div class="form-group col-sm-12">
						<span class="help-block help-block-static">
							{{ __('trackers.parts.receptacles.sizes_help_block') }}
						</span>
				</div>
			</div>

			<div class="row">

				<div class="form-group col-sm-4 receptacle-width-block">
					<label for="inputWidthCm">
                        {{ trans('admin.width') }}, <span class="length-measure-unit"></span>
						<span class="color-red">*</span>
					</label>
					<input type="text" min="0" step="0.01" autocomplete="off" class="only-numeric form-control input-width" name="receptacles[{{ $index }}][width]" value="{{ old('receptacles.0.width') }}">
				</div>
				<div class="form-group col-sm-4 receptacle-length-block">
					<label for="inputLengthCm">
                        {{ trans('admin.length') }}, <span class="length-measure-unit"></span>
						<span class="color-red">*</span>
					</label>
					<input type="text" min="0" step="0.01" autocomplete="off" class="only-numeric form-control input-length" name="receptacles[{{ $index }}][length]" value="{{ old('receptacles.0.length') }}">
				</div>
				<div class="form-group col-sm-4 receptacle-height-block">
					<label for="inputHeightCm">
                        {{ trans('admin.height') }}, <span class="length-measure-unit"></span>
						<span class="color-red">*</span>
					</label>
					<input type="text" min="0" step="0.01" autocomplete="off" class="only-numeric form-control input-height" name="receptacles[{{ $index }}][height]" value="{{ old('receptacles.0.height') }}">
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="form-group col-sm-4">
					<label for="inputActualWeight">{{ trans('admin.actual_weight') }}, <span class="weight-measure-unit"></span> <span class="color-red">*</span></label>
					<input type="text" min="0" autocomplete="off" class="only-numeric form-control input-actual-weight"  name="receptacles[{{ $index }}][actual_weight]" value="{{ old('receptacles.0.actual_weight') }}">
					@yield('receptacle-weight-help-block')
				</div>
				<div class="form-group col-sm-4">
					<label for="inputVolumeWeight">{{ trans('admin.volume_weight') }}, <span class="weight-measure-unit"></span> </label>
					<input type="text" min="0" autocomplete="off" class="only-numeric form-control input-volume-weight" name="receptacles[{{ $index }}][volume_weight]" readonly="readonly" value="{{ old('receptacles.0.volume_weight') }}">
					@yield('receptacle-weight-help-block')
				</div>

				<div class="form-group col-sm-2">
					<label for="inputAssessedPrice">{{ trans('admin.assessed_value') }}</label>
					<input type="text" min="0" autocomplete="off" class="only-numeric form-control"
						   id="inputAssessedPrice" name="receptacles[{{ $index }}][assessed_price]"
						   value="{{ old('receptacles.0.assessed_price') }}">
				</div>

				<div class="form-group col-sm-2">
					<label for="selectCurrencyType">{{ trans('main.tracker_create.content.label_11') }}</label>
					<select class="form-control selectpicker select-currency-type"
							data-size="10"
							name="receptacles[{{ $index }}][currency_type]">
						<option value="" style="display: none;">--</option>
						@foreach($currencies as $currency)
							<option value="{{ $currency->id }}">{{ trans('admin.' . $currency->code) }}
                                ({{ $currency->code }})</option>
						@endforeach
					</select>

				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="form-group col-sm-12">
					<label for="textareaDescriptionItem">{{ trans('admin.description_in_position') }}<span class="color-red">*</span></label>

					@yield('receptacle-cargos-block')

					<div class="cargos-container receptacle-{{ $index }}-cargos-container">

						@php
							$inputCargoNameLimit = \App\Models\Cargo::CARGO_NAME_LIMIT;
						@endphp

						<div class="row row-cargo">
							<div class="form-group col-md-8">
								<input type="text" class="form-control input-cargo-name" maxlength="{{ $inputCargoNameLimit }}" placeholder="{{ trans('admin.cargo_name') }}" name="receptacles[{{ $index }}][cargos][0][description]" style="display: inline-block">
								<span class="cargo-chars-count">{{ trans('admin.characters_left') }}: <span>{{ $inputCargoNameLimit }}</span></span>
							</div>
							{{--<div class="form-group col-md-3">--}}
								{{--<select class="form-control selectpicker select-cargo-is-used"--}}
										{{--data-size="10"--}}
										{{--name="receptacles[{{ $index }}][cargos][0][is_used]">--}}

									{{--<option value="0" selected>--}}
										{{--{{ __('trackers.parts.receptacles.cargos.is_used_false') }}--}}
									{{--</option>--}}

									{{--<option value="1">--}}
										{{--{{ __('trackers.parts.receptacles.cargos.is_used_true') }}--}}
									{{--</option>--}}

								{{--</select>--}}
							{{--</div>--}}
							<div class="form-group col-md-3">
								<input type="number" class="form-control quantity-field" placeholder="{{ trans('admin.quantity') }}" value="1" min="1" name="receptacles[{{ $index }}][cargos][0][quantity]" style="display: inline-block">
							</div>
						</div>

					</div>
					<a data-receptacle-index="{{ $index }}"
					   data-cargos-container-selector=".receptacle-{{ $index }}-cargos-container"
					   data-get-template-url="{{ route('template-get-receptacle-cargo') }}"
					   class="add-one-more-cargo add-one-more">
						<i class="fa fa-2x fa-plus-square"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
