<div class="row row-cargo">
	<div class="form-group col-md-8">
		<input type="text" name="receptacles[{{ $receptacle_index  }}][cargos][{{ $new_block_index  }}][description]"
			   class="form-control input-cargo-name"
			   placeholder="{{ __('trackers.parts.receptacles.cargos.description') }}"  style="display: inline-block">
		<span class="cargo-chars-count">
			{{ __('trackers.parts.receptacles.cargos.chars_left') }} :
			<span>40</span>
		</span>
	</div>
	{{--<div class="form-group col-md-3">--}}
		{{--<select class="form-control selectpicker select-cargo-is-used"--}}
				{{--data-size="10"--}}
				{{--name="receptacles[{{ $receptacle_index }}][cargos][{{$new_block_index}}][is_used]">--}}

			{{--<option value="0" selected>--}}
				{{--{{ __('trackers.parts.receptacles.cargos.is_used_false') }}--}}
			{{--</option>--}}

			{{--<option value="1">--}}
				{{--{{ __('trackers.parts.receptacles.cargos.is_used_true') }}--}}
			{{--</option>--}}

		{{--</select>--}}
	{{--</div>--}}
	<div class="form-group col-md-3">
		<input type="text" class="form-control quantity-field"
			   value="1"
			   min="1"
			   placeholder="{{ __('trackers.parts.receptacles.cargos.quantity') }}"
			   name="receptacles[{{ $receptacle_index  }}][cargos][{{ $new_block_index  }}][quantity]" style="display: inline-block">
	</div>
	<div class="form-group col-md-1">
		<a class="remove-cargo-block remove-one-more"
		   data-cargos-container-selector=".receptacle-{{ $receptacle_index  }}-cargos-container">
			<i class="fa fa-2x fa-minus-square"></i>
		</a>
	</div>
</div>
