<section class="buttons-create-back">
    <div class="panel-footer" style="position: fixed; top: 50px; left: 0; width: 100%; z-index: 99998; border-bottom: 1px solid #d3e0e9;">
        <div class="container">
            <div class="pull-left">
                <a href="{{ route('trackers') }}" class="btn btn-default">{{ trans('admin.back_button') }}</a>
            </div>
            <div class="pull-right">
                <button type="submit" class="btn btn-success">{{ trans('admin.save') }}</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>