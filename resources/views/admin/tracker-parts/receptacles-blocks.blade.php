@section('receptacle-weight-help-block')
	<span class="help-block help-block-static">
		{{ __('trackers.parts.receptacles.weight_help_block')  }}
	</span>
@endsection

@section('receptacle-cargos-block')
	<span class="help-block help-block-static">
		{{ __('trackers.parts.receptacles.cargos_help_block_box')  }}
	</span>
@endsection

<section class="shipping-information step" data-id=3>

	<div class="panel-group">
		<div class="panel panel-default">

			<div class="panel-heading clearfix" role="tab">
				<h4 class="pull-left panel-title">{{ trans('admin.ct_title_1') }}</h4>

			</div>
			<div class="panel-collapse collapse in" role="tabpanel">
				<div class="panel-body">
					<div class="receptacle-container">
						@if (isset($tracker) && $tracker->receptacles->count())
							@foreach($tracker->receptacles as $index => $receptacle)
								@include('admin.tracker-parts.receptacles.receptacle-existing')
							@endforeach
						@else
							@include('admin.tracker-parts.receptacles.receptacle-new')
						@endif
					</div>

					<br>

					<div class="row">
						<div class="col-lg-12 pull-right">
							<a class="btn btn-success add-one-more-receptacle" data-get-template-url="{{ route('template-get-receptacle') }}">
								<i class="fa fa-plus"></i> {{ trans('admin.ct_title_2') }}
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
