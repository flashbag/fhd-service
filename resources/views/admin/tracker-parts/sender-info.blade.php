<section class="sender-info step" data-id=6>
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('admin.sender') }}</div>

        <div class="panel-body">
            <div class="row">

				<div class="form-group col-sm-3 {{ $errors->has('number_sender') ? ' has-error' : '' }}">
					<label for="numberSender">{{ trans('admin.phone') }}<span class="color-red">*</span></label>
					<div class="typehead typehead-number-sender">

						<input type="text" class="form-control input__custom" id="numberSender" name="number_sender"
							@if (isset($tracker) && $tracker->sender)
								value="{{ old('number_sender', $tracker->sender->number) }}"
							@else
								value="{{ old('number_sender') }}"
							@endif
						>
						<span id="numberSender-valid-msg" class="hide phone-valid-msg">✓ Valid</span>
						<span id="numberSender-error-msg" class="hide phone-error-msg"></span>
						<i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
						<ul class="typehead-list"></ul>
					</div>
					@if ($errors->has('number_sender'))
						<span class="help-block">
							<strong>{{ $errors->first('number_sender') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group col-sm-3 {{ $errors->has('email_sender') ? ' has-error' : '' }}">
					<label for="inputEmailSender">Email <span class="color-red">*</span></label>

					<input type="text" class="form-control input__custom" id="inputEmailSender" name="email_sender"
						   data-error-msg="Некорректный Email"
						@if (isset($tracker) && $tracker->sender)
							value="{{ old('email_sender', $tracker->sender->email) }}"
						@else
							value="{{ old('email_sender') }}"
						@endif
					>
					@if ($errors->has('email_sender'))
						<span class="help-block">
							<strong>{{ $errors->first('email_sender') }}</strong>
						</span>
					@endif
				</div>

				<div class="form-group col-sm-3 {{ $errors->has('sender_person_type') ? ' has-error' : '' }}">

					<label for="senderPersonType">{{ trans('admin.person_type') }}</label>

					<select class="form-control selectpicker select-person-type" id="senderPersonType" name="sender_person_type">

						@foreach(\App\Models\ClientInfo::getPersonTypes() as $personType)

							@if (isset($tracker) && $tracker->sender)

								<option value={{ $personType }}
										{{ old('sender_person_type') != $tracker->sender->person_type ?: 'selected' }} @if($tracker->sender->person_type == $personType)selected @endif>

							@else
								<option value={{ $personType }} {{ old('sender_person_type') != $personType ?: 'selected' }}>
							@endif


							{{ __('trackers.parts.person_types.' . $personType) }}
						@endforeach

					</select>

					@if ($errors->has('sender_person_type'))
						<span class="help-block">
						<strong>{{ $errors->first('sender_person_type') }}</strong>
					</span>
					@endif

				</div>

				<div class="form-group col-sm-3 {{ $errors->has('inn_sender') ? ' has-error' : '' }}">
					<label for="senderInputInn">{{ trans('admin.itn') }}</label>
					<input type="text" class="only-numeric form-control input__custom" id="senderInputInn" name="inn_sender"
						   @if (isset($tracker) && $tracker->sender)
						   value="{{ old('inn_sender', $tracker->sender->inn) }}"
						   @else
						   value="{{ old('inn_sender') }}"
							@endif
					>
					@if ($errors->has('inn_sender'))
						<span class="help-block">
							<strong>{{ $errors->first('inn_sender') }}</strong>
						</span>
					@endif
				</div>

			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="form-group col-sm-3 {{ $errors->has('full_name_sender') ? ' has-error' : '' }}">
				<label for="inputSenderFullName">{{ trans('admin.name') }} <span class="color-red">*</span></label>
				<input type="text" class="form-control input__custom" id="inputSenderFullName" name="full_name_sender"
					   data-without-digits="true"
					   @if (isset($tracker) && $tracker->sender)
					   value="{{ old('full_name_sender', $tracker->sender->full_name) }}"
					   @else
					   value="{{ old('full_name_sender') }}"
						@endif
				>
				@if ($errors->has('full_name_sender'))
					<span class="help-block">
							<strong>{{ $errors->first('full_name_sender') }}</strong>
						</span>
				@endif
			</div>

				<div class="form-group col-sm-3 {{ $errors->has('sender_company_name') ? ' has-error' : '' }}">
					<label for="inputSenderCompanyName">{{ trans('admin.company') }} <span class="color-red">*</span></label>
					<input type="text" class="form-control input__custom" id="inputSenderCompanyName" name="sender_company_name"
						   @if (isset($tracker) && $tracker->sender)
						   value="{{ old('sender_company_name', $tracker->sender->company_name) }}"
						   @else
						   value="{{ old('sender_company_name') }}"
							@endif
					>
					@if ($errors->has('sender_company_name'))
						<span class="help-block">
						<strong>{{ $errors->first('sender_company_name') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group col-sm-3 senderNumberVatBlock {{ $errors->has('number_vat') ? ' has-error' : '' }}">
					<label for="numberVat">{{ trans('admin.VAT') }}</label>
					<input type="text" class="form-control input__custom" id="numberVat" name="number_vat"
						   @if (isset($tracker) && $tracker->sender)
						   value="{{ old('number_vat', $tracker->sender->vat) }}"
						   @else
						   value="{{ old('number_vat') }}"
							@endif
					>
					@if ($errors->has('number_vat'))
						<span class="help-block">
						<strong>{{ $errors->first('number_vat') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group col-sm-3 {{ $errors->has('passport_data_sender') ? ' has-error' : '' }}">
					<label for="senderInputPassportData">{{ trans('admin.passport') }}</label>
					<input type="text" class="form-control input__custom" id="senderInputPassportData" name="passport_data_sender"
						   @if (isset($tracker) && $tracker->sender)
						   value="{{ old('passport_data_sender', $tracker->sender->passport_data) }}"
						   @else
						   value="{{ old('passport_data_sender') }}"
							@endif
					>
					@if ($errors->has('passport_data_sender'))
						<span class="help-block">
						<strong>{{ $errors->first('passport_data_sender') }}</strong>
					</span>
					@endif
				</div>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">

				<div class="form-group col-sm-3 {{ $errors->has('city_id_sender') ? ' has-error' : '' }}">
					<label for="inputCitySenderId">{{ trans('admin.city') }} <span class="color-red">*</span></label>

					<div class="typehead typehead-google-address typehead-google-address-city">
						<input type="text" class="form-control input__custom"
							   name="city_id_sender"
							   id="inputCitySenderId"
							   data-without-digits="true"
							   data-country-selector="#inputCountrySenderId"
							   data-country-disabled-selector=".disabled-country-sender"
							   data-region-selector="#inputSenderRegion"
							   data-address-selector="#inputAddressSenderId"
							   @if (isset($tracker) && $tracker->sender)
							   value="{{ old('city_id_sender', $tracker->sender->city) }}"
							   @else
							   value="{{ old('city_id_sender') }}"
								@endif
						>
						<i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
						<ul class="typehead-list"></ul>
					</div>

					@if ($errors->has('city_id_sender'))
						<span class="help-block">
                                <strong>{{ $errors->first('city_id_sender') }}</strong>
                            </span>
					@endif
				</div>

				<div class="form-group col-sm-3 {{ $errors->has('region_sender') ? ' has-error' : '' }}">
					<label for="inputSenderRegion">{{ trans('admin.state') }}  <span class="color-red">*</span></label>

					<div class="typehead typehead-google-address typehead-google-address-region">
						<input type="text" class="form-control input__custom"
							   name="region_sender"
							   id="inputSenderRegion"
							   data-without-digits="true"
							   data-country-selector="#inputCountrySenderId"
							   data-city-selector="#inputCitySenderId"
							   data-address-selector="#inputAddressSenderId"
							   @if (isset($tracker) && $tracker->sender)
							   value="{{ old('region_sender', $tracker->sender->region) }}"
							   @else
							   value="{{ old('region_sender') }}"
								@endif
						>
						<i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
						<ul class="typehead-list"></ul>
					</div>

					@if ($errors->has('region_sender'))
						<span class="help-block">
							<strong>{{ $errors->first('region_sender') }}</strong>
						</span>
					@endif
				</div>

                <div class="form-group col-sm-3 {{ $errors->has('country_id_sender') ? ' has-error' : '' }}">
                    <label for="inputCountrySenderId">{{ trans('admin.country') }} <span class="color-red">*</span></label>

					<div class="typehead typehead-google-address typehead-google-address-country">
						<input type="text" class="form-control input__custom"
							   readonly="readonly"
							   name="country_id_sender"
							   id="inputCountrySenderId"
							   data-region-selector="#inputSenderRegion"
							   data-city-selector="#inputCitySenderId"
							   data-address-selector="#inputAddressSenderId"
							   @if (isset($tracker) && $tracker->sender)
								value="{{ old('country_id_sender', $tracker->sender->country) }}"
								@else
                                    @if (isset($warehouse_country))
                                        value="{{ old('country_id_sender', $warehouse_country->name) }}"
                                    @endif
                               value="{{ old('country_id_sender') }}"
                                @endif
                    >
                    <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                    <ul class="typehead-list"></ul>
                </div>

                @if ($errors->has('country_id_sender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('country_id_sender') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-sm-3 {{ $errors->has('address_sender') ? ' has-error' : '' }}">
                <label for="inputAddressSenderId">{{ trans('admin.address') }}<span class="color-red">*</span></label>

                <div class="typehead typehead-google-address typehead-google-address-address">
                    <input type="text" class="form-control input__custom"
                           name="address_sender"
                           id="inputAddressSenderId"
                           data-country-selector="#inputCountrySenderId"
                           data-region-selector="#inputSenderRegion"
                           data-city-selector="#inputCitySenderId"
                           data-index-selector="#inputSenderIndex"
                            @if (isset($tracker) && $tracker->sender)
                            value="{{ old('address_sender', $tracker->sender->address) }}"
                            @else
                            value="{{ old('address_sender') }}"
                            @endif
                    >
                    <i class="fa fa-refresh refresh-input-informer" aria-hidden="true"></i>
                    <ul class="typehead-list"></ul>
                </div>
                <span class="help-block help-block-static">
                    {{ __('trackers.parts.address_components.address_help_block') }}
                </span>
                @if ($errors->has('address_sender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address_sender') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-sm-3 {{ $errors->has('index_sender') ? ' has-error' : '' }}">
                <label for="inputSenderIndex">{{ trans('admin.post_code') }} <span class="color-red">*</span></label>

                <input type="text" class="form-control input__custom" id="inputSenderIndex" name="index_sender"
                       @if (isset($tracker) && $tracker->sender)
                       value="{{ old('index_sender', $tracker->sender->index) }}"
                       @else
                       value="{{ old('index_sender') }}"
                        @endif
                >
                @if ($errors->has('index_sender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('index_sender') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">

            <div class="form-group col-sm-3 {{ $errors->has('sender_number_account') ? ' has-error' : '' }}">
                <label for="senderAccountNumber">
                    {{ trans('admin.account') }}
                </label>
                <input type="text" class="only-numeric form-control input__custom" id="senderAccountNumber" name="sender_number_account"
                       @if (isset($tracker) && $tracker->sender)
                       value="{{ old('sender_number_account', $tracker->sender->number_account) }}"
                       @else
                       value="{{ old('sender_number_account') }}"
                        @endif
                >
                @if ($errors->has('sender_number_account'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sender_number_account') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-sm-3 {{ $errors->has('sender_number_service') ? ' has-error' : '' }}">
                <label for="inputSenderNumberService">
                    {{ trans('admin.service') }}
                </label>
                <input type="text" class="form-control only-numeric input__custom" id="inputSenderNumberService" name="sender_number_service"
                       @if (isset($tracker) && $tracker->sender)
                       value="{{ old('sender_number_service', $tracker->sender->number_service) }}"
                       @else
                       value="{{ old('sender_number_service') }}"
                        @endif
                >
                @if ($errors->has('sender_number_service'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sender_number_service') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-sm-3 {{ $errors->has('sender_service_delivery_id') ? ' has-error' : '' }}">
                <label for="senderServiceDeliverySelect">{{ trans('admin.delivery') }}</label>

                <select class="form-control selectpicker" id="senderServiceDeliverySelect" name="sender_service_delivery_id">

                    <option selected value="null">--</option>

                    @foreach($service_deliveries as $service_delivery)

                        @if (isset($tracker) && $tracker->sender)

                            <option value="{{ $service_delivery->id }}"
                                    {{ old('sender_service_delivery_id') != $service_delivery->id ?: 'selected' }} @if($tracker->sender->service_delivery_id == $service_delivery->id)selected @endif>

                        @else
                            <option value="{{ $service_delivery->id }}" {{ old('sender_service_delivery_id') != $service_delivery->id ?: 'selected' }}>
                        @endif

                        {{ $service_delivery->name }}
                    @endforeach
                    <option value="" @if (isset($tracker) && $tracker->sender && $tracker->sender->service_delivery_other) selected @endif>{{ trans('admin.another_delivery') }}</option>

                </select>

                @if ($errors->has('sender_service_delivery_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sender_service_delivery_id') }}</strong>
                    </span>
                @endif

            </div>

            <div class="form-group col-sm-3 senderServiceDeliveryOther {{ $errors->has('sender_service_delivery_other') ? ' has-error' : '' }}" style="display: none">
                <label for="bookingNumber">{{ trans('admin.another_delivery') }}</label>

                <input type="text" disabled="disabled" class="form-control input__custom" name="sender_service_delivery_other"
                       @if (isset($tracker) && $tracker->sender)
                       value="{{ old('sender_service_delivery_other', $tracker->sender->service_delivery_other) }}"
                       @else
                       value="{{ old('sender_service_delivery_other') }}"
                        @endif
                >

                @if ($errors->has('sender_service_delivery_other'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sender_service_delivery_other') }}</strong>
                    </span>
                @endif

            </div>

        </div>
    </div>

    <div class="panel-body">
        <div class="row">

            <div class="form-group col-sm-12">

                <label>{{ trans('admin.senders_documents') }}</label>

                <br>

                <div class="sender-files-container">

                    @if (isset($tracker) && $tracker->senderDocuments->count())
                        @foreach($tracker->senderDocuments as $index => $senderDocument)
                            <div class="form-group file-block">
                                <input type="hidden" name="sender_documents[{{$index}}][id]" value="{{ $senderDocument->id }}">
                                <input type="file" name="sender_documents[{{$index}}][file]" style="display: inline-block">
                                <a href="{{ $senderDocument->getDocumentUrl() }}" style="margin-right: 15px;" target="_blank">
                                    {{ trans('admin.view_doc') }} #{{ $senderDocument->id }}
                                </a>
                                <a class="remove-file-input is-existing-file"
                                   data-input-name="sender_documents"
                                   data-container-selector=".sender-files-container"
                                   data-index="{{ $index }}" data-id="{{ $senderDocument->id }}"
                                   >
                                    <i class="fa fa-2x fa-minus-square"></i>
                                </a>
                            </div>
                        @endforeach
                    @else
                    <div class="form-group file-block">
                        <!-- This hidden field is needed! -->
                        <input type="hidden" name="sender_documents[1][beacon]" value="beacon">
                        <input type="file" name="sender_documents[1][file]" style="display: inline-block">
                    </div>
                    @endif

                </div>


                <a data-input-name="sender_documents" data-get-template-url="{{ route('template-get-file') }}" data-container-selector=".sender-files-container" class="add-one-more-file">
                    <i class="fa fa-2x fa-plus-square"></i>
                </a>


            </div>
        </div>
    </div>

</div>
</section>
