@extends('admin.layouts.app')

@section('content')

	<div class="row">
		<div class="col-md-4 pull-left">
			<a class="btn btn-primary" data-toggle="collapse" href="#filters" role="button" aria-expanded="false" aria-controls="collapseExample">
				Фильтры
			</a>
		</div>

		<div class="col-md-2 pull-right">
			<a href="{{ route('create-users') }}" class="btn btn-success pull-right">Добавить пользователя</a>
		</div>

		<div class="col-md-2 pull-right">
			<a href="{{ route('admin-create-trackers') }}" class="btn btn-success pull-right">Добавить трекер</a>
		</div>

	</div>

	<h1 class="pull-left">Список пользователей</h1>

	@include('filters.filters-users')

    <div class="clearfix"></div>

	<div id="filters-container">
		@include('admin/users-table')
	</div>

@endsection
