@extends('admin.layouts.app')

@section('content')

    <h1 class="pull-left">Список категорий новостей</h1>
    <a href="{{ route('create-news-category') }}" class="btn btn-success pull-right">Добавить категорию</a>
    <div class="clearfix"></div>

    @if(isset($newsCategories) && $newsCategories->count() < 1)

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Информация</div>

                    <div class="panel-body">
                        Категории не существуют!
                    </div>
                </div>
            </div>
        </div>

    @else

        <table class="table table-bordered table-tracker">
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Родительская категория</th>
                <th>Описание</th>
                <th>Статус</th>
                <th>Дата создания</th>
                <th>Редактировать</th>
            </tr>
            @foreach($newsCategories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->title }}</td>
                    <td>--</td>
                    <td>{!! str_limit($category->description, 150, '...') !!}</td>
                    <td>{{ $category->is_active == '0' ? 'Не активно' : 'Активно' }}</td>
                    <td>{{ $category->created_at }}</td>
                    <td><a href="{{ route('edit-news-category', $category->id) }}">Изменить</a></td>
                </tr>
            @endforeach
        </table>

        {{ $newsCategories->links() }}

    @endif

@endsection