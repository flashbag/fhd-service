@extends('admin.layouts.app')

@section('content')

    <h1>Создание пользователя</h1>

    <form action="{{ route('store-users') }}" method="post">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">Поля для заполнения</div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('name_user') ? ' has-error' : '' }}">
                        <label for="inputNameUser">ФИО пользователя</label>
                        <input type="text" class="form-control" id="inputNameUser" name="name_user"
                               value="{{ old('name_user') }}">
                        @if ($errors->has('name_user'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name_user') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="inputEmail">Email</label>
                        <input type="text" class="form-control" id="inputEmail" name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="inputPassword">Пароль</label>
                        <input type="password" class="form-control" id="inputPassword" name="password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('repeat-password') ? ' has-error' : '' }}">
                        <label for="inputRepeatPassword">Подтвердите пароль</label>
                        <input type="password" class="form-control" id="inputRepeatPassword" name="repeat-password"
                               value="">
                        @if ($errors->has('repeat-password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('repeat-password') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-sm-4 {{ $errors->has('role_id') ? ' has-error' : '' }}">
                        <label for="roleId">Роль пользователя</label>
                        <select id="roleId" class="form-control selectpicker role_id" name="role_id">
                            <option selected style="display: none;" value="">--</option>
                        </select>
                        @if ($errors->has('role_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-4 {{ $errors->has('number') ? ' has-error' : '' }}">
                        <label for="number">{{ trans('main.login.reg_form.text_4') }}</label>
                        <input id="numberReg" type="text" class="form-control" name="number" value="{{ old('number') }}"
                               required>

                        @if ($errors->has('number'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                        @endif
                    </div>

                </div>


            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <a href="{{ route('users') }}" class="btn btn-default">Назад к пользователям</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Создать пользователя</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </form>

@endsection

@push('script')

    <script>

        $(document).ready(function () {

            $('.role_id').click(function () {
                var role_id = $('select[name="role_id"]');
                if (!role_id.hasClass('fetch-roles')) {
                    $.get('/admin/users/get/roles', function (data) {
                        if (data !== null) {
                            role_id.addClass('fetch-roles');
                            role_id.empty();
                            role_id.append('<option selected style="display: none;" value="">--</option>');
                            data.forEach(function (item) {
                                if (item.parent_id == null) {
                                    role_id.append('<option value="' + item.id + '">' + item.name + '</option>');
                                }
                            });
                            role_id.selectpicker('refresh');
                        }
                    }, 'json');
                }
            });

        });

    </script>

@endpush
