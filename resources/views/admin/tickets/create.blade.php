@extends('admin.layouts.app')
@section('textarea')
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=cnvl1esea3e9bn318ynmvoszeckcecwv626o4acpa1yq03xw"></script>
    {{--<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>--}}
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'link code',
            menubar: false,
        });
    </script>
    <style>
        .tox-statusbar__branding{
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Open New Ticket</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/new_ticket') }}">
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('tracker') ? ' has-error' : '' }}">
                            <label for="tracker" class="col-md-4 control-label">Tracker</label>
                            <div class="col-md-6">
                                <select id="tracker" type="tracker" class="form-control" name="tracker">
                                    <option value="{{ $tracker_id }}">{{ $id_tracker }}</option>
                                </select>
                                @if ($errors->has('tracker'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tracker') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                            <label for="message" class="col-md-4 control-label">Message</label>

                            <div class="col-md-6">
                                <textarea rows="10" id="message" class="form-control" name="message"></textarea>

                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <p><input type="file" name="file" style="text-align: right">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-ticket"></i> Open Ticket
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
