<section class="services-section" id="calculator">
	<div class="container">
		<div class="title-block">
			<h2 class="title">{{ trans('main.main_page.count_myself') }}</h2>
		</div>
	</div>

	<div class="calculator-custom calculator-custom_bg">

		<!-- Nav tabs -->
		<div id="tab-calculator" class="nav nav-tabs calculator-custom__tabs">
			<div class="calculator-custom__tab active">
				<a href="#air" id="calculator-href-air" data-toggle="tab"
				   data-type-transport-string="{{ __('main.main_page.calculator.type_transport_strings.air') }}">
					<svg style="width: 32px;height: 32px;"><use xlink:href="#air_SVG"></use></svg>
					{{ trans('main.main_page.air') }}
				</a>
			</div>
			<div class="calculator-custom__tab">
				<a href="#water" id="calculator-href-water" data-toggle="tab"
				   data-type-transport-string="{{ __('main.main_page.calculator.type_transport_strings.water') }}">
					<svg style="width: 48px; height: 32px;"><use xlink:href="#water_SVG"></use></svg>
					{{ trans('main.main_page.water') }}
				</a>
			</div>
		</div>

		<div class="container">

			<div class="row">

				<!-- Country FROM -->
				<div class="col-xs-12 col-sm-6 col-md-3 mb-2">

					<div class="select__custom_wrapp">
						<select class="form-control selectpicker" name="country_from">
							<option>{{ trans('main.main_page.sender_country') }}</option>
							@foreach($countriesFrom as $country)
								<option value="{{ $country->iso_3166_2 }}">
									{{ __('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2) }}
								</option>
							@endforeach
						</select>
					</div>

				</div>

				<!-- Country TO -->
				<div class="col-xs-12 col-sm-6 col-md-3 mb-2">

					<div class="select__custom_wrapp">
						<select class="form-control selectpicker" name="country_to">
							<option>{{ trans('main.main_page.recipient_country') }}</option>
							@foreach($countriesTo as $country)
								<option value="{{ $country->iso_3166_2 }}">
									{{ __('trackers.parts.countries.by_iso_3166_2.' . $country->iso_3166_2) }}
								</option>
							@endforeach
						</select>
					</div>

				</div>

				<!-- Service Delivery -->
				<div class="col-xs-12 col-sm-6 col-md-3 mb-2" style="display: none">

					<!--// Select-->
					<div class="select__custom_wrapp">
						<select class="form-control selectpicker" name="delivery_service">
							<option>{{ trans('main.main_page.delivery_service') }}</option>
							<option>Самовывоз</option>
							<option>Новая почта</option>
							<option>Интайм</option>
						</select>
					</div>
					<!--// Select-->

				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 mb-2" style="min-width: 92px;">
					<input type="number" class="input__custom  form-control" name="actual_weight" placeholder="{{ trans('main.main_page.weight') }}">
				</div>


				<div class="col-xs-6 col-sm-3 col-md-2" style="max-width: 160px;">
					<div class="checkbox">
						<label class="calculator-custom__checkbox">
							<span>{{ trans('main.main_page.kg') }}</span>
							<input type="checkbox" name="checkbox_weight">
							<span class="calculator-custom__checkbox_tumbler active"></span>
							<span>{{ trans('main.main_page.pd') }}</span>
						</label>
					</div>
				</div>
			</div>

			<!-- Dimensions -->
			<div class="row" style="max-width: 940px;">
				<div class="col-xs-12">
					<div class="checkbox">
						<label class="calculator-custom__checkbox2 calculator-custom__checkbox_inline">
							<span>{{ trans('main.main_page.with_dimensions') }}</span>
							<input type="checkbox" name="checkbox_dimensions">
							<span class="calculator-custom__checkbox2_check active"></span>
						</label>
					</div>
				</div>

				<div class="calculator-dimensions-block">
					<div class="col-xs-12 col-sm-6 col-md-3 mb-2">
						<input type="number" class="input__custom  form-control" name="calculator-length" placeholder="{{ trans('main.main_page.length') }}">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 mb-2">
						<input type="number" class="input__custom  form-control" name="calculator-width" placeholder="{{ trans('main.main_page.width') }}">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 mb-2">
						<input type="number" class="input__custom  form-control" name="calculator-height" placeholder="{{ trans('main.main_page.height') }}">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3" style="max-width: 160px;">
						<div class="checkbox">
							<label class="calculator-custom__checkbox">
								<span>{{ trans('main.main_page.cm') }}</span>
								<input type="checkbox" name="checkbox_length">
								<span class="calculator-custom__checkbox_tumbler active"></span>
								<span>{{ trans('main.main_page.in') }}</span>
							</label>
						</div>
					</div>
					<div class="col-xs-12">
						<p class="info-message">{{ trans('main.main_page.comment1') }}</p>
					</div>
				</div>
			</div>

			<!-- Custom Duty -->
			<div class="row" style="max-width: 940px; ">
				<div class="col-xs-12">
					<div class="checkbox">
						<label class="calculator-custom__checkbox2 calculator-custom__checkbox_inline">
							<span>{{ trans('main.main_page.with_custom_duty') }}</span>
							<input type="checkbox" name="checkbox_custom_duty">
							<span class="calculator-custom__checkbox2_check active"></span>
						</label>
					</div>
				</div>
				<div class="calculator-custom-duty-block">
					<div class="col-xs-12 col-sm-6 col-md-3 mb-2">
						<input type="number" class="input__custom  form-control" name="calculator-assessed-price" placeholder="{{ trans('main.main_page.assessed_value') }}">
						<input type="hidden" name="eur-multiplier-to-usd" value="{{ $eurMultiplierToUsd }}">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 mb-2">

						<!--// Select-->
						<div class="select__custom_wrapp">
							<select class="form-control selectpicker" name="calculator-currency">
								<option>{{ trans('main.main_page.currency') }}</option>
								@foreach($currencies as $currency)
                                    <option value="{{ $currency->id }}"
											data-multiplier-usd="{{  $currency->usd }}"
											data-multiplier-eur="{{  $currency->eur }}"
									>
                                        {{ $currency->code }}
                                    </option>
								@endforeach
							</select>
						</div>
						<!--// Select-->

					</div>
					<div class="col-xs-12">
						<p class="info-message">{{ trans('main.main_page.comment2') }} <a href="#">{{ trans('main.main_page.link') }}</a></p>
					</div>
				</div>
			</div>

			<div class="calculator-btn">
				<button id="calculator-calculate" class="btn btn-main calculator-btn_padding">
					{{ trans('main.main_page.count') }}
				</button>
			</div>


			<div class="row" id="calculator-results">
				<div class="col-xs-12 results-delivery-primary">
					<p style="padding: 10px; ">
						<span class="pull-left delivery-primary-title">
							{!!  __('main.main_page.calculator.results.delivery_primary') !!}
							<span class="volume-weight-counting" style="display: none">
								{{ __('main.main_page.calculator.results.with_volume_weight') }}
							</span>
						</span>
						<span class="pull-right">
							<span id="result-delivery-primary">
								<span>0</span>$
							</span>
						</span>
					</p>
					<div class="calculator-results-separator"></div>
				</div>

				<div class="col-xs-12 results-custom-duty">
					<p style="padding: 10px; ">
						<span class="pull-left">
							{{ __('main.main_page.calculator.results.custom_duty') }}
						</span>
						<span class="pull-right">
							<span id="result-custom-duty">
								<span>0</span>$
							</span>
						</span>
					</p>
					<div class="calculator-results-separator"></div>
				</div>

				<div class="col-xs-12 results-delivery-secondary" style="display: none;">
					<p style="padding: 10px; ">
						<span class="pull-left">
							{{ __('main.main_page.calculator.results.delivery_secondary') }}
						</span>
						<span class="pull-right">
							<span id="result-delivery-secondary">
								<span>0</span>$
							</span>
						</span>
					</p>
					<div class="calculator-results-separator"></div>
				</div>

				<div class="col-xs-12 results-insurance">
					<p style="padding: 10px; ">
						<span class="pull-left">
							{{ __('main.main_page.calculator.results.insurance') }}
						</span>
						<span class="pull-right">
							<span id="result-insurance">
								<span>0</span>$
							</span>
						</span>
					</p>
				</div>

				<div class="col-xs-12 text-right calculator-total-block">
					<p style="padding-top:20px; line-height: 30px;font-size: 20px;">
						<span class="pull-right">
							{{ __('main.main_page.calculator.results.total') }}
							<span id="result-total">
								<span>0</span>$
							</span>
						</span>
					</p>
				</div>
			</div>

		</div>
	</div>

</section>

@push('script')

	<script>
		$(document).ready(function(){


		});

	</script>

@endpush
