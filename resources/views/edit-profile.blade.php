@extends('layouts.app')

@section('slider')

    <section class="hero-carousel" style="height: 85%;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('http://cargo.bold-themes.com/transport-company/wp-content/uploads/sites/2/2015/09/Dark_Plane.jpg') 50% -50px; background-size: cover;">
                    <div class="container">
                        <div class="carousel-caption-new">

                            <div class="pull-left" style="width: 65%;">
                                <p style="font-size: 21px; line-height: 41px; font-weight: 600; margin-bottom: 0;">{{ trans('main.history.p') }}</p>
                                <h1>{{ trans('main.history.h1') }}</h1>
                                <h5 class="subtitle">{{ trans('main.history.h5') }}</h5>
                                <div class="btn-group-block">
                                    <a href="/about-us" class="btn btn-default-new">{{ trans('main.history.link2') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('content')

    {{--@include('include.after-hero-block')--}}

    <div class="wrap">
        <div class="contacts-page">
            <div class="container">

                @if(Session::has('success-message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">{{ trans('main.custom.inform') }}</div>

                                <div class="panel-body">
                                    {{ Session::get('success-message') }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                    @if(Session::has('error-message'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">{{ trans('main.custom.error') }}</div>

                                    <div class="panel-body">
                                        {{ Session::get('error-message') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @if($errors->count())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-danger">
                                <div class="panel-heading">{{ trans('main.custom.error') }}</div>
                                <div class="panel-body">
                                    @foreach ($errors->all() as $key => $value)
                                        {{ $value }}<br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">

                    @if(Auth::check())
                        @if(Auth::user()->hasRole('User'))

                            <div class="edit-profile col-xs-12 col-sm-10 col-sm-offset-1">

                                <div class="page-header-block">
{{--                                    <p style="text-align: left;">{{ trans('main.custom.profile1') }}</p>--}}
                                    <h3 class="fhd-title fhd-title_size fhd-title_blue my-5">{{ trans('main.custom.formprofile') }}</h3>
                                </div>

                                <form class="default-form" action="{{ route('update-profile') }}" method="post">
                                    {{ csrf_field() }}
                                    <input name="_method" type="hidden" value="PUT">
                                    <div class="edit-profile__panel">
                                        <div class="edit-profile__panel-heading">
                                            <h5>{{ trans('main.history.content.h5') }}</h5>
                                        </div>

                                        <div class="edit-profile__panel-body">

                                            <div class="row mb-4">
                                                <div class="col-sm-6">
                                                    <label for="inputUsername" class="input-title">{{trans('main.custom.fioorcomp') }}</label>
                                                    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputUsername" name="username" value="{{ old('username', $user->name) }}">
                                                    </div>
                                                    @if ($errors->has('username'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputEmail" class="input-title">{{trans('main.custom.mail') }}</label>
                                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputEmail" name="email" value="{{ old('email', $user->email) }}">
                                                    </div>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-sm-6">
                                                    <label for="inputNumber" class="input-title">{{trans('main.custom.number') }}</label>
                                                    <div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputNumber" name="number" value="{{ old('number', $user->number) }}">
                                                    </div>
                                                    @if ($errors->has('number'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('number') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="inputINN" class="input-title">{{trans('main.custom.inn') }}</label>
                                                    <div class="form-group {{ $errors->has('inn') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputINN" name="inn" value="{{ old('inn', $clientInfo->inn) }}">
                                                        @if ($errors->has('inn'))
                                                            <span class="help-block">
                                                        <strong>{{ $errors->first('inn') }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-sm-6">
                                                    <label for="inputPassportData" class="input-title">{{trans('main.custom.pasport') }}</label>
                                                    <div class="form-group {{ $errors->has('passport_data') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputPassportData" name="passport_data" value="{{ old('passport_data', $clientInfo->passport_data) }}">
                                                    </div>
                                                    @if ($errors->has('passport_data'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('passport_data') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputIndex" class="input-title">{{trans('main.custom.index') }}</label>
                                                    <div class="form-group {{ $errors->has('index') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputIndex" name="index" value="{{ old('index', $clientInfo->index) }}">
                                                    </div>
                                                    @if ($errors->has('index'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('index') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row mb-4">

                                                <div class="col-sm-6">
                                                    <label for="inputCountry" class="input-title">{{trans('main.custom.country') }}</label>
                                                    <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputCountry" name="country" value="{{ old('country', $clientInfo->country) }}">
                                                    </div>
                                                    @if ($errors->has('country'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('country') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputCity" class="input-title">{{trans('main.custom.city') }}</label>
                                                    <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputCity" name="city" value="{{ old('city', $clientInfo->city) }}">
                                                    </div>
                                                    @if ($errors->has('city'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <div class="col-sm-6">
                                                    <label for="inputAddress" class="input-title">{{trans('main.custom.adres') }}</label>
                                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputAddress" name="address" value="{{ old('address', $clientInfo->address) }}">
                                                    </div>
                                                    @if ($errors->has('address'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                          
                                            <div class="row mb-4 edit-profile_border">
                                                <div class="col-sm-6">
                                                    <label for="inputPassword" class="input-title">{{trans('main.custom.newpas') }}</label>
                                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <input type="password" class="input__custom input__custom_bg" id="inputPassword" name="password">
                                                    </div>
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputRepeatPassword" class="input-title">{{trans('main.custom.acceptpas') }}</label>
                                                    <div class="form-group {{ $errors->has('repeat_password') ? ' has-error' : '' }}">
                                                        <input type="password" class="input__custom input__custom_bg" id="inputRepeatPassword" name="repeat_password">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-end mt-5">
                                        <button type="submit" class="btn-custom btn-custom_yellow-bg">{{trans('main.custom.updatepas') }}</button>
                                    </div>
                                </form>
                                <br>
                                <br>
                                <br>
                            </div>
{{--                            <div class="col-sm-3">--}}

{{--                                <style>--}}
{{--                                    .mini-profile-links h4{--}}
{{--                                        text-transform: uppercase;--}}
{{--                                        color: #02a7ca;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links ul{--}}
{{--                                        padding-left: 0;--}}
{{--                                        margin-top: 20px;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li{--}}
{{--                                        color: #333;--}}
{{--                                        background: rgba(0, 0, 0, .05);--}}
{{--                                        list-style-type: none;--}}
{{--                                        padding: 10px 15px;--}}
{{--                                        border-bottom: 1px solid #fff;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li:hover{--}}
{{--                                        background: #02a7ca;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li a{--}}
{{--                                        color:#333;--}}
{{--                                        font-size: 13px;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li:hover a{--}}
{{--                                        color: #fff;--}}
{{--                                        text-decoration: none;--}}
{{--                                    }--}}
{{--                                </style>--}}

{{--                                <div class="mini-profile-links">--}}
{{--                                    <h4>{{trans('main.custom.myprof') }}</h4>--}}
{{--                                    <ul>--}}
{{--                                        <li><a href="{{ route('create-trackers') }}">{{ trans('main.mini_nav.create_trackers') }}</a></li>--}}
{{--                                        <li><a href="{{ route('history-trackers') }}">{{ trans('main.mini_nav.history_trackers') }}</a></li>--}}
{{--                                        @if(Auth::check())--}}
{{--                                            @if(Auth::user()->hasRole('User'))--}}
{{--                                                <li><a href="{{ route('edit-profile') }}">{{ trans('main.mini_nav.edit_profile') }}</a></li>--}}
{{--                                            @endif--}}
{{--                                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ trans('main.mini_nav.log_out') }}</a></li>--}}
{{--                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">--}}
{{--                                                {{ csrf_field() }}--}}
{{--                                            </form>--}}
{{--                                        @endif--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                            </div>--}}

                        @else
                            <div class="edit-profile col-xs-12 col-sm-10 col-sm-offset-1">

                                <div class="page-header-block">
                                    {{-- <p style="text-align: left;">{{ trans('main.custom.profile1') }}</p>--}}
                                    <h3 class="fhd-title fhd-title_size fhd-title_blue my-5">{{ trans('main.custom.formprofile') }}</h3>
                                </div>

                                <form class="default-form" action="{{ route('update-profile') }}" method="post">
                                    {{ csrf_field() }}
                                    <input name="_method" type="hidden" value="PUT">
                                    <div class="edit-profile__panel">
                                        <div class="edit-profile__panel-heading">
                                            <h5>{{ trans('main.history.content.h5') }}</h5>
                                        </div>

                                        <div class="edit-profile__panel-body">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="inputUsername" class="input-title">{{trans('main.custom.fioorcomp') }}</label>
                                                    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputUsername" name="username" value="{{ old('username', $user->name) }}">
                                                    </div>
                                                    @if ($errors->has('username'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputEmail" class="input-title">{{trans('main.custom.mail') }}</label>
                                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputEmail" name="email" value="{{ old('email', $user->email) }}">
                                                    </div>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="inputNumber" class="input-title">{{trans('main.custom.number') }}</label>
                                                    <div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputNumber" name="number" value="{{ old('number', $user->number) }}">
                                                    </div>
                                                    @if ($errors->has('number'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('number') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="inputINN" class="input-title">{{trans('main.custom.inn') }}</label>
                                                    <div class="form-group  {{ $errors->has('inn') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputINN" name="inn" value="{{ old('inn', $clientInfo->inn) }}">
                                                    </div>
                                                    @if ($errors->has('inn'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('inn') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                            </div>
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <label for="inputPassportData" class="input-title">{{trans('main.custom.pasport') }}</label>
                                                    <div class="form-group {{ $errors->has('passport_data') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputPassportData" name="passport_data" value="{{ old('passport_data', $clientInfo->passport_data) }}">
                                                    </div>
                                                    @if ($errors->has('passport_data'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('passport_data') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputIndex" class="input-title">{{trans('main.custom.index') }}</label>
                                                    <div class="form-group {{ $errors->has('index') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputIndex" name="index" value="{{ old('index', $clientInfo->index) }}">
                                                    </div>
                                                    @if ($errors->has('index'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('index') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                            </div>
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <label for="inputCountry" class="input-title">{{trans('main.custom.country') }}</label>
                                                    <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputCountry" name="country" value="{{ old('country', $clientInfo->country) }}">
                                                    </div>
                                                    @if ($errors->has('country'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('country') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputCity" class="input-title">{{trans('main.custom.city') }}</label>
                                                    <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputCity" name="city" value="{{ old('city', $clientInfo->city) }}">
                                                    </div>
                                                    @if ($errors->has('city'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                            </div>
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <label for="inputAddress" class="input-title">{{trans('main.custom.adres') }}</label>
                                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                                        <input type="text" class="input__custom input__custom_bg" id="inputAddress" name="address" value="{{ old('address', $clientInfo->address) }}">
                                                    </div>
                                                    @if ($errors->has('address'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row edit-profile_border">

                                                <div class="col-sm-6">
                                                    <label for="inputPassword" class="input-title">{{trans('main.custom.newpas') }}</label>
                                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <input type="password" class="input__custom input__custom_bg" id="inputPassword" name="password">
                                                    </div>
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="inputRepeatPassword" class="input-title">{{trans('main.custom.acceptpas') }}</label>
                                                    <div class="form-group {{ $errors->has('repeat_password') ? ' has-error' : '' }}">
                                                        <input type="password" class="input__custom input__custom_bg" id="inputRepeatPassword" name="repeat_password">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-end mt-5">
                                        <button type="submit" class="btn-custom btn-custom_yellow-bg">{{trans('main.custom.updatepas') }}</button>
                                    </div>
                                </form>
                                <br>
                                <br>
                                <br>
                            </div>
{{--                            <div class="col-sm-3">--}}

{{--                                <style>--}}
{{--                                    .mini-profile-links h4{--}}
{{--                                        text-transform: uppercase;--}}
{{--                                        color: #02a7ca;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links ul{--}}
{{--                                        padding-left: 0;--}}
{{--                                        margin-top: 20px;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li{--}}
{{--                                        color: #333;--}}
{{--                                        background: rgba(0, 0, 0, .05);--}}
{{--                                        list-style-type: none;--}}
{{--                                        padding: 10px 15px;--}}
{{--                                        border-bottom: 1px solid #fff;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li:hover{--}}
{{--                                        background: #02a7ca;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li a{--}}
{{--                                        color:#333;--}}
{{--                                        font-size: 13px;--}}
{{--                                    }--}}

{{--                                    .mini-profile-links li:hover a{--}}
{{--                                        color: #fff;--}}
{{--                                        text-decoration: none;--}}
{{--                                    }--}}
{{--                                </style>--}}

                                {{--                                <div class="mini-profile-links">--}}
                                {{--                                    <h4>{{trans('main.custom.myprof') }}</h4>--}}
                                {{--                                    <ul>--}}
                                {{--                                        <li><a href="{{ route('create-trackers') }}">{{ trans('main.mini_nav.create_trackers') }}</a></li>--}}
                                {{--                                        <li><a href="{{ route('history-trackers') }}">{{ trans('main.mini_nav.history_trackers') }}</a></li>--}}
                                {{--                                        @if(Auth::check())--}}
                                {{--                                            @if(Auth::user()->hasRole('User'))--}}
                                {{--                                                <li><a href="{{ route('edit-profile') }}">{{ trans('main.mini_nav.edit_profile') }}</a></li>--}}
                                {{--                                            @endif--}}
                                {{--                                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ trans('main.mini_nav.log_out') }}</a></li>--}}
                                {{--                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">--}}
                                {{--                                                {{ csrf_field() }}--}}
                                {{--                                            </form>--}}
                                {{--                                        @endif--}}
                                {{--                                    </ul>--}}
                                {{--                                </div>--}}

{{--                            </div>--}}
                        @endif

                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection

<!--
// TODO connect google-address.js functionality to this page
-->
