<form id="search-tracker-form" class="search-tracker-form main-form py-5" method="get" action="{{ route('search-tracker') }}">
	<div class="row">
		<div class="col-xs-12 col-sm-5">
			<div class="form_group mb-2">
				{{--<input type="text" name="tracker" id="id_tracker" class="form_control" placeholder="{{ trans('main.trackers.label2') }}" value={{ old('id_tracker') }} >--}}
				<input type="text" name="id_tracker" id="tracker" class="form-control input__custom" placeholder="{{ trans('main.trackers.label2') }}" value="@if(isset($id_tracker) && $id_tracker != null){{ old('id_tracker', $id_tracker) }}@else{{ old('id_tracker') }}@endif">

				@if ($errors->has('id_tracker'))
					<span class="help-block">
				<strong>{{ trans('main.main_page.tracking_number') }}</strong>
			</span>
				@endif
			</div>
		</div>
		<div class="col-xs-12 col-sm-5 mb-2">
			<div class="form_group">
				<input type="text" name="number" id="inputNumber" class="form_control input__custom" placeholder="{{ trans('main.trackers.label1') }}" value="@if(isset($number) && $number != null){{ old('number', $number) }}@else{{ old('number') }}@endif">
				<span id="numberSender-valid-msg" class="hide phone-valid-msg">✓ Valid</span>
				<span id="numberSender-error-msg" class="hide phone-error-msg"></span>
				@if ($errors->has('number'))
					<span class="help-block">
				<strong>{{ trans('main.main_page.phone_number') }}</strong>
			</span>
				@endif
				@if (session('error2'))
					<span class="help-block">
				<strong>{{ trans ('main.custom.errorNoTrecking')  }}</strong>
			</span>
				@endif
			</div>
		</div>
		<div class="col-xs-12 col-sm-2">
			<button class="btn-custom btn-custom_yellow-bg" type="submit">{{ trans('main.main_page.track') }}</button>
		</div>
	</div>
</form>
