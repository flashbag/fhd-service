<?php

return [
	'view_tracker' => 'View tracker',
    'edit_tracker' => 'Edit tracker',
	'add_tracker' => 'Create tracker',
    'fill_form_lang' => 'Please fill the form in english language',
	'tracker_not_found' => 'Tracker not found',
	'tracker_history_not_found' => 'Tracker history not found',
	'tracker_required_fields' => 'Required fields',
	'rules_agreement' => 'Do you agree with the :link_start terms :link_end of transportation?',
	'print_options' => [
		'downloadPDFTitle' => 'Download PDF tracker',
		'downloadCleanPDFTitle' => 'Download clean PDF tracker',
		'settingsTitle' => 'PDF settings',
		'colorLabel' => 'Color',
		'colors' => [
			'default' => 'Default',
			'grey' => 'Grey',
			'blue' => 'Blue',
			'orange' => 'Orange',
		],
		'download' => 'Download',
		'show' => 'Show PDF',
		'browser_warning' => 'To view the PDF correctly, it is recommended to use the Google Chrome browser.',
	],
	'parts' => [
		'type_inventories' => [
			'1' => 'Box',
			'2' => 'Pallet',
			'3' => 'Envelope',
		],
		'prices' => [
			'help_block' => 'This data is entered by the warehouse operator of the warehouse of the recipient country. The checksum, amount of duty and currency are determined by the customs inspector.'
		],
		'countries' => [
			'country_departure' => 'Sender\'s country',
			'country_arrival' => 'Recipient\'s country',
			'by_iso_3166_2' => [
				'CN' => 'China',
				'UA' => 'Ukraine',
				'US' => 'United States',
			],
		],
		'additional' => [
			'title' => 'Additional information',
		],
		'warehouse' => [
			'country_departure_warehouse' => 'Warehouse',
			'country_arrival_warehouse' => 'Warehouse',
			'title'  => 'Warehouse',
			'select_option' => 'Choose warehouse'
		],
		'person_types' => [
			1 => 'Individual',
			2 => 'Legal Entity'
		],

        'status' => [
            '0' => 'Expected in warehouse',
            '10' => 'Delivered to the warehouse of the sender\'s country',
            '15' => 'In the process of preparing documents in the country of origin',
            '20' => 'In the process of customs clearance in the country of origin',
            '30' => 'Customs control in the country of the sender is passed, your package should be in the country of the recipient',
            '49' => 'On the way to the recipient\'s country',
            '50' => 'Departure arrived in the recipient country',
            '60' => 'Delivered to the warehouse of the recipient\'s country, preparations are under way for customs clearance.',
            '70' => 'In the process of customs clearance in the recipient country',
            '80' => 'Sent to internal operator for targeted delivery to the recipient',
            '99' => 'Delivered',
        ],

        'type_container' => [
            '1' => '20ft standard container',
            '2' => '40ft standard container',
            '3' => '40ft high cube container',
            '4' => '20ft Open Top container',
            '5' => '40ft Open Top container',
        ],

		'transport' => [
			'select_type_transport' => 'Select transport type',
			'type_transport' => 'Transport type',
			'type_transport_air' => 'By air',
			'type_transport_sea' => 'By sea',
			'type_transport_road' => 'By road',
			'type_transport_train' => 'By train',
		],

        'service_delivery' => [
            'american_express' => 'American Express',
            'another' => 'Another',
            'dhl' => 'DHL',
            'fedex' => 'FEDEX',
            'novaposhta' => 'Nova poshta',
            'ups' => 'UPS',
            'usps' => 'USPS',
        ],

		'shipping' => [
			'unit_length' => [
				'cm' => 'cm',
				'mm' => 'mm',
				'in' => 'inch',
				'ft' => 'foot'
			],
			'unit_weight' => [
				'kg' => 'kg',
				'lb' => 'lb'
			]
		],
		'bill_duties' => [
			'title' => 'Who pays customs costs',
			'type_sender' => 'Sender',
			'type_recipient' => 'Recipient',
			'type_third_party' => 'Third party email',
			'type_fhd_account' => 'FHD Account',
		],
		'bill_transportations' => [
			'title' => 'Who pays for transportation costs',
			'type_sender' => 'Sender',
			'type_recipient' => 'Recipient',
			'type_third_party' => 'Third party',
			'type_customer' => 'Customer'
		],
		'freight_services' => [
			'title' => 'Cargo delivery',
			'type_priority_freight' => 'Priority Freight',
			'type_economy_freight' => 'Economy Freight',
			'type_booking_number' => 'Booking Number'
		],
		'special_handlings' => [
			'title' => 'Special handlings',
			'type_hold_at_fhd_warehouse' => 'Hold at FHD warehouse',
			'type_saturday_delivery' => 'Saturday Delivery',
			'type_address_delivery' => 'Address Delivery'
		],
		'receptacles' => [
            'cargos_help_block_box' => 'If your departure contains several items - describe each of these items.',
            'cargos_help_block_pallet' => 'If your pallet contains several items - describe each of these items.',
            'cargos_help_block_envelope' => 'If your envelope contains several items - describe each of these items.',
			'sizes_help_block' => 'Indicate the exact or approximate values of the dimensions of W x L x and the weight of the item. Accurate data may be corrected by the warehouse worker.',
			'weight_help_block' => 'For the calculation of shipping costs a higher weight value is used.',
			'title_main' => 'Position Information',
			'departure' => 'Departure',
			'add_position' => 'Add position',
			'position' => 'Position',
			'type_inventory' => 'Package type',
			'unit_of_length' => 'Unit of length',
			'unit_of_weight' => 'Unit of weight',
			'width' => 'Width',
			'length' => 'Length',
			'height' => 'Height',
			'actual_weight' => 'Actual weight',
			'volume_weight' => 'Volume weight',
			'assessed_price' => 'Assessed Price',
			'description_cargos' => 'Description of cargos in position',
			'cargos' => [
				//'is_used_false' => 'New',
				//'is_used_true' => 'Used',
				'quantity' => 'Quantity',
				'description' => 'Cargo name',
				'chars_left' => 'Chars left'
			]
		],
		'address_components' => [
			'city' => 'City',
			'region' => 'Region/State',
			'country' => 'Country',
			'address' => 'Street Address',
			'address_help_block' => 'format: Street, house #, flat',
			'index' => 'Postcode',
		],
		'documents' => [
			'title_sender' => 'Sender\'s accompanying documents',
			'title_recipient' => 'Recipient\'s accompanying documents',
			'title_receptacle' => 'Cargos accompanying documents',
			'view_document' => 'View document',
			'file_not_selected' => 'File not selected',
			'select_file' => 'Select file',
		],
		'sender' => [
			'title' => 'Sender',
			'number' => 'Phone',
			'email' => 'Email',
			'person_type' => 'Type of person',
			'full_name' => 'Full Name',
			'number_account' => 'Account number',
			'inn' => 'Tax number',
			'passport_data' => 'Passport data',
			'number_service' => 'Service number',
			'service_delivery' => 'Delivery service',
			'company_name' => 'Company name',
			'number_vat' => 'Number VAT',
			'service_delivery_other' => 'Other delivery service',
		],
		// I know that fields of sender and recipient almost the same
		// But for now I separated them in case when labels will be different in future
		'recipient' => [
			'title' => 'Recipient',
			'number' => 'Phone',
			'email' => 'Email',
			'person_type' => 'Type of person',
			'full_name' => 'Full Name',
			'number_account' => 'Account number',
			'inn' => 'SSN',
			'passport_data' => 'Passport data',
			'number_service' => 'Service number',
			'service_delivery' => 'Delivery service',
			'company_name' => 'Company name',
			'number_vat' => 'Number VAT',
			'service_delivery_other' => 'Other delivery service ',
			'custom_id' => 'Tax number',
			'contact_name' => 'Contact name'
		],
        'warehouses' => [
            'UA_1'=> 'Main warehouse in Ukraine',
            'US_1'=> 'Main warehouse in the USA',
            'CN_1'=> 'Main warehouse in China',
        ],
	]
];
