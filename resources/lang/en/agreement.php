<?php

return [
    'label11' => 'Contract for individuals person',
    'label21' => 'Contract for legal entities',
    'label12' => 'PUBLIC CONTRACT',
    'label23' => 'or the delivery of international express units services',
    'label13' => 'for the delivery of international express units services edition No 1.1',
    'button1' => 'DOWNLOAD IN RUSSIAN',
    'button2' => 'DOWNLOAD IN UKRAINIAN',
    'button3' => 'DOWNLOAD IN ENGLISH',
    'fiz' => '<style type="text/css">ol{margin:0;padding:0}table td,table th{padding:0}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:10pt;font-family:"Arial";font-style:normal}.c3{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:center}.c0{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c7{background-color:#ffffff;max-width:468pt;padding:72pt 72pt 72pt 72pt}.c5{color:inherit;text-decoration:inherit}.c4{background-color:#ffffff;font-size:10pt}.c6{font-size:10pt}.c2{height:11pt}.title{padding-top:0pt;color:#000000;font-size:26pt;padding-bottom:3pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:0pt;color:#666666;font-size:15pt;padding-bottom:16pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Arial"}p{margin:0;color:#000000;font-size:11pt;font-family:"Arial"}h1{padding-top:20pt;color:#000000;font-size:20pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-size:16pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:16pt;color:#434343;font-size:14pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:14pt;color:#666666;font-size:12pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}</style>
   </head>
   <div>
      <p class="c3"><span class="c1">PUBLIC CONTRACT</span></p>
      <p class="c3"><span class="c1">for the delivery of international express units services</span></p>
      <p class="c3"><span class="c1">edition № 1.1</span></p>
      <p class="c3"><span class="c1">Chornomorsk &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></p>
      <p class="c3"><span class="c1">June 13, 2018</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c6">LIMITED LIABILITY COMPANY "</span><span class="c4">FAST HOME DELIVERY SERVICE</span><span class="c1">" (hereinafter referred to as the "Contractor") offers an unlimited number of individual persons who have sufficient rights and powers (hereinafter referred to as the "Customer") to receive services for the international express delivery of units in accordance with the provisions of this Public Contract (hereinafter referred to as the Contract).</span></p>
      <p class="c0"><span class="c1">This Contract is in its legal nature mixed and contains elements of a public offer in accordance with the provisions of art. art. 638, 641 of the Civil Code of Ukraine (hereinafter referred to as the CC of Ukraine), freight forwarding services contract, contract of carriage, contract for provision of services and agency contract.</span></p>
      <p class="c0"><span class="c1">The Contract is concluded by unconditionally and fully joining the Customer to this Contract and accepting all essential terms of the Contract and is legally valid in accordance with the provisions of art. 633 of the CC of Ukraine.</span></p>
      <p class="c0"><span class="c1">The Contract is considered concluded from the moment of drawing up of the international shipment waybill. Drawing up of each subsequent international shipment 
         waybill in each individual case is the conclusion of a new Contract. The unconditional and complete acceptance (acceptance - in the meaning of part 2 of article 638 of the CC of Ukraine) of the terms of the Contract by the Customer is that the Customer committed the acts aimed at receiving services, namely: the transfer by the Customer or third parties acting in the interests of the Customer, to the Contractor of the international shipment waybill for the provision of services provided for the Contract, regardless of the presence / absence of the signature of the Sender on the copy of the international shipment waybill of the Contractor.</span>
      </p>
      <p class="c0"><span class="c1">The Contract is placed on the website http://fhd.com.ua/, in the free access and in a way that provides access to the content of this Contract of each person applying to the Contractor. To the relations stipulated by this Contract, the Parties also apply the Terms of Services, which are public and located on the website http://fhd.com.ua/.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">1. Subject of the Contract</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">1.1. The Contractor undertakes to provide to the Customer services of the delivery of international express units (hereinafter referred to as "IEU"), which are defined in item 1.2. of the Contract, and the Customer undertakes to receive the rendered services and to pay in time for services rendered by the Contractor.</span></p>
      <p class="c0"><span class="c1">1.2. In accordance with this Contract, the Contractor provides the following services:</span></p>
      <p class="c0"><span class="c1">- organization of transportation of the IEU by all kinds of transport, namely: acceptance and issuance of IEU; acceptance of information from the Customer and supporting IEU documents; registration of transport documents; organization of transportation of IEU from abroad and abroad, organization of transportation of IEU on the territory of Ukraine;</span></p>
      <p class="c0"><span class="c1">- customs clearance of IEU, namely: registration and placement of IEU at the central sorting station (hereinafter - CSS); declaration and providing IEU for customs control; payment of the corresponding customs duties, fees and taxes;</span></p>
      <p class="c0"><span class="c1">- storage of IEU at CSS, warehousing operations with IEU (packaging, unpacking, accumulation, etc.);</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">- other services provided by the Terms of Services.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">1.3. The final volume of a particular service is determined in the international shipment waybill (hereinafter - ISW).</span></p>
      <p class="c0"><span class="c1">1.4. IEU under this Contract shall be understood as properly packed international parcel with documents or goods enclosures that are accepted, processed, transported by any mode of transport under an international transport document for the purpose of delivery to the recipient in an expedited way within a specified period.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">1.5. By transfer of the IEU to Contractor, the Customer confirms that he is familiar with and agrees to this Contract and to the Terms of Services located on the website http://fhd.com.ua/, and undertakes to comply with them.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">2. Rights and obligations of the Parties</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">2.1. Contractor\'s Rights:</span></p>
      <p class="c0"><span class="c1">2.1.1. Demand from the Customer documents necessary for the proper fulfillment of the provisions in art. 1. of Contract.</span></p>
      <p class="c0"><span class="c1">2.1.2. Require Customer to pay for services provided in art. 1. of Contract.</span></p>
      <p class="c0"><span class="c1">2.1.3. Carry out re-weighing 
         and measuring of the IEU in order to confirm the accuracy of the calculations stated by the Customer weight. The parties agreed that the indicators determined during the re-weighing and measurement of the IEU by the Contractor at the central sorting station (address: Ukraine, Chornomorsk, Promyslova Str., 1), are the basis for carrying out the recalculation of the cost of the rendered services.</span>
      </p>
      <p class="c0"><span class="c6">2.1.4. To suspend fulfillment of its obligations under this Contract and to hold the IEU / ICU in case of non-fulfillment by the Customer of its obligations stipulated by this Contract, until they are </span><span class="c6">fulfilled fully</span><span class="c1">.</span></p>
      <p class="c0"><span class="c1">2.1.5. To change tariffs, placed on the website http://fhd.com.ua/ before acceptance of IEU.</span></p>
      <p class="c0"><span class="c1">2.2. Contractor\'s duties:</span></p>
      <p class="c0"><span class="c1">2.2.1. to provide services according to art. 1. of Contract;</span></p>
      <p class="c0"><span class="c1">2.2.2. to file an non-conformance Act in case of revealing the facts of damage to the IEU, its packaging or marking or its loss;</span></p>
      <p class="c0"><span class="c1">2.2.3. at the request of the Customer inform him about the progress of the provision of services;</span></p>
      <p class="c0"><span class="c1">2.2.4. the Contractor is not obliged to check the content of the IEU packed by the Customer and the conformity of the packaging with the characteristics of the IEU, the requirements of the applicable&nbsp;legislation and state standards.</span></p>
      <p class="c0"><span class="c1">2.3. Customer\'s Rights:</span></p>
      <p class="c0"><span class="c1">2.3.1. to receive services in accordance with the terms of the Contract;</span></p>
      <p class="c0"><span class="c1">2.3.2. to inventory own IEU, if necessary;</span></p>
      <p class="c0"><span class="c1">2.3.3. to demand from the Contractor information on the rendered services provided by the Contract.</span></p>
      <p class="c0"><span class="c1">2.4. Obligations of the Customer:</span></p>
      <p class="c0"><span class="c1">2.4.1. before the time of transfer of the IEU to the Contractor, familiarize the Terms of Services;</span></p>
      <p class="c0"><span class="c1">2.4.2. to fill out and sign the ISW correctly and legibly, informing the Contractor about the IEU (its contents);</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">2.4.3. to provide the Contractor with all supporting documents and information necessary for the proper provision of the services by the Contractor, and upon request of the Contractor to provide the IEU content for inspection;</span></p>
      <p class="c0"><span class="c1">2.4.4. in case of receiving an IEU in improper condition (damage, shortage, etc.), fix its state in the relevant Act, with obligatory participation of representatives of the Customer and the Contractor. An Act of delivery-acceptance (in which fixed the damage of the IEU, shortage, etc.) is filed during the transfer of the IEU to the Customer;</span></p>
      <p class="c0"><span class="c1">2.4.5. to pack (and make necessary marking) the IEU properly for its preservation during transportation and loading and unloading services;</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">2.4.6. inform the Contractor about the conditions of warehousing and storage of IEU at CSS;</span></p>
      <p class="c0"><span class="c1">2.4.7. to pay the Contractor\'s services in a timely manner;</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">3. Cost of services and payment procedure.</span></p>
      <p class="c0"><span class="c1">Delivery-acceptance of the provided services.</span></p>
      <p class="c0 c2"><span 
         class="c1"></span></p>
      <p class="c0"><span class="c1">3.1. The total value of the Contract is determined by the ISW and is finally calculated at receiving the IEU by the recipient.</span></p>
      <p class="c0"><span class="c1">3.2. Payment of the service cost is made by the Customer in the Ukrainian national currency (hryvnia). The Customer carries out payment in advance for the Contractor’ services.</span></p>
      <p class="c0"><span class="c1">3.3. The cost of services is defined in Addendum No. 1 to the Contract.</span></p>
      <p class="c0"><span class="c1">3.4. Revision of tariffs is carried out in accordance with sub-item 2.1.5 of the Contract.</span></p>
      <p class="c0"><span class="c1">3.5. Transfer by the Customer of the IEU to the Contractor for the provision of services and the signing of the ISW is a confirmation that the Customer agrees with the Contractor\'s rates.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">4. Responsibility of the Parties</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">4.1. The Contractor shall be responsible for safety of the Customer\'s IEU, as well as for mechanical and other damage that arose because of improper guilty conduct of the Contractor.</span></p>
      <p class="c0"><span class="c1">4.2. The Contractor is not responsible for the quality, completeness and number of IEUs accepted at the CSS if the packaging is broken or damaged.</span></p>
      <p class="c0"><span class="c1">4.3. The Customer transfers for the customs processing and storage of IEU, the import / export of which is not prohibited by the legislation of Ukraine.</span></p>
      <p class="c0"><span class="c1">4.4. For delayed payment of the rendered services, the Customer pays the Contractor a penalty in the amount of double the discount rate of the National Bank of Ukraine from the overdue amount for each day of delay and 20% per annum from the amount of the debt, including compensation to the Contractor caused by losses and compensates for lost profits.</span></p>
      <p class="c0"><span class="c1">4.5. The execution of the Customer\'s obligations to pay for services rendered by the Contractor is secured by a pledge - the MEG transmitted by the Customer. In case of non-fulfillment by the Customer of its obligations, the Contractor acquires the right of recourse against the subject of a pledge by his own decision. </span></p>
      <p class="c0"><span class="c1">4.6. Any of the Parties shall not be liable for failure to comply with the terms of this Contract due to force majeure circumstances. In the event of force majeure, through which the Parties cannot complete their obligations, each Party undertakes to inform the other Party within 5 days and confirm the above circumstances in accordance with the procedure established by the current legislation.</span></p>
      <p class="c0"><span class="c1">4.7. The Customer is responsible for the authenticity of the data in the documents, compliance with their content, nature, quality and quantity of IEU provided to them for customs clearance, as well as compliance of the packaging with the characteristics of the IEU, the requirements of current Ukrainian legislation and the requirements of the legislation of the country of destination (transit).</span></p>
      <p class="c0"><span class="c1">4.8. The Contractor is liable in case of poor performance of his obligations to the Customer within the limits established by the current legislation in Ukraine and this Contract.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">4.9. The Contractor is released from liability for delay in the delivery of the IEU, if the delay is not due to his fault, in particular, but not exclusively, if such delay is caused by the untimely provision by the Customer of the documents and information necessary for the execution of customs procedures and conditions of the Contract; untimely implementation of customs procedures due to customs authorities, etc.</span></p>
      <p class="c0"><span class="c1">4.10. The Customer is informed about the 
         responsibility for violation of the Customs Code of Ukraine.</span>
      </p>
      <p class="c0"><span class="c1">4.11. The Contractor shall not be liable for the obligations of the Customer, which the last one directly performed before the customs authority.</span></p>
      <p class="c0"><span class="c1">4.12. The codes of the UKTZED for goods subject to customs clearance are determined according to the explanations and documents provided by the Customer. However, decisions of the bodies of the fiscal service on the classification of goods for customs purposes are mandatory, according to art. 69 Custom Code of Ukraine.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">5. Term of the Contract</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">5.1. This Contract is deemed to be concluded after the signing of the ISW, or from the moment when the Customer transfers IEU to the Contractor for the provision of the services under this Contract and remains in force to the transfer of the IEU to the recipient. In the part of settlements between the Parties, the Contract remains in force until all obligations under this agreement have been fully completed. The termination of this Contract or its rescission does not make Parties without prejudice of responsibility for its violation that occurred during the term of this Contract.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">6. Procedure for resolving disputes</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">6.1. Discrepancies arising from the implementation of this Contract shall be resolved through negotiations between the Parties.</span></p>
      <p class="c0"><span class="c1">6.2. In case of the impossibility of resolving disContracts by negotiation and pre-trial settlement of the dispute, disputes between the Parties shall be resolved in accordance with the current legislation of Ukraine.</span></p>
      <p class="c0"><span class="c1">6.3. The IEU inspection acts, commercial acts, expert opinions and other documents containing information on the nature and causes of damage, damage, loss, lack of IEU content, which were drawn up without the participation of the Authorized Representative of the Contractor, are invalid and are not taken into consideration when considering the claim.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">7. Other terms</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">7.1. After the conclusion of this Contract, all prior negotiations, correspondence, minutes of intentions and any other oral and written agreements of the Parties on matters that are in any way related to this Contract shall become legally invalid.</span></p>
      <p class="c0"><span class="c1">7.2. The Contractor may unilaterally change the Terms of this Contract by placing them at the website http://fhd.com.ua/.</span></p>
      <p class="c0"><span class="c1">7.3. Neither of the Parties shall have the right to transfer its rights under this Contract, namely: the assignment of the right to claim and / or the transfer of debt under this Contract to third parties without the written consent of the other Party.</span></p>
      <p class="c0"><span class="c1">7.4. Nullity (invalidation) of any of the terms and conditions of this Contract and / or its annexes shall not be grounds for nullity (invalidation) of other provisions (terms) of this Contract and / or the Contract as a whole.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">7.5. The term of storage of IEU in the CSS under customs control, can not exceed 30 days.</span></p>
      <p class="c0"><span class="c1">7.6. The acceptance and issuance of the IEU is carried out from 9:00 to 18:00 every day, except for Saturday, Sunday, holidays and non-working days stipulated by the current legislation in Ukraine.</span></p>
      <p class="c0"><span class="c1">7.7. The Parties undertake not to disclose the confidential information of the other Party, 
         received in connection with the fulfillment of obligations under this Contract.</span>
      </p>
      <p class="c0"><span class="c1">7.8. Governed by existing Ukrainian legislation regulating legal relations in the field of receiving, collecting, storing, processing and protecting personal data, the Customer agrees to processing personal data for the implementation of this Contract. This consent is granted for the performance of any actions concerning legally obtained personal data of the Customer, which are necessary or desirable for the achievement of the Contractual purposes, namely: collection, systematization, accumulation, storage, refinement (renewal, change), use within the limits necessary for the above-mentioned Contractual purposes, distribution (including transfer) within the limits necessary for the above Contractual purposes, data depersonalization, destruction, and also carrying out of any other actions with the received personal data within the limits necessary for the above-mentioned purposes and taking into account the current legislation of Ukraine.</span></p>
      <p class="c0"><span class="c1">7.9. In all other things not provided for by this Contract, the Parties shall be governed by the current legislation in Ukraine.</span></p>
      <p class="c0"><span class="c1">7.10 The Contractor confirms that he is a payer of the corporate income tax on the general terms and with the VAT payer.</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">8. Contact details</span></p>
      <p class="c0 c2"><span class="c1"></span></p>
      <p class="c0"><span class="c1">Contractor</span></p>
      <p class="c0"><span class="c1">Website http://fhd.com.ua/</span></p>
      <p class="c0"><span class="c6">Phone</span></p>
      </div>',
    'jur' => 'or the delivery of international express units services'
];