<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'hello' => 'Good afternoon',
    'thanks' => 'Thank you for registering.',
    'ForVerifyGoTo' => 'To verify your email, go to the',
    'link' => 'link.',
    'message1' => 'By registering with our service, you accept the terms and conditions of our company.
                    Enjoy your work with FHD Service.',
    'ChangeEmail' => 'You have just created an e-mail change request. To activate a new e-mail, go to',
];
