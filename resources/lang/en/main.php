<?php

return [
	'ukraine' => 'Ukraine',
    'custom' => [
        'new_ticket' => 'Create ticket',
        'ticket' => 'Go to ticket',
        'map' => 'Map',
        'settings' => 'Settings',
        'message' => 'Message',
        'view' => 'View',
        'send_country' => 'Sender\'s country',
        'send_city' => 'Sender\'s city',
        'send_name' => 'Sender\'s name',
        'rec_country' => 'Recipient\'s country',
        'rec_city' => 'Recipient\'s city',
        'rec_name' => '	Recipient\'s name',
        'resPas' => 'Password',
        'resPasconf' => 'Confirm password',
        'resPasres' => 'Reset password',
        'inform' => 'Information',
        'error' => 'Error',
        'fioorcomp' => 'Full name or company name',
        'mail' => 'Email',
        'number' => 'Phone number',
        'inn' => 'INN',
        'pasport' => 'Passport data (series, number)',
        'index' => 'Index',
        'country' => 'Country',
        'city' => 'City',
        'adres' => 'Address',
        'newpas' => 'New password',
        'acceptpas' => 'Accept new password',
        'updatepas' => 'Update profile',
        'myprof' => 'My profile',
        'profile1' => 'Profile',
        'formprofile' => 'Data of your profile',
        'errorTelTrecking' => 'Phone too short',
        'errorNakladTrecking' => 'Invoice number is too short',
        'errorNoTrecking' => 'No such waybill',
        'acceptZak' => 'Confirmation of an order',
        'edit' => 'Edit',
        'downloadPDF' => 'Download PDF',
        'delete' => 'Delete',
        'reset' => 'Reset',
        'show' => 'Show',
        'date' => 'Date',
        '№invoice' => '№ invoice',
        'status' => 'Status',
        'toReceive' => 'To receive',
        'toDeparture' => 'To departure',
        'hideMap' => 'Hide map',
        'purchaseHistory' => 'Tracker history',
        'showMap' => 'Show map',
        'archive' => 'Archive',
        'draft' => 'Drafts',
        'trash' => 'Trash',
        'TagFriend' => 'Tag a friend',
        'profileSettings' => 'Profile settings',
        'paymentCards' => 'Payment cards',
        'item' => 'Item',
        'deliveryAddress' => 'Delivery address',
        'recipientContacts' => 'Recipient contacts',
        'transportSticker' => 'Transport sticker',
        'createTracker' => 'Create tracker',
        'actualOrders' => 'Actual orders',
        'acceptText' => 'Please note that when you make an application on the site, your post should
                    meet the following criteria:
                    Weight of departure must not exceed 50 kg.
                    Overall dimensions (width, height, length) are not more than 1 m.
                    In case your cargo exceeds the preset parameters, please contact one of the following:
                    our offices. You can also contact us by phone (-- //phone number//-- ) and
                    to coordinate the dispatch with our manager.',
        'IacceptZak' => 'I accept',
        'rulesAnd' => ' Terms & Conditions',
        'rulesCancel' => 'Cancel',
        'rulesAccept' => 'Аccept',
    ],
    'mini_nav' => [
        'admin_panel' => 'Admin panel',
        'create_trackers' => 'Create tracker',
        'create_news' => 'Add news',
        'history_trackers' => 'History of orders',
        'edit_profile' => 'Edit profile',
        'login' => 'Login and Registration',
        'log_out' => 'Logout',
        'log_in' => 'Log In',
        'custom' => 'Custom',
        'all_trackers' => 'Trackers',
        'trackers' => 'Trackers',
		'tickets' => 'Tickets',
        'account' => 'Account',
        'menu' => 'Site menu',
        'address-warehouse' => 'Addresses of our warehouses'
    ],
    'links' => [
        'home' => 'Home',
        'tracker' => 'Tracking',
        'create_tracker' => 'Add tracking',
        'documents' => 'Documents',
        'news' => 'News',
        'about_us' => 'About us',
        'contacts' => 'Contacts',
        'rules' => 'Terms & Conditions'
    ],
    'footer' => [
        'title_news' => 'Our news',
        'title_feed' => 'Our ribbon',
        'title_nav' => 'Navigation',
    ],
    'we_top' => [
        'h4' => 'WE are BETTER',
        'h3' => 'FHD In NUMBERS',
        'description' => 'We aim to set long-term, reliable relationships with all Clients, regardless of volume, industry, location and other moments.',
        'progress_block' => [
            'label1' => 'Export',
            'label2' => 'Import',
            'label3' => 'Delivery all over the world',
            'label4' => 'Reliable watching',
        ]
    ],
    'create_tracker_block' => [
        'h5' => 'Do you want to create a tracker?',
        'h3' => 'Try to create on-line request!',
        'link' => 'Create a tracker'
    ],
    'stats_block' => [
        'h4' => 'OUR VALUES',
        'h3' => 'SKILLS And EXAMINATION',
        'description' => 'FHD successfully works at the market, we help to develop to business for hundreds of clients from the different spheres of activity. We does not decide on attained, constantly extending the spectrum of services and improving quality of work.',
        'stats_counters' => [
            'counter1' => [
                'h4' => 'IT IS DELIVERED PACKAGES'
            ],
            'counter2' => [
                'h4' => 'A KILOMETRE IS APPROXIMATELY ON YEAR'
            ],
            'counter3' => [
                'h4' => 'TONS OF COMMODITIES'
            ],
            'counter4' => [
                'h4' => 'SATISFIED CLIENTS'
            ],
        ]
    ],
    'our_customers_block' => [
        'h4' => 'THAT is TALKED',
        'h3' => 'OUR CLIENTS',
        'testimonials' => [
            'testimonial_desc1' => 'FHD gives to us the complete complex of services at transportation in the containers of our loads from China to Ukraine.',
            'testimonial_desc2' => 'In times of collaboration we repeatedly made sure in professionalism and competence of FHD.',
        ]
    ],
    'our_services_block' => [
        'h4' => 'OUR SERVICES',
        'h3' => 'THAT WE CAN',
        'service_block1' => [
            'h4' => 'Service and professionalism',
            'p' => 'Due to large experience we pick up optimal routes, and similarly we give the most optimal decisions to our clients.'
        ],
        'service_block2' => [
            'h4' => 'Permanent watching of load',
            'p' => 'We use the authorial program that allows to watch motion of container real-time from our official web-site.'
        ],
        'service_block3' => [
            'h4' => 'Guarantees of maintenance of load',
            'p' => 'For all the time of work of work of company are minimum reclamations, there is not unfulfilled undertake. Our company bears the complete responsibility for maintenance of load.'
        ],
        'service_block4' => [
            'h4' => 'Absence of the hidden payments',
            'p' => 'We wire for sound a maximally honest and transparent cost at once, while many competitors often dumped on a freight, and then earn services on hidden.'
        ]
    ],
    'after_hero_block' => [
        'col_1' => [
            'h4' => 'CALL-CENTER',
            'p1' => 'Free ring',
        ],
        'col_2' => [
            'h4' => 'WE WORK',
            'p1' => 'Mon – Fri 10:00 am – 07:00 pm',
            'p2' => 'Sat 11:00 am – 05:00 pm',
        ],
        'col_3' => [
            'h4' => 'OUR ADDRESS',
            'p1' => 'Chornomorsk, Promyslova 1',
            'p2' => '',
        ],
    ],
    'our_services_block_detail' => [
        'h4' => 'OUR SERVICES',
        'h3' => 'BASIC VALUES',
        'service_block1' => [
            'h4' => 'TIME',
            'p' => 'We save Your time, care of Your nerves and experiencing.'
        ],
        'service_block2' => [
            'h4' => 'COMPOSITION',
            'p' => 'We will provide temporal storage in port on arrival of load.'
        ],
        'service_block3' => [
            'h4' => 'TRANSPORTATION',
            'p' => 'On arrival I load in Client receives message. At a desire, delivery comes true to the "door" of Customer.'
        ],
        'service_block4' => [
            'h4' => 'DEFENCE',
            'p' => 'We use the authorial program Pulse, that allows to watch motion of container real-time.'
        ],
        'service_block5' => [
            'h4' => 'VERIFICATION of YOUR LOAD',
            'p' => 'We will check Your load on demand on accordance to the declared parameters of volume and weight. Verification of all necessary documentation.'
        ]
    ],
    'home' => [
        'carousel' => [
            'slide_1' => [
                'p' => 'Do you have a waybill already?',
                'h1' => 'Enter your tracking',
                'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
                'link2' => 'About us',
                'form' => [
                    'h4' => 'To obtain information',
                    'span' => '* enter the number of the information superimposed for a revision',
                    'label1' => 'Phone number',
                    'label2' => 'Tracker number',
                    'button' => 'To find'
                ]
            ],
            'slide_2' => [
                'p' => 'For your parcels',
                'h1' => 'The BEST TRANSPORT',
                'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
                'link2' => 'About us'
            ],
            'slide_3' => [
                'p' => 'Send whatever',
                'h1' => 'DELIVERY',
                'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
                'link2' => 'About us'
            ]
        ]
    ],


    'contacts' => [
        'p' => 'Feel free to contact with us',
        'h1' => 'CONTACTS of COMPANY',
        'h5' => 'Fill a form below in an order to set any Your question to us.',
        'link2' => 'About us',
        'content' => [
            'p1' => 'Write to us',
            'h3' => 'CONTACT With US At any time',
            'label1' => 'Your name(necessarily)',
            'label2' => 'Your Email(necessarily)',
            'label3' => 'Theme',
            'label4' => 'Your report',
            'submit' => 'send'
        ],
        'our_address' => 'Our warehouses',
        'country' => 'Country',
        'region' => 'Region',
        'state' => 'State',
        'city' => 'City',
        'street' => 'Street',
        'home_number' => 'House number',
        'index' => 'Postcode',
        'telephone' => 'Phone number',
        'provinces' => 'Province',

        'ukraine_country' => 'Ukraine',
        'ukraine_region' => 'Odesa',
        'ukraine_city' => 'Chornomorsk',
        'ukraine_street' => 'Promyslova, 1',
        'ukraine_postcode' => '68001',
        'ukraine_phone' => '+38 0994408001',

        'usa_country' => 'USA',
        'usa_region' => 'New Jersey',
        'usa_city' => 'New York',
        'usa_street' => 'Harbor Drive, 84',
        'usa_postcode' => '07305',
        'usa_phone' => '+1 9177553122',

        'china_country' => 'China',
        'china_region' => 'Guangdong',
        'china_city' => 'Guangzhou',
        'china_street' => 'Jiahewanggang Changshui Industrial Zone 5/6',
        'china_postcode' => '510000',
        'china_phone' => '+86 18620883781',

        'contact_us' => 'Contact us',
        'name' => 'Your name',
        'email' => 'Your e-mail',
        'subject' => 'Subject',
        'message' => 'Your message',
        'send' => 'Send',
        'comment1' => 'Minimum number of characters allowed',
        'comment2' => 'Text length now',
        'specify_address' => 'HOW TO SPECIFY ADDRESS',
        'suite' => 'Suite',
    ],
    'about_us' => [
        'p' => 'FHD',
        'h1' => 'About OUR COMPANY',
        'h5' => 'We aim to set long-term, reliable relationships with all Clients, regardless of volume, industry, location and other moments.',
        'link2' => 'About us'
    ],
    'faq' => [
        'p' => 'FAQ',
        'h1' => 'A QUESTION / is ANSWER',
        'h5' => 'On the page of FAQs you can get answers for possible questions.',
        'link2' => 'About us',
        'content' => [
            'p' => 'Answers',
            'h3' => 'Questions',
            'panel1' => [
                'a' => 'How do we work?',
                'desc' => 'Text'
            ],
            'panel2' => [
                'a' => 'What services do we offer?',
                'desc' => 'Text'
            ],
            'panel3' => [
                'a' => 'What transport vehicles do we have?',
                'desc' => 'Text'
            ],
            'panel4' => [
                'a' => 'As far as are we reliable?',
                'desc' => 'Text'
            ],
            'panel5' => [
                'a' => 'How are we obligated?',
                'desc' => 'Text'
            ]
        ]
    ],
    'rules' => [
        'p' => 'Terms of use',
        'h1' => 'Terms & Conditions',
        'h5' => 'On the FAQs page you can get answers to possible questions.',
        'link2' => 'About us',
        'content' => [
            'p' => 'Terms & Conditions',
            'h3' => 'Terms of use',
            'panel1' => [
                'a' => 'Как мы работаем?',
                'desc' => 'Текст'
            ],
            'panel2' => [
                'a' => 'Какие услуги мы предлагаем?',
                'desc' => 'Текст'
            ],
            'panel3' => [
                'a' => 'Какие у нас транспортные средства?',
                'desc' => 'Текст'
            ],
            'panel4' => [
                'a' => 'Насколько мы надежны?',
                'desc' => 'Текст'
            ],
            'panel5' => [
                'a' => 'Как мы обязуемся?',
                'desc' => 'Текст'
            ]
        ]
    ],
    'news' => [
        'p' => 'Do you have a waybill already?',
        'h1' => 'OUR NEWS',
        'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
        'link2' => 'About us',
        'inform' => 'Information',
        'inform_message' => 'There are not news!'
    ],
    'documents' => [
        'p' => 'Do you have a waybill already?',
        'h1' => 'DOCUMENTS',
        'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
        'link2' => 'About us',
        'content' => [
            'p' => 'Documents',
            'h3' => 'For custom registration of export loads',
            'h4_1' => 'Legal entities',
            'panel1_1' => [
                'a' => 'Loads by a cost less than 150 EUR',
                'desc' => 'List of shipping documents(TSD) for sending of load service of Express(15N) or Economy Express(48N) for East Terminal of the Kyiv custom of WMO №2 UKRINFORM:
Invoice of FHD(pass 4 signed copies courier, you leave one for yourself).
Original of invoice(4 bilingual copies) with the signatures of declarant and wet printing of Your company. To load a form / to load the standard of filling.
Original of statement on a custom 15N or 48N Kyiv (a 1 copy by the Ukrainian or Russian languages after the signatures of declarant and wet printing of Your company. To load a form / to Load the standard of filling.
Original of confirmation of cost of commodity in hryvnyas(for the Ukrainian custom) : for legal entities is a calculation, expense/pl(expense) invoice. Or the imported documents with the marks(printing) of the Ukrainian custom in currency(USD or EUR).
Copy of operating card of accreditation of Your company with the wet printing of Your company to every page.
Other additional documents are in the original(if necessary) of fito \ vet \ sanitary certificates, permissive documents on an export.'
            ],
            'panel2_1' => [
                'a' => 'Loads by a cost over 150 EUR',
                'desc' => 'List of shipping documents(TSD) for sending of load service of Express(15N) or Economy Express(48N) for East Terminal of the Kyiv custom of WMO №2 UKRINFORM:
Invoice of FHD(pass 4 signed copies courier, you leave one for yourself).
Original of invoice(4 bilingual copies) with the signatures of declarant and wet printing of Your company. To load a form / to load the standard of filling.
Original of confirmation of cost of commodity in hryvnyas(for the Ukrainian custom) : for legal entities is a calculation, expense/pl(expense) invoice. Or the imported documents with the marks(printing) of the Ukrainian custom in currency(USD or EUR).
Copy of operating card of accreditation of Your company with the wet printing of Your company to every page.
Foreign economic treaty or document that replaces *, him and also addition and specifications
Packing sheet
Other additional documents are in the original(if necessary) of fito \ vet \ sanitary certificates, permissive documents on an export.
Customs declaration.
* Information about foreign economic treaties / of contracts and other documents that is used in international practice instead of agreement / of contract, contained in the letter of DMSU № 11 / 6-10.10 / 6714-EP from 14.07.2009
If you wish to take advantage of services of FHD in custom registration of loads, will appeal, please, after a telephone ..., or send a query to electronic address'
            ],
            'panel3_1' => [
                'a' => 'Agreement and tariffs on services in custom registration',
                'desc' => 'To load an agreement on the grant of services in custom registration of FHD'
            ],


            'panel4_1' => [
                'a' => 'Essential elements for payment of custom payments',
                'desc' => 'To load'
            ],
            'h4_2' => 'Physical persons',
            'panel1_2' => [
                'a' => 'Loads by a cost less than 10000 EUR',
                'desc' => 'List of shipping documents(TSD) for sending of load service of Express(15N) or Economy Express(48N) for East Terminal of the Kyiv custom of  WMO №2 UKRINFORM:
Original of statement on a custom 15N or 48N Kyiv (a 1 copy by the Ukrainian or Russian languages after the signatures of sender. To load a form / to Load the standard of filling.
Invoice of FHD(pass to 4 signed copies the courier, you leave one for yourself).
Original of invoice(4 bilingual copies) with the signatures of декларанта. To load a form / to load the standard of filling.
Original of confirmation of cost of commodity in the hryvnyas(for the Ukrainian custom) : for physical persons it is a check, invoice.
Copy of passport(1,2 and page with registration).
Copy of identification koda.
Other additional documents are in the original(if necessary) of fito \ vet \ sanitary certificates, permissive documents on an export.
If you wish to get consultation of FHD at necessary documents for the export of sending, will appeal, please, after a telephone ... or send a query to electronic address.'
            ],
            'panel2_2' => [
                'a' => 'Bill of goods export(sending) of that outside custom territory of Ukraine shut out citizens',
                'desc' => 'In accordance with the decision of Cabinet of Ministers of Ukraine №468 from 28.05.12 the bill of the goods forbidden to the export from Ukraine is ratified.
To revise on-line.',
            ],
            'panel3_2' => [
                'a' => 'Essential elements for payment of custom payments',
                'desc' => 'To load'
            ]
        ],
        'treaty' => 'Treaty',
        'description' => 'Terms of Service'
        ],


    'trackers' => [
        'p' => 'Enter',
        'h1' => 'TRACKING is CONTROL',
        'h5' => 'And to watch motion of Your container real-time from our official web-site.',
        'link2' => 'About us',
        'h4' => 'Enter data after an invoice',
        'label1' => 'Phone number',
        'label2' => 'Tracker number',
        'submit' => 'To find',
        'content' => [
            'p' => 'State',
            'h3' => 'Your order',
            'title' => 'Stage',
            'date_title' => 'A date is not active'
        ]
    ],
    'tracker_create' => [
        'error' => 'Error',
        'p' => 'Do you have a waybill already?',
        'h1' => 'Track now',
        'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
        'link2' => 'About us',
        'content' => [
            'p' => 'To design on-line',
            'h3' => 'We will check a tracker in the near time',
            'title_1' => 'Fulfill the form',
            'title_2' => 'Information about sending',
            'title_3' => 'Description',
            'title_4' => 'Sender',
            'title_5' => 'Recipient',
            'label_1' => 'Type',
            'label_2' => 'Invoice number',
            'label_3' => 'Date of registration',
            'label_4' => 'Number of packages',
            'label_5' => 'Type of package',
            'label_6' => 'Width, cm',
            'label_7' => 'Length, cm',
            'label_8' => 'Height, cm',
            'label_9' => 'Total weight, kg',
            'label_10' => 'Estimated Cost',
            'label_11' => 'Currency',
            'label_12' => 'Full description of contents',
            'label_13' => 'Notes',
            'label_14' => 'Phone number',
            'label_15' => 'Full name / Company name',
            'label_16' => 'ITN',
            'label_17' => 'Passport data (series, number)',
            'label_18' => 'Country',
            'label_19' => 'City',
            'label_20' => 'Address',
            'label_21' => 'Post code',
            'label_22' => 'Accompanying documents of sender',
            'label_23' => 'Phone number',
            'label_24' => 'Full name / is the company of recipient Name',
            'label_25' => 'ITN',
            'label_26' => 'Passport data (series, number)',
            'label_27' => 'Country',
            'label_28' => 'City',
            'label_29' => 'Address',
            'label_30' => 'Post code',
            'label_32' => 'Accompanying documents of recipient sender\'s',
            'label_33' => 'Does Your delivery contain dangerous objects?',
            'label_34' => 'No',
            'label_35' => 'Yes',
            'label_36' => 'Unfortunately, this parcel it is impossible to send on-line.',
            'label_37' => 'Write to us',
            'label_38' => ', for the search of variants of transportation of dangerous loads.',
            'label_39' => 'CREATE',
			'label_390' => 'SAVE',
            'label_40' => 'Email',
			'label_42' => 'SAVE AS DRAFT',
            'label_60' => 'The final cost is determined by the actual weight and dimensions, the measurement of which is carried out by FHD and may differ from the cost mentioned here. You might also get an additional invoice according to the taxes. All the costs are indicated without VAT and exclude any taxes, duties or related government fees. For information about additional feesplease contact the FHD manager. Attention! The dispatch is taken into delivery process only after making a 100% payment in advance. After completing the order, we will send you an invoice to the email address that you have provided. FHD company courier will take the shipment on the date you specify and deliver it to the warehouse. When the delivery is ready for departure, after we check the receipt of the payment, we will send it immediately to the recipient. If the payment is not received, we will notify you, and we will send your delivery on the day we get the payment or you will present the banks order.',
            'label_61' => 'Choose file',
            'label_62' => 'Sender’s VAT',
            'label_63' => 'Number of FHD account',
            'back' => 'CANCEL'
        ]
    ],
    'login' => [
        'p' => 'Already have an account?',
        'h1' => 'AUTHORIZING',
        'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
        'link2' => 'About us',
        'login_form' => [
            'text_1' => 'Log In',
            'text_2' => 'E-mail or phone number',
            'text_3' => 'Password',
            'text_4' => 'Remember me',
            'text_5' => 'Log In',
            'text_6' => 'TO ENTER ACCOUNT WITH GOOGLE',
            'text_7' => 'TO ENTER ACCOUNT WITH FACEBOOK',
            'text_8' => 'TO ENTER ACCOUNT WITH INSTAGRAM',
            'text_9' => 'TO ENTER ACCOUNT WITH TWITTER',
            'text_10' => 'Don\'t have an account?',
            'text_11' => 'Sign Up',
            'forgotten' => 'Reset password'
        ],
        'reg_form' => [
            'text_1' => 'Sign Up',
            'text_2' => 'Full name',
            'text_3' => 'E-mail',
            'text_4' => 'Phone number',
            'text_5' => 'Password',
            'text_6' => 'Confirm password',
            'text_7' => 'SIGN UP'
        ]
    ],
    'history' => [
        'p' => 'Do you have an account already?',
        'h1' => 'HISTORY of ORDERS',
        'h5' => 'Pass to tracking inset to look for the information and status of your delivery',
        'link2' => 'About us',
        'content' => [
            'p' => 'HISTORY',
            'h3' => 'YOUR HISTORY of ORDERS',
            'h5' => 'Your profile',
            'number' => 'Invoice number',
            'status' => 'status of tracker',
            'on_mod' => 'on moderation',
            'off_mod' => 'tested',
            'false' => 'There are not existent orders!'
        ]
    ],

    'custom_department' => [
        'number_tracker' => 'Number of tracker',
        'type_transportation' => 'Type of transportation',
        'country' => 'Country',
        'city' => 'City',
        'address' => 'Address',
        'status' => 'Status'
    ],
    'main_page' => [
        'track_shipment' => 'Track your shipment',
        'tracking_number' => 'Tracking number',
        'phone_number' => 'Phone number',
        'track' => 'Track',
        'create_tracker' => 'Create tracker',
        'our_services' => 'Our services',
        'core_values' => 'Core values',
        'protection' => 'DATA PROTECTION',
        'protection_value' => 'Our service is created by professionals using the latest technologies, thus ensuring an high level of protection',
        'buy' => 'BEST BUY',
        'buy_value' => 'Shop online several times cheaper than in local stores',
        'authenticity' => 'ORIGINAL PRODUCTS',
        'authenticity_value' => 'Buying at official online shops, you get guarantees for branded, original products',
        'documents' => 'DOCUMENTS',
        'documents_for' => 'FOR CUSTOMS CLEARANCE OF EXPORT GOODS',
        'legal' => 'LEGAL PERSONS',
        'cargoes<150' => 'Cargoes worth less than 150 euros',
        'cargoes>150' => ' Cargoes worth more than 150 euros',
        'contract_custom' => 'Contract and tariffs for customs clearance services',
        'details_custom' => 'Details for the payment of customs duties',
        'natural' => 'NATURAL PERSONS',
        'list_prohibited' => 'List of prohibited goods',
        'terms' => 'Terms of Service',
        'contact' => 'CONTACT US',
        'anytime' => 'ANYTIME',
        'name' => 'Name',
        'e-mail' => 'E-mail',
        'subject' => 'Subject',
        'message' => 'Message',
        'submit' => 'SUBMIT',
        'sign_in' => 'Log In',
        'sign_up' => 'Sign Up',
        'buy_online' => 'BUY ONLINE SHIP WITH US',
        'buy_online_value' => 'Buying at official online shops, you get guarantees for branded, original products',
        'custom_w' => 'CUSTOMS WAREHOUSE',
        'custom_w_value' => 'Our own licensed warehouse where all the custom clearances take place',
        'online_tracking' => 'ONLINE TRACKING',
        'online_tracking_value' => 'Track your delivery from the moment it arrives at our warehouse, until the moment you receive it',
        'safety' => 'SAFETY DELIVERY',
        'safety_value' => 'Each shipment has basic insurance, provided by our insurance partners',
        'time' => 'FAST DELIVERY',
        'time_value' => 'We are doing our best to deliver your items in minimum time',
        'tariffs' => 'LOW TARIFFS',
        'tariffs_value' => "We are using most optimal delivery logistic solutions. That's why our tariffs are competitive and transparent",
        'count_myself' => 'Count yourself',
        'air' => 'Air',
        'water' => 'Water',
        'sender_country' => 'Sender\'s country',
        'recipient_country' => 'Recipient country',
        'delivery_service' => 'Delivery service',
        'weight' => 'Weight',
        'length' => 'Length',
        'width' => 'Width',
        'height' => 'Height',
        'kg' => 'Kg',
        'pd' => 'lb',
        'cm' => 'Cm',
        'in' => 'In',
        'with_dimensions' => 'With indication of dimensions',
		'with_custom_duty' => 'With regard to customs duties',
        'assessed_value' => 'Assessed value',
        'currency' => 'Currency',
        'comment1' => 'The accuracy of the determination of this parameter depends on the actual dimensions of the shipment, including packaging',
        'comment2' => 'To justify the estimated value of your sender, go to',
        'link' => 'link',
        'count' => 'Calculate',
		'calculator' => [
			'type_transport_strings' => [
				'air' => 'by air',
				'water' => 'by water'
			],
			'results' => [
				'with_volume_weight' => '(with volume weight counting)',
				'delivery_primary' => 'Delivery (<span></span>)',
				'custom_duty' => 'Custom duty',
				'delivery_secondary' => 'Delivery (courier)',
				'insurance' => 'Insurance',
				'total' => 'Total:',
			]
		],
        'add_info' => 'Additional information',
        'customs_regulations' => 'Customs regulations',
        'delivery_restriction' => 'Delivery restriction',
        'terms_of_service' => 'Terms of service',
        'agreement_physical' => 'Contract for individual entities',
        'agreement_legal' => 'Contract for legal entities',
    ],
    'reset_pass' => [
        'send_mess' => 'An email has been sent to your email to change your password!',
        'dont_send_mess' => 'This email is not in the system!',
        'reset_pass' => 'Reset password',
        'email_verify' => 'Email verify',
        'password' => 'Passwords do not match',
        'verify_email' => 'A verification email has been sent to your email!',
    ],
    'ticket' => [
        'new' => 'New',
        'all' => 'All message',
        'archive' => 'Archive',
        'not_new' => 'You have no new tickets',
        'not_all' => 'You have no tickets yet',
    ]
];
