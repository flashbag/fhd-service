<?php


return [
    //***********************
    //* Трекеры, до таблицы *
    //***********************
    'unprocessed' => 'List of unprocessed trackers',
    'processed' => 'List of processed trackers',
    'ready_departure' => 'List of ready for departure trackers',
    'trash' => 'List of trackers in the trash',
    'all' => 'List of all trackers',
    'trackers' => 'Trackers',
    'receive' => 'For receive',
    'departure' => 'For departure',
    'history' => 'Client history',
    'logout' => 'Logout',
    'site' => 'Go to the website',
    'info' => 'Info',
    'error' => 'Error',
    //*************************
    //* Трекеры, найти трекер *
    //*************************
    'find_tracker' => 'Find a tracker',
    'history_change_status' => 'History status change',
    'who_installed' => 'Who set the status',
    'date' => 'Date',
    'not_found' => 'Tracker not found',
    'filtrate' => 'Filter',
    //********************
    //* Трекеры, фильтры *
    //********************
    'filters' => 'Filters',
    'tracker_number' => 'Tracker number',
    'from_date' => 'From the date of issue',
    'to_date' => 'To the date of issue',
    'status' => 'Status',
    'invoice_number' => 'Invoice number',
	'batch_id' => 'Attach to batch',
    'carrier' => 'Carrier',
    'container_number' => 'Container number',
    'container_type' => 'Container type',
    'sender_country' => 'Sender\'s country',
    'sender_city' => 'Sender\'s city',
    'sender_fullname' => 'Sender\'s fullname',
    'sender_phone' => 'Sender\'s phone number',
	'sender_email' => 'Sender\'s email',
    'sender_company' => 'Sender\'s company name',
    'recipient_country' => 'Recipient\'s country',
    'recipient_city' => 'Recipient\'s city',
    'recipient_fullname' => 'Recipient\'s fullname',
    'recipient_phone' => 'Recipient\'s phone number',
	'recipient_email' => 'Recipient\'s email',
    'recipient_company' => 'Recipient\'s company name',
    'clear_filter' => 'Clear filter',
    'type_transport' => 'Type transport',
    //*************************
    //* Трекеры, выбор склада *
    //*************************

    'warehouse' => 'Warehouse',
    'select_warehouse' => 'Choose the warehouse',
    'ukraine' => 'Main warehouse in Ukraine',
    'china' => 'Main warehouse in China',
    'usa' => 'Main warehouse in USA',



    //***********************************
    //* Трекеры, кнопка добавить трекер *
    //***********************************
    'add' => 'Add tracker',



    //**************************
    //* Трекеры, шапка таблицы *
    //**************************
    'barcode' => 'Barcode',
	'created_at' => 'Created',
	'updated_at' => 'Updated',
    'date_of_issue' => 'Date of issue',
    'delivery_date' => 'Delivery date',
    'total_weight' => 'Total weight',
    'volume_weight' => 'Volume weight',
    'actual_weight' => 'Actual weight',
    'assessed_value' => 'Assessed value',
    'duty_rate' => 'Duty rate',
    'cost_reference' => 'Cost reference',
    'shipping_rate' => 'Shipping rate',
    'shipping_currency' => 'Shipping currency',
    'total_shipping_cost' => 'Total shipping cost',
    'accepted_employee' => 'Name of the employee who accepted the IES',
    'issued_employee' => 'Name of the employee who issued the IES',
    'edit' => 'Edit',
    'edit_trackers' => 'Edit tracker',
    'create_trackers' => 'Create tracker',


    //********************
    //* Трекеры, корзина *
    //********************
    'trash_restore' => 'Restore',
    'trash_delete' => 'Delete',



    //***********************************
    //* Трекеры, список изменить статус *
    //***********************************
    'change' => 'Change',
    'change_status' => 'Change status',
    'status_0' => 'Expected in the warehouse',
    'status_1' => 'Delivered to the warehouse in the country of the sender',
    'status_2' => 'In the process of preparing documents in the country of the sender',
    'status_3' => 'In the process of customs clearance in the country of the sender',
    'status_4' => 'Customs clearance in the country of the sender is passed, your parcel moves to the country of the recipient',
    'status_5' => 'Moves to the country of the recipient',
    'status_6' => 'Arrived in the country of the recipient',
    'status_7' => 'In the process of customs clearance in the country of the recipient',
    'status_8' => 'Customs clearance passed. Ready to receive in the warehouse',
    'status_9' => 'Sent to internal operator for address delivery to the recipient',
    'status_10' => 'Delivered',



    //**************************************
    //* Трекеры, включить/выключить трекер *
    //**************************************
    'enable_tracker' => 'Enable tracker',
    'disable_tracker' => 'Disable tracker',



    //********************
    //* Создание трекера *
    //********************
    'back_button' => 'Back to trackers',
    'delete' => 'Delete',
    'save' => 'Save',
    'send_to_processed' => 'Send to "Processed"',
    'confirm' => 'Confirm',
    'ticket' => 'Create ticket',
    'ct_page_title' => 'Create tracker',
    'attention_title' => 'Please fill the form in english language',
    'transport_type' => 'Transport type',
    'transport_1' => 'By air',
    'transport_2' => 'By sea',
    'transport_3' => 'By road',
    'transport_4' => 'By train',
    'departure_country' => 'Sender\'s country',
    'arrival_country' => 'Recipient\'s country',
    'ct_title_1' => 'Position information',
    'ct_title_2' => 'Add position',
    'position' => 'Position',
    'del_position' => 'Delete position',
    'package_type' => 'Package type',
    'unit_length' => 'Unit of length',
    'unit_weight' => 'Unit of weight',
    'paragraph_size' => 'Enter the measured sizes of W x L x D and the weight of the shipment.',
    'length' => 'Length',
    'width' => 'Width',
    'height' => 'Height',
    'paragraph_actual_weight' => 'For the calculation of shipping costs a higher weight value is used.',
    'paragraph_volume_weight' => 'For the calculation of shipping costs a higher weight value is used.',
    'assessed_price' => 'Assessed price',
    'currency' => 'Currency',
    'description_in_position' => 'Description of cargoes in position',
    'paragraph_description_in_position' => 'If your departure contains several items - describe each of these items.',
    'senders_documents' => 'Sender\'s accompanying documents',
    'recipients_documents' => 'Recipient\'s accompanying documents',
    'ct_title_3' => 'Recipient',
    'customs_costs' => 'Who pays customs costs',
    'transportation_costs' => 'Who pays for transportation costs',
    'delivery_type' => 'Cargo delivery type',
    'special_handlings' => 'Special handlings',
    'dangerous objects' => 'Does Your delivery contain dangerous objects?',
    'terms' => 'Do you agree with the terms of transportation?',
    'yes' => 'Yes',
    'no' => 'No',
    'cancel_button' => 'Cancel',
    'create_button' => 'Create',
    'draft_button' => 'Save as draft',
    'cargo_name' => 'Cargo name',
    'characters_left' => 'Number of characters left',
    'quantity' => 'Quantity',
    'validate_1' => 'This field is read only. Filled in the sender unit.',
    'sender' => 'Sender',
    'recipient' => 'Recipient',
    'person_type' => 'Person type',
    'phone' => 'Phone number',
    'itn' => 'ITN',
    'name' => 'Name',
    'company' => 'Company name',
    'VAT' => 'Sender’s VAT',
    'passport' => 'Passport data (series, number)',
    'city' => 'City',
    'state' => 'State',
    'country' => 'Country',
    'address' => 'Address',
    'post_code' => 'Post code',
    'account' => 'Account number',
    'service' => 'Service tracker number',
    'delivery' => 'Delivery service',
    'another_delivery' => 'Another delivery service',
    'view_doc' => 'View document',
    'customId' => 'Custom identify',
    'contact_name' => 'Contact name',
    'fields_to_fill' => 'Fields to fill',
    'paid' => 'Paid',
    'paid_label' => 'By setting this option you confirm the fact of payment or an agreement to pay in writing or orally',
    'info_employee' => 'Employee information',
    'next' => 'Next',
    'previous' => 'Previous',
    'cancel' => 'Cancel',
    'reason' => [
        '1' => 'Incorrect data',
        '2' => 'The shipment was not delivered within 21 days',
        '3' => 'The client himself refused to forward',
        '4' => 'The client was going to transport prohibited cargo'
    ],
    'variant' => 'Your option',
    'specify_reason' => 'Specify the reason for deletion',
    'cant_del_without_reason' => 'You can not delete the tracker without a reason!',
    'success_del' => 'Tracker successfully deleted!',
    'success_recovery' => 'Tracker successfully restored!',
    'UAH' => 'Ukrainian hryvnia',
    'USD' => 'U.S. dollar',
    'EUR' => 'Euro',
    'CNY' => 'Chinese yuan',
    'systemic' => 'Systemic',
    'delete_reason' => 'Reason for deletion',
    'delete_by' => 'Who deleted',

	/**
	 * Партии
	 */
	'batches' => [
		'title' => 'Batches',
		'all' => 'All batches',
		'create' => 'Create new batch',
		'cannot_delete' => 'Cannot delete this batch because it has linked trackers',
		'delete_success' => 'Batch successfully deleted',
		'update_success' => 'Batch successfully updated',
	],
    'buttons' => 'Buttons',
    'shift' => 'Shift + scroll',
    'shift_text' => 'or the slider at the bottom of the table, you can move the contents of the table to the right and left',
    'transport' => [
        'air' => 'Air',
        'road' => 'Road',
        'sea' => 'Sea',
        'train' => 'Train',
    ]
];
