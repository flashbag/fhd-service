<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_spaces'         => 'The :attribute may only contain letters and spaces.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute  is incorrect.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute is of the wrong type or size. Supported file types: :values bite.',
    'mimetypes'            => 'The :attribute is of the wrong type or size. Supported file types: :values bite.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'Enter the :attribute.',
    'required_if'          => 'Enter the :attribute.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'Enter the :attribute.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'Passwords do not match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',
	'phone_number' 		   => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes'           => [
        'id_tracker' => 'invoice number',
		'type_transport_id' => 'transport type',
		'from_warehouse_id' => 'warehouse',
		'to_warehouse_id' => 'warehouse',

		'number_sender' =>  'phone number',
		'email_sender' => 'email',
		'full_name_sender' => 'full name',
		'sender_number_account' => 'account number',
		'city_id_sender' => 'city',
		'region_sender' => 'state',
		'country_id_sender' => 'country',
		'address_sender' => 'address',
		'index_sender' => 'postcode',
		'inn_sender' => 'TAX number',
		'passport_data_sender' => 'passport data',
		'sender_number_service' => 'service number',
		'sender_company_name' => 'company name',
		'sender_service_delivery_id' => 'delivery service',
		'sender_service_delivery_other' => 'other delivery service',

		'number_vat' => 'number VAT',
		'sender_documents.*.file' => 'file',

		'number_recipient' => 'phone number',
		'email_recipient' => 'email',
		'full_name_recipient' => 'full name',
		'recipient_number_account' => 'account number',
		'city_id_recipient' => 'city',
		'region_recipient' => 'state',
		'country_id_recipient' => 'country',
		'address_recipient' => 'address',
		'index_recipient' => 'postcpde',
		'inn_recipient' => 'SSN',
		'passport_data_recipient' => 'passport data',
		'recipient_number_service' => 'service number',
		'recipient_company_name' => 'company name',
		'recipient_service_delivery_id' => 'delivery service',
		'recipient_service_delivery_other' => 'other delivery service',

		'custom_id' => '',
		'representative_recipient' => '',
        'recipient_documents.*.file' => 'file',
        'receptacles.*.cargos.*.description' => 'description of cargo',
        'receptacles.*.cargos.*.quantity' => ' quantity',
        'receptacles.*.actual_weight' => 'weight',
        'receptacles.*.width' => 'width',
        'receptacles.*.length' => 'length',
        'receptacles.*.height' => 'height',
        'receptacles.*.assessed_price' => 'price',
        'receptacles.*.currency_type' => 'currency',
        'delivery_rate_kg' => 'delivery rate per kg',
        'bill_duty_third_party' => 'third party email',
        'bill_duty_fhd_account' => 'FHD account',
        'username' => 'Full name or company name',
        'inn' => 'inn',
        'index' => 'index',
        'passport_data' => 'passport data',
    ],
    'captcha' => 'Captcha error',
];
