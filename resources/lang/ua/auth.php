<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Цей email не відповідає нашим записам.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'hello' => 'Доброго дня',
    'thanks' => 'Дякуємо за реєстрацію.',
    'ForVerifyGoTo' => 'Для верифікації email перейдіть за',
    'link' => 'посиланням.',
    'message1' => 'Зареєструвавшись в нашому сервісі ви прийняли правила і умови нашої компанії.
                    Бажаємо приємної роботи з FHD Service.',
    'ChangeEmail' => 'Ви тільки що створили запит на зміну e-mail. Для активації нового e-mail перейдіть по',
];
