<?php

return [
	'view_tracker' => 'Перегляд трекера',
    'edit_tracker' => 'Редагування трекера',
	'tracker_not_found' => 'Трекер не знайдено',
    'fill_form_lang' => 'Пожалуйста, заполните форму на русском или английском',
	'tracker_history_not_found' => 'Історії трекена не знайдено',
	'parts' => [
		'person_types' => [
			1 => 'Фізична особа',
			2 => 'Юридична особа'
		],
		'transport' => [
			'select_type_transport' => 'Виберіть тип транспорту',
			'type_transport' => 'Тип транспорту',
			'type_transport_air' => 'Повітря',
			'type_transport_sea' => 'Вода',
			'type_transport_road' => 'Дорога',
			'type_transport_train' => 'Поїзд',
		],
		'shipping' => [
			'unit_length' => [
				'cm' => 'см',
				'mm' => 'мм',
				'in' => 'дюйм',
				'ft' => 'фут'
			],
			'unit_weight' => [
				'kg' => 'кг',
				'lb' => 'фунти'
			]
		],
		'bill_duties' => [
			'type_sender' => 'Відправник',
			'type_recipient' => 'Одержувач',
			'type_third_party' => 'Третя сторона',
			'type_fhd_account' => 'FHD аккаунт',
		],
		'bill_transportations' => [
			'type_sender' => 'Відправник',
			'type_recipient' => 'Одержувач',
			'type_third_party' => 'Третя сторона'
		],
		'freight_services' => [
			'type_priority_freight' => 'Priority Freight',
			'type_economy_freight' => 'Economy Freight',
			'type_booking_number' => 'Booking Number'
		],
		'special_handlings' => [
			'type_hold_at_fhd_warehouse' => 'HOLD at FHD warehouse',
			'type_saturday_delivery' => 'SATURDAY delivery',
			'type_address_delivery' => 'Address Delivery'
		],
	],
];
