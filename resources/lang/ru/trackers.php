<?php

return [
	'view_tracker' => 'Просмотр трекера',
	'edit_tracker' => 'Редактирование трекера',
	'add_tracker' => 'Добавить трекер',
    'fill_form_lang' => 'Пожалуйста, заполните форму на английском',
	'tracker_not_found' => 'Трекер не найден',
	'tracker_history_not_found' => 'История отсутствует',
	'tracker_required_fields' => 'Обязательные поля для заполнения',
	'rules_agreement' => 'Вы согласны с :link_start условиями :link_end перевозки?',
	'print_options' => [
		'downloadPDFTitle' => 'Скачать PDF накладную',
		'downloadCleanPDFTitle' => 'Скачать чистую PDF накладную',
		'settingsTitle' => 'Настройки PDF накладной',
		'colorLabel' => 'Цвет',
		'colors' => [
			'default' => 'Стандартный',
			'grey' => 'Серый',
			'blue' => 'Голубой',
			'orange' => 'Оранжевый',
		],
		'download' => 'Скачать PDF',
		'show' => 'Просмотреть PDF',
		'browser_warning' => 'Для правильного просмотра PDF рекомендуеться использовать браузер Google Chrome',
	],
	'parts' => [
		'type_inventories' => [
			'1' => 'Коробка',
			'2' => 'Палета',
			'3' => 'Конверт',
		],
		'prices' => [
			'help_block' => 'Эти данные вносит складской оператор склада страны получателя. Контрольную сумму, размер пошлины и валюту определяет таможенный инспектор'
		],
		'countries' => [
			'country_departure' => 'Страна отправления',
			'country_arrival' => 'Страна получения',
			'by_iso_3166_2' => [
				'CN' => 'Китай',
				'UA' => 'Украина',
				'US' => 'США',
			],
		],
		'additional' => [
			'title' => 'Дополнительная информация'
		],
		'warehouse' => [
			'country_departure_warehouse' => 'Склад',
			'country_arrival_warehouse' => 'Склад',
			'title'  => 'Склад',
			'select_option' => 'Выберите склад'
		],
		'person_types' => [
			1 => 'Физическое лицо',
			2 => 'Юридическое лицо'
		],
		'transport' => [
			'select_type_transport' => 'Выберите тип транспорта',
			'type_transport' => 'Тип транспорта',
			'type_transport_air' => 'Воздух',
			'type_transport_sea' => 'Вода',
			'type_transport_road' => 'Дорога',
			'type_transport_train' => 'Поезд',
		],

        'status' => [
          '0' => 'Ожидается на складе',
          '10' => 'Прибыло на склад страны отправителя',
          '15' => 'В процессе подготовки документов в стране отправителя',
          '20' => 'В процессе таможенного оформления в стране отправителя',
          '30' => 'Таможенный контроль пройден, отправление следует в страну получателя',
          '49' => 'В пути следования в страну получателя',
          '50' => 'Прибыло в страну получателя',
          '60' => 'В процессе таможенного оформления в стране получателя',
          '70' => 'Таможенный контроль пройден. Готово к получению на складе',
          '80' => 'Передано внутреннему оператору для адресной доставки получателю',
          '99' => 'Доставлено',
        ],

        'type_container' => [
            '1' => '20-ти футовый стандартный контейнер',
            '2' => '40-ка футовый стандартный контейнер',
            '3' => '40-ка футовый "high cube" контейнер',
            '4' => '20-ти футовый Open Top контейнер',
            '5' => '40-ка футовый Open Top контейнер',
        ],
        'service_delivery' => [
            'american_express' => 'American Express',
            'another' => 'Другое',
            'dhl' => 'DHL',
            'fedex' => 'FEDEX',
            'novaposhta' => 'Новая почта',
            'ups' => 'UPS',
            'usps' => 'USPS',
        ],
		'shipping' => [
			'unit_length' => [
				'cm' => 'см',
				'mm' => 'мм',
				'in' => 'дюйм',
				'ft' => 'футы'
			],
			'unit_weight' => [
				'kg' => 'кг',
				'lb' => 'фунты'
			]
		],
		'bill_duties' => [
			'title' => 'Кто оплачивает таможенные затраты',
			'type_sender' => 'Отправитель',
			'type_recipient' => 'Получатель',
			'type_third_party' => 'Email третьего лица',
			'type_fhd_account' => 'FHD аккаунт',
		],  
		'bill_transportations' => [
			'title' => 'Кто оплачивает транспортные затраты',
			'type_sender' => 'Отправитель',
			'type_recipient' => 'Получатель',
			'type_third_party' => 'Третье лицо',
			'type_customer' => 'Заказчик'
		],
		'freight_services' => [
			'title' => 'Доставка грузов',
			'type_priority_freight' => 'Приоритетная доставка',
			'type_economy_freight' => 'Экономная доставка',
			'type_booking_number' => 'Номер букинга'
		],
		'special_handlings' => [
			'title' => 'Особые инструкции',
			'type_hold_at_fhd_warehouse' => 'Держать на складе FHD',
			'type_saturday_delivery' => 'Доставка в суботу',
			'type_address_delivery' => 'Доставка по адресу'
		],
		'receptacles' => [
			'cargos_help_block_box' => 'Если Ваше отправление содержит несколько предметов - опишите каждый из этих предметов',
			'cargos_help_block_pallet' => 'Если Ваша палета содержит несколько предметов - опишите каждый из этих предметов',
			'cargos_help_block_envelope' => 'Если Ваш конверт содержит несколько предметов - опишите каждый из этих предметов',
			'sizes_help_block' => 'Укажите точные или приблизительные значения размеров ШхВхД и веса отправления. Точные данные может откорректировать работник склада',
			'weight_help_block' => 'Для расчётов стоимости доставки используется большее значение веса',
			'title_main' => 'Информация о позиции',
			'departure' => 'Отправление',
			'add_position' => 'Добавить позицию',
			'position' => 'Позиция',
			'type_inventory' => 'Тип упаковки',
			'unit_of_length' => 'Ед. измерения размеров',
			'unit_of_weight' => 'Ед. измерения веса',
			'width' => 'Ширина',
			'length' => 'Длина',
			'height' => 'Высота',
			'actual_weight' => 'Фактический вес',
			'volume_weight' => 'Объемный вес',
			'assessed_price' => 'Стоимость',
			'description_cargos' => 'Описание содержимого позиции',
			'cargos' => [
				//'is_used_false' => 'Новое',
				//'is_used_true' => 'Б/у',
				'quantity' => 'Количество',
				'description' => 'Название груза',
				'chars_left' => 'Количество символов осталось'
			]
		],
		'address_components' => [
			'city' => 'Город',
			'region' => 'Регион',
			'country' => 'Страна',
			'address' => 'Адрес',
			'address_help_block' => 'формат: Улица, дом, квартира',
			'index' => 'Индекс',
		],
		'documents' => [
			'title_sender' => 'Сопроводительные документы отправителя',
			'title_recipient' => 'Сопроводительные документы получателя',
			'title_receptacle' => 'Сопроводительные документы содержимого позиции',
			'view_document' => 'Просмотр документа',
			'file_not_selected' => 'Файл не выбран',
			'select_file' => 'Выбрать файл',
		],
		'sender' => [
			'title' => 'Отправитель',
			'number' => 'Номер телефона',
			'email' => 'Email',
			'person_type' => 'Тип персоны',
			'full_name' => 'ФИО',
			'number_account' => 'Номер аккаунта',
			'inn' => 'ИНН',
			'passport_data' => 'Паспортные данные (серия, номер)',
			'number_service' => 'Номер сервис трекера',
			'service_delivery' => 'Служба доставки',
			'company_name' => 'Название компании',
			'number_vat' => 'Номер плательщика НДС',
			'service_delivery_other' => 'Другая служба доставки',
		],
		// I know that fields of sender and recipient almost the same
		// But for now I separated them in case when labels will be different in future
		'recipient' => [
			'title' => 'Получатель',
			'number' => 'Номер телефона',
			'email' => 'Email',
			'person_type' => 'Тип персоны',
			'full_name' => 'ФИО',
			'number_account' => 'Номер аккаунта',
			'inn' => 'ИНН',
			'passport_data' => 'Паспортные данные (серия, номер)',
			'number_service' => 'Номер сервис трекера',
			'service_delivery' => 'Служба доставки',
			'company_name' => 'Название компании',
			'number_vat' => 'Номер плательщика НДС',
			'service_delivery_other' => 'Другая служба доставки',
			'custom_id' => 'Налоговый идентификатор',
			'contact_name' => 'Представитель получателя'
		],
        'warehouses' => [
            'UA_1' => 'Главный склад в Украине',
            'US_1' => 'Главный склад в США',
            'CN_1' => 'Главный склад в Китае',
        ],
	]
];
