<?php

return [
    'label11' => 'Договор для физических лиц',
    'label21' => 'Договор для юридических лиц',
    'label12' => 'ПУБЛИЧНЫЙ ДОГОВОР',
    'label23' => 'о предоставлении услуг доставки международных экспресс-отправлений',
    'label13' => 'редакция № 1.1',
    'button1' => 'СКАЧАТЬ НА РУССКОМ',
    'button2' => 'СКАЧАТЬ НА УКРАИНСКОМ',
    'button3' => 'СКАЧАТЬ НА АНГЛИЙСКОМ',
    'fiz' => '
<style type="text/css">ol{margin:0;padding:0}table td,table th{padding:0}.c1{margin-left:18pt;padding-top:0pt;text-indent:-18pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:justify}.c4{margin-left:54pt;padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:justify}.c2{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:10pt;font-family:"Arial";font-style:normal}.c12{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:"Arial";font-style:normal}.c11{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:left;height:11pt}.c9{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:center}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:justify}.c13{text-decoration-skip-ink:none;-webkit-text-decoration-skip:none;color:#1155cc;text-decoration:underline}.c10{background-color:#ffffff;max-width:451.4pt;padding:72pt 72pt 72pt 72pt}.c8{color:inherit;text-decoration:inherit}.c7{margin-left:90pt;text-indent:-36pt}.c6{margin-left:90pt;text-indent:-18pt}.c3{font-size:10pt}.c0{font-size:7pt}.title{padding-top:0pt;color:#000000;font-size:26pt;padding-bottom:3pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:0pt;color:#666666;font-size:15pt;padding-bottom:16pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Arial"}p{margin:0;color:#000000;font-size:11pt;font-family:"Arial"}h1{padding-top:20pt;color:#000000;font-size:20pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-size:16pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:16pt;color:#434343;font-size:14pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:14pt;color:#666666;font-size:12pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}</style>
<div>
   <p class="c9"><span class="c2">ПУБЛИЧНЫЙ ДОГОВОР</span></p>
   <p class="c9"><span class="c2">о предоставлении услуг доставки международных экспресс-отправлений</span></p>
   <p class="c9"><span class="c2">редакция № 1.1</span></p>
   <p class="c9"><span class="c2">г. Черноморск 13.06.2018 года</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c5"><span class="c2">ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ «Ф.Х.Д. СЕРВИС» (далее – «Исполнитель») предлагает неограниченному кругу физических лиц, которые наделены достаточным объемом прав и полномочий (далее – «Заказчик») получить услуги по международной экспресс-доставке отправлений в соответствии с положениями настоящего Публичного договора (далее – Договор).</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c5"><span class="c2">Настоящий Договор по своей правовой природе является смешанным и содержит в себе элементы публичной оферты в соответствии с положениями ст. ст. 638, 641 Гражданского кодекса Украины (далее – ГК Украины), транспортного экспедирования, перевозки, предоставления услуг и договора поручения.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c5"><span class="c2">Договор заключается путем безусловного и полного присоединения Заказчика к настоящему Договору и принятия всех существенных условий Договора 
      и имеет юридическую силу в соответствии с положениями ст. 633 ГК Украины.</span>
   </p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c5"><span class="c2">Договор считается заключенным с момента оформления международной экспресс-накладной. Оформление каждой последующей международной экспресс-накладной в любом случае является заключением нового Договора. Безусловное и полное принятие (акцепт – в понимании ч. 2 ст. 638 ГК Украины) условий Договора Заказчиком заключается в осуществлении Заказчиком действий, направленных на получение услуг, а именно: передачи Заказчиком или третьими лицами, действующими в интересах Заказчика, Исполнителю отправления по международной экспресс-накладной для предоставления услуг, предусмотренных Договором, независимо от наличия / отсутствия подписи отправителя на экземпляре международной экспресс-накладной Исполнителя.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c5"><span class="c2">Договор размещен на сайте http://fhd.com.ua/, в свободном доступе и таким образом, чтобы обеспечить ознакомление с содержанием настоящего Договора каждого лица, обращающегося к Исполнителю. К отношениям, определенных настоящим Договором, Стороны применяют также Условия предоставления услуг, которые являются публичными и размещены на сайте http://fhd.com.ua/.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">1.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Предмет Договора</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">1.1.</span><span class="c0">&nbsp; </span><span class="c2">Исполнитель обязуется предоставить Заказчику услуги по международной экспресс-доставке международных экспресс-отправлений (далее - МЭО), которые определены в п. 1.2. Договора, а Заказчик обязуется принять предоставленные услуги и своевременно произвести оплату за предоставленные Исполнителем услуги.</span></p>
   <p class="c1"><span class="c3">1.2.</span><span class="c0">&nbsp; </span><span class="c2">В соответствии с настоящим Договором Исполнитель предоставляет следующие услуги:</span></p>
   <p class="c5 c6"><span class="c3">-</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">организация перевозки МЭО любыми видами транспорта, а именно: прием и выдача МЭО; прием от Заказчика информации и сопроводительных документов на МЭО; оформление товарно-транспортных документов; организация перевозки МЭО из-за границы и за границу Украины, организация перевозки МЭО по территории Украины;</span></p>
   <p class="c5 c6"><span class="c3">-</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">таможенное оформление МЭО, а именно: оформление и размещение МЭО на центральной сортировочной станции (далее - ЦСС); декларирование и предъявление МЭО для таможенного контроля; оплата соответствующих таможенных платежей, сборов и налогов;</span></p>
   <p class="c5 c6"><span class="c3">-</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">хранение МЭО на ЦСС, осуществление складских операций с МЭО (упаковка, распаковка, накопление и др.);</span></p>
   <p class="c5 c6"><span class="c3">-</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">другие услуги, предусмотренные Условиями предоставления услуг.</span></p>
   <p class="c1"><span class="c3">1.3.</span><span class="c0">&nbsp; </span><span class="c2">Окончательный объем конкретной услуги определяется в международной экспресс-накладной (далее - МЭН).</span></p>
   <p class="c1"><span class="c3">1.4.</span><span class="c0">&nbsp; </span><span class="c2">Под МЭО в настоящем Договоре следует понимать надлежащим образом упакованные международные отправления с документами или товарным вложением, которые принимаются, обрабатываются и перевозятся любым видом транспорта по международному транспортному документу с целью доставки получателю ускоренным способом в определенный срок.</span></p>
   <p class="c1"><span class="c3">1.5.</span><span class="c0">&nbsp; </span><span class="c2">Путем передачи МЭО 
      Исполнителю, Заказчик подтверждает, что он ознакомлен и согласен с настоящим Договором и Условиями предоставления услуг, размещенными на сайте http://fhd.com.ua/, и обязуется их выполнять.</span>
   </p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">2.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Права и обязанности Сторон</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">2.1.</span><span class="c0">&nbsp; </span><span class="c2">Права Исполнителя:</span></p>
   <p class="c5 c7"><span class="c3">2.1.1.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Требовать от Заказчика документы необходимы для надлежащего выполнения условий ст. 1. настоящего Договора.</span></p>
   <p class="c5 c7"><span class="c3">2.1.2.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Требовать от Заказчика оплаты услуг, предусмотренных в ст. 1. настоящего Договора.</span></p>
   <p class="c5 c7"><span class="c3">2.1.3.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Осуществлять повторное взвешивание и измерение МЭО с целью подтверждения правильности расчетов заявленного Заказчиком веса. Стороны пришли к соглашению о том, что показатели, определенные при повторном взвешивании и измерении МЭО Исполнителем на центральной сортировочной станции (по адресу: Украина, г. Черноморск, ул. Промышленная, д. 1), является основанием для осуществления перерасчета стоимости предоставленных услуг.</span></p>
   <p class="c5 c7"><span class="c3">2.1.4.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Приостановить исполнение своих обязательств по настоящему Договору и придержать МЭО/МГО в случае невыполнения Заказчиком своих обязанностей, предусмотренных настоящим Договором, до момента их полного исполнения.</span></p>
   <p class="c5 c7"><span class="c3">2.1.5.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Изменять тарифы, размещенные на сайте http://fhd.com.ua/ до принятия МЭО.</span></p>
   <p class="c1"><span class="c3">2.2.</span><span class="c0">&nbsp; </span><span class="c2">Обязанности Исполнителя:</span></p>
   <p class="c5 c7"><span class="c3">2.2.1.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">предоставлять услуги в соответствии со ст. 1 Договора</span></p>
   <p class="c5 c7"><span class="c3">2.2.2.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">составлять Акт о несоответствии в случае выявления фактов повреждения МЭО, его упаковки или маркировки либо о его утери;</span></p>
   <p class="c5 c7"><span class="c3">2.2.3.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">по требованию Заказчика информировать его о ходе предоставления услуг;</span></p>
   <p class="c5 c7"><span class="c3">2.2.4.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Исполнитель не обязан осуществлять проверку содержания МЭО, упакованного Заказчиком, и соответствия упаковки особенностям МЭО, требованиям действующего законодательства и государственным стандартам.</span></p>
   <p class="c1"><span class="c3">2.3.</span><span class="c0">&nbsp; </span><span class="c2">Права Заказчика:</span></p>
   <p class="c5 c7"><span class="c3">2.3.1.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">на получение услуг в соответствии с условиями Договора;</span></p>
   <p class="c5 c7"><span class="c3">2.3.2.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">на инвентаризацию своего МЭО, при необходимости;</span></p>
   <p class="c5 c7"><span class="c3">2.3.3.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">требовать от Исполнителя информацию о предоставлении услуг, предусмотренных Договором.</span></p>
   <p class="c4"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">2.4.</span><span class="c0">&nbsp; </span><span class="c2">Обязанности Заказчика:</span></p>
   <p 
      class="c5 c7"><span class="c3">2.4.1.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">до момента передачи МЭО Исполнителю ознакомиться с Условиями предоставления услуг;</span></p>
   <p class="c5 c7"><span class="c3">2.4.2.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">правильно и разборчиво заполнить и подписать МЭН, сообщив Исполнителю информацию о МЭО (его содержании);</span></p>
   <p class="c5 c7"><span class="c3">2.4.3.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">предоставить Исполнителю все сопроводительные документы и информацию, необходимые для надлежащего предоставления услуг Исполнителем, и по требованию Исполнителя предоставить содержание МЭО для осмотра;</span></p>
   <p class="c5 c7"><span class="c3">2.4.4.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">в случае получения МЭО в ненадлежащем состоянии (повреждение, недостача и т.п.) зафиксировать его состояние в соответствующем акте, с обязательным участием представителей Заказчика и Исполнителя. Акт приема-передачи (в котором фиксируются повреждения МЭО, недостача т.д.) составляется во время передачи МЭО Заказчику;</span></p>
   <p class="c5 c7"><span class="c3">2.4.5.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">запаковать (и нанести необходимую маркировку) МЭО должным образом для его сохранения во время транспортировки и погрузочно-разгрузочных работ;</span></p>
   <p class="c5 c7"><span class="c3">2.4.6.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">информировать Исполнителя об условиях складирования и хранения МЭО на ЦСС;</span></p>
   <p class="c5 c7"><span class="c3">2.4.7.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">своевременно оплачивать услуги Исполнителя.</span></p>
   <p class="c4"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">3.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Стоимость услуг и порядок оплаты. Передача-приемка предоставленных услуг.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">3.1.</span><span class="c0">&nbsp; </span><span class="c2">Общая стоимость Договора определяется МЭН и окончательно рассчитывается при получении МЭО получателем.</span></p>
   <p class="c1"><span class="c3">3.2.</span><span class="c0">&nbsp; </span><span class="c2">Оплата стоимости услуг осуществляется Заказчиком в украинской национальной валюте (гривне). Заказчик осуществляет предварительную оплату услуг Исполнителя. В случае начисления соответствующих таможенных платежей, сборов и налогов Заказчик (получатель) обязуется оплатить их до получения МЭО.</span></p>
   <p class="c1"><span class="c3">3.3.</span><span class="c0">&nbsp; </span><span class="c2">Стоимость услуг определяется в Приложении № 1 к Договору.</span></p>
   <p class="c1"><span class="c3">3.4.</span><span class="c0">&nbsp; </span><span class="c2">Пересмотр тарифов осуществляется в соответствии с п.п. 2.1.5 Договора.</span></p>
   <p class="c1"><span class="c3">3.5.</span><span class="c0">&nbsp; </span><span class="c2">Передача Заказчиком МЭО Исполнителю для предоставления услуг и подписание МЭН является свидетельством того, что Заказчик согласен с тарифами Исполнителя.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">4.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Ответственность Сторон</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">4.1.</span><span class="c0">&nbsp; </span><span class="c2">Исполнитель несет ответственность за сохранность МЭО Заказчика, а также за механические и другие повреждения, возникшие вследствие ненадлежащего виновного поведения Исполнителя.</span></p>
   <p class="c1"><span class="c3">4.2.</span><span class="c0">&nbsp; </span><span class="c2">Исполнитель не несет ответственности за качество, комплектацию и количество принятых на ЦСС МЭО, если их упаковка нарушена или 
      повреждена.</span>
   </p>
   <p class="c1"><span class="c3">4.3.</span><span class="c0">&nbsp; </span><span class="c2">Заказчик передает для таможенной обработки и хранения МЭО, ввоз / вывоз которых не запрещен законодательством Украины.</span></p>
   <p class="c1"><span class="c3">4.4.</span><span class="c0">&nbsp; </span><span class="c2">За несвоевременную оплату предоставленных услуг, Заказчик выплачивает Исполнителю пеню в размере двойной учетной ставки Национального банка Украины от просроченной суммы за каждый день просрочки и 20% годовых от суммы задолженности, в том числе возмещает Исполнителю причиненные убытки и компенсирует упущенную выгоду.</span></p>
   <p class="c1"><span class="c3">4.5.</span><span class="c0">&nbsp; </span><span class="c2">Исполнение Заказчиком своих обязательств по оплате за предоставленные Исполнителем услуги обеспечивается залогом – МЭО переданным Заказчиком. В случае невыполнения Заказчиком своих обязательств, Исполнитель приобретает право обращения взыскания на предмет залога по собственному решению.</span></p>
   <p class="c1"><span class="c3">4.6.</span><span class="c0">&nbsp; </span><span class="c2">Любая из Сторон не несет ответственности за невыполнение условий настоящего Договора в силу форс-мажорных обстоятельств. При возникновении форс-мажорных обстоятельств, из-за которых Стороны не могут выполнять свои обязательства, каждая из Сторон обязуется уведомить об этом другую Сторону в течение 5-ти дней и подтвердить вышеуказанные обстоятельства в порядке, определенном действующим законодательством Украины.</span></p>
   <p class="c1"><span class="c3">4.7.</span><span class="c0">&nbsp; </span><span class="c2">Заказчик несет ответственность за достоверность данных в документах, соответствие их содержанию, характеру, качеству и количеству МЭО, предоставленных им для осуществления таможенного оформления, а также соответствие упаковки особенностям МЭО, требованиям действующего в Украине законодательства и требованиям законодательства страны назначения (транзита).</span></p>
   <p class="c1"><span class="c3">4.8.</span><span class="c0">&nbsp; </span><span class="c2">Исполнитель несет ответственность, в случае некачественного выполнения своих обязательств перед Заказчиком в пределах, установленных действующим в Украине законодательством и настоящим Договором.</span></p>
   <p class="c1"><span class="c3">4.9.</span><span class="c0">&nbsp; </span><span class="c2">Исполнитель освобождается от ответственности за просрочку в доставке МЭО, если просрочка произошла не по его вине, в частности, но не исключительно, если такая просрочка вызвана несвоевременным предоставлением Заказчиком документов и информации, необходимых для выполнения таможенных процедур и условий Договора, несвоевременным осуществлением таможенных процедур по вине таможенных органов и тому подобное.</span></p>
   <p class="c1"><span class="c3">4.10.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">Заказчик проинформирован об ответственности за нарушение Таможенного законодательства Украины.</span></p>
   <p class="c1"><span class="c3">4.11.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">Исполнитель не несет ответственности по обязательствам Заказчика, которые последний непосредственно выполнял перед таможенными органами.</span></p>
   <p class="c1"><span class="c3">4.12.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">Коды УКТВЭД товаров, подлежащих таможенному оформлению, определяются согласно пояснений и документов, предоставляемых Заказчиком. Однако решение органов фискальной службы по классификации товаров для таможенных целей являются обязательными, согласно ст. 69 Таможенного кодекса Украины.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">5.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Срок действия Договора</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p 
      class="c1"><span class="c3">5.1.</span><span class="c0">&nbsp; </span><span class="c2">Настоящий Договор считается заключенным после подписания МЭН, или с момента передачи Заказчиком МЭО Исполнителю для предоставления услуг, предусмотренных условиями настоящего Договора и действует до передачи МЭО получателю. В части расчетов между Сторонами Договор действует до полного исполнения обязательств. Окончание срока настоящего Договора или его расторжение, не освобождает Стороны от ответственности за его нарушение, которое имело место во время действия настоящего Договора.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">6.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Порядок разрешения споров</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">6.1.</span><span class="c0">&nbsp; </span><span class="c2">Разногласия, возникающие при выполнении настоящего Договора, должны решаться путем переговоров между Сторонами.</span></p>
   <p class="c1"><span class="c3">6.2.</span><span class="c0">&nbsp; </span><span class="c2">В случае невозможности урегулирования разногласий путем переговоров и досудебного урегулирования спора, споры между Сторонами подлежат разрешению в соответствии с действующим в Украине законодательством.</span></p>
   <p class="c1"><span class="c3">6.3.</span><span class="c0">&nbsp; </span><span class="c2">Акты осмотра МЭО, коммерческие акты, экспертные заключения и другие документы, содержащие информацию о характере и причине повреждений, порчи, утраты, недостачи содержания МЭО, которые были составлены без участия уполномоченного представителя Исполнителя, недействительны и при рассмотрении претензии не принимаются во внимание.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">7.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Другие условия</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">7.1.</span><span class="c0">&nbsp; </span><span class="c2">После заключения настоящего Договора, все предыдущие переговоры, переписки, протоколы о намерениях и любые другие устные и письменные договоренности Сторон по вопросам, так или иначе касаются этого Договора, теряют юридическую силу.</span></p>
   <p class="c1"><span class="c3">7.2.</span><span class="c0">&nbsp; </span><span class="c3">Исполнитель в одностороннем порядке может изменять Условия настоящего Договора путем размещения их на сайте</span><span class="c3"><a class="c8" href="https://www.google.com/url?q=http://fhd.com.ua/&amp;sa=D&amp;ust=1554720113870000">&nbsp;</a></span><span class="c3 c13"><a class="c8" href="https://www.google.com/url?q=http://fhd.com.ua/&amp;sa=D&amp;ust=1554720113871000">http://fhd.com.ua/</a></span><span class="c2">.</span></p>
   <p class="c1"><span class="c3">7.3.</span><span class="c0">&nbsp; </span><span class="c2">Ни одна из Сторон не имеет права передавать свои права по настоящему Договору, а именно: уступка права требования и (или) перевода долга по настоящему Договору, третьим лицам без письменного согласия другой Стороны.</span></p>
   <p class="c1"><span class="c3">7.4.</span><span class="c0">&nbsp; </span><span class="c2">Недействительность (признание недействительным) любого из положений (условий) настоящего Договора и (или) приложений к нему, не является основанием для недействительности (признания недействительным) других положений (условий) настоящего Договора и (или) Договора в целом.</span></p>
   <p class="c1"><span class="c3">7.5.</span><span class="c0">&nbsp; </span><span class="c2">Срок хранения МЭО на ЦСС под таможенным контролем, не может превышать 30 дней.</span></p>
   <p class="c1"><span class="c3">7.6.</span><span class="c0">&nbsp; </span><span class="c2">Прием и выдача МЭО осуществляется с 9:00 до 18:00 каждый день, кроме субботы, воскресенья, праздничных и нерабочих дней, предусмотренных действующим в Украине законодательством.</span></p>
   <p class="c1"><span class="c3">7.7.</span><span class="c0">&nbsp; </span><span class="c2"
      >Стороны обязуются не разглашать конфиденциальную информацию другой Стороны, полученной в связи с исполнением обязательств по настоящему Договору.</span></p>
   <p class="c1"><span class="c3">7.8.</span><span class="c0">&nbsp; </span><span class="c2">Руководствуясь действующим в Украине законодательством, регулирующим правоотношения в сфере получения, сбора, накопления, обработки и защиты персональных данных, Заказчик дает свое согласие на обработку персональных данных для выполнения настоящего Договора. Настоящее согласие предоставляется на осуществление любых действий в отношении законным образом полученных персональных данных Заказчика, которые необходимы или желательны для достижения договорных целей, а именно: сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование в пределах, необходимых для вышеуказанных договорных целей, распространение (в том числе передача) в пределах, необходимых для вышеуказанных договорных целей, обезличивание, уничтожение, а также осуществление любых иных действий с полученными персональными данными в пределах, необходимых для вышеуказанных целей и с учетом действующего в Украине законодательства.</span></p>
   <p class="c1"><span class="c3">7.9.</span><span class="c0">&nbsp; </span><span class="c2">Во всем остальном, что не предусмотрено настоящим Договором, Стороны руководствуются действующим в Украине законодательством.</span></p>
   <p class="c1"><span class="c3">7.10.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c2">Исполнитель подтверждает, что он является плательщиком налога на прибыль предприятий на общих условиях и плательщиком НДС.</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c1"><span class="c3">8.</span><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c2">Контактные данные</span></p>
   <p class="c5"><span class="c2">&nbsp;</span></p>
   <p class="c5"><span class="c2">Исполнитель:</span></p>
   <p class="c5"><span class="c2">сайт http://fhd.com.ua/</span></p>
   <p class="c5"><span class="c2">Телефон</span></p>
   <p class="c11"><span class="c12"></span></p>
</div>',
    'jur' => '
    <style type="text/css">ol.lst-kix_vi3ziu60akgv-2.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-2 0
    }

    .lst-kix_rqm22k7viro-8 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-8, lower-roman) ". "
    }

    .lst-kix_rqm22k7viro-5 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-5, lower-roman) ". "
    }

    .lst-kix_fu1iokomnhaj-0 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-0, decimal) ". "
    }

    .lst-kix_rqm22k7viro-2 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-2
    }

    .lst-kix_rqm22k7viro-7 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-7, lower-latin) ". "
    }

    .lst-kix_mk6t5v5oqpss-3 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-3
    }

    .lst-kix_rqm22k7viro-6 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-6, decimal) ". "
    }

    ol.lst-kix_9fnqcvwn8ipt-8.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-8 0
    }

    .lst-kix_rqm22k7viro-1 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-1, lower-latin) ". "
    }

    ol.lst-kix_bh55r9bgsafo-5.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-5 0
    }

    .lst-kix_371pnvembmsw-0 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-0
    }

    .lst-kix_rqm22k7viro-0 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-0, decimal) ". "
    }

    .lst-kix_rqm22k7viro-4 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-4, lower-latin) ". "
    }

    .lst-kix_rqm22k7viro-3 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-3, decimal) ". "
    }

    ol.lst-kix_mk6t5v5oqpss-6.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-6 0
    }

    .lst-kix_rqm22k7viro-2 > li:before {
        content: "" counter(lst-ctn-kix_rqm22k7viro-2, lower-roman) ". "
    }

    .lst-kix_mk6t5v5oqpss-1 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-1
    }

    ol.lst-kix_9fnqcvwn8ipt-8 {
        list-style-type: none
    }

    .lst-kix_vi3ziu60akgv-0 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-0
    }

    .lst-kix_bh55r9bgsafo-2 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-2
    }

    .lst-kix_6gxo5agl0rwt-7 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-7
    }

    ol.lst-kix_9fnqcvwn8ipt-3 {
        list-style-type: none
    }

    .lst-kix_rqm22k7viro-4 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-4
    }

    .lst-kix_371pnvembmsw-2 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-2
    }

    ol.lst-kix_9fnqcvwn8ipt-2 {
        list-style-type: none
    }

    ol.lst-kix_9fnqcvwn8ipt-1 {
        list-style-type: none
    }

    ol.lst-kix_9fnqcvwn8ipt-0 {
        list-style-type: none
    }

    ol.lst-kix_9fnqcvwn8ipt-7 {
        list-style-type: none
    }

    ol.lst-kix_9fnqcvwn8ipt-6 {
        list-style-type: none
    }

    ol.lst-kix_9fnqcvwn8ipt-5 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-4.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-4 0
    }

    ol.lst-kix_9fnqcvwn8ipt-4 {
        list-style-type: none
    }

    ol.lst-kix_vi3ziu60akgv-8.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-8 0
    }

    ol.lst-kix_fu1iokomnhaj-8 {
        list-style-type: none
    }

    ol.lst-kix_fu1iokomnhaj-7 {
        list-style-type: none
    }

    .lst-kix_6gxo5agl0rwt-5 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-5, lower-roman) ". "
    }

    .lst-kix_6gxo5agl0rwt-6 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-6, decimal) ". "
    }

    ol.lst-kix_fu1iokomnhaj-4 {
        list-style-type: none
    }

    ol.lst-kix_fu1iokomnhaj-3 {
        list-style-type: none
    }

    .lst-kix_6gxo5agl0rwt-4 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-4, lower-latin) ". "
    }

    .lst-kix_6gxo5agl0rwt-8 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-8, lower-roman) ". "
    }

    ol.lst-kix_fu1iokomnhaj-6 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-5.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-5 0
    }

    ol.lst-kix_fu1iokomnhaj-5 {
        list-style-type: none
    }

    ol.lst-kix_371pnvembmsw-8.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-8 0
    }

    .lst-kix_6gxo5agl0rwt-1 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-1, lower-latin) ". "
    }

    .lst-kix_6gxo5agl0rwt-2 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-2, lower-roman) ". "
    }

    .lst-kix_mk6t5v5oqpss-5 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-5
    }

    .lst-kix_6gxo5agl0rwt-3 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-3, decimal) ". "
    }

    .lst-kix_bh55r9bgsafo-6 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-6
    }

    .lst-kix_9fnqcvwn8ipt-2 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-2
    }

    ol.lst-kix_fu1iokomnhaj-4.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-4 0
    }

    .lst-kix_6gxo5agl0rwt-7 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-7, lower-latin) ". "
    }

    .lst-kix_rqm22k7viro-0 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-0
    }

    .lst-kix_fu1iokomnhaj-5 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-5
    }

    .lst-kix_vi3ziu60akgv-2 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-2
    }

    ol.lst-kix_6gxo5agl0rwt-8.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-8 0
    }

    .lst-kix_6gxo5agl0rwt-0 > li:before {
        content: "" counter(lst-ctn-kix_6gxo5agl0rwt-0, decimal) ". "
    }

    .lst-kix_6gxo5agl0rwt-5 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-5
    }

    ol.lst-kix_fu1iokomnhaj-0 {
        list-style-type: none
    }

    ol.lst-kix_fu1iokomnhaj-5.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-5 0
    }

    ol.lst-kix_bh55r9bgsafo-4.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-4 0
    }

    ol.lst-kix_fu1iokomnhaj-2 {
        list-style-type: none
    }

    ol.lst-kix_fu1iokomnhaj-1 {
        list-style-type: none
    }

    ol.lst-kix_vi3ziu60akgv-3.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-3 0
    }

    ol.lst-kix_rqm22k7viro-3.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-3 0
    }

    .lst-kix_9fnqcvwn8ipt-7 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-7, lower-latin) ". "
    }

    ol.lst-kix_6gxo5agl0rwt-5.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-5 0
    }

    .lst-kix_rqm22k7viro-7 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-7
    }

    .lst-kix_fu1iokomnhaj-1 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-1
    }

    .lst-kix_mk6t5v5oqpss-8 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-8
    }

    ol.lst-kix_rqm22k7viro-0.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-0 0
    }

    .lst-kix_9fnqcvwn8ipt-0 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-0, decimal) ". "
    }

    ol.lst-kix_6gxo5agl0rwt-2.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-2 0
    }

    .lst-kix_9fnqcvwn8ipt-4 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-4
    }

    ol.lst-kix_fu1iokomnhaj-0.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-0 0
    }

    ol.lst-kix_vi3ziu60akgv-1.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-1 0
    }

    ol.lst-kix_bh55r9bgsafo-3.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-3 0
    }

    ol.lst-kix_mk6t5v5oqpss-5.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-5 0
    }

    ol.lst-kix_mk6t5v5oqpss-8.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-8 0
    }

    .lst-kix_bh55r9bgsafo-8 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-8
    }

    ol.lst-kix_9fnqcvwn8ipt-7.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-7 0
    }

    .lst-kix_bh55r9bgsafo-0 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-0
    }

    ol.lst-kix_fu1iokomnhaj-3.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-3 0
    }

    .lst-kix_371pnvembmsw-5 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-5
    }

    .lst-kix_fu1iokomnhaj-0 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-0
    }

    .lst-kix_bh55r9bgsafo-7 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-7, lower-latin) ". "
    }

    ol.lst-kix_6gxo5agl0rwt-0 {
        list-style-type: none
    }

    ol.lst-kix_6gxo5agl0rwt-2 {
        list-style-type: none
    }

    ol.lst-kix_6gxo5agl0rwt-1 {
        list-style-type: none
    }

    ol.lst-kix_fu1iokomnhaj-1.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-1 0
    }

    ol.lst-kix_6gxo5agl0rwt-4 {
        list-style-type: none
    }

    ol.lst-kix_6gxo5agl0rwt-3 {
        list-style-type: none
    }

    ol.lst-kix_6gxo5agl0rwt-6 {
        list-style-type: none
    }

    .lst-kix_bh55r9bgsafo-7 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-7
    }

    ol.lst-kix_vi3ziu60akgv-0.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-0 0
    }

    ol.lst-kix_6gxo5agl0rwt-5 {
        list-style-type: none
    }

    .lst-kix_bh55r9bgsafo-5 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-5, lower-roman) ". "
    }

    .lst-kix_vi3ziu60akgv-4 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-4
    }

    ol.lst-kix_6gxo5agl0rwt-3.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-3 0
    }

    ol.lst-kix_6gxo5agl0rwt-8 {
        list-style-type: none
    }

    ol.lst-kix_6gxo5agl0rwt-7 {
        list-style-type: none
    }

    .lst-kix_bh55r9bgsafo-3 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-3, decimal) ". "
    }

    .lst-kix_vi3ziu60akgv-0 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-0, decimal) ". "
    }

    .lst-kix_bh55r9bgsafo-1 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-1, lower-latin) ". "
    }

    ol.lst-kix_bh55r9bgsafo-0.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-0 0
    }

    .lst-kix_6gxo5agl0rwt-3 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-3
    }

    .lst-kix_vi3ziu60akgv-6 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-6, decimal) ". "
    }

    .lst-kix_mk6t5v5oqpss-1 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-1, lower-latin) ". "
    }

    .lst-kix_vi3ziu60akgv-4 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-4, lower-latin) ". "
    }

    .lst-kix_vi3ziu60akgv-8 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-8, lower-roman) ". "
    }

    .lst-kix_371pnvembmsw-4 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-4
    }

    .lst-kix_mk6t5v5oqpss-3 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-3, decimal) ". "
    }

    ol.lst-kix_bh55r9bgsafo-1.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-1 0
    }

    .lst-kix_vi3ziu60akgv-2 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-2, lower-roman) ". "
    }

    ol.lst-kix_rqm22k7viro-2.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-2 0
    }

    .lst-kix_371pnvembmsw-1 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-1, lower-latin) ". "
    }

    .lst-kix_371pnvembmsw-3 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-3, decimal) ". "
    }

    .lst-kix_fu1iokomnhaj-6 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-6, decimal) ". "
    }

    .lst-kix_fu1iokomnhaj-8 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-8, lower-roman) ". "
    }

    ol.lst-kix_fu1iokomnhaj-2.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-2 0
    }

    ol.lst-kix_6gxo5agl0rwt-4.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-4 0
    }

    .lst-kix_rqm22k7viro-8 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-8
    }

    ol.lst-kix_mk6t5v5oqpss-7.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-7 0
    }

    .lst-kix_fu1iokomnhaj-2 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-2, lower-roman) ". "
    }

    .lst-kix_fu1iokomnhaj-4 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-4, lower-latin) ". "
    }

    .lst-kix_fu1iokomnhaj-7 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-7
    }

    .lst-kix_9fnqcvwn8ipt-3 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-3
    }

    .lst-kix_mk6t5v5oqpss-7 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-7, lower-latin) ". "
    }

    .lst-kix_mk6t5v5oqpss-5 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-5, lower-roman) ". "
    }

    .lst-kix_bh55r9bgsafo-1 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-1
    }

    ol.lst-kix_bh55r9bgsafo-8 {
        list-style-type: none
    }

    ol.lst-kix_bh55r9bgsafo-6 {
        list-style-type: none
    }

    ol.lst-kix_bh55r9bgsafo-7 {
        list-style-type: none
    }

    .lst-kix_vi3ziu60akgv-1 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-1
    }

    .lst-kix_6gxo5agl0rwt-6 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-6
    }

    ol.lst-kix_371pnvembmsw-6.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-6 0
    }

    ol.lst-kix_bh55r9bgsafo-0 {
        list-style-type: none
    }

    ol.lst-kix_bh55r9bgsafo-1 {
        list-style-type: none
    }

    .lst-kix_fu1iokomnhaj-6 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-6
    }

    ol.lst-kix_bh55r9bgsafo-4 {
        list-style-type: none
    }

    ol.lst-kix_bh55r9bgsafo-5 {
        list-style-type: none
    }

    ol.lst-kix_bh55r9bgsafo-2 {
        list-style-type: none
    }

    ol.lst-kix_bh55r9bgsafo-2.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-2 0
    }

    ol.lst-kix_bh55r9bgsafo-3 {
        list-style-type: none
    }

    .lst-kix_371pnvembmsw-6 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-6, decimal) ". "
    }

    .lst-kix_bh55r9bgsafo-3 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-3
    }

    .lst-kix_371pnvembmsw-5 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-5, lower-roman) ". "
    }

    .lst-kix_371pnvembmsw-7 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-7, lower-latin) ". "
    }

    .lst-kix_371pnvembmsw-4 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-4, lower-latin) ". "
    }

    .lst-kix_371pnvembmsw-8 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-8, lower-roman) ". "
    }

    .lst-kix_mk6t5v5oqpss-2 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-2
    }

    ol.lst-kix_9fnqcvwn8ipt-0.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-0 0
    }

    .lst-kix_9fnqcvwn8ipt-1 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-1
    }

    ol.lst-kix_mk6t5v5oqpss-1 {
        list-style-type: none
    }

    ol.lst-kix_mk6t5v5oqpss-2 {
        list-style-type: none
    }

    .lst-kix_fu1iokomnhaj-8 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-8
    }

    ol.lst-kix_mk6t5v5oqpss-3 {
        list-style-type: none
    }

    ol.lst-kix_mk6t5v5oqpss-4 {
        list-style-type: none
    }

    ol.lst-kix_fu1iokomnhaj-7.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-7 0
    }

    ol.lst-kix_mk6t5v5oqpss-5 {
        list-style-type: none
    }

    ol.lst-kix_mk6t5v5oqpss-6 {
        list-style-type: none
    }

    ol.lst-kix_mk6t5v5oqpss-7 {
        list-style-type: none
    }

    ol.lst-kix_mk6t5v5oqpss-8 {
        list-style-type: none
    }

    .lst-kix_6gxo5agl0rwt-4 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-4
    }

    .lst-kix_bh55r9bgsafo-5 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-5
    }

    ol.lst-kix_rqm22k7viro-7.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-7 0
    }

    ol.lst-kix_rqm22k7viro-1.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-1 0
    }

    ol.lst-kix_vi3ziu60akgv-5.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-5 0
    }

    ol.lst-kix_6gxo5agl0rwt-6.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-6 0
    }

    .lst-kix_vi3ziu60akgv-3 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-3
    }

    .lst-kix_rqm22k7viro-1 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-1
    }

    ol.lst-kix_mk6t5v5oqpss-0 {
        list-style-type: none
    }

    .lst-kix_mk6t5v5oqpss-4 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-4
    }

    ol.lst-kix_371pnvembmsw-1.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-1 0
    }

    .lst-kix_bh55r9bgsafo-8 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-8, lower-roman) ". "
    }

    ol.lst-kix_9fnqcvwn8ipt-6.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-6 0
    }

    ol.lst-kix_6gxo5agl0rwt-0.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-0 0
    }

    ol.lst-kix_mk6t5v5oqpss-4.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-4 0
    }

    ol.lst-kix_371pnvembmsw-0 {
        list-style-type: none
    }

    ol.lst-kix_vi3ziu60akgv-4.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-4 0
    }

    ol.lst-kix_371pnvembmsw-4 {
        list-style-type: none
    }

    ol.lst-kix_6gxo5agl0rwt-7.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-7 0
    }

    ol.lst-kix_371pnvembmsw-3 {
        list-style-type: none
    }

    .lst-kix_371pnvembmsw-3 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-3
    }

    ol.lst-kix_371pnvembmsw-2 {
        list-style-type: none
    }

    ol.lst-kix_371pnvembmsw-1 {
        list-style-type: none
    }

    .lst-kix_9fnqcvwn8ipt-8 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-8
    }

    ol.lst-kix_9fnqcvwn8ipt-5.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-5 0
    }

    .lst-kix_fu1iokomnhaj-2 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-2
    }

    ol.lst-kix_371pnvembmsw-0.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-0 0
    }

    ol.lst-kix_mk6t5v5oqpss-3.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-3 0
    }

    .lst-kix_rqm22k7viro-3 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-3
    }

    .lst-kix_vi3ziu60akgv-5 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-5
    }

    ol.lst-kix_rqm22k7viro-6.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-6 0
    }

    .lst-kix_9fnqcvwn8ipt-1 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-1, lower-latin) ". "
    }

    ol.lst-kix_371pnvembmsw-7.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-7 0
    }

    .lst-kix_9fnqcvwn8ipt-2 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-2, lower-roman) ". "
    }

    .lst-kix_6gxo5agl0rwt-8 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-8
    }

    .lst-kix_9fnqcvwn8ipt-4 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-4, lower-latin) ". "
    }

    .lst-kix_6gxo5agl0rwt-2 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-2
    }

    ol.lst-kix_6gxo5agl0rwt-1.start {
        counter-reset: lst-ctn-kix_6gxo5agl0rwt-1 0
    }

    .lst-kix_9fnqcvwn8ipt-3 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-3, decimal) ". "
    }

    .lst-kix_9fnqcvwn8ipt-6 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-6, decimal) ". "
    }

    .lst-kix_9fnqcvwn8ipt-5 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-5, lower-roman) ". "
    }

    .lst-kix_vi3ziu60akgv-7 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-7
    }

    ol.lst-kix_fu1iokomnhaj-6.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-6 0
    }

    .lst-kix_6gxo5agl0rwt-0 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-0
    }

    .lst-kix_9fnqcvwn8ipt-8 > li:before {
        content: "" counter(lst-ctn-kix_9fnqcvwn8ipt-8, lower-roman) ". "
    }

    .lst-kix_371pnvembmsw-6 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-6
    }

    .lst-kix_vi3ziu60akgv-8 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-8
    }

    .lst-kix_9fnqcvwn8ipt-5 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-5
    }

    .lst-kix_rqm22k7viro-6 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-6
    }

    ol.lst-kix_rqm22k7viro-0 {
        list-style-type: none
    }

    .lst-kix_mk6t5v5oqpss-7 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-7
    }

    .lst-kix_9fnqcvwn8ipt-7 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-7
    }

    ol.lst-kix_371pnvembmsw-8 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-7 {
        list-style-type: none
    }

    ol.lst-kix_371pnvembmsw-7 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-8 {
        list-style-type: none
    }

    ol.lst-kix_371pnvembmsw-6 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-5 {
        list-style-type: none
    }

    ol.lst-kix_371pnvembmsw-2.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-2 0
    }

    ol.lst-kix_371pnvembmsw-5 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-6 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-3 {
        list-style-type: none
    }

    ol.lst-kix_bh55r9bgsafo-6.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-6 0
    }

    ol.lst-kix_rqm22k7viro-4 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-1 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-2 {
        list-style-type: none
    }

    .lst-kix_fu1iokomnhaj-3 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-3
    }

    ol.lst-kix_9fnqcvwn8ipt-1.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-1 0
    }

    .lst-kix_6gxo5agl0rwt-1 > li {
        counter-increment: lst-ctn-kix_6gxo5agl0rwt-1
    }

    ol.lst-kix_371pnvembmsw-5.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-5 0
    }

    ol.lst-kix_mk6t5v5oqpss-2.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-2 0
    }

    ol.lst-kix_9fnqcvwn8ipt-4.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-4 0
    }

    .lst-kix_vi3ziu60akgv-6 > li {
        counter-increment: lst-ctn-kix_vi3ziu60akgv-6
    }

    .lst-kix_bh55r9bgsafo-6 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-6, decimal) ". "
    }

    .lst-kix_371pnvembmsw-8 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-8
    }

    ol.lst-kix_vi3ziu60akgv-7.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-7 0
    }

    .lst-kix_fu1iokomnhaj-4 > li {
        counter-increment: lst-ctn-kix_fu1iokomnhaj-4
    }

    .lst-kix_bh55r9bgsafo-2 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-2, lower-roman) ". "
    }

    .lst-kix_bh55r9bgsafo-4 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-4, lower-latin) ". "
    }

    ol.lst-kix_bh55r9bgsafo-7.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-7 0
    }

    .lst-kix_rqm22k7viro-5 > li {
        counter-increment: lst-ctn-kix_rqm22k7viro-5
    }

    .lst-kix_mk6t5v5oqpss-0 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-0
    }

    ol.lst-kix_9fnqcvwn8ipt-2.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-2 0
    }

    ol.lst-kix_vi3ziu60akgv-1 {
        list-style-type: none
    }

    ol.lst-kix_mk6t5v5oqpss-1.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-1 0
    }

    ol.lst-kix_vi3ziu60akgv-0 {
        list-style-type: none
    }

    .lst-kix_bh55r9bgsafo-0 > li:before {
        content: "" counter(lst-ctn-kix_bh55r9bgsafo-0, decimal) ". "
    }

    ol.lst-kix_vi3ziu60akgv-5 {
        list-style-type: none
    }

    ol.lst-kix_371pnvembmsw-4.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-4 0
    }

    ol.lst-kix_vi3ziu60akgv-4 {
        list-style-type: none
    }

    ol.lst-kix_vi3ziu60akgv-3 {
        list-style-type: none
    }

    .lst-kix_mk6t5v5oqpss-6 > li {
        counter-increment: lst-ctn-kix_mk6t5v5oqpss-6
    }

    ol.lst-kix_vi3ziu60akgv-2 {
        list-style-type: none
    }

    ol.lst-kix_vi3ziu60akgv-8 {
        list-style-type: none
    }

    ol.lst-kix_rqm22k7viro-8.start {
        counter-reset: lst-ctn-kix_rqm22k7viro-8 0
    }

    ol.lst-kix_vi3ziu60akgv-7 {
        list-style-type: none
    }

    ol.lst-kix_vi3ziu60akgv-6 {
        list-style-type: none
    }

    .lst-kix_371pnvembmsw-1 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-1
    }

    .lst-kix_mk6t5v5oqpss-2 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-2, lower-roman) ". "
    }

    ol.lst-kix_bh55r9bgsafo-8.start {
        counter-reset: lst-ctn-kix_bh55r9bgsafo-8 0
    }

    .lst-kix_vi3ziu60akgv-5 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-5, lower-roman) ". "
    }

    .lst-kix_vi3ziu60akgv-7 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-7, lower-latin) ". "
    }

    .lst-kix_371pnvembmsw-0 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-0, decimal) ". "
    }

    .lst-kix_371pnvembmsw-7 > li {
        counter-increment: lst-ctn-kix_371pnvembmsw-7
    }

    .lst-kix_mk6t5v5oqpss-0 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-0, decimal) ". "
    }

    .lst-kix_mk6t5v5oqpss-4 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-4, lower-latin) ". "
    }

    .lst-kix_371pnvembmsw-2 > li:before {
        content: "" counter(lst-ctn-kix_371pnvembmsw-2, lower-roman) ". "
    }

    .lst-kix_fu1iokomnhaj-7 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-7, lower-latin) ". "
    }

    ol.lst-kix_vi3ziu60akgv-6.start {
        counter-reset: lst-ctn-kix_vi3ziu60akgv-6 0
    }

    ol.lst-kix_9fnqcvwn8ipt-3.start {
        counter-reset: lst-ctn-kix_9fnqcvwn8ipt-3 0
    }

    .lst-kix_vi3ziu60akgv-1 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-1, lower-latin) ". "
    }

    .lst-kix_vi3ziu60akgv-3 > li:before {
        content: "" counter(lst-ctn-kix_vi3ziu60akgv-3, decimal) ". "
    }

    ol.lst-kix_mk6t5v5oqpss-0.start {
        counter-reset: lst-ctn-kix_mk6t5v5oqpss-0 0
    }

    .lst-kix_fu1iokomnhaj-3 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-3, decimal) ". "
    }

    ol.lst-kix_371pnvembmsw-3.start {
        counter-reset: lst-ctn-kix_371pnvembmsw-3 0
    }

    .lst-kix_fu1iokomnhaj-1 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-1, lower-latin) ". "
    }

    .lst-kix_fu1iokomnhaj-5 > li:before {
        content: "" counter(lst-ctn-kix_fu1iokomnhaj-5, lower-roman) ". "
    }

    .lst-kix_9fnqcvwn8ipt-6 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-6
    }

    .lst-kix_mk6t5v5oqpss-8 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-8, lower-roman) ". "
    }

    .lst-kix_mk6t5v5oqpss-6 > li:before {
        content: "" counter(lst-ctn-kix_mk6t5v5oqpss-6, decimal) ". "
    }

    ol.lst-kix_fu1iokomnhaj-8.start {
        counter-reset: lst-ctn-kix_fu1iokomnhaj-8 0
    }

    .lst-kix_bh55r9bgsafo-4 > li {
        counter-increment: lst-ctn-kix_bh55r9bgsafo-4
    }

    .lst-kix_9fnqcvwn8ipt-0 > li {
        counter-increment: lst-ctn-kix_9fnqcvwn8ipt-0
    }

    ol {
        margin: 0;
        padding: 0
    }

    table td, table th {
        padding: 0
    }

    .c9 {
        border-right-style: solid;
        padding: 5pt 5pt 5pt 5pt;
        border-bottom-color: #000000;
        border-top-width: 2.2pt;
        border-right-width: 2.2pt;
        border-left-color: #000000;
        vertical-align: top;
        border-right-color: #000000;
        border-left-width: 1pt;
        border-top-style: solid;
        border-left-style: solid;
        border-bottom-width: 2.2pt;
        width: 179.2pt;
        border-top-color: #000000;
        border-bottom-style: solid
    }

    .c13 {
        border-right-style: solid;
        padding: 5pt 5pt 5pt 5pt;
        border-bottom-color: #000000;
        border-top-width: 2.2pt;
        border-right-width: 1pt;
        border-left-color: #000000;
        vertical-align: top;
        border-right-color: #000000;
        border-left-width: 2.2pt;
        border-top-style: solid;
        border-left-style: solid;
        border-bottom-width: 2.2pt;
        width: 262.5pt;
        border-top-color: #000000;
        border-bottom-style: solid
    }

    .c6 {
        padding-top: 0pt;
        text-indent: 36pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c14 {
        margin-left: 36pt;
        padding-top: 0pt;
        padding-bottom: 12pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify;
        height: 11pt
    }

    .c12 {
        margin-left: 36pt;
        padding-top: 0pt;
        padding-left: 0pt;
        padding-bottom: 12pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c2 {
        margin-left: 90pt;
        padding-top: 0pt;
        text-indent: -36pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c3 {
        margin-left: 56pt;
        padding-top: 0pt;
        text-indent: -19pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c15 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: left;
        height: 11pt
    }

    .c1 {
        margin-left: 36pt;
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c4 {
        margin-left: 54pt;
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c5 {
        margin-left: 37pt;
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c0 {
        color: #000000;
        font-weight: 400;
        text-decoration: none;
        vertical-align: baseline;
        font-size: 10pt;
        font-family: "Arial";
        font-style: normal
    }

    .c7 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: justify
    }

    .c10 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: center
    }

    .c18 {
        border-spacing: 0;
        border-collapse: collapse;
    }

    .c17 {
        background-color: #ffffff;
        padding: 72pt 72pt 72pt 72pt
    }

    .c8 {
        margin-left: 90pt;
        text-indent: -18pt
    }

    .c11 {
        padding: 0;
        margin: 0
    }

    .c16 {
        height: 60pt
    }

    .title {
        padding-top: 0pt;
        color: #000000;
        font-size: 26pt;
        padding-bottom: 3pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    .subtitle {
        padding-top: 0pt;
        color: #666666;
        font-size: 15pt;
        padding-bottom: 16pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    li {
        color: #000000;
        font-size: 11pt;
        font-family: "Arial"
    }

    p {
        margin: 0;
        color: #000000;
        font-size: 11pt;
        font-family: "Arial"
    }

    h1 {
        padding-top: 20pt;
        color: #000000;
        font-size: 20pt;
        padding-bottom: 6pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h2 {
        padding-top: 18pt;
        color: #000000;
        font-size: 16pt;
        padding-bottom: 6pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h3 {
        padding-top: 16pt;
        color: #434343;
        font-size: 14pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h4 {
        padding-top: 14pt;
        color: #666666;
        font-size: 12pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h5 {
        padding-top: 12pt;
        color: #666666;
        font-size: 11pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h6 {
        padding-top: 12pt;
        color: #666666;
        font-size: 11pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        font-style: italic;
        orphans: 2;
        widows: 2;
        text-align: left
    }</style>
<div><p class="c10"><span class="c0">Договор № __________</span></p>
<p class="c10"><span class="c0">о предоставлении услуг доставки международных экспресс-отправлений</span></p>
<p class="c10"><span class="c0">&nbsp;</span></p>
<p class="c10"><span class="c0">г.
 Черноморск &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «__» 
_________2018 г.</span></p>
<p class="c7"><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
<p class="c7"><span class="c0">&nbsp;
 &nbsp; _____________, в дальнейшем Исполнитель, в лице 
________________, действующего на основании Устава, и 
__________________________________________, в дальнейшем Заказчик, в 
лице _____________________________________, действующего на основании 
Устава, с другой стороны, а вместе именуемые Стороны заключили настоящий
 Договор о предоставлении услуг доставки международных 
экспресс-отправлений (далее – Договор) о нижеследующем:</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">Настоящий
 Договор по своей правовой природе является смешанным и содержит в себе 
элементы договоров транспортного экспедирования, перевозки, 
предоставления услуг и договора поручения.</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">К
 отношениям, определенных настоящим Договором, Стороны применяют также 
Условия предоставления услуг, которые являются публичными и размещены на
 сайте :.</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_rqm22k7viro-0 start" start="1">
    <li class="c12"><span class="c0">Предмет Договора</span></li>
</ol>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">1.1.
 &nbsp; Исполнитель обязуется предоставить Заказчику услуги по 
международной экспресс-доставке международных экспресс-отправлений 
(далее – МЭО), которые определены в п. 1.2. Договора, а Заказчик 
обязуется принять предоставленные услуги и своевременно произвести 
оплату за предоставленные Исполнителем услуги.</span></p>
<p class="c3"><span class="c0">1.2. &nbsp; В соответствии с настоящим Договором Исполнитель предоставляет следующие услуги:</span>
</p>
<p class="c7 c8"><span class="c0">-
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;организация перевозки МЭО любыми 
видами транспорта, а именно: прием и выдача МЭО; прием от Заказчика 
информации и сопроводительных документов на МЭО; оформление 
товарно-транспортных документов; организация перевозки МЭО из-за границы
 и за границу Украины, организация перевозки МЭО по территории Украины;</span></p>
<p class="c7 c8"><span class="c0">-
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;таможенное оформление МЭО, а именно: 
оформление и размещение МЭО на центральной сортировочной станции (далее –
 ЦСС); декларирование и предъявление МЭО для таможенного контроля; 
оплата соответствующих таможенных платежей, сборов и налогов;</span></p>
<p class="c7 c8"><span class="c0">-
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;хранение МЭО на ЦСС, осуществление 
складских операций с МЭО (упаковка, распаковка, накопление и др.);</span></p>
<p class="c7 c8"><span class="c0">- &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;другие услуги, предусмотренные Условиями предоставления услуг.</span>
</p>
<p class="c3"><span class="c0">1.3. &nbsp; Окончательный объем конкретной услуги определяется в Приложении № 1 к Договору.</span>
</p>
<p class="c3"><span class="c0">1.4.
 &nbsp; Под МЭО в настоящем Договоре следует понимать надлежащим образом
 упакованные международные отправления с документами или товарным 
вложением, которые принимаются, обрабатываются и перевозятся любым видом
 транспорта по международному транспортному документу с целью доставки 
получателю ускоренным способом в определенный срок.</span></p>
<p class="c3"><span class="c0">1.5.
 &nbsp; Путем подписания настоящего Договора Заказчик подтверждает, что 
он ознакомлен и согласен с Условиями предоставления услуг, размещенными 
на сайте ___________, и обязуется их выполнять.</span></p>
<p class="c5"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_9fnqcvwn8ipt-0 start" start="1">
    <li class="c12"><span class="c0">Права и обязанности Сторон</span></li>
</ol>
<p class="c1"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">2.1. &nbsp; Права Исполнителя:</span></p>
<p class="c2"><span class="c0">2.1.1.
 &nbsp; &nbsp; &nbsp; &nbsp; Требовать от Заказчика документы необходимы
 для надлежащего выполнения условий ст. 1. настоящего Договора.</span></p>
<p class="c2"><span class="c0">2.1.2. &nbsp; &nbsp; &nbsp; &nbsp; Требовать от Заказчика оплаты услуг, предусмотренных в ст. 1. настоящего Договора.</span>
</p>
<p class="c2"><span class="c0">2.1.3.
 &nbsp; &nbsp; &nbsp; &nbsp; Осуществлять повторное взвешивание и 
измерение МЭО с целью подтверждения правильности расчетов заявленного 
Заказчиком веса. Стороны пришли к соглашению о том, что показатели, 
определенные при повторном взвешивании и измерении МЭО Исполнителем на 
центральной сортировочной станции (по адресу __________________, 
Украина), является основанием для осуществления перерасчета стоимости 
предоставленных услуг.</span></p>
<p class="c2"><span class="c0">2.1.4.
&nbsp; &nbsp; &nbsp; &nbsp; Приостановить исполнение своих обязательств 
по настоящему Договору и придержать МЭО / МГО в случае невыполнения 
Заказчиком своих обязанностей, предусмотренных настоящим Договором, до 
момента их полного исполнения.</span></p>
<p class="c2"><span class="c0">2.1.5.
 &nbsp; &nbsp; &nbsp; &nbsp; Предлагать внести изменения в согласованные
 тарифы стоимости услуг (Приложение № 1). В случае неподписания 
Заказчиком дополнительного соглашения об изменении условий Договора в 
течение 15 календарных дней со дня получения такого предложения, 
Исполнитель вправе прекратить предоставление услуг по настоящему 
Договору без применения к нему каких-либо штрафных санкций и/или 
возмещений.</span></p>
<p class="c4"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">2.2. &nbsp; Обязанности Исполнителя:</span></p>
<p class="c2"><span
        class="c0">2.2.1. &nbsp; &nbsp; &nbsp; &nbsp; предоставлять услуги в соответствии со ст. 1 Договора;</span></p>
<p class="c2"><span class="c0">2.2.2.
 &nbsp; &nbsp; &nbsp; &nbsp; составлять Акт о несоответствии в случае 
выявления фактов повреждения МЭО, его упаковки или маркировки либо о его
 утери;</span></p>
<p class="c2"><span class="c0">2.2.3. &nbsp; &nbsp; &nbsp; &nbsp; по требованию Заказчика информировать его о ходе предоставления услуг;</span>
</p>
<p class="c2"><span class="c0">2.2.4.
 &nbsp; &nbsp; &nbsp; &nbsp; Исполнитель не обязан осуществлять проверку
 содержания МЭО, упакованного Заказчиком, и соответствия упаковки 
особенностям МЭО, требованиям действующего законодательства и 
государственным стандартам.</span></p>
<p class="c4"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">2.3. &nbsp; Права Заказчика:</span></p>
<p class="c2"><span class="c0">2.3.1. &nbsp; &nbsp; &nbsp; &nbsp; на получение услуг в соответствии с условиями Договора;</span>
</p>
<p class="c2"><span
        class="c0">2.3.2. &nbsp; &nbsp; &nbsp; &nbsp; на инвентаризацию своего МЭО, при необходимости;</span></p>
<p class="c2"><span class="c0">2.3.3. &nbsp; &nbsp; &nbsp; &nbsp; требовать от Исполнителя информацию о предоставлении услуг, предусмотренных Договором.</span>
</p>
<p class="c4"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">2.4. &nbsp; Обязанности Заказчика:</span></p>
<p class="c2"><span class="c0">2.4.1. &nbsp; &nbsp; &nbsp; &nbsp; до момента передачи МЭО Исполнителю ознакомиться с Условиями предоставления услуг;</span>
</p>
<p class="c2"><span class="c0">2.4.2.
 &nbsp; &nbsp; &nbsp; &nbsp; правильно и разборчиво заполнить и 
подписать международную экспресс-накладную (далее - МЭН), сообщив 
Исполнителю информацию о МЭО (его содержании);</span></p>
<p class="c2"><span class="c0">2.4.3.
 &nbsp; &nbsp; &nbsp; &nbsp; предоставить Исполнителю все 
сопроводительные документы и информацию, необходимые для надлежащего 
предоставления услуг Исполнителем, и по требованию Исполнителя 
предоставить содержание МЭО для осмотра;</span></p>
<p class="c2"><span class="c0">2.4.4.
 &nbsp; &nbsp; &nbsp; &nbsp; в случае получения МЭО в ненадлежащем 
состоянии (повреждение, недостача и т.п.) зафиксировать его состояние в 
соответствующем акте, с обязательным участием представителей Заказчика и
 Исполнителя. Акт приема-передачи (в котором фиксируются повреждения 
МЭО, недостача и т.д.) составляется во время передачи МЭО Заказчику;</span></p>
<p class="c2"><span class="c0">2.4.5.
 &nbsp; &nbsp; &nbsp; &nbsp; запаковать (и нанести необходимую 
маркировку) МЭО должным образом для его сохранения во время 
транспортировки и погрузочно-разгрузочных работ;</span></p>
<p class="c2"><span class="c0">2.4.6. &nbsp; &nbsp; &nbsp; &nbsp; информировать Исполнителя об условиях складирования и хранения МЭО на ЦСС;</span>
</p>
<p class="c2"><span class="c0">2.4.7. &nbsp; &nbsp; &nbsp; &nbsp; своевременно оплачивать услуги Исполнителя;</span></p>
<p class="c2"><span class="c0">2.4.8.
 &nbsp; &nbsp; &nbsp; &nbsp; подписывать и отправлять Исполнителю акты 
предоставленных услуг в 5-дневный срок с момента их получения.</span></p>
<p class="c4"><span class="c0">&nbsp;</span></p>
<p class="c4"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_6gxo5agl0rwt-0 start" start="1">
    <li class="c12"><span class="c0">Стоимость услуг и порядок оплаты. Передача-приемка предоставленных услуг.</span>
    </li>
</ol>
<p class="c1"><span class="c0">&nbsp;</span></p>
<p class="c3"><span
        class="c0">3.1. &nbsp; Общая стоимость Договора составляет сумму всех Актов по настоящему Договору.</span></p>
<p class="c3"><span class="c0">3.2.
 &nbsp; Оплата стоимости услуг осуществляется Заказчиком в украинской 
национальной валюте (гривне). Датой оплаты по настоящему Договору 
является дата зачисления денежных средств на банковский счет 
Исполнителя. Заказчик осуществляет оплату услуг Исполнителя в течении 7 
(семи) дней с даты получения Акта приема-передачи предоставленных услуг 
(ранее и далее – Акт). Оплата может осуществляться авансом. Списание 
авансированных денежных средств производится в соответствии с Актами.</span></p>
<p class="c3"><span class="c0">3.3. &nbsp; Стоимость услуг определяется в Приложении № 1 к Договору.</span></p>
<p class="c3"><span class="c0">3.4. &nbsp; Пересмотр тарифов осуществляется в соответствии с п.п. 2.1.5 Договора.</span>
</p>
<p class="c3"><span class="c0">3.5.
 &nbsp; Передача Заказчиком МЭО Исполнителю для предоставления услуг и 
подписание МЭН является свидетельством того, что Заказчик согласен с 
тарифами Исполнителя.</span></p>
<p class="c3"><span class="c0">3.6.
&nbsp; В конце каждого месяца Стороны подписывают Акт, которым 
удостоверяется факт предоставления услуг за указанный период и который 
является основанием для оплаты.</span></p>
<p class="c3"><span
        class="c0">3.7. &nbsp; Подписание Актов происходит в порядке, предусмотренном в п 2.4.8. Договора.</span></p>
<p class="c5"><span class="c0">&nbsp;</span></p>
<p class="c5"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_fu1iokomnhaj-0 start" start="1">
    <li class="c12"><span class="c0">Ответственность Сторон</span></li>
</ol>
<p class="c5"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">4.1.
 &nbsp; Исполнитель несет ответственность за сохранность МЭО Заказчика, а
 также за механические и другие повреждения, возникшие вследствие 
ненадлежащего виновного поведения Исполнителя.</span></p>
<p class="c3"><span class="c0">4.2.
 &nbsp; Исполнитель не несет ответственности за качество, комплектацию и
 количество принятых на ЦСС МЭО, если их упаковка нарушена или 
повреждена.</span></p>
<p class="c3"><span class="c0">4.3. &nbsp;
Заказчик передает для таможенной обработки и хранения МЭО, ввоз / вывоз 
которых не запрещен законодательством Украины.</span></p>
<p class="c3"><span class="c0">4.4.
 &nbsp; За несвоевременную оплату предоставленных услуг, Заказчик 
выплачивает Исполнителю пеню в размере двойной учетной ставки 
Национального банка Украины от просроченной суммы за каждый день 
просрочки и 20% годовых от суммы задолженности, в том числе возмещает 
Исполнителю причиненные убытки и компенсирует упущенную выгоду.</span></p>
<p class="c3"><span class="c0">4.5.
 &nbsp; Исполнение Заказчиком своих обязательств по оплате за 
предоставленные Исполнителем услуги обеспечивается залогом – МЭО 
переданным Заказчиком. В случае невыполнения Заказчиком своих 
обязательств, Исполнитель приобретает право обращения взыскания на 
предмет залога по собственному решению.</span></p>
<p class="c3"><span class="c0">4.6.
 &nbsp; Любая из Сторон не несет ответственности за невыполнение условий
 настоящего Договора в силу форс-мажорных обстоятельств. При 
возникновении форс-мажорных обстоятельств, из-за которых Стороны не 
могут выполнять свои обязательства, каждая из Сторон обязуется уведомить
 об этом другую Сторону в течение 5-ти дней и подтвердить вышеуказанные 
обстоятельства в порядке, определенном действующим законодательством 
Украины.</span></p>
<p class="c3"><span class="c0">4.7. &nbsp; Заказчик
несет ответственность за достоверность данных в документах, соответствие
 их содержанию, характеру, качеству и количеству МЭО, предоставленных им
 для осуществления таможенного оформления, а также соответствие упаковки
 особенностям МЭО, требованиям действующего в Украине законодательства и
 требованиям законодательства страны назначения (транзита).</span></p>
<p class="c3"><span class="c0">4.8.
 &nbsp; Исполнитель несет ответственность, в случае некачественного 
выполнения своих обязательств перед Заказчиком в пределах, установленных
 действующим в Украине законодательством и настоящим Договором.</span></p>
<p class="c3"><span class="c0">4.9.
 &nbsp; Исполнитель освобождается от ответственности за просрочку в 
доставке МЭО, если просрочка произошла не по его вине, в частности, но 
не исключительно, если такая просрочка вызвана несвоевременным 
предоставлением Заказчиком документов и информации, необходимых для 
выполнения таможенных процедур и условий Договора, несвоевременным 
осуществлением таможенных процедур по вине таможенных органов и тому 
подобное.</span></p>
<p class="c3"><span class="c0">4.10. &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Заказчик 
проинформирован об ответственности за нарушение Таможенного 
законодательства Украины.</span></p>
<p class="c3"><span class="c0">4.11.
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Исполнитель не несет ответственности по обязательствам Заказчика, 
которые последний непосредственно выполнял перед таможенными органами.</span></p>
<p class="c3"><span class="c0">4.12.
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Коды УКТВЭД товаров, подлежащих таможенному оформлению, 
определяются согласно пояснений и документов, предоставляемых 
Заказчиком. Однако решение органов фискальной службы по классификации 
товаров для таможенных целей являются обязательными, согласно ст. 69 
Таможенного кодекса Украины.</span></p>
<p class="c1"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_371pnvembmsw-0 start" start="1">
    <li class="c12"><span class="c0">Срок действия Договора</span></li>
</ol>
<p class="c5"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">5.1.
 &nbsp; Договор вступает в силу с момента его подписания Сторонами и 
действует до 31.12.2019 г., но в любом случае до полного исполнения 
Сторонами своих обязательств по настоящему Договору. В случае, если ни 
одна из Сторон письменно за 30 (тридцать) календарных дней до окончания 
срока действия Договора не уведомит другую Сторону о прекращении 
действия Договора, срок его действия пролонгируется (продлевается) на 
каждый следующий год на тех же условиях (количество пролонгаций не 
ограничено).</span></p>
<p class="c3"><span class="c0">5.2. &nbsp;
Настоящий Договор может быть досрочно расторгнут в одностороннем порядке
 при условии письменного предупреждения об этом другой Стороны не 
позднее чем за 20 календарных дней до желаемой даты расторжения.</span></p>
<p class="c3"><span class="c0">5.3.
 &nbsp; Окончание срока настоящего Договора или его расторжение, не 
освобождает Стороны от ответственности за его нарушение, которое имело 
место во время действия настоящего Договора.</span></p>
<p class="c3"><span class="c0">5.4.
 &nbsp; Обязательства Сторон, возникшие в период действия настоящего 
Договора, но не были выполнены на момент прекращения его действия, 
продолжают действовать после прекращения настоящего Договора до полного 
выполнения таких обязательств Сторонами.</span></p>
<p class="c5"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_mk6t5v5oqpss-0 start" start="1">
    <li class="c12"><span class="c0">Порядок разрешения споров</span></li>
</ol>
<p class="c1"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">6.1. &nbsp; Разногласия, возникающие при выполнении настоящего Договора, должны решаться путем переговоров между Сторонами.</span>
</p>
<p class="c3"><span class="c0">6.2.
 &nbsp; В случае невозможности урегулирования разногласий путем 
переговоров и досудебного урегулирования спора, споры между Сторонами 
подлежат разрешению в соответствии с действующим в Украине 
законодательством.</span></p>
<p class="c3"><span class="c0">6.3. &nbsp;
Акты осмотра МЭО, коммерческие акты, экспертные заключения и другие 
документы, содержащие информацию о характере и причине повреждений, 
порчи, утраты, недостачи содержания МЭО, которые были составлены без 
участия уполномоченного представителя Исполнителя, недействительны и при
 рассмотрении претензии не принимаются во внимание.</span></p>
<p class="c5"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_vi3ziu60akgv-0 start" start="1">
    <li class="c12"><span class="c0">Другие условия</span></li>
</ol>
<p class="c5"><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">7.1.
 &nbsp; После подписания настоящего Договора, все предыдущие переговоры,
 переписки, протоколы о намерениях и любые другие устные и письменные 
договоренности Сторон по вопросам, так или иначе касаются этого 
Договора, теряют юридическую силу.</span></p>
<p class="c3"><span class="c0">7.2.
 &nbsp; Изменения в настоящий Договор могут быть внесении по взаимному 
согласию Сторон, которые оформляются дополнительными соглашениями к 
настоящему Договору.</span></p>
<p class="c3"><span class="c0">7.3.
&nbsp; Изменения и дополнения, дополнительные соглашения и приложения к 
настоящему Договору, акты и любые другие документы, заключенные 
Сторонами в соответствии с настоящим Договором, являются его 
неотъемлемой частью и имеют юридическую силу в случае, если они 
совершены в письменной форме и подписаны Сторонами.</span></p>
<p class="c3"><span class="c0">7.4.
 &nbsp; Ни одна из Сторон не имеет права передавать свои права по 
настоящему Договору, а именно: уступка права требования и (или) перевода
 долга по настоящему Договору третьим лицам без письменного согласия 
другой Стороны.</span></p>
<p class="c3"><span class="c0">7.5. &nbsp; Все
 изменения и дополнения к настоящему Договору имеют юридическую силу и 
могут приниматься во внимание исключительно при условии, что они в 
каждом конкретном случае датированы и заверены подписями Сторон.</span></p>
<p class="c3"><span class="c0">7.6.
 &nbsp; Стороны несут ответственность за правильность указанных в 
настоящем Договоре реквизитов и обязуется своевременно в письменной 
форме сообщать об их изменении, а в случае несообщения несут риск 
наступления связанных с этим неблагоприятных последствий.</span></p>
<p class="c3"><span class="c0">7.7.
 &nbsp; Недействительность (признание недействительным) любого из 
положений (условий) настоящего Договора и (или) приложений к нему, не 
является основанием для недействительности (признания недействительным) 
других положений (условий) настоящего Договора и (или) Договора в целом.</span></p>
<p class="c3"><span class="c0">7.8. &nbsp; Срок хранения МЭО на ЦСС под таможенным контролем, не может превышать 30 дней.</span>
</p>
<p class="c3"><span class="c0">7.9.
 &nbsp; Прием и выдача МЭО осуществляется с 9:00 до 18:00 каждый день, 
кроме субботы, воскресенья, праздничных и нерабочих дней, 
предусмотренных действующим в Украине законодательством.</span></p>
<p class="c3"><span class="c0">7.10.
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Стороны обязуются не разглашать конфиденциальную информацию другой
 Стороны, полученной в связи с исполнением обязательств по настоящему 
Договору.</span></p>
<p class="c3"><span class="c0">7.11. &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Руководствуясь действующим в Украине законодательством, 
регулирующим правоотношения в сфере получения, сбора, накопления, 
обработки и защиты персональных данных, Заказчик дает свое согласие на 
обработку персональных данных для выполнения настоящего Договора. 
Настоящее согласие предоставляется на осуществление любых действий в 
отношении законным образом полученных персональных данных Заказчика, 
которые необходимы или желательны для достижения договорных целей, а 
именно сбор, систематизацию, накопление, хранение, уточнение 
(обновление, изменение), использование в пределах, необходимых для 
вышеуказанных договорных целей, распространение (в том числе передача) в
 пределах, необходимых для вышеуказанных договорных целей, 
обезличивание, уничтожение, а также осуществление любых иных действий с 
полученными персональными данными в пределах, необходимых для 
вышеуказанных целей и с учетом действующего в Украине законодательства.</span></p>
<p class="c3"><span class="c0">7.12.
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Во всем остальном, что не предусмотрено настоящим Договором, 
Стороны руководствуются действующим в Украине законодательством.</span></p>
<p class="c3"><span class="c0">7.13.
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Настоящий Договор заключен при полном понимании Сторонами его 
условий и терминологии на русском языке в 2 (двух) экземплярах, имеющих 
одинаковую юридическую силу, по одному для каждой Стороны.</span></p>
<p class="c3"><span class="c0">7.14.
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Исполнитель подтверждает, что он является плательщиком налога на 
прибыль предприятий на общих условиях и плательщиком НДС.</span></p>
<p class="c3"><span class="c0">7.15.
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;Стороны договорились, что, в порядке ст. 207 Гражданского кодекса 
Украины, Исполнитель вправе использовать факсимильное воспроизведение 
подписи директора Исполнителя с помощью средств механического или иного 
копирования либо иного аналога собственноручной подписи при подписании 
приложений, дополнительных соглашений, дополнительных договоров и других
 документов, являющихся неотъемлемой частью настоящего Договора. При 
этом, факсимильная подпись имеет такую ​​же юридическую силу и 
последствия, что и личная подпись директора Исполнителя и признается 
Сторонами Договора. Образец факсимильной подписи директора _____________
 приводится ниже:</span></p>
<p class="c1"><span class="c0">&nbsp;</span></p>
<ol class="c11 lst-kix_bh55r9bgsafo-0 start" start="1">
    <li class="c12"><span class="c0">Адреса Сторон и банковские реквизиты:</span></li>
</ol>
<p class="c14"><span class="c0"></span></p>
<p class="c14"><span class="c0"></span></p>
<p class="c14"><span class="c0"></span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c6"><span class="c0">Исполнитель
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Заказчик</span></p><a
        id="t.5e4a6fec47581994c7f63c7ff3fde070e975ecee"></a><a id="t.0"></a>
<table class="c18">
    <tbody>
    <tr class="c16">
        <td class="c13" colspan="1" rowspan="1"><p class="c7"><span class="c0">&nbsp;</span></p>
            <p class="c7"><span class="c0">&nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp;</span></p></td>
        <td class="c9" colspan="1" rowspan="1"><p class="c7"><span class="c0">&nbsp;</span></p>
            <p class="c7"><span class="c0">&nbsp;</span></p></td>
    </tr>
    </tbody>
</table>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c7"><span class="c0">&nbsp;</span></p>
<p class="c15"><span class="c0"></span></p></div>
'
];