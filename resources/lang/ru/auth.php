<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Этот email не соответствует нашим записям.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'hello' => 'Добрый день',
    'thanks' => 'Благодарим Вас за регистрацию.',
    'ForVerifyGoTo' => 'Для верификации email перейдите по',
    'link' => 'ссылке.',
    'message1' => 'Зарегистрировавшись в нашем сервисе вы приняли правила и условия нашей компании. 
                    Желаем приятной работы с FHD Service.',
    'ChangeEmail' => 'Вы только что создали запрос на смену e-mail. Для активации нового e-mail перейдите по',
];
