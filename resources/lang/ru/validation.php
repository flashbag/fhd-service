<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы для проверки значений
    |--------------------------------------------------------------------------
    |
    | Последующие языковые строки содержат сообщения по умолчанию, используемые
    | классом, проверяющим значения (валидатором). Некоторые из правил имеют
    | несколько версий, например, size. Вы можете поменять их на любые
    | другие, которые лучше подходят для вашего приложения.
    |
    */
    'accepted'             => 'Вы должны принять :attribute.',
    'active_url'           => 'Поле :attribute содержит недействительный URL.',
    'after'                => 'В поле :attribute должна быть дата после :date.',
    'after_or_equal'       => 'В поле :attribute должна быть дата после или равняться :date.',
    'alpha'                => 'Поле :attribute может содержать только буквы.',
    'alpha_spaces'         => 'Поле :attribute может содержать только буквы и пробелы.',
    'alpha_dash'           => 'Поле :attribute может содержать только буквы, цифры и дефис.',
    'alpha_num'            => 'Поле :attribute может содержать только буквы и цифры.',
    'array'                => 'Поле :attribute должно быть массивом.',
    'before'               => 'В поле :attribute должна быть дата до :date.',
    'before_or_equal'      => 'В поле :attribute должна быть дата до или равняться :date.',
    'between'              => [
        'numeric' => 'Поле :attribute должно быть между :min и :max.',
        'file'    => 'Размер файла в поле :attribute должен быть между :min и :max Килобайт(а).',
        'string'  => 'Количество символов в поле :attribute должно быть между :min и :max.',
        'array'   => 'Количество элементов в поле :attribute должно быть между :min и :max.',
    ],
    'boolean'              => 'Поле :attribute должно иметь значение логического типа.', // калька 'истина' или 'ложь' звучала бы слишком неестественно
    'confirmed'            => 'Поле :attribute не совпадает с подтверждением.',
    'date'                 => 'Поле :attribute не является датой.',
    'date_format'          => 'Поле :attribute не соответствует формату :format.',
    'different'            => 'Поля :attribute и :other должны различаться.',
    'digits'               => 'Длина цифрового поля :attribute должна быть :digits.',
    'digits_between'       => 'Длина цифрового поля :attribute должна быть между :min и :max.',
    'dimensions'           => 'Поле :attribute имеет недопустимые размеры изображения.',
    'distinct'             => 'Поле :attribute содержит повторяющееся значение.',
    'email'                => 'Email указан некорректно',
    'file'                 => 'Поле :attribute должно быть файлом.',
    'filled'               => 'Выберите :attribute.',
    'exists'               => 'Выбранное значение для :attribute некорректно.',
    'image'                => 'Поле :attribute должно быть изображением.',
    'in'                   => 'Выбранное значение для :attribute ошибочно.',
    'in_array'             => 'Поле :attribute не существует в :other.',
    'integer'              => 'Поле :attribute должно быть целым числом.',
    'ip'                   => 'Поле :attribute должно быть действительным IP-адресом.',
    'ipv4'                 => 'Поле :attribute должно быть действительным IPv4-адресом.',
    'ipv6'                 => 'Поле :attribute должно быть действительным IPv6-адресом.',
    'json'                 => 'Поле :attribute должно быть JSON строкой.',
    'max'                  => [
        'numeric' => 'Поле :attribute не может быть более :max.',
        'file'    => 'Размер файла в поле :attribute не может быть более :max Килобайт(а).',
        'string'  => 'Количество символов в поле :attribute не может превышать :max.',
        'array'   => 'Количество элементов в поле :attribute не может превышать :max.',
    ],
    'mimes'                => ':attribute неподходящего типа или размера. Поддерживаемые типы файлов: :values байт.',
    'mimetypes'            => ':attribute неподходящего типа или размера. Поддерживаемые типы файлов: :values байт.',
    'min'                  => [
        'numeric' => 'Поле :attribute должно быть не менее :min.',
        'file'    => 'Размер файла в поле :attribute должен быть не менее :min Килобайт(а).',
        'string'  => 'Количество символов в поле :attribute должно быть не менее :min.',
        'array'   => 'Количество элементов в поле :attribute должно быть не менее :min.',
    ],
    'not_in'               => 'Выбранное значение для :attribute ошибочно.',
    'numeric'              => 'Поле :attribute должно быть числом.',
    'present'              => 'Поле :attribute должно присутствовать.',
    'regex'                => 'Поле :attribute имеет ошибочный формат.',
    'required'             => 'Укажите :attribute.',
    'required_if'          => 'Укажите :attribute.',
    'required_unless'      => 'Поле :attribute обязательно для заполнения, когда :other не равно :values.',
    'required_with'        => 'Поле :attribute обязательно для заполнения, когда :values указано.',
    'required_with_all'    => 'Поле :attribute обязательно для заполнения, когда :values указано.',
    'required_without'     => 'Укажите :attribute',
    'required_without_all' => 'Поле :attribute обязательно для заполнения, когда ни одно из :values не указано.',
    'same'                 => 'Пароли не совпадают.',
    'size'                 => [
        'numeric' => 'Поле :attribute должно быть равным :size.',
        'file'    => 'Размер файла в поле :attribute должен быть равен :size Килобайт(а).',
        'string'  => 'Количество символов в поле :attribute должно быть равным :size.',
        'array'   => 'Количество элементов в поле :attribute должно быть равным :size.',
    ],
    'string'               => 'Поле :attribute должно быть строкой.',
    'timezone'             => 'Поле :attribute должно быть действительным часовым поясом.',
    'unique'               => 'Такое значение поля :attribute уже существует.',
    'uploaded'             => 'Загрузка поля :attribute не удалась.',
    'url'                  => 'Поле :attribute имеет ошибочный формат URL.',
	'phone_number' 		   => 'Поле :attribute имеет ошибочный формат телефона.',
    /*
    |--------------------------------------------------------------------------
    | Собственные языковые ресурсы для проверки значений
    |--------------------------------------------------------------------------
    |
    | Здесь Вы можете указать собственные сообщения для атрибутов.
    | Это позволяет легко указать свое сообщение для заданного правила атрибута.
    |
    | http://laravel.com/docs/validation#custom-error-messages
    | Пример использования
    |
    |   'custom' => [
    |       'email' => [
    |           'required' => 'Нам необходимо знать Ваш электронный адрес!',
    |       ],
    |   ],
    |
    */
    'custom'               => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Собственные названия атрибутов
    |--------------------------------------------------------------------------
    |
    | Последующие строки используются для подмены программных имен элементов
    | пользовательского интерфейса на удобочитаемые. Например, вместо имени
    | поля "email" в сообщениях будет выводиться "электронный адрес".
    |
    | Пример использования
    |
    |   'attributes' => [
    |       'email' => 'электронный адрес',
    |   ],
    |
    */
    'attributes'           => [
        'id_tracker' => 'номер накладной',
		'type_transport_id' => 'тип транспорта',
		'from_warehouse_id' => 'склад',
		'to_warehouse_id' => 'склад',

		'number_sender' =>  'номер телефона',
		'email_sender' => 'email',
		'full_name_sender' => 'ФИО',
		'sender_number_account' => 'номер аккаунта',
		'city_id_sender' => 'город',
		'region_sender' => 'регин',
		'country_id_sender' => 'страну',
		'address_sender' => 'адрес',
		'index_sender' => 'индекс',
		'inn_sender' => 'ИНН',
		'passport_data_sender' => 'паспортные данные',
		'sender_number_service' => 'номер сервис трекера',
		'sender_company_name' => 'название компании',
		'sender_service_delivery_id' => 'служба доставки',
		'sender_service_delivery_other' => 'другая служба доставки',

		'number_vat' => 'номер плательщика НДС',
		'sender_documents.*.file' => 'файл',

		'number_recipient' => 'номер телефона',
		'email_recipient' => 'email',
		'full_name_recipient' => 'ФИО',
		'recipient_number_account' => 'номер аккаунта',
		'city_id_recipient' => 'город',
		'region_recipient' => 'регин',
		'country_id_recipient' => 'страну',
		'address_recipient' => 'адрес',
		'index_recipient' => 'индекс',
		'inn_recipient' => 'ИНН',
		'passport_data_recipient' => 'паспортные данные',
		'recipient_number_service' => 'номер сервис трекера',
		'recipient_company_name' => 'название компании',
		'recipient_service_delivery_id' => 'служба доставки',
		'recipient_service_delivery_other' => 'другая служба доставки',
		'custom_id' => '',
		'representative_recipient' => '',
		'recipient_documents.*.file' => 'файл',
        'receptacles.*.cargos.*.description' => 'название груза',
        'receptacles.*.cargos.*.quantity' => 'количество',
        'receptacles.*.actual_weight' => 'вес',
        'receptacles.*.width' => 'ширину',
        'receptacles.*.length' => 'длину',
        'receptacles.*.height' => 'высоту',
        'receptacles.*.assessed_price' => 'стоимость',
        'receptacles.*.currency_type' => 'валюту',
        'delivery_rate_kg' => 'тариф доставки за кг',
        'bill_duty_third_party' => 'Email третьего лица',
        'bill_duty_fhd_account' => 'FHD аккаунт',
        'country' => 'страна',
        'city' => 'город',
        'username' => 'ФИО или название компании',
        'inn' => 'ИНН',
        'index' => 'индекс',
        'passport_data' => 'паспортные данные',
        'number' => 'номер',
        'password' => 'пароль',
        'name' => 'ФИО',
    ],
    'captcha' => 'Неверная капча',
];
