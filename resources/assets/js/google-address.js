const TYPE_TO_UKRAINE = 1;
const TYPE_FROM_UKRAINE = 2;

function getLinkedElement($element, dataAttribute) {

	let linkedElementSelector = $element.data(dataAttribute);
	return $(linkedElementSelector);

}

function filterResultsByType(placesResults, types) {

	let outputList = [];
	let checkTextsArray = [];

	$.each(placesResults, function (index, result) {

		$.each(types, function (i, type) {
			if (result.types.includes(type)) {
				let listText = false;

				switch (type) {
					case 'country':
						listText = result.description;
						break;

					case 'locality':
						listText = result.description;
						break;

					case 'route':
						listText = result.structured_formatting.main_text;
						break;

					case 'street_address':
						listText = result.structured_formatting.main_text;
						break;

					case 'administrative_area_level_1':
						listText = result.structured_formatting.main_text;
						break;

				}

				if (listText && !checkTextsArray.includes(listText)) {
					// console.log(result);
					checkTextsArray.push(listText);
					outputList.push({
						text: listText,
						main_text: result.structured_formatting.main_text,
						place_id: result.place_id
					});
				}
			}
		});

	});

	return outputList;

}

function getPlacesPredictions(autocompleteServiceInstance, request, $element, placesType) {

	autocompleteServiceInstance.getPlacePredictions(request, function (results, status) {
		let filteredResults = [];
		if (status === google.maps.places.PlacesServiceStatus.OK) {
			filteredResults = filterResultsByType(results, placesType);
		}

		// console.log('getPlacesPredictions callback');
		// console.log(filteredResults);

		typeheadFunctions.listClear($element);

		typeheadFunctions.listFill($element, filteredResults, function (item) {

			let $a = $('<a/>', {
				'data-place-id': item.place_id,
				'data-main-text': item.main_text
			}).text(item.text).attr('href', '#');

			$a.wrap("<li></li>");

			return $a.parent();
		});
	});

}

function LinkedPlaceAutocomplete($countryElement, $regionElement, $cityElement, $addressElement) {

	/*
	Private variables
	 */
	let processCountry = $countryElement && $countryElement.length > 0 && (!$countryElement.attr('readonly') && !$countryElement.attr('disabled'));
	let processRegion = $regionElement && $regionElement.length > 0 && (!$regionElement.attr('readonly') && !$regionElement.attr('disabled'));
	let processCity = $cityElement && $cityElement.length > 0 && (!$cityElement.attr('readonly') && !$cityElement.attr('disabled'));
	let processAddress = $addressElement && $addressElement.length > 0 && (!$addressElement.attr('readonly') && !$addressElement.attr('disabled'));

	let autocomplete_service = new google.maps.places.AutocompleteService();

	/*
	Elements events
	 */
	if (processCountry) {

		$countryElement.keyup(function (event) {
			let $self = $(this);
			let $linkedCity = getLinkedElement($self, 'city-selector');
			let $linkedAddress = getLinkedElement($self, 'address-selector');
			let $linkedIndex = getLinkedElement($self, 'index-selector');

			// if (event.keyCode === 13) {
			// 	$linkedCity.removeAttr('readonly');
			// }

			let continueAutocomplete = typeheadFunctions.handleListNavigation($self, event);
			if (!continueAutocomplete) {
				return false;
			}

			let requestInput = $self.val();

			let request = {
				input: requestInput,
				types: ['(regions)']
			};

			if ($self.val().length > 2) {
				typeheadFunctions.listProgress($self);

				// console.log('$countryElement input:', request.input);


				getPlacesPredictions(autocomplete_service, request, $self, ['country']);

				// $linkedCity.attr('readonly', 'readonly').val('');
				// $linkedAddress.attr('readonly', 'readonly').val('');

			} else {
				typeheadFunctions.listClear($self);
			}

		});
	}

	if (processRegion) {

		$regionElement.keyup(function (event) {

			let $self = $(this);
			let $linkedCountry = getLinkedElement($self, 'country-selector');
			let $linkedRegion = getLinkedElement($self, 'region-selector');

			// if (event.keyCode === 13) {
			// 	$linkedRegion.removeAttr('readonly');
			// }

			let continueAutocomplete = typeheadFunctions.handleListNavigation($self, event);
			if (!continueAutocomplete) {
				return false;
			}

			// $linkedAddress.attr('readonly', 'readonly').val('');

			let requestInput = '';

			if ($linkedCountry.length) {
				requestInput += $linkedCountry.val() + ' ';
			}

			requestInput += $self.val();

			let request = {
				input: requestInput
			};

			if ($self.val().length > 2) {
				typeheadFunctions.listProgress($self);

				// console.log('$regionElement input:', request.input);

				getPlacesPredictions(autocomplete_service, request, $self, ['administrative_area_level_1']);

				// $linkedAddress.attr('readonly', 'readonly').val('');

			} else {
				typeheadFunctions.listClear($self);
			}

		});

	}

	if (processCity) {

		$cityElement.keyup(function (event) {

			let $self = $(this);
			let $linkedCountry = getLinkedElement($self, 'country-selector');
			let $linkedRegion = getLinkedElement($self, 'region-selector');
			let $linkedAddress = getLinkedElement($self, 'address-selector');

			// if (event.keyCode === 13) {
			// 	$linkedAddress.removeAttr('readonly');
			// }

			let continueAutocomplete = typeheadFunctions.handleListNavigation($self, event);
			if (!continueAutocomplete) {
				return false;
			}

			// $linkedAddress.attr('readonly', 'readonly').val('');

			let requestInput = '';

			if ($linkedCountry.length) {
				requestInput += $linkedCountry.val() + ' ';
			}

			if ($linkedRegion.length) {
				requestInput += $linkedRegion.val() + ' ';
			}

			requestInput += $self.val();

			let request = {
				input: requestInput,
				types: ['(cities)']
			};

			if ($self.val().length > 2) {
				typeheadFunctions.listProgress($self);

				// console.log('$countryElement input:', request.input);

				getPlacesPredictions(autocomplete_service, request, $self, ['locality']);

				// $linkedAddress.attr('readonly', 'readonly').val('');

			} else {
				typeheadFunctions.listClear($self);
			}

		});
	}

	if (processAddress) {

		$addressElement.keyup(function (event) {

			let $self = $(this);
			let $linkedCountry = getLinkedElement($self, 'country-selector');
			let $linkedRegion = getLinkedElement($self, 'region-selector');
			let $linkedCity = getLinkedElement($self, 'city-selector');
			let $linkedIndex = getLinkedElement($self, 'index-selector');

			let continueAutocomplete = typeheadFunctions.handleListNavigation($self, event);
			if (!continueAutocomplete) {
				return false;
			}

			// $linkedIndex.attr('readonly', 'readonly').val('');

			let requestInput = '';

			if ($linkedCountry.length) {
				requestInput += $linkedCountry.val() + ' ';
			}

			if ($linkedRegion.length) {
				requestInput += $linkedRegion.val() + ' ';
			}

			if ($linkedCity.length) {
				requestInput += $linkedCity.val() + ' ';
			}

			requestInput += $self.val();

			let request = {
				input: requestInput
			};

			if ($self.val().length > 2) {
				typeheadFunctions.listProgress($self);
				getPlacesPredictions(autocomplete_service, request, $self, ['route', 'street_address']);

			} else {
				typeheadFunctions.listClear($self);
			}

		});
	}
}

function cityItemClickCallback($city, $itemClicked) {

	let $regionElement = getLinkedElement($city, 'region-selector');
	let $countryElement = getLinkedElement($city, 'country-selector');
	let $countryDisabledElement = getLinkedElement($city, 'country-disabled-selector');
	let $countryCodeElement = getLinkedElement($city, 'country_code-selector');

	let placeid = $itemClicked.data('place-id');
	let main_text = $itemClicked.data('main-text');

	$city.val(main_text);

	let region, country;
	let url = '/google/place-details/' + placeid;

	$.ajax({
		url: url,
		success: function( data, textStatus, jQxhr){

			if (!data.hasOwnProperty('result')) {
				return false;
			}

			for (var i = 0; i < data.result.address_components.length; i++) {
				for (var j = 0; j < data.result.address_components[i].types.length; j++) {

					if (data.result.address_components[i].types[j] === "administrative_area_level_1") {

						if ($regionElement.val() === '') {
							region = data.result.address_components[i].long_name;
							$regionElement.val(region);
						}
					}

					// if (data.result.address_components[i].types[j] === "country") {
					//
					// 	if ($countryElement.val() === '') {
					// 		country = data.result.address_components[i].long_name;
					// 		$countryElement.val(country);
					//
					// 		if ($countryDisabledElement.length) {
					// 			$countryDisabledElement.val(country);
					// 		}
					// 	}
					//
					// }

					if (data.result.address_components[i].types[j] === "country") {
						if ($('#country_code').val() === '') {
							country = data.result.address_components[i].short_name;
							$('#country_code').val(country);
						}

					}
					if (data.result.address_components[i].types[j] === "country_code") {

						if ($countryCodeElement.val() === '') {
							country = data.result.address_components[i].long_name;
							$countryCodeElement.val(country_code);
						}

					}
				}
			}

		},
		error: function( jqXhr, textStatus, errorThrown ){
			console.error( errorThrown );
		}
	});

	// console.log('placeid:', placeid);
}

function getPostalCode($address, $itemClicked, addressText) {

	let $indexElement = getLinkedElement($address, 'index-selector');

	let placeid = $itemClicked.data('place-id');

	let postalCode;
	let url = '/google/place-details/' + placeid;

	$.ajax({
		url: url,
		success: function( data, textStatus, jQxhr){
			// console.log('SUCCESS!');
			// console.log(data);

			for (var i = 0; i < data.result.address_components.length; i++) {
				for (var j = 0; j < data.result.address_components[i].types.length; j++) {
					if (data.result.address_components[i].types[j] === "postal_code") {

						postalCode = data.result.address_components[i].long_name;
						$indexElement.val(postalCode);
					}
				}
			}

		},
		error: function( jqXhr, textStatus, errorThrown ){
			console.error( errorThrown );
		}
	});

	// console.log('placeid:', placeid);
}

function initializeGooglePlaces() {

	let $inputSenderCountry = $('#inputCountrySenderId');
	let $inputSenderRegion = $('#inputSenderRegion');
	let $inputSenderCity = $('#inputCitySenderId');
	let $inputSenderAddress = $('#inputAddressSenderId');

	let $inputRecipientCountry = $('#inputCountryRecipientId');
	let $inputRecipientRegion = $('#inputRecipientRegion');
	let $inputRecipientCity = $('.inputCityRecipient');
	let $inputRecipientAddress = $('.inputAddressRecipient');

	new LinkedPlaceAutocomplete($inputSenderCountry, $inputSenderRegion, $inputSenderCity, $inputSenderAddress);
	new LinkedPlaceAutocomplete($inputRecipientCountry, $inputRecipientRegion, $inputRecipientCity, $inputRecipientAddress);

	let commonSelectors = {
		'country' : '.typehead-google-address-country .typehead-list li',
		'region' : '.typehead-google-address-region .typehead-list li',
		'city' : '.typehead-google-address-city .typehead-list li',
		'address' : '.typehead-google-address-address .typehead-list li',
	};

	let senderFieldsSelectors = {};
	let recipientFieldsSelectors = {};


	for (var k in commonSelectors) {
		if (commonSelectors.hasOwnProperty(k)) {
			senderFieldsSelectors[k] = '.sender-info ' + commonSelectors[k];
			recipientFieldsSelectors[k] = '.recipient-info ' + commonSelectors[k];
		}
	}



	typeheadFunctions.bindListItemSelect($inputSenderCountry, senderFieldsSelectors.country, function($elementInner, $item, selected) {
		$('.disabled-country-sender').val(selected);
	});

	typeheadFunctions.bindListItemSelect($inputRecipientCountry, recipientFieldsSelectors.country, function($elementInner, $item, selected) {
		$('.disabled-country-recipient').val(selected);
	});

	typeheadFunctions.bindListItemSelect($inputSenderRegion, senderFieldsSelectors.region, function($elementInner, $item, selected) {});

	typeheadFunctions.bindListItemSelect($inputRecipientRegion, recipientFieldsSelectors.region, function($elementInner, $item, selected) {});

	typeheadFunctions.bindListItemSelect($inputSenderCity, senderFieldsSelectors.city, cityItemClickCallback);

	typeheadFunctions.bindListItemSelect($inputRecipientCity, recipientFieldsSelectors.city, cityItemClickCallback);

	typeheadFunctions.bindListItemSelect($inputSenderAddress, senderFieldsSelectors.address, getPostalCode);

	typeheadFunctions.bindListItemSelect($inputRecipientAddress, recipientFieldsSelectors.address, getPostalCode);

}

function activateSenderPlaces() {
	initializeGooglePlaces();
}

window.onload = initializeGooglePlaces;

