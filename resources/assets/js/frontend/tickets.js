(function(){
    'use strict';

    document.addEventListener('DOMContentLoaded', function(){
        const toCutOff = document.querySelectorAll('.js-ToCutOff');
        toCutOff.forEach(function(item){

            let arrLetters = item.innerText.split('');
            let arrWords = item.innerText.split(' ');

            if( arrLetters.length > 13 ){
                arrLetters.splice(0,14);
                let stg = arrLetters.join('') + ' ...';

                item.innerText = stg;
            }
            if( arrWords.length > 0 ){
                arrWords.splice(1);
                let stg = arrWords.join(' ') + ' ...';

                item.innerText = stg;
            }

        })
    });

}());