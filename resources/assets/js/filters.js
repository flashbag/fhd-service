$(document).ready(function () {

	let filtersFormDelay = 500;
	let filtersFormTimeout = null;

	let $form = $('#filters-form');
	let $filtersBlock = $('#filters');

	let $formInputs = $form.find('input');
	let $formSelects = $form.find('select');

	window.filtersFields = window.filtersFields || {};

	if ($('#filters-form').length) {
		$('body').addClass('has-filters');
	}

	function collectFormData() {

		let jsonData = {};

		let formSerialized = $form.serializeArray();

		for (var key in formSerialized) {
			if (formSerialized.hasOwnProperty(key)) {
				let element = formSerialized[key];
				jsonData[element.name] = element.value;
			}
		}

		return jsonData;
	}

	function submitFilters(data) {

		let url = window.location.href + '?';

		for (var key in data) {
			if (data.hasOwnProperty(key)) {
				url = url + key + '=' + data[key] + '&';
			}
		}

		$.get(url, function (data) {

			window.history.pushState({url: url }, 'page #' + data.page, url);

			$('#filters-container').html(data);
			if (typeof window.ajaxFiltersCompleteCallback !== "undefined") {
				window.ajaxFiltersCompleteCallback();
			}
		}).fail(function (jqXhr, textStatus, errorThrown) {
			console.error(errorThrown);
			// alert('Что то пошло не так!');
		});

	}



	$form.on('reset', function (event) {

		event.preventDefault();

		$formInputs.val('');
		$formSelects.each(function () {
			let $select = $(this);

			$select.val($select.find('option:nth-child(1)').val()).trigger('change');

		});

		localStorage.showFilters = false;

		for (var key in window.filtersFields) {
			if (window.filtersFields.hasOwnProperty(key)) {
				localStorage[key] = '';
			}
		}


		let jsonData = collectFormData();

		submitFilters(jsonData);
	});

	$form.on('submit', function (event) {

		event.preventDefault();

		let jsonData = collectFormData();

		submitFilters(jsonData);
	});

	function processFiltersWithTimeout() {
		if (filtersFormTimeout) {
			clearTimeout(filtersFormTimeout);
		}
		filtersFormTimeout = setTimeout(function () {
			let jsonData = collectFormData();
			submitFilters(jsonData);
		}, filtersFormDelay);
	}

	$formInputs.on('keyup', processFiltersWithTimeout);

	$formSelects.on('change', processFiltersWithTimeout);

	$(document).on('click', 'body.has-filters ul.pagination li a', function (event) {

		if (!$('body').hasClass('has-filters')) {
			return false;
		}

		event.preventDefault();

		let queryParams = {};
		let href = $(this).attr('href').split('?')[1];

		href.split("&").forEach(function (part) {
			var item = part.split("=");
			queryParams[item[0]] = decodeURIComponent(item[1]);
		});

		let jsonData = collectFormData();
		jsonData.page = queryParams.page;

		submitFilters(jsonData);
	});

	$('#inputDateDelivery, #inputStartDateRegistered, #inputEndDateRegistered')
		.datepicker({
			dateFormat: "yy-mm-dd",
			onSelect: function() {

				let fieldLocalStorageKey = 'filters-' + $(this).attr('name');

				window.filtersFields[fieldLocalStorageKey] = $(this).val();

				localStorage[fieldLocalStorageKey] = $(this).val();

				processFiltersWithTimeout();
			}
		});

	if (localStorage.showFilters) {
		$filtersBlock.collapse('show');
	} else {
		$filtersBlock.collapse('hide');
	}

	$filtersBlock.on('shown.bs.collapse', function () {
		localStorage.showFilters = true;
	});

	$filtersBlock.on('hidden.bs.collapse', function () {
		localStorage.showFilters = false;
	});

	function loadFiltersState() {

		$formInputs.each(function () {

			let $input = $(this);

            let fieldLocalStorageKey = 'filters-' + $input.attr('name');
            let fieldLocalStorageValue = localStorage[fieldLocalStorageKey];

            if (fieldLocalStorageValue) {
                $input.val(fieldLocalStorageValue).trigger('change');
            }

		});

		$formSelects.each(function () {

			let $select = $(this);

            let fieldLocalStorageKey = 'filters-' + $select.attr('name');
            let fieldLocalStorageValue = localStorage[fieldLocalStorageKey];

            let $neededOption = $select.find('option[value="' + fieldLocalStorageValue + '"]');
            $neededOption.attr('selected', 'selected');

            $select.trigger('change');

            if ($select.hasClass('selectpicker')) {
                $select.selectpicker('refresh');
            }

		});

	}

	function saveFiltersState() {

		$formInputs.each(function () {

			let $input = $(this);

            $input.on('change', function () {

                let fieldLocalStorageKey = 'filters-' + $(this).attr('name');
                let fieldLocalStorageValue = $(this).val();

                window.filtersFields[fieldLocalStorageKey] = fieldLocalStorageValue
                localStorage[fieldLocalStorageKey] = fieldLocalStorageValue;

            });
		});

		$formSelects.each(function () {

			let $select = $(this);

            $select.on('change', function(){

                let fieldLocalStorageKey = 'filters-' + $(this).attr('name');
                let fieldLocalStorageValue = $(this).find('option:selected').val();

                window.filtersFields[fieldLocalStorageKey] = fieldLocalStorageValue

                localStorage[fieldLocalStorageKey] = fieldLocalStorageValue

            });

		});

	}

	saveFiltersState();
	loadFiltersState();

});
