$(document).ready(function(){

	// console.log('multiple inputs cargos');

	let $addOneMoreInput = $('a.add-one-more-cargo');

	$(document).on('click', 'a.add-one-more-cargo', function(){

		let blocksContainerSelector = $(this).data('cargos-container-selector');

		let $blocksContainer = $(blocksContainerSelector);

		let blocksCounter = $blocksContainer.find('div.row-cargo').length;

		let newBlockIndex = blocksCounter;
		let receptaclesIndex = $(this).data('receptacle-index');

		if (newBlockIndex === 10) {
			$addOneMoreInput.hide();
		}

		if (newBlockIndex > 10) {
			return false;
		}

		let getTemplateUrl = $(this).data('get-template-url');

		if (!getTemplateUrl) {
			return false;
		}

		getTemplateUrl = getTemplateUrl + '?new_block_index=' + newBlockIndex + '&receptacle_index=' + receptaclesIndex;

		$.ajax({
			url: getTemplateUrl,
			success: function( data, textStatus, jQxhr){
				$blocksContainer.append(data);

				$('.select-cargo-is-used').selectpicker('refresh').trigger('change');

			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.error( errorThrown );
			}
		});


	});

	$(document).on('click', 'a.remove-cargo-block', function(){

		let $link = $(this);

		let blocksContainerSelector = $(this).data('cargos-container-selector');

		let $blocksContainer = $(blocksContainerSelector);

		let blocksCounter = $blocksContainer.find('div.row-cargo').length;

		let $blockDiv = $link.closest('div.row-cargo');

		if ($link.hasClass('is-existing-cargo')) {

			if (!confirm('Вы уверены что хотите удалить существующий блок?')) {
				return false;
			}

			let cargoId = $link.data('cargo-id');
			let cargoIndex = $link.data('index');
			let receptaclesIndex = $link.data('receptacle-index');

			let $inputId = $('<input/>',{ 'type': 'hidden', 'name':  'receptacles[' + receptaclesIndex + '][cargos]['+ cargoIndex + '][id]', 'value' : cargoId});
			let $inputAction = $('<input/>',{ 'type': 'hidden', 'name':  'receptacles[' + receptaclesIndex + '][cargos]['+ cargoIndex + '][delete]', 'value' : '1'});

			$blockDiv.html('');
			$blockDiv.append($inputId).append($inputAction);

		} else {
			$blockDiv.remove();
		}

		if (blocksCounter <= 10) {
			$addOneMoreInput.show();
		}

	});


	$(document).on('keyup', 'input.input-cargo-name', function(){

		let $input = $(this);
		let $cargoRow = $input.closest('.row-cargo');
		let $nameCharsCount = $cargoRow.find('span.cargo-chars-count span');

		$nameCharsCount.text( 50 - $input.val().length );

	});
});
