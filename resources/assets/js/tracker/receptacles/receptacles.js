$(document).ready(function(){

	// console.log('multiple objects  receptables');

	let $addOneMoreReceptacle = $('a.add-one-more-receptacle');

	// dynamic elements event binding
	$(document).on('click', 'a.add-one-more-receptacle', function(){

		let $blocksContainer = $('.receptacle-container');

		let blocksCounter = $blocksContainer.find('div.receptacle-block').length;

		let newBlockIndex = blocksCounter + 1;

		if (newBlockIndex === 10) {
			$addOneMoreReceptacle.hide();
		}

		if (newBlockIndex > 10) {
			return false;
		}

		let getTemplateUrl = $(this).data('get-template-url');

		if (!getTemplateUrl) {
			return false;
		}

		getTemplateUrl = getTemplateUrl + '?new_block_index=' + newBlockIndex;

		$.ajax({
			url: getTemplateUrl,
			success: function( data, textStatus, jQxhr){
				// console.log('SUCCESS!');
				// console.log(data);

				$blocksContainer.append(data);

				$('.select-unit-of-length').selectpicker('refresh').trigger('change');
				$('.select-unit-of-weight').selectpicker('refresh').trigger('change');
				$('.select-type-inventory').selectpicker('refresh').trigger('change');
				$('.select-currency-type').selectpicker('refresh').trigger('change');
				$('.select-cargo-is-used').selectpicker('refresh').trigger('change');

				let $receptacleBlock = $('#receptacle-block-' + newBlockIndex);

				if ($receptacleBlock.length) {
					$([document.documentElement, document.body]).animate({
						scrollTop: $receptacleBlock.offset().top - 200
					}, 1000);
				}
			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.error( errorThrown );
			}
		});

	});

	$(document).on('click', 'a.remove-receptacle-block', function(){

		let $link = $(this);

		let $blocksContainer = $('.receptacle-container');

		let blocksCounter = $blocksContainer.find('div.receptacle-block').length;

		// if (blocksCounter === 2) {
		// 	// if only one block will be left, remove the delete link
		// 	$('a.remove-receptacle-block').remove();
		// }

		let $blockDiv = $link.closest('div.receptacle-block');

		if ($link.hasClass('is-existing-receptacle')) {

			if (!confirm('Вы уверены что хотите удалить существующее позицию?')) {
				return false;
			}

			let receptacleId = $link.data('receptacle-id');
			let receptaclesIndex = $link.data('receptacle-index');

			let $inputId = $('<input/>',{ 'type': 'hidden', 'name':  'receptacles[' + receptaclesIndex + '][id]', 'value' : receptacleId});
			let $inputAction = $('<input/>',{ 'type': 'hidden', 'name':  'receptacles[' + receptaclesIndex + '][delete]', 'value' : '1'});

			$blockDiv.html('');
			$blockDiv.append($inputId).append($inputAction);

		} else {
			$blockDiv.remove();
		}

		if (blocksCounter <= 10) {
			$addOneMoreReceptacle.show();
		}

	});
});
