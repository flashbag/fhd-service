$(document).ready(function(){

	function highlightBiggerWeight($element) {

		let $receptacleBlock = $element.closest('.receptacle-block');

		let $inputActualWeight = $receptacleBlock.find('input.input-actual-weight');
		let $inputVolumeWeight = $receptacleBlock.find('input.input-volume-weight');

		let actualWeightVal = $inputActualWeight.val();
		let volumeWeightVal = $inputVolumeWeight.val();

		if (parseFloat(actualWeightVal) > 0 || parseFloat(volumeWeightVal) > 0) {

			if (parseFloat(actualWeightVal) > parseFloat(volumeWeightVal)) {

				$inputActualWeight.css('border-color', '#2ab27b').css('box-shadow', 'inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px #2ab27b');
				$('.weight_copy').val(actualWeightVal).css('border-color', '#2ab27b').css('box-shadow', 'inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px #2ab27b');
				$inputVolumeWeight.css('border-color', '#ccd0d2').css('box-shadow', 'none');
				$('.volume_copy').val(volumeWeightVal).css('border-color', '#ccd0d2').css('box-shadow', 'none');

			} else if (parseFloat(actualWeightVal) < parseFloat(volumeWeightVal)) {

				$inputVolumeWeight.css('border-color', '#2ab27b').css('box-shadow', 'inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px #2ab27b');
				$('.volume_copy').val(volumeWeightVal).css('border-color', '#2ab27b').css('box-shadow', 'inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px #2ab27b');
				$inputActualWeight.css('border-color', '#ccd0d2').css('box-shadow', 'none');
				$('.weight_copy').val(actualWeightVal).css('border-color', '#ccd0d2').css('box-shadow', 'none');

			}
		}
	}

	function selectLengthUnitChange($select) {
		
		let $receptacleBlock = $select.closest('.receptacle-block');

		let $selected = $select.find(':selected');

		// let isMetric = $selected.data('is-metric');
		//
		// let $receptacleUnitOfWeightSelect = $receptacleBlock.find('.select-unit-of-weight');
		//
		// if ($receptacleUnitOfWeightSelect.length) {
		// 	$receptacleUnitOfWeightSelect.find('option').removeAttr('selected');
		// 	$receptacleUnitOfWeightSelect.find('option[data-is-metric="' + isMetric + '"]').attr('selected','selected');
		// 	$receptacleUnitOfWeightSelect.selectpicker('refresh').trigger('change');
		// }

		let $inputLengthMultiplier = $receptacleBlock.find('input.length-multiplier-to-base');
		
		let $spanUnitLengthText = $receptacleBlock.find('.length-measure-unit');
		let $spanUnitOriginalLengthText = $receptacleBlock.find('.length-measure-unit-original');

		$inputLengthMultiplier.val($selected.data('multiplier-to-base'));
		
		$spanUnitOriginalLengthText.text($selected.data('original-text'));
		$spanUnitLengthText.text($selected.text());
	}

	function selectWeightUnitChange($select) {

		let $receptacleBlock = $select.closest('.receptacle-block');

		let $selected = $select.find(':selected');

		// let isMetric = $selected.data('is-metric');
		//
		// let $receptacleUnitOfLengthSelect = $receptacleBlock.find('.select-unit-of-length');
		//
		// if ($receptacleUnitOfLengthSelect.length) {
		// 	$receptacleUnitOfLengthSelect.find('option').removeAttr('selected');
		// 	$receptacleUnitOfLengthSelect.find('option[data-is-metric="' + isMetric + '"]').attr('selected','selected');
		// 	$receptacleUnitOfLengthSelect.selectpicker('refresh').trigger('change');
		// }

		let $inputWeightMultiplier = $receptacleBlock.find('input.weight-multiplier-to-base');

		let $spanUnitLengthText = $receptacleBlock.find('.weight-measure-unit');
		let $spanUnitOriginalLengthText = $receptacleBlock.find('.weight-measure-unit-original');

		$inputWeightMultiplier.val($selected.data('multiplier-to-base'));

		$spanUnitOriginalLengthText.text($selected.data('original-text'));
		$spanUnitLengthText.text($selected.text());
	}

	function calculateWeight($element) {

		let $receptacleBlock = $element.closest('.receptacle-block');

		let $inputWidth = $receptacleBlock.find('input.input-width');
		let $inputLength = $receptacleBlock.find('input.input-length');
		let $inputHeight = $receptacleBlock.find('input.input-height');

		let $inputVolumeWeight = $receptacleBlock.find('input.input-volume-weight');

		let $inputLengthMultiplier = $receptacleBlock.find('input.length-multiplier-to-base');
		let $inputWeightMultiplier = $receptacleBlock.find('input.weight-multiplier-to-base');

		let width = $inputWidth.val() || 0;
		let length = $inputLength.val() || 0;
		let height = $inputHeight.val() || 0;

		if (isNaN(width) || isNaN(length) || isNaN(height)) {
			return false;
		}

		let lengthMultiplier = $inputLengthMultiplier.val();
		let weightMultiplier = $inputWeightMultiplier.val();

		width = width * lengthMultiplier;
		length = length * lengthMultiplier;
		height = height * lengthMultiplier;

		let volumeWeightKg = Math.ceil((width * length * height ) / 5000);
		let volumeWeight = Math.ceil(volumeWeightKg * weightMultiplier);

		$inputVolumeWeight.val(volumeWeight > 0 ? volumeWeight : 0);

		highlightBiggerWeight($element);
	}

	$(document).on('change', 'input.input-actual-weight', function(){
		let $input = $(this);
		highlightBiggerWeight($input);
	});

	$(document).on('change', 'input.input-volume-weight', function(){
		let $input = $(this);
		highlightBiggerWeight($input);
	});

	$(document).on('change keyup', 'input.input-width, input.input-length, input.input-height', function(){
		let $input = $(this);
		calculateWeight($input);
	});

	selectLengthUnitChange($('select.select-unit-of-length'));
	selectWeightUnitChange($('select.select-unit-of-weight'));

	$(document).on('change', 'select.select-unit-of-length', function(){
		let $select = $(this);
		selectLengthUnitChange($select);
		calculateWeight($select);
	});

	$(document).on('change', 'select.select-unit-of-weight', function(){
		let $select = $(this);
		selectWeightUnitChange($select);
		calculateWeight($select);
	});

});
