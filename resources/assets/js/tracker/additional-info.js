$(document).ready(function () {

	let $billDutySelect = $('select[name="bill_duty_id"]');
	let $billTransportationSelect = $('select[name="bill_transportation_id"]');
	let $freightServiceSelect = $('select[name="freight_service_id"]');
	let $specialHandlingsSelect = $('select[name="special_handling_id"]');

	function hideBillDutyAdditional() {
		$('.billDutyThirdParty').hide();
		$('.billDutyFhdAccount').hide();
		$('input[name="bill_duty_third_party"]').val('');
		$('input[name="bill_duty_fhd_account"]').val('');
	}

	function hideBillTransportationAdditional() {
		$('.billTransportationThirdParty').hide();
		$('input[name="bill_transportation_third_party"]').val('');
	}

	function hideFreightServiceAdditional() {
		$('.freightServiceBookingNumber').hide();
		$('input[name="freight_service_booking_number"]').val('');
	}

	function hideSpecialHandlingAdditional() {
	}

	function toggleBillDutyAdditional(selectedBillDuty) {
		if (selectedBillDuty === 'third_party') {
			$('.billDutyThirdParty').show();
		}
		if (selectedBillDuty === 'fhd_account') {
			$('.billDutyFhdAccount').show();
		}
	}

	function toggleBillTransportationAdditional(selectedBillTransportation) {
		if (selectedBillTransportation === 'third_party') {
			$('.billTransportationThirdParty').show();
		}

	}

	function toggleFreightServiceAdditional(selectedFreightService) {
		if (selectedFreightService === 'booking_number') {
			$('.freightServiceBookingNumber').show();
		}
	}

	function toggleSpecialHandlingsAdditional(selectedSpecialHandling) {

	}

	let billDutyPreset = $billDutySelect.find(':selected').data('type');
	let billTransportationPreset = $billTransportationSelect.find(':selected').data('type');

	let freightServicePreset = $freightServiceSelect.find(':selected').data('type');
	let specialHandlingsPreset = $specialHandlingsSelect.find(':selected').data('type');

	$('input[name="bill_duty_type"]').val(billDutyPreset);
	$('input[name="bill_transportation_type"]').val(billTransportationPreset);
	$('input[name="freight_service_type"]').val(freightServicePreset);
	$('input[name="special_handling_type"]').val(specialHandlingsPreset);

	toggleBillDutyAdditional(billDutyPreset);
	toggleBillTransportationAdditional(billTransportationPreset);
	toggleFreightServiceAdditional(freightServicePreset);
	toggleSpecialHandlingsAdditional(specialHandlingsPreset);

	$billDutySelect.on('change', function() {

		let selectedBillDuty = $(this).find(':selected').data('type');

		hideBillDutyAdditional();
		toggleBillDutyAdditional(selectedBillDuty);

		$('input[name="bill_duty_type"]').val(selectedBillDuty);
	});

	$billTransportationSelect.on('change', function() {

		let selectedBillTransportation = $(this).find(':selected').data('type');

		hideBillTransportationAdditional();
		toggleBillTransportationAdditional(selectedBillTransportation);

		$('input[name="bill_transportation_type"]').val(selectedBillTransportation);
	});

	$freightServiceSelect.on('change', function() {

		let selectedFreightService = $(this).find(':selected').data('type');

		hideFreightServiceAdditional();
		toggleFreightServiceAdditional(selectedFreightService);

		$('input[name="freight_service_type"]').val(selectedFreightService);
	});

	$specialHandlingsSelect.on('change', function() {

		let selectedSpecialHandling = $(this).find(':selected').data('type');

		hideSpecialHandlingAdditional();
		toggleSpecialHandlingsAdditional(selectedSpecialHandling);

		$('input[name="special_handling_type"]').val(selectedSpecialHandling);
	});


});
