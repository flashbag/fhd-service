$(document).ready(function(){

	const PERSON_TYPE_INDIVIDUAL = 1;
	const PERSON_TYPE_LEGAL_ENTITY = 2;

	let $checkboxSenderSameRecipient = $('#sender_same_recipient');

	let senderFields = {
		personTypeSelect: $('select[name="sender_person_type"]'),
		numberVatBlock: $('.senderNumberVatBlock'),

		phoneNumber: $('#numberSender'),
		email: $('#inputEmailSender'),
		accountNumber: $('#senderAccountNumber'),
		fullNameInput: $('#inputSenderFullName'),
		companyNameInput: $('#inputSenderCompanyName'),

		numberVatInput: $('#numberVat'),

		serviceDeliverySelect : $('select[name="sender_service_delivery_id"]'),
		serviceDeliveryOtherInput:  $('input[name="sender_service_delivery_other"]'),
		serviceDeliveryOtherBlock: $('.senderServiceDeliveryOther'),
	};

	let recipientFields = {
		personTypeSelect: $('select[name="recipient_person_type"]'),
		customIdentifyBlock: $('.recipientCustomIdentifyBlock'),

		phoneNumber: $('#numberRecipient'),
		email: $('#inputEmailRecipient'),
		accountNumber: $('#recipientAccountNumber'),
		fullNameInput: $('#inputRecipientFullName'),
		companyNameInput: $('#inputRecipientCompanyName'),

		customIdentifyInput: $('#customId'),

		serviceDeliverySelect : $('select[name="recipient_service_delivery_id"]'),
		serviceDeliveryOtherInput:  $('input[name="recipient_service_delivery_other"]'),
		serviceDeliveryOtherBlock: $('.recipientServiceDeliveryOther'),
	};

	function serviceDeliveryChange(value, $input, $block) {
		if (value === '') {
			$input.removeAttr('disabled');
			$block.show();
		} else {
			$input.attr('disabled', 'disabled');
			$block.hide();
		}
	}

	function personTypeChange(value, fields, isInitialCall) {
		// При выборе Юр лица поле ФИО изменяется на Название компании и наоборот.
		//
		// Также при выборе Юр лица у отправителя поле Номер плательщика НДС видимое при Физ лице скрыто,
		// у получателя при выборе Юр лица Налоговый идентификатор видимое при Физ лице скрыто.
		value = parseInt(value);

		if (!isInitialCall) {
			fields.fullNameInput.val('');
			fields.companyNameInput.val('');

			if (fields.numberVatInput) {
				fields.numberVatInput.val('');
			}
			if (fields.customIdentifyInput) {
				fields.customIdentifyInput.val('');
			}
		}

		if (value === PERSON_TYPE_INDIVIDUAL) {

			fields.fullNameInput.removeAttr('disabled');

			if (fields.numberVatBlock) {
				fields.numberVatBlock.hide();
			}
			if (fields.customIdentifyBlock) {
				fields.customIdentifyBlock.hide();
			}
		}

		if (value === PERSON_TYPE_LEGAL_ENTITY) {

			fields.fullNameInput.attr('disabled', 'disabled');

			if (fields.numberVatBlock) {
				fields.numberVatBlock.show();
			}
			if (fields.customIdentifyBlock) {
				fields.customIdentifyBlock.show();
			}
		}
	}

	function disableAnother($input, $anotherInput) {

		$input.on('change keyup', function(){
			let value = $(this).val();
			let $inputFormGroup = $input.closest('.form-group');
			let $anotherInputFormGroup = $anotherInput.closest('.form-group');

			if (value !== "" || value.length > 0) {
				$anotherInput.attr('disabled','disabled');
				$inputFormGroup.addClass('required');
				$anotherInputFormGroup.removeClass('required');
			} else {
				$inputFormGroup.removeClass('required');
				$anotherInputFormGroup.addClass('required');
				$anotherInput.removeAttr('disabled');
			}
		});
	}

	disableAnother(senderFields.fullNameInput, senderFields.companyNameInput);
	disableAnother(senderFields.companyNameInput, senderFields.fullNameInput);
	disableAnother(recipientFields.fullNameInput, recipientFields.companyNameInput);
	disableAnother(recipientFields.companyNameInput, recipientFields.fullNameInput);

	personTypeChange(senderFields.personTypeSelect.val(),senderFields, true);

	personTypeChange(recipientFields.personTypeSelect.val(), recipientFields, true);

	serviceDeliveryChange(senderFields.serviceDeliverySelect.val(),
		senderFields.serviceDeliveryOtherInput,
		senderFields.serviceDeliveryOtherBlock
	);

	serviceDeliveryChange(recipientFields.serviceDeliverySelect.val(),
		recipientFields.serviceDeliveryOtherInput,
		recipientFields.serviceDeliveryOtherBlock
	);


	senderFields.serviceDeliverySelect.on('change', function(){
		serviceDeliveryChange(senderFields.serviceDeliverySelect.val(),
			senderFields.serviceDeliveryOtherInput,
			senderFields.serviceDeliveryOtherBlock
		);
	});

	recipientFields.serviceDeliverySelect.on('change', function(){
		serviceDeliveryChange(recipientFields.serviceDeliverySelect.val(),
			recipientFields.serviceDeliveryOtherInput,
			recipientFields.serviceDeliveryOtherBlock
		)
	});

	senderFields.personTypeSelect.on('change', function(){
		personTypeChange(senderFields.personTypeSelect.val(),senderFields, false);
	});

	recipientFields.personTypeSelect.on('change', function(){
		personTypeChange(recipientFields.personTypeSelect.val(), recipientFields, false);
	});


	$checkboxSenderSameRecipient.on('change', function(){
		// console.log($(this).val());
		if ($(this).prop('checked')==true){
			// checked
			// console.log('checked');
			let senderPersonTypeSelected = senderFields.personTypeSelect.val();
			recipientFields.personTypeSelect.find('option[value="' + senderPersonTypeSelected + '"]');

			recipientFields.phoneNumber.attr('readonly', 'readonly').val(senderFields.phoneNumber.val());
			recipientFields.accountNumber.attr('readonly', 'readonly').val(senderFields.accountNumber.val());
			recipientFields.fullNameInput.attr('readonly', 'readonly').val(senderFields.fullNameInput.val());
			recipientFields.companyNameInput.attr('readonly', 'readonly').val(senderFields.companyNameInput.val());

		} else {
			// unchecked
			// console.log('unchecked');

			recipientFields.phoneNumber.removeAttr('readonly');
			recipientFields.accountNumber.removeAttr('readonly');
			recipientFields.fullNameInput.removeAttr('readonly');
			recipientFields.companyNameInput.removeAttr('readonly');
		}
	});


});
