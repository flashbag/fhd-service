$(document).ready(function () {

	let timeout;
	let delay = 500;

	let $inputNumberSender = $('input[name="number_sender"]');
	let $inputNumberRecipient = $('input[name="number_recipient"]');

	//Sender
	$inputNumberSender.on('keyup', function(event) {

		let $element = $(this);
		let continueAutocomplete = typeheadFunctions.handleListNavigation($element, event);
		if (!continueAutocomplete) {
			return false;
		}
		if ($element.val().length > 6) {
			typeheadFunctions.listProgress($element);
			if (timeout) {
				clearTimeout(timeout);
			}
			timeout = setTimeout(function () {
				getSearchUsers($element);
			}, delay);

		} else {
			typeheadFunctions.listClear($element);
		}
	});

	// Recipient
	$inputNumberRecipient.keyup(function (event) {

		let $element = $(this);
		let continueAutocomplete = typeheadFunctions.handleListNavigation($element, event);
		if (!continueAutocomplete) {
			return false;
		}
		if ($element.val().length > 6) {
			typeheadFunctions.listProgress($element);
			if (timeout) {
				clearTimeout(timeout);
			}
			timeout = setTimeout(function () {
				getSearchUsers($element);
			}, delay);
		} else {
			typeheadFunctions.listClear($element);
		}
	});


	function getPhoneListItemTemplate(item) {
		return $('<li/>', {
			'data-client_id': item.client_id,
			'data-number': item.number,
			'data-email': item.email,
			'data-full_name': item.full_name,
			'data-inn': item.inn,
			'data-passport_data': item.passport_data,
			'data-country_id': item.country,
			'data-city_id': item.city,
			'data-address': item.address,
			'data-index': item.index,
		}).text(item.number);
	}

	function getSearchUsers($element) {

		let searchUrl = '/admin/users/search/' + $element.val();

		$.get(searchUrl, function (data) {
			typeheadFunctions.listFill($element, data, getPhoneListItemTemplate);
		}, 'json');
	}

	function blurredLines($el) {
		// by Robin Thicke
		setTimeout(function () {
			$el.trigger('blur');
		}, 100);
	}

	let itemDataMappingToSender = {
		'email' : 'email_sender',
		'full_name': 'full_name_sender',
		'inn': 'inn_sender',
		'country_id': 'country_id_sender',
		'city_id': 'city_id_sender',
		'address' : 'address_sender',
		'index' : 'index_sender',
		'passport_data': 'passport_data_sender',
		'client_id': 'sender_id'
	};

	let itemDataMappingToRecipient = {
		'email' : 'email_recipient',
		'full_name': 'full_name_recipient',
		'inn': 'inn_recipient',
		'country_id': 'country_id_recipient',
		'city_id': 'city_id_recipient',
		'address' : 'address_recipient',
		'index' : 'index_recipient',
		'passport_data': 'passport_data_recipient',
		'client_id': 'recipient_id'
	};

	function fillMappedInputs($item, mapping) {
		for (var dataAttribute in mapping) {
			if (!mapping.hasOwnProperty(dataAttribute)) {
				continue;
			}

			let inputName = mapping[dataAttribute];

			let dataValue = $item.data(dataAttribute);
			let $neededInput = $('input[name="' + inputName + '"');

			if (dataValue !== 'null' && $neededInput.length) {
				$neededInput.val(dataValue);

				blurredLines($neededInput);
			}
		}
	}

	typeheadFunctions.bindListItemSelect($inputNumberSender, '.typehead-number-sender .typehead-list li', function($elementInner, $item) {
		blurredLines($elementInner);
		fillMappedInputs($item, itemDataMappingToSender);
	});

	typeheadFunctions.bindListItemSelect($inputNumberRecipient, '.typehead-number-recipient .typehead-list li', function($elementInner, $item) {
		blurredLines($elementInner);
		fillMappedInputs($item, itemDataMappingToRecipient);
	});


});
