$(document).ready(function() {
	var timeout;
	var delay = 500;

	$('input[name="number_container"]').keyup(function (e) {
		if ($(this).val().length > 3) {
			$(this).parent().find('.refresh-input-informer').addClass('rotating');
			$(this).parent().find('.typehead-list').empty();
			if (timeout) {
				clearTimeout(timeout);
			}
			timeout = setTimeout(function () {
				getContainers();
			}, delay);
		} else {
			$(this).parent().find('.typehead-list').empty();
			if ($(this).parent().find('.typehead-list').hasClass('visible-typehead-list')) {
				$(this).parent().find('.typehead-list').removeClass('visible-typehead-list');
			}
		}
		if ($(this).hasClass('edited')) {
			getTypeContainers();
		}
	});

	function getContainers() {

		$.get('/admin/containers/search/' + $('input[name="number_container"]').val(), function (data) {
			if (data.length > 0) {
				data.forEach(function (item) {
					$('.typehead-number-container').find('.typehead-list').append('<li class="typehead-li" data-id="' + item.id_type_container + '">' + item.number_container + '</li>');
				});
				$('.typehead-number-container').find('.typehead-list').addClass('visible-typehead-list');
				$('.typehead-number-container').find('.refresh-input-informer').removeClass('rotating');
			} else {
				$('.typehead-number-container').find('.refresh-input-informer').removeClass('rotating');
				$('.typehead-number-container').find('.typehead-list').removeClass('visible-typehead-list');
			}
		}, 'json');

	}

	function getTypeContainerById(id) {
		$.get('/admin/containers/type/search/' + id, function (data) {
			if (data !== null) {
				$('select[name="id_type_container"]').empty();
				$('select[name="id_type_container"]').append('<option selected="selected" value="' + data.id + '">' + data.title + '</option>');
				$('select[name="id_type_container"]').selectpicker('refresh');
			}
		}, 'json');
	}

	function getTypeContainers() {
		$.get('/admin/containers/types/', function (data) {
			if (data.length > 0) {
				$('select[name="id_type_container"]').empty();
				data.forEach(function (item) {
					$('select[name="id_type_container"]').append('<option value="' + item.id + '">' + item.title + '</option>');
				});
				$('select[name="id_type_container"]').selectpicker('refresh');
				$('input[name="number_container"]').removeClass('edited');
			}
		}, 'json');
	}
	$('.typehead-number-container').on('click', '.typehead-list li', function(){

		if ($(this).parent().find('input[name="number_container"]')) {
			$('input[name="number_container"]').val($(this).text());
			$('input[name="number_container"]').addClass('edited');
			$('.typehead-list').removeClass('visible-typehead-list');
			getTypeContainerById($(this).attr('data-id'));
		}

	});

});

