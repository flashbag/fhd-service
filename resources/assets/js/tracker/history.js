;(function ($) {
	/* tracker history */
	function resizeHistoryContent() {
		if ($('.trackers-history-page').length) {
			const documentHeight = $(document).height();
			const headerHeight = $('.header-section').height();
			const footerHeight = $('.footer-section').height();

			var historySection = $('.trackers-history-page .history-section');
			var resH = documentHeight - headerHeight - footerHeight;
			historySection.height(resH);
		}
	}

	$(document).ready(function () {
		resizeHistoryContent();
		$('body').on('click', '.trackers-history-page .create-tracker', function (e) {
			location.href = $(this).find('a').attr('href');
		});
	});
	$(window).on('resize', resizeHistoryContent);

})(jQuery);
