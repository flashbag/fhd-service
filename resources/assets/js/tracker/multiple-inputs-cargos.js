$(document).ready(function(){

	// console.log('multiple inputs cargos');

	let $addOneMoreInput = $('a.add-one-more-cargo');

	$addOneMoreInput.on('click', function(){

		let blocksContainerSelector = $(this).data('blocks-container-selector');

		let $blocksContainer = $(blocksContainerSelector);

		let blocksCounter = $blocksContainer.find('div.row-cargo').length;

		let newBlockIndex = blocksCounter + 1;

		if (newBlockIndex === 10) {
			$addOneMoreInput.hide();
		}

		if (newBlockIndex > 10) {
			return false;
		}

		let blockTemplate = `
			<div class="row row-cargo">
				<div class="form-group col-md-8">
					<input type="text" class="form-control input-cargo-name" placeholder="Название груза" name="cargos[${newBlockIndex}][description]" style="display: inline-block">
					<span class="cargo-chars-count">Количество символов осталось: <span>40</span></span>
				</div>
				<div class="form-group col-md-3">
					<input type="text" class="form-control" placeholder="Количество" name="cargos[${newBlockIndex}][quantity]" style="display: inline-block">
				</div>
				<div class="col-md-1">
					<a class="remove-cargo-block">
						<i class="fa fa-2x fa-minus-square"></i>
					</a>
				</div>	
			</div>
		`;

		$blocksContainer.append(blockTemplate);

	});

	$(document).on('keyup', 'input.input-cargo-name', function(){

		let $input = $(this);
		let $cargoRow = $input.closest('.row-cargo');
		let $nameCharsCount = $cargoRow.find('span.cargo-chars-count span');

		$nameCharsCount.text( 50 - $input.val().length );

	});

	$(document).on('click', 'a.remove-cargo-block', function(){

		let $link = $(this);

		let $blocksContainer = $('div.cargos-container');

		let blocksCounter = $blocksContainer.find('div.row-cargo').length;

		let $blockDiv = $link.closest('div.row-cargo');

		if ($link.hasClass('is-existing-cargo')) {

			if (!confirm('Вы уверены что хотите удалить существующий блок?')) {
				return false;
			}

			let blockId = $link.data('id');
			let blockIndex = $link.data('index');


			let $inputId = $('<input/>',{ 'type': 'hidden', 'name':  'cargos['+ blockIndex + '][id]', 'value' : blockId});
			let $inputAction = $('<input/>',{ 'type': 'hidden', 'name':  'cargos['+ blockIndex + '][delete]', 'value' : '1'});

			$blockDiv.html('');
			$blockDiv.append($inputId).append($inputAction);

		} else {
			$blockDiv.remove();
		}

		if (blocksCounter <= 10) {
			$addOneMoreInput.show();
		}

	});

});
