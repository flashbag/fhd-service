$(document).ready(function(){

	// console.log('multiple files');

	let $addOneMoreFile = $('a.add-one-more-file');

	$addOneMoreFile.on('click', function(){

		let inputName = $(this).data('input-name');
		let containerSelector = $(this).data('container-selector');

		let $blocksContainer = $(containerSelector);

		let blocksCounter = $blocksContainer.find('div.file-block').length;

		let newBlockIndex = blocksCounter + 1;

		if (newBlockIndex === 10) {
			$addOneMoreFile.hide();
		}

		if (newBlockIndex > 10) {
			return false;
		}

		let getTemplateUrl = $(this).data('get-template-url');

		if (!getTemplateUrl) {
			return false;
		}

		getTemplateUrl = getTemplateUrl + '?new_block_index=' + newBlockIndex + '&input_name=' + inputName + '&container_selector=' + containerSelector;

		$.ajax({
			url: getTemplateUrl,
			success: function( data, textStatus, jQxhr){
				$blocksContainer.append(data);
			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.error( errorThrown );
			}
		});

	});

	$(document).on('click', 'a.remove-file-input', function(){

		let $link = $(this);
		
		let blocksContainerSelector = $link.data('container-selector');

		let $blocksContainer = $(blocksContainerSelector);

		let blocksCounter = $blocksContainer.find('div.file-block').length;

		let $blockDiv = $link.closest('div.file-block');
		
		if ($link.hasClass('is-existing-file')) {

			if (!confirm('Вы уверены что хотите удалить существующий документ?')) {
				return false;
			}

			let blockId = $link.data('id');
			let blockIndex = $link.data('index');

			let inputName = $link.data('input-name');

			let $inputId = $('<input/>',{ 'type': 'hidden', 'name':  inputName + '['+ blockIndex + '][id]', 'value' : blockId});
			let $inputAction = $('<input/>',{ 'type': 'hidden', 'name':  inputName + '['+ blockIndex + '][delete]', 'value' : '1'})

			$blockDiv.html('');
			$blockDiv.append($inputId).append($inputAction);

		} else {
			$blockDiv.remove();
		}
			
		if (blocksCounter <= 10) {
			$addOneMoreFile.show();
		}

		
	});

});
