function removeErrorWhenElementChanged(element) {

	let $element = $(element);

	let $elementFormGroup = $element.closest('div.form-group');

	if ($elementFormGroup.hasClass('has-error')) {
		$elementFormGroup.removeClass('has-error');
	}

	let $elementHelpBlock = $elementFormGroup.find('span.help-block');
	if ($elementHelpBlock.length && !$elementHelpBlock.hasClass('help-block-static')) {
		$elementHelpBlock.remove();
	}

}

function ajaxFormProcessErrors($form, errors) {

	// console.log(errors);
	let firstErrorProcessed = false;

	let elementErrors, elementParts, elementName;

	for (var element in errors) {

		if (errors.hasOwnProperty(element)) {

			/*

			element can be such types:

			email_sender
			city_id_sender
			delivery_rate_cur
			sender_document.0
			sender_document.15
			receptacles.10.cargos.4.description

			if it is multiple fields like last two examples, need to convert name to following:

				sender_document[0]
				sender_document[15]

			to match real inputs on page!

			 */

			let $element = $form.find('[name="' + element + '"]');

			elementParts = element.split('.');

			if (elementParts.length === 2) {
				elementName = elementParts[0] + '[' + elementParts[1] + ']';
			} else if (elementParts.length === 3) {
				elementName = elementParts[0] + '[' + elementParts[1] + '][' + elementParts[2] + ']';
			} else if (elementParts.length === 4) {
				elementName = elementParts[0] + '[' + elementParts[1] + '][' + elementParts[2] + '][' + elementParts[3] + ']';
			} else if (elementParts.length === 5) {
				elementName = elementParts[0] + '[' + elementParts[1] + '][' + elementParts[2] + '][' + elementParts[3] + '][' + elementParts[4] + ']';
			}

			if (!$element.length) {
				$element = $form.find('[name="' + elementName + '"]');
			}


			elementErrors = errors[element];

			if (!firstErrorProcessed && $element.length) {
				$([document.documentElement, document.body]).animate({
					scrollTop: $element.offset().top - 200
				}, 1000);
				firstErrorProcessed = true;
			}

			// console.log(element);
			// console.log(elementErrors);

			let $elementFormGroup = $element.closest('div.form-group');

			$elementFormGroup.addClass('has-error');

			let $elementHelpBlock = $elementFormGroup.find('span.help-block:not(.help-block-static)');

			if (!$elementHelpBlock.length) {
				$elementHelpBlock = $('<span/>', { 'class': 'help-block' });
				$elementFormGroup.append($elementHelpBlock);
			}

			$elementHelpBlock.html('');

			for (var errKey in elementErrors) {

				if (elementErrors.hasOwnProperty(errKey)) {
					$elementHelpBlock.append(
						$('<strong/>').text(elementErrors[errKey])
					);
				}
			}
		}
	}
}

$(document).ready(function(){

	// console.log($('form.ajax-form'));

	$('form.ajax-form').find('input').on('blur', function (e) {
		removeErrorWhenElementChanged(this);
	});

	$('form.ajax-form').find('select').on('change', function (e) {
		removeErrorWhenElementChanged(this);
	});

	$('form.ajax-form').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) {
			e.preventDefault();
			return false;
		}
	});

	$('form.ajax-form').on('submit', function (e) {
		e.preventDefault();

		// debugger;

		let $form = $(this);

		let formData = new FormData(this);
		let formAction = $form.attr('action');

		let $formToken = $form.find('input[name="_token"]');

		let formSuccessCallback = $form.data('success-callback');
		let formErrorCallback = $form.data('error-callback');

		$.ajax({
			url: formAction,
			method: 'POST',
			headers: {
				'Accept': "applications/json",
				'X-CSRF-TOKEN': $formToken.val()
			},
			contentType: false,
			processData: false,
			data: formData,
			success: function( data, textStatus, jQxhr){
				if (data.hasOwnProperty('redirect')) {
					window.location.href = data.redirect;
				} else if (data.hasOwnProperty('modal')) {

					if (!data.modal.hasOwnProperty('id')) {
						console.error('FORM-AJAX: response modal object has no id field');
					}

					if (!data.modal.hasOwnProperty('action')) {
						console.error('FORM-AJAX: response modal object has no action field');
					}

					let modalId = data.modal.id;
					let modalAction = data.modal.action;

					let $modal = $('#' + modalId);

					if (!$modal.length) {
						console.error('FORM-AJAX: can\'t find modal #' + modalId);
					}

					$modal.modal(modalAction);

				} else {
					console.info('success! but haven\'t redirect or modal command');
				}

				if (formSuccessCallback && window.hasOwnProperty(formSuccessCallback) && typeof window[formSuccessCallback] === 'function') {
					window[formSuccessCallback](data);
				}
			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.error( errorThrown );

				// remove old errors
				$('div.form-group').removeClass('has-error');
				$('div.form-group').find('.help-block').find('strong').remove();

				if (jqXhr.hasOwnProperty('responseJSON') && jqXhr.responseJSON.hasOwnProperty('errors')) {
					ajaxFormProcessErrors($form, jqXhr.responseJSON.errors);
				}

				if (formErrorCallback && window.hasOwnProperty(formErrorCallback) && typeof window[formErrorCallback] === 'function') {
					window[formErrorCallback](jqXhr, textStatus, errorThrown);
				}
			}
		});
	});


	// console.groupEnd();

});
