function processCountrySelect($select, $linkedInput) {

	let warehouseSelector = $select.data('warehouse-selector');
	let $warehouseSelect = $(warehouseSelector);

	let token = $('meta[name="csrf-token"]').attr('content');

	if (!$warehouseSelect.length) {
		console.error('Cant find element with selector: ', warehouseSelector);
		return false;
	}

	$select.on('change', function(){

		let selectedCountryId = $(this).val();
		let selectedCountryName = $(this).find('option:selected').text();
		let selectedCountryNameEN = $(this).find('option:selected').data('en-value');

		if (!selectedCountryId) {
			$warehouseSelect.find('option').remove();
			$warehouseSelect.removeAttr('readonly').selectpicker('refresh').trigger('change');
			return false;
		}

		$linkedInput.val(selectedCountryNameEN.trim());

		let url = '/country/' + selectedCountryId + '/warehouses';

		$.ajax({
			url: url,
			method: 'GET',
			headers: {
				'Accept': "applications/json",
				'X-CSRF-TOKEN': token
			},
			contentType: false,
			processData: false,
			success: function( data, textStatus, jQxhr) {

				$warehouseSelect.find('option').remove();

				for(var i in data) {
					if (data.hasOwnProperty(i)) {
						let warehouse = data[i];
						$warehouseSelect.append('<option value="' + warehouse.id +'">' + warehouse.name + '</option>');
					}
				}

				$warehouseSelect.removeAttr('readonly').selectpicker('refresh').trigger('change');

			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.error( errorThrown );
			}
		});

	});
}

$(document).ready(function(){
	processCountrySelect($('select[name="from_country"]'), $('#inputCountrySenderId'));
	processCountrySelect($('select[name="to_country"]'), $('#inputCountryRecipientId'));
});
