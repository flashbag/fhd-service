$(document).ready(function () {

	let currenciesArray = null;

	let $inputAssessedPrice = $('input[name="assessed_price"]');
	let $inputDutyPrice = $('input[name="duty_price"]');
	let $inputTotalPrice =  $('input[name="total_price"]');

	let $selectCurrencyType = $('select[name="currency_type"]');

	let $selectAssessedPrice = $('select[name="assessed_price_currency"]');
	let $selectDutyPrice = $('select[name="duty_price_currency"]');
	let $selectTotalPrice =  $('select[name="total_price_currency"]');


	// getCurrencies();
	//
	// function getCurrencies() {
	// 	$.ajax({
	// 		type: 'GET',
	// 		url: '/admin/get/currencies',
	// 		headers: {
	// 			"X-CSRF-TOKEN": $('input[name=\'_token\']').val()
	// 		},
	// 		beforeSend: function () {
	//
	// 		},
	// 		complete: function (data) {
	//
	// 		},
	// 		success: function (data) {
	// 			currenciesArray = data;
	// 			mathCurrencies();
	// 		},
	// 		dataType: "json"
	// 	});
	// }

	$inputAssessedPrice.keyup(function () {
		mathSecondarySelect($selectAssessedPrice, $inputAssessedPrice);
	});
	$inputDutyPrice.keyup(function () {
		mathSecondarySelect($selectDutyPrice, $inputDutyPrice);
	});
	$inputTotalPrice.keyup(function () {
		mathSecondarySelect($selectTotalPrice, $inputTotalPrice);
	});

	$selectCurrencyType.on('hidden.bs.select', function (e) {
		mathCurrencies();
	});

	function mathSecondarySelect($select, $input) {

		let currentCurrency = $selectCurrencyType.find('option:selected').data('currencies');

		if (isNaN($selectCurrencyType.val()) || !currentCurrency) {
			return false;
		}

		let index = 0;

		$select.empty();
		currenciesArray.forEach(function (item) {
			$select.append('<option>' + item.name + ': ' + ($input.val() * currentCurrency[index++]).toFixed(2) + ' ' + item.code + '</option>');
		});
		$select.selectpicker('refresh');

	}

	function mathCurrencies() {
		mathSecondarySelect($selectAssessedPrice, $inputAssessedPrice);
		mathSecondarySelect($selectDutyPrice, $inputDutyPrice);
		mathSecondarySelect($selectTotalPrice, $inputTotalPrice);
	}

});


$(document).ready(function(){

	let $inputDeliveryRateKg = $('input[name="delivery_rate_kg"]');
	let $inputWeightCopy = $('input.weight_copy');
	let $inputTotalShipCost = $('input[name="total_ship_cost"]');

	function calcTotalShipCost() {
		let deliveryRateKg = parseInt($inputDeliveryRateKg.val());
		let weightKg = parseInt($inputWeightCopy.val());

		let totalShipCost = 0;

		if (deliveryRateKg && weightKg) {
			totalShipCost = deliveryRateKg * weightKg;
		}

		$inputTotalShipCost.val(totalShipCost);
	}

	calcTotalShipCost();

	$inputDeliveryRateKg.on('keyup', calcTotalShipCost);
	$inputWeightCopy.on('change', calcTotalShipCost);

});
