// SWAL
(function(){
	const successMini = document.querySelector('.js-swal-success_mini');
	const errorMini = document.querySelector('.js-swal-error_mini');
	const warningMini = document.querySelector('.js-swal-warning_mini');

	const success = document.querySelector('.js-swal-success');
	const error = document.querySelector('.js-swal-error');
	const warning = document.querySelector('.js-swal-warning');

	const success2 = document.querySelector('.js-swal-success_border');
	const error2 = document.querySelector('.js-swal-error_border');
	const warning2 = document.querySelector('.js-swal-warning_border');

	const successTime = document.querySelector('.js-swal-success_time');

	if(successMini){
		successMini.addEventListener('click',function(){
			swal({
				className: "swal-modal_successMini",
				title: "Lorem ipsum dolor sit",
				icon: false,
				button: false,
			});
		});
	}

	if(errorMini){
		errorMini.addEventListener('click',function(){
			swal({
				className: "swal-modal_errorMini",
				title: "Lorem ipsum dolor sit",
				icon: false,
				button: false,
			});
		});
	}

	if(warningMini) {
		warningMini.addEventListener('click', function () {
			swal({
				className: "swal-modal_warningMini",
				title: "Lorem ipsum dolor sit",
				icon: false,
				button: false,
			});
		});
	}

	if(success) {
		success.addEventListener('click', function () {
			swal({
				className: "swal-modal_success",
				title: "Lorem ipsum dolor sit",
				text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
				icon: false,
				button: "Button",
			});
		});
	}

	if(error) {
		error.addEventListener('click', function () {
			swal({
				className: "swal-modal_error",
				title: "Lorem ipsum dolor sit",
				text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
				icon: false,
				button: "Button",
			});
		});
	}

	if(warning) {
		warning.addEventListener('click', function () {
			swal({
				className: "swal-modal_warning",
				title: "Lorem ipsum dolor sit",
				text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
				icon: false,
				button: "Button",
			});
		});
	}

	if(success2) {
		success2.addEventListener('click', function () {
			swal({
				className: "swal-modal_success-border",
				title: "Lorem ipsum dolor sit",
				text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
				icon: false,
				button: "Button",
			});
		});
	}

	if(error2) {
		error2.addEventListener('click',function(){
			swal({
				className: "swal-modal_error-border",
				title: "Lorem ipsum dolor sit",
				text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
				icon: false,
				button: "Button",
			});
		});
	}

	if(warning2) {
		warning2.addEventListener('click', function () {
			swal({
				className: "swal-modal_warning-border",
				title: "Lorem ipsum dolor sit",
				text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
				icon: false,
				button: "Button",
			});
		});
	}

	if(successTime) {
		successTime.addEventListener('click', function () {
			swal({
				className: "swal-modal_success_time",
				title: "Время сессии истекает через 15 секунд",
				text: "Пожалуйста нажмите эту кнопку “отмена или авторизуйтесь повторно позже",
				icon: false,
				button: "Отмена",
				timer: 15000,
			});
		});
	}
}());
