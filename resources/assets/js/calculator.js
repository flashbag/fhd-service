const TARIFFS = {
	air: {
		'US_UA': 4.75,
		'CN_UA': 6.7,

		// 'US_CN': 0,
		// 'CN_US': 0,
		//
		// 'UA_US': 0,
		// 'UA_CN': 0,
	},
	water: {
		'US_UA': 2.75,
		'CN_UA': 2.7,

		// 'US_CN': 0,
		// 'CN_US': 0,
		//
		// 'UA_US': 0,
		// 'UA_CN': 0,

	}
};

window.calculator = {
	// boolean defaults
	isImperialSystem: false,
	withDimensions: false,
	withCustomDuty: false,

	transportType: 'air',
	transportTypeLocalized: $('#calculator-href-air').data('type-transport-string'),
	countryFrom: null,
	countryTo: null,
	actualWeight: 0,
	volumeWeight: 0,

	countedWeight: 0,

	dimensions: {
		length: 0,
		width: 0,
		height: 0
	},

	results: {
		deliveryPrimary: 0,
		customDuty: 0,
		deliverySecondary: 0,
		insurance: 0,
		total: 0
	},

	syncWeightLengthCheckboxes: function() {

		let $weightChk = $('input[name="checkbox_weight"]');
		let $lengthChk = $('input[name="checkbox_length"]');

		let isImperialWeight, isImperialLength;

		$weightChk.on('change', function(){
			isImperialWeight = $(this).prop('checked');
			window.calculator.isImperialSystem = isImperialWeight;
			$lengthChk.prop('checked', isImperialWeight);
		});

		$lengthChk.on('change', function(){
			isImperialLength = $(this).prop('checked');
			window.calculator.isImperialSystem = isImperialLength;
			$weightChk.prop('checked', isImperialLength);
		});
	},

	handleAdditionalBlocks: function() {

		let $blockDimensions = $('.calculator-dimensions-block');
		let $blockCustomDuty = $('.calculator-custom-duty-block');

		let $checkboxDimensions = $('input[name="checkbox_dimensions"]');
		let $checkboxCustomDuty = $('input[name="checkbox_custom_duty"]');

		_toggleBlock($checkboxDimensions.prop('checked'), $blockDimensions);
		_toggleBlock($checkboxCustomDuty.prop('checked'), $blockCustomDuty);

		$checkboxDimensions.on('change', function(){
			let isChecked = $(this).prop('checked');
			_toggleBlock(isChecked, $blockDimensions);
			window.calculator.withDimensions = isChecked;
		});

		$checkboxCustomDuty.on('change', function(){
			let isChecked = $(this).prop('checked');
			_toggleBlock(isChecked, $blockCustomDuty);
			window.calculator.withCustomDuty = isChecked;
		});

	},

	handleTransportTypeButtons: function() {

		$('#calculator-href-air').on('click', function(){
			window.calculator.transportType = 'air';
			window.calculator.transportTypeLocalized = $(this).data('type-transport-string');
			window.calculator.updateTypeTransportLabel();
		});

		$('#calculator-href-water').on('click', function(){
			window.calculator.transportType = 'water';
			window.calculator.transportTypeLocalized = $(this).data('type-transport-string');
			window.calculator.updateTypeTransportLabel();
		});
	},

	handleCountriesSelects: function() {

		let $countryFrom = $('#calculator').find('select[name="country_from"]');
		let $countryTo = $('#calculator').find('select[name="country_to"]');

		$countryFrom.on('change', function(){
			let selected = $(this).find('option:selected').val();
			if (selected) {
				window.calculator.countryFrom = selected;
			}
		});

		$countryTo.on('change', function(){
			let selected = $(this).find('option:selected').val();
			if (selected) {
				window.calculator.countryTo = selected;
			}
		});
	},

	getNeededTariff: function () {

		let firstKey = window.calculator.transportType;
		let secondKey = window.calculator.countryFrom + '_' + window.calculator.countryTo;

		if (TARIFFS.hasOwnProperty(firstKey) &&
			TARIFFS[firstKey].hasOwnProperty(secondKey) ) {
			return TARIFFS[firstKey][secondKey];
		}

		return 0;

	},

	normalizeToMetric: function () {

		let actualWeight = parseFloat($('input[name="actual_weight"]').val()) || 0;

		let length = parseInt($('input[name="calculator-length"]').val()) || 0;
		let width = parseInt($('input[name="calculator-width"]').val()) || 0;
		let height = parseInt($('input[name="calculator-height"]').val()) || 0;

		if (!window.calculator.isImperialSystem) {

			window.calculator.actualWeight = actualWeight;
			window.calculator.countedWeight = actualWeight;

			window.calculator.dimensions.length = length;
			window.calculator.dimensions.width = width;
			window.calculator.dimensions.height = height;

			return false;
		}


		// convert weight in ft to kg
		// 0.45359237 is inverted coefficient = 1/2.204622622
		let actualWeightNormalized = actualWeight * 0.45359237;


		window.calculator.actualWeight = actualWeightNormalized;
		window.calculator.countedWeight = actualWeightNormalized;

		if (window.calculator.withDimensions) {

			let multiplier = 2.54;

			let lengthNormalized = length * multiplier;
			let widthNormalized = width * multiplier;
			let heightNormalized = height * multiplier;

			window.calculator.dimensions.length = lengthNormalized;
			window.calculator.dimensions.width = widthNormalized;
			window.calculator.dimensions.height = heightNormalized;

		}

	},

	updateTypeTransportLabel: function() {
		$('.results-delivery-primary').find('.delivery-primary-title span:not(.volume-weight-counting)').text(window.calculator.transportTypeLocalized);
	},

	updateResults: function() {

		if (window.calculator.countedWeight > window.calculator.actualWeight) {
			$('.volume-weight-counting').show();
		} else {
			$('.volume-weight-counting').hide();
		}

		$('#result-delivery-primary').find('span').text(window.calculator.results.deliveryPrimary);
		$('#result-custom-duty').find('span').text(window.calculator.results.customDuty);
		$('#result-delivery-secondary').find('span').text(window.calculator.results.deliverySecondary);
		$('#result-insurance').find('span').text(window.calculator.results.insurance);
		$('#result-total').find('span').text(window.calculator.results.total);
	},
	
	sendStatistic: function() {

		let token = $('meta[name="csrf-token"]').attr('content');
		
		let data = {
			'_token' : token,
			'type_transport': window.calculator.transportType,
			'country_from' : window.calculator.countryFrom,
			'country_to' : window.calculator.countryTo,
			'is_imperial' : window.calculator.isImperialSystem,
			'with_dimensions' : window.calculator.withDimensions,
			'with_custom_duty' : window.calculator.withCustomDuty,
			'actual_weight' : window.calculator.actualWeight,
			'width' : window.calculator.dimensions.width,
			'length' : window.calculator.dimensions.length,
			'height' : window.calculator.dimensions.height,
			'assessed_price' : $('input[name="calculator-assessed-price"]').val(),
			'currency' : $('select[name="calculator-currency"]').find('option:selected').val(),
			'results' : JSON.stringify(window.calculator.results)
		};

		$.ajax({
			url: '/calculator',
			method: 'POST',
			headers: {
				'Accept': "applications/json",
				'X-CSRF-TOKEN': token
			},
			data: data,
			success: function( data, textStatus, jQxhr) {
			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.error( errorThrown );
			}
		});
		
	},

	calculateVolumeWeight: function() {

		if (window.calculator.withDimensions) {

			let volumeWeightKg = Math.ceil((
				window.calculator.dimensions.length *
				window.calculator.dimensions.width *
				window.calculator.dimensions.height
			) / 5000);

			if (volumeWeightKg > window.calculator.actualWeight) {
				window.calculator.countedWeight = volumeWeightKg;
			}
		}

	},

	magicHappensHere: function() {

		window.calculator.normalizeToMetric();
		window.calculator.calculateVolumeWeight();

		let usdPerKg = window.calculator.getNeededTariff();

		window.calculator.results.deliveryPrimary = usdPerKg * window.calculator.actualWeight;

		if (window.calculator.withDimensions) {
			window.calculator.results.deliveryPrimary = usdPerKg * window.calculator.countedWeight;
		}

		if (window.calculator.withCustomDuty) {

			let eurMultiplier = 0;
			let usdMultiplier = 0;

			let eurMultiplierToUsd = parseFloat($('input[name="eur-multiplier-to-usd"]').val()) || 0;
			let $currencyOption = $('select[name="calculator-currency"]').find('option:selected');

			if ($currencyOption.length && $currencyOption.val()) {
				eurMultiplier = $currencyOption.data('multiplier-eur');
				usdMultiplier = $currencyOption.data('multiplier-usd');
			}

			let assessedPrice = parseFloat($('input[name="calculator-assessed-price"]').val()) || 0;
			let assessedPriceInEur = assessedPrice * eurMultiplier;
			let assessedPriceInUsd = assessedPrice * usdMultiplier;


			if (assessedPriceInEur > 150) {
				let customDutyEur = ((assessedPriceInEur - 150 ) * 0.32);
				let customDutyUsd = customDutyEur * eurMultiplierToUsd;
				window.calculator.results.customDuty = customDutyUsd;
			} else {
				window.calculator.results.customDuty = 0;
			}

			if (assessedPriceInUsd) {
				window.calculator.results.insurance = assessedPriceInUsd * 0.02;
			} else {
				window.calculator.results.insurance = 0;
			}


		} else {
			window.calculator.results.customDuty = 0;
			window.calculator.results.insurance = 0;
		}

		window.calculator.results.total = 0;

		window.calculator.results.total += parseFloat(window.calculator.results.deliveryPrimary);
		window.calculator.results.total += parseFloat(window.calculator.results.customDuty);
		window.calculator.results.total += parseFloat(window.calculator.results.deliverySecondary);
		window.calculator.results.total += parseFloat(window.calculator.results.insurance);

		window.calculator.results.deliveryPrimary = formatMoney(window.calculator.results.deliveryPrimary,2);
		window.calculator.results.customDuty = formatMoney(window.calculator.results.customDuty, 2);
		window.calculator.results.deliverySecondary = formatMoney(window.calculator.results.deliverySecondary, 2);
		window.calculator.results.insurance = formatMoney(window.calculator.results.insurance, 2);

		window.calculator.results.total = formatMoney(window.calculator.results.total, 2);

		window.calculator.updateResults();
		window.calculator.sendStatistic();

	}
};

function tabsNavigation() {

	const tabs = document.querySelectorAll('.calculator-custom__tab');

	tabs.forEach((item) => {
		item.addEventListener('click',function(e){
			e.preventDefault();
			tabs.forEach(tab => tab.classList.remove('active'));
			this.classList.add('active');
		},false)
	});

}
function _toggleBlock(isChecked, $block) {

	if (isChecked) {
		$block.show();
	} else {
		$block.hide();
	}
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
	try {
		decimalCount = Math.abs(decimalCount);
		decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

		const negativeSign = amount < 0 ? "-" : "";

		let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
		let j = (i.length > 3) ? i.length % 3 : 0;

		return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	} catch (e) {
	}
}


$(document).ready(function() {

	tabsNavigation();

	window.calculator.handleAdditionalBlocks();
	window.calculator.updateTypeTransportLabel();

	window.calculator.syncWeightLengthCheckboxes();

	window.calculator.handleTransportTypeButtons();
	window.calculator.handleCountriesSelects();

	// $('#calculator-calculate').on('click', window.calculator.magicHappensHere);


	$('#calculator-calculate').on('click',function() {
		this.parentNode.classList.add('results');

	    const checkboxDuty = document.querySelector('input[name="checkbox_custom_duty"]');

		const calculatorCurrency = $('select[name="calculator-currency"]').next('.select2-container--default').find('.select2-selection__rendered');
		const calculatorAssessedPrice = $('input[name="calculator-assessed-price"]');

		const calculatorAssessedPriceNew = $('input[name="calculator-assessed-price"]').val();
		const calculatorCurrencyNew = $('select[name="calculator-currency"]').find('option:selected').val();

	    	if( checkboxDuty.checked ) {

				if(calculatorAssessedPriceNew.length < 1 && calculatorCurrencyNew !== 'Валюта'){
					calculatorAssessedPrice.css('border-color','#EB5757');
					calculatorCurrency.css('border-color','#0cad07');
					return false;
				}
				else if(calculatorAssessedPriceNew.length > 0 && calculatorCurrencyNew === 'Валюта'){
					calculatorAssessedPrice.css('border-color','#0cad07');
					calculatorCurrency.css('border-color','#EB5757');
					return false;
				}
				else if (calculatorAssessedPriceNew.length > 0 && calculatorCurrencyNew !== 'Валюта'){
					calculatorAssessedPrice.removeAttr('style');
					calculatorCurrency.css('border-color','#E0E0E0');

					// alert( 'Success 2' );
					return window.calculator.magicHappensHere();
				}

				else {
					calculatorAssessedPrice.css('border-color','#EB5757');
					calculatorCurrency.css('border-color','#EB5757');
					return false;
				}
			}
	    	else{
				calculatorAssessedPrice.removeAttr('style');
				calculatorCurrency.css('border-color','#E0E0E0');

				// alert( 'Success 1' );
				return window.calculator.magicHappensHere();
			}
		}
	);

});

