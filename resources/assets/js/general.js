function isChromeBrowser() {

	var isChromium = window.chrome;
	var winNav = window.navigator;
	var vendorName = winNav.vendor;
	var isOpera = typeof window.opr !== "undefined";
	var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
	var isIOSChrome = winNav.userAgent.match("CriOS");

	if (isIOSChrome) {
		// Google Chrome on iOS
		return true;
	} else if(
		isChromium !== null &&
		typeof isChromium !== "undefined" &&
		vendorName === "Google Inc." &&
		isOpera === false &&
		isIEedge === false
	) {
		return true;
	}

	return false;

}

$(document).ready(function () {

	//mobile menu
	$('.mobile-menu').on('click', function () {
		$('.navigation').toggleClass('show-mobile');
	});

	//dropdown
	$(document).on('click', function (e) {
		let dropdown = $('.dropdown-custom'),
			mobileMenu = $('.navigation');

		if (dropdown.has(e.target).length === 0) {
			dropdown.children().removeClass('show-dropdown');
		}
		if (mobileMenu.has(e.target).length === 0) {
			mobileMenu.removeClass('show-mobile');
		}
	});

	$('.dropdown-custom').on('click', '.dropdown-button', function () {

		if ($(this).next().hasClass('show-dropdown')) {
			$(this).parents('.dropdown-custom').children('.dropdown-block').removeClass('show-dropdown');
		} else {
			$('.dropdown-block').removeClass('show-dropdown');
			$(this).parents('.dropdown-custom').children('.dropdown-block').addClass('show-dropdown');
		}

		if ($(this).parents('.dropdown-custom').hasClass('profile')) {
			$('.navigation').removeClass('show-mobile');
		}
	});


	$('body').on('click', 'a[href="#"]', function (e) {
		e.preventDefault();
	});

});


$(document).ready(function () {

	// disable autocomplete for all inputs
	$(document).on('focus', ':input', function () {
		$(this).attr('autocomplete', 'off');
	});

	function filterOnlyNumeric() {
		var sanitized = $(this).val().replace(/[^0-9.]/g, '');
		sanitized = sanitized.replace(/\.(?=.*\.)/, '');
		$(this).val(sanitized);
	}

	function filterOnlyInteger() {
		var sanitized = $(this).val().replace(/[^0-9]/g, '');
		sanitized = sanitized.replace(/\.(?=.*\.)/, '');
		$(this).val(sanitized);
	}

	$(document).on('keyup', '.only-numeric', filterOnlyNumeric);
	$(document).on('change', '.only-numeric', filterOnlyNumeric);

	$(document).on('keyup', '.only-integer', filterOnlyInteger);
	$(document).on('change', '.only-integer', filterOnlyInteger);


	$("#inputNumber").intlTelInput();
	$("#inputNumberRecipient").intlTelInput();
	$('.search-slider-tracker input[name="number"]').intlTelInput();
	$('#numberReg').intlTelInput();
	$('.contacts-page .default-form input[name="number"]').intlTelInput();

	$('#numberReg').on("countrychange", function (e, countryData) {
		$(this).val('+' + countryData.dialCode);
	});

	$('.search-slider-tracker input[name="number"]').on("countrychange", function (e, countryData) {
		$(this).val('+' + countryData.dialCode);
	});

	$("#inputNumber").on("countrychange", function (e, countryData) {
		$(this).val('+' + countryData.dialCode);
	});

	$("#inputNumberRecipient").on("countrychange", function (e, countryData) {
		$(this).val('+' + countryData.dialCode);
	});

	$('.contacts-page .default-form input[name="number"]').on("countrychange", function (e, countryData) {
		$(this).val('+' + countryData.dialCode);
	});

	$(".carousel").swipe({

		swipe: function (event, direction, distance, duration, fingerCount, fingerData) {

			if (direction == 'left') $(this).carousel('next');
			if (direction == 'right') $(this).carousel('prev');

		},
		allowPageScroll: "vertical"

	});


	/* FAKE CHECKBOX EVENTS*/
	function toggleFakeCheckbox(input) {
		var wrapper = input.closest('.fake-checkbox');
		if (input.prop('checked')) {
			wrapper.addClass('checked');
		} else {
			wrapper.removeClass('checked');
		}
	}

	function toggleFakeRadio(input) {
		var wrapper = input.closest('.fake-radio');
		var currentInputChecked = input.prop('checked');
		var inputName = input.attr('name');
		input.closest('form')
			.find('input[name="' + inputName + '"]').each(function () {
			$(this).prop('checked', false).closest('.fake-radio').removeClass('checked');
		});


		input.prop('checked', currentInputChecked);
		if (currentInputChecked) {
			wrapper.addClass('checked');
		} else {
			wrapper.removeClass('checked');
		}
	}

	function InitFakeCheckbox() {
		$('body').find('.fake-checkbox').each(function () {
			toggleFakeCheckbox($(this).find('input'));
		});
	}

	function InitFakeRadio() {
		$('body').find('.fake-radio').each(function () {
			if ($(this).find('input').prop('checked')) {
				$(this).addClass('checked');
			} else {
				$(this).removeClass('checked');
			}
		});
	}

	$(document).on('change', '.fake-checkbox input', function () {
		toggleFakeCheckbox($(this));
	});
	$(document).on('change', '.fake-radio input', function () {
		toggleFakeRadio($(this));
	});
	InitFakeCheckbox();
	InitFakeRadio();

	$(document).on('change', '.fake-input-file input', function () {
		var val = $(this).val().replace(/C:\\fakepath\\/i, '');
		var parent = $(this).closest('.fake-input-file');
		parent.find('.placeholder').text(val);
	});

	/*********************/

	function trackersMapTrigger() {
		$('.map-section').toggle();
	}

	$(document).on('click', '.map-trigger', function () {
		$('.map-trigger, .map-section').toggle();
		// $('.map-section').toggle();
		$('.history-section').toggleClass('col-lg-4').toggleClass('col-lg-10');
	});

});


// Initialize and add the map
function initMap() {
	let mapElement = document.getElementById('google-map');
	if (mapElement !== null) {
		let uluru = {lat: 50.445073, lng: 30.563222};
		let map = new google.maps.Map(
			mapElement, {zoom: 4, center: uluru}
		);
		// var marker = new google.maps.Marker({position: uluru, map: map});
	}
}


// FROM ADMIN LAYOUT

$(document).ready(function () {


	$('.search-modal').click(function () {
		getTypeContainers();
	});

	function getTypeContainers() {
		$.get('/admin/containers/types/', function (data) {
			if (data.length > 0) {
				$('select[name="container_id"]').empty();
				$('select[name="container_id"]').append('<option selected value="">--</option>');
				data.forEach(function (item) {
					$('select[name="container_id"]').append('<option value="' + item.id + '">' + item.title + '</option>');
				});
				$('select[name="container_id"]').selectpicker('refresh');
			}
		}, 'json');
	}

});

$('.summernote').summernote({
	height: 200,
	lang: 'ru-RU',
	toolbar: [
		['style', ['bold', 'italic', 'underline', 'clear']],
	]
});


(function ($) {
	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	function showError(field, msg) {
		msg = msg || 'Error';
		const wrapper = field.closest('.form-group');
		wrapper.find('.help-block').remove();
		wrapper.removeClass('has-error');

		if (field && field.length) {
			setTimeout(function () {
				wrapper.addClass('has-error')
					.append($('<span class="help-block"><strong>' + msg + '</strong></span>'));
			});
		}
	}

	function isDigit(letter) {
		return /^\d+$/.test(letter);
	}

	$(document).ready(function () {
		//######## field validation
		function inputTypeCheck() {
			const field = $(this);
			if (field.attr('type') === 'number') {
				if (field.val() < field.attr('min') && field.val() !== '') {
					field.val(field.attr('min'));
				}
			}
		}
		$(document)
			.on('focusout', 'input', function () {
				$(this).val($(this).val().trim());
				const field = $(this);
				field.val(field.val().trim());

				// email check
				if (field.attr('type') === 'email') {
					const email = field.val();
					if (email.length && !validateEmail(email)) {
						showError($(this), $(this).data('error-msg'));
					}
				}
			})
			.on('keydown', 'input', function (event) {
				const field = $(this);
				// for without digits
				if (field.data('without-digits') && isDigit(event.key)) {
					return false;
				}

				if (field.attr('type') === 'number' && !isDigit(event.key)) {
					if (event.keyCode !== 8 && event.keyCode !== 37 && event.keyCode !== 39) {
						return false;
					}
				}
			})
			.on('keyup', 'input', inputTypeCheck)
			.on('change', 'input', inputTypeCheck);
	});

})(jQuery);

	(function(){
		'use strict';

		// preloader
		const getPreloader = (el) => {
			const preloader = document.querySelector(el);
			let timeID = null;

			if(preloader){
				window.addEventListener('load',function(){
					const loadData = () => {
						return new Promise((resolve, reject) => {
							timeID = setTimeout(resolve, 400); //wait for page load PLUS two seconds.
						})
					};
					loadData()
						.catch((reject) => { return console.log('getPreloader ', reject)})
						.then(() => {
							preloader.classList.add('preloader_hidden'); //makes page more lightweight
						})
						.then(() => {
							clearTimeout(timeID);
						})
				});
			}else{
				return false
			}
		};
		getPreloader('#js_preloader');

	}());