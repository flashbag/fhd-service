$(document).ready(function () {

	let toggle = $('.toggle'),
		title = $('.blank-title'),
		settings = $('.start-work'),
		buttons = $('.buttons'),
		$body = $('body'),
		btnSubmit = $('.button-submit'),
		localServer = window.location.host,
		protocol = window.location.protocol;


	$('head').append("<link id='a4' href=" + protocol + "//" + localServer + "/css/print/a4.css rel='stylesheet' />");

	// setTimeout(function () {
	// 	body.addClass('mode-on');
	// 	settings.addClass('show');
	// }, 1000);


	// toggle.on('click', function () {
	// 	$(this).toggleClass('active');
	// 	$(this).children().toggleClass('checked');
	// 	$('body').toggleClass('hide');
	// 	$('.content .checkbox').toggleClass('checked');
	// });

	// buttons.on('click', '.types', function () {
	// 	if ($(this).text() === 'A4') {
	//
	// 		$('.types').removeClass('active');
	// 		$('#a5').remove();
	// 		if(!$('#a4').length) {
	// 			$('head').append("<link id='a4' href=" + protocol + "//" + localServer + "/css/a4.css rel='stylesheet' />");
	// 		}
	// 		$(this).addClass('active');
	// 		$('.current-type').addClass('show').text('A4');
	//
	// 	} else {
	//
	// 		$('.types').removeClass('active');
	// 		$('#a4').remove();
	// 		$('head').append("<link id='a5' href=" + protocol + "//" + localServer + "/css/a5.css rel='stylesheet' />");
	// 		$(this).addClass('active');
	// 		$('.current-type').addClass('show').text('A5');
	//
	// 	}
	//
	// });

	function getUrlParameter(name) {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
		var results = regex.exec(location.search);
		return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	}

	let color = getUrlParameter('color');

	$body.removeClass('grey blue orange');

	if (color === 'grey') {
		$body.addClass('grey');
	} else if(color === 'blue') {
		$body.addClass('blue');
	} else if(color === 'orange') {
		$body.addClass('orange');
	}

	// btnSubmit.on('click', function () {
	// 	body.removeClass('mode-on');
	// 	settings.removeClass('show');
	// 	setTimeout(function () {
	// 		window.print();
	// 	}, 1000);
	// });

	// title.on('click', function () {
	// 	$(this).next().slideToggle();
	// 	$(this).children('.title-number').toggleClass('animate');
	// });


	$('.wrapoff').each(function(){

		let $wrapoff = $(this);
		let $text = $wrapoff.find('.text');

		if (!$text.length) {
			return false;
		}

		let textStyle = window.getComputedStyle($text[0]);

		let textStyleWidth = parseInt(textStyle['width'].replace('px',''));
		let textStyleMaxWidth = parseInt(textStyle['max-width'].replace('px',''));

		if (textStyleWidth < textStyleMaxWidth) {
			$wrapoff.addClass('wrapoff-disabled');
		}

	});

});
