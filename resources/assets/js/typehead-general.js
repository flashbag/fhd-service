let typeheadFunctions = {

	keysObj: {
		// left: 37,
		up: 38,
		// right: 39,
		down: 40
	},

	keysArray: [38, 40],

	clickOutsideList: function(event){

		let $allTypeheadLists = $('ul.typehead-list');

		$allTypeheadLists.each(function(){
			let $list = $(this);
			let $container = $(this).closest('.typehead');

			if ($container.hasClass('open-list')) {
				// if clicked outside any list
				if (!$(event.target).closest('.typehead').length) {
					$list.removeClass('visible-typehead-list');
				}
			}
		});

	},

	handleListNavigation: function ($element, event) {

		let that = this;
		let keyCode = event.keyCode;

		if (event.keyCode === 13) {
			// console.log('handleListNavigation: pressed Enter. stop');
			return false;
		}

		if (that.keysArray.indexOf(keyCode) === -1) {
			// console.log('handleListNavigation: pressed not up or down. continue');
			// key pressed is not "Up" or "Down", return true, continue autocomplete
			return true;
		}

		let $parentOfParent = $element.parent().parent();
		let $typeheadList = $parentOfParent.find('.typehead-list');

		let optionsCount = $typeheadList.find('li').length;

		if (!optionsCount) {
			// console.log('list is empty. continue');
			// if list is empty, return true, continue autocomplete
			return true;
		}

		$typeheadList.find('li').removeClass('active');

		let optionSelectedIndex = $element.data('option-selected');

		if (isNaN(optionSelectedIndex)) {
			optionSelectedIndex = -1;
			$element.data('option-selected', -1);
		}

		let newIndex = optionSelectedIndex;

		if (keyCode === that.keysObj.up && (optionSelectedIndex === -1 || optionSelectedIndex > 0 )) {
			newIndex = optionSelectedIndex - 1;
		}

		if (keyCode === that.keysObj.down && optionSelectedIndex < optionsCount - 1) {
			newIndex = optionSelectedIndex + 1;
		}

		// console.log('handleListNavigation: newIndex:', newIndex, 'stop autocomplete');

		// move cursor to the end
		var fldLength= $element.val().length;
		// $element.focus();
		$element[0].setSelectionRange(fldLength, fldLength);

		$element.data('option-selected', newIndex);

		$($typeheadList.find('li')[newIndex]).addClass('active');

		return false;
	},

	bindListItemSelect: function ($elementOriginal, selector, handleCallback) {

		if (typeof handleCallback !== 'function') {
			throw new Error("You didn't passed handleCallback function to bindListItemSelect!. Navigation may not be working properly")
		}

		$(document).on('click', selector, function (eventClick) {

			// console.log('bindListItemSelect item.click:', selector);

			let $target = $(eventClick.target);
			let $element = $target.closest('.typehead').find('input');

			let selected = $target.text();

			$element.val(selected);

			typeheadFunctions.listClear($element);

			handleCallback($element, $target, selected);


		});

		$elementOriginal.on('keyup', function (event) {

			let $target = $(event.target);
			let $element = $target.closest('.typehead').find('input');

			let $parentOfParent = $element.parent().parent();
			let $typeheadList = $parentOfParent.find('.typehead-list');
			let optionSelectedIndex = $element.data('option-selected');

			if (event.keyCode === 13 && optionSelectedIndex > -1 && $typeheadList.hasClass('visible-typehead-list')) {

				// console.log('bindListItemSelect keyup ENTER');

				let $selectedListItem = $($typeheadList.find('li')[optionSelectedIndex]);

				let selected = $selectedListItem.text();

				$element.val(selected);


				handleCallback($element, $selectedListItem, selected);
				typeheadFunctions.listClear($element);
			}
 		});

	},

	listProgress: function ($element) {
		let $parentOfParent = $element.parent().parent();

		$parentOfParent.find('.refresh-input-informer').addClass('rotating');
		$parentOfParent.find('.typehead-list').empty();
	},

	listClear: function ($element) {
		let $parentOfParent = $element.parent().parent();
		let $typeheadList = $parentOfParent.find('.typehead-list');

		$element.data('option-selected', -1);

		$typeheadList.empty();
		if ($typeheadList.hasClass('visible-typehead-list')) {
			$typeheadList.removeClass('visible-typehead-list');
		}
	},

	listFill: function ($element, results, itemTemplateFunction) {

		let $parentOfParent = $element.parent().parent();
		let $typeheadList = $parentOfParent.find('.typehead-list');
		let $typeheadInformer = $parentOfParent.find('.refresh-input-informer');

		if (results.length > 0) {
			$parentOfParent.addClass('open-list');

			results.forEach(function (item) {
				let item2append = itemTemplateFunction(item);
				$typeheadList.append(item2append);
			});

			$typeheadList.addClass('visible-typehead-list');
		} else {
			$typeheadList.removeClass('visible-typehead-list');
		}

		$typeheadInformer.removeClass('rotating');
	}
};

$(document).ready(function () {
	$(document).click(typeheadFunctions.clickOutsideList);
});
