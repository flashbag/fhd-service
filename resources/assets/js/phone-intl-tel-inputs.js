$(document).ready(function () {

	let $numberSender = $("#numberSender");
	let $numberRecipient = $("#numberRecipient");
	let $inputNumberSearch = $("#inputNumberSearch");


	let phoneValidationObject = {
		options: {
			autoHideDialCode: false,
			nationalMode: false,
			utilsScript: "//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
		},
		errorMap: [
			"Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"
		],

		init: function () {

			let self = this;

			if ($numberSender.length) {
				$numberSender.intlTelInput(self.options); // initialise plugin
				$numberSender.blur(self.elementBlur); // on blur: elementBlur
				$numberSender.on("keyup change", self.elementReset); // on keyup / change flag: elementReset
				$numberSender.on("keyup change", self.clearChars); // on keyup / change flag: clearChars
			}

			if ($numberRecipient.length) {
				$numberRecipient.intlTelInput(self.options); // initialise plugin
				$numberRecipient.blur(self.elementBlur); // on blur: elementBlur
				$numberRecipient.on("keyup change", self.elementReset); // on keyup / change flag: elementReset
				$numberRecipient.on("keyup change", self.clearChars); // on keyup / change flag: clearChars
			}

			if ($inputNumberSearch.length) {
				$inputNumberSearch.intlTelInput(self.options); // initialise plugin
				$inputNumberSearch.blur(self.elementBlur); // on blur: elementBlur
				$inputNumberSearch.on("keyup change", self.elementReset); // on keyup / change flag: elementReset
				$inputNumberSearch.on("keyup change", self.clearChars); // on keyup / change flag: clearChars
			}
		},
		clearChars: function () {
			// $numberSender
			let sanitized = $(this).val().replace(/[^+0-9.]/g, '');
			// Remove the first point if there is more than one
			sanitized = sanitized.replace(/\.(?=.*\.)/, '');
			$(this).val(sanitized);
		},
		elementReset: function (event) {
			let $element = $(event.target);
			let $elementFormGroup = $element.closest('.form-group');

			$element.removeClass("error");
			$elementFormGroup.removeClass("has-error");

			let $phoneValidMsg = $elementFormGroup.find('.phone-valid-msg');
			let $phoneErrorMsg = $elementFormGroup.find('.phone-error-msg');

			$phoneErrorMsg.html('').addClass('hide');
			$phoneValidMsg.addClass('hide');
		},
		elementBlur: function (event) {

			phoneValidationObject.elementReset(event);

			let $element = $(event.target);
			let $elementFormGroup = $element.closest('.form-group');

			if ($.trim($element.val())) {
				if ($element.intlTelInput("isValidNumber")) {

					let $phoneValidMsg = $elementFormGroup.find('.phone-valid-msg');
					$phoneValidMsg.removeClass("hide");
					let getCode = $element.intlTelInput('getSelectedCountryData').dialCode;

				} else {
					let errorCode = $element.intlTelInput('getValidationError');
					let $phoneErrorMsg = $elementFormGroup.find('.phone-error-msg');

					$elementFormGroup.addClass("has-error");
					$phoneErrorMsg.html(phoneValidationObject.errorMap[errorCode]).removeClass('hide');
				}
			}
		}
	};

	phoneValidationObject.init();

});
