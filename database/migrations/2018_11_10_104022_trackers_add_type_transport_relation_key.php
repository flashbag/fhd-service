<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddTypeTransportRelationKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trackers', function(Blueprint $table){
            $table->dropColumn('type_transport');
        });

        Schema::table('trackers', function(Blueprint $table){
            $table->integer('type_transport_id')->unsigned()->nullable()->after('id_moderator');;
            $table->foreign('type_transport_id')->references('id')->on('type_transports')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trackers', function(Blueprint $table){
            $table->dropForeign(['type_transport_id']);
            $table->dropColumn('type_transport_id');
        });

        Schema::table('trackers', function(Blueprint $table){
            $table->string('type_transport', 100)->default('FHDbySea')->after('id_moderator');
        });

    }
}
