<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_duties', function (Blueprint $table) {
            $table->integer('order_number')->after('type');
        });

        Schema::table('bill_transportations', function (Blueprint $table) {
            $table->integer('order_number')->after('type');
        });

        Schema::table('special_handlings', function (Blueprint $table) {
            $table->integer('order_number')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_duties', function (Blueprint $table) {
            $table->dropColumn('order_number');
        });
        Schema::table('bill_transportations', function (Blueprint $table) {
            $table->dropColumn('order_number');
        });
        Schema::table('special_handlings', function (Blueprint $table) {
            $table->dropColumn('order_number');
        });
    }
}
