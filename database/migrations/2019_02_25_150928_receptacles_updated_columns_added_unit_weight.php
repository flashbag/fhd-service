<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceptaclesUpdatedColumnsAddedUnitWeight extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receptacles', function (Blueprint $table) {

			$table->integer('unit_of_weight_id')->unsigned()->nullable()->after('unit_of_length_id');
			$table->foreign('unit_of_weight_id')->references('id')->on('measure_units')->onDelete('cascade');

			$table->dropColumn('volume_weight_ft');

			$table->renameColumn('width_cm', 'width');
			$table->renameColumn('length_cm', 'length');
			$table->renameColumn('height_cm', 'height');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receptacles', function (Blueprint $table) {

			$table->dropForeign(['unit_of_weight_id']);
			$table->dropColumn('unit_of_weight_id');

			$table->double('volume_weight_ft')->nullable()->after('volume_weight');

			$table->renameColumn('width', 'width_cm');
			$table->renameColumn('length', 'length_cm');
			$table->renameColumn('height', 'height_cm');

		});
	}
}
