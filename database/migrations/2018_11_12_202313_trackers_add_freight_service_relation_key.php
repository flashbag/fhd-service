<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddFreightServiceRelationKey extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function(Blueprint $table){
			$table->dropColumn('express_freight');

		});

		Schema::table('trackers', function(Blueprint $table){
			$table->integer('freight_service_id')->unsigned()->nullable()->after('bill_transportation_id');;
			$table->foreign('freight_service_id')->references('id')->on('freight_services')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function(Blueprint $table){
			$table->dropForeign(['freight_service_id']);
			$table->dropColumn('freight_service_id');

		});

		Schema::table('trackers', function(Blueprint $table){

			$table->string('express_freight', 100)->default('priority freight')->after('bill_transportation_id')->nullable();
		});

	}
}
