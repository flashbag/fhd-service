<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trackers', function(Blueprint $table)
        {
            $table->string('type_transport', 100)->default('FHDbySea')->after('id_moderator');
            $table->string('sender_vat')->after('sender_index')->nullable();
            $table->integer('sender_number_account')->after('sender_vat')->nullable();
            $table->string('recipient_custom_identify')->after('recipient_index')->nullable();
            $table->integer('recipient_number_account')->after('recipient_custom_identify')->nullable();

            $table->string('bill_duties', 100)->default('sender')->after('recipient_document_id')->nullable();
            $table->string('bill_transportation', 100)->default('sender')->after('bill_duties')->nullable();
            $table->string('express_freight', 100)->default('priority freight')->after('bill_transportation')->nullable();
            $table->string('special_handling', 100)->default('hold at FHD warehouse')->after('express_freight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trackers', function(Blueprint $table)
        {
            $table->dropColumn('type_transport');
            $table->dropColumn('sender_vat');
            $table->dropColumn('sender_number_account');
            $table->dropColumn('recipient_custom_identify');
            $table->dropColumn('recipient_number_account');
            $table->dropColumn('bill_duties');
            $table->dropColumn('bill_transportation');
            $table->dropColumn('express_freight');
            $table->dropColumn('special_handling');


        });
    }
}

