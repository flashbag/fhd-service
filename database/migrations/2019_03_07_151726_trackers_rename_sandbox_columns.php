<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersRenameSandboxColumns extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->renameColumn('is_sandbox', 'is_unprocessed');
			$table->renameColumn('is_sandbox_lvl2', 'is_processed');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->renameColumn('is_unprocessed', 'is_sandbox');
			$table->renameColumn('is_processed', 'is_sandbox_lvl2');

		});
	}
}
