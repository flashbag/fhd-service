<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersRemoveReceptaclesColumns extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function(Blueprint $table){

			$table->dropColumn('type_inventories');

			$table->dropColumn('width_cm');
			$table->dropColumn('length_cm');
			$table->dropColumn('height_cm');

			$table->dropColumn('actual_weight');
			$table->dropColumn('volume_weight');
			$table->dropColumn('volume_weight_ft');

			$table->dropColumn('assessed_price');

			$table->dropColumn('description_item');
			$table->dropColumn('note_item');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function(Blueprint $table){

			$table->integer('type_inventories')->after('number_seats');

			$table->double('width_cm')->nullable()->after('type_inventories');
			$table->double('length_cm')->nullable()->after('width_cm');
			$table->double('height_cm')->nullable()->after('length_cm');

			$table->double('actual_weight')->after('height_cm');
			$table->double('volume_weight')->nullable()->after('actual_weight');
			$table->double('volume_weight_ft')->nullable()->after('volume_weight');

			$table->double('assessed_price')->nullable()->after('volume_weight_ft');

			$table->text('description_item')->after('ship_rate_cur');
			$table->text('note_item')->nullable()->after('description_item');

		});

	}
}
