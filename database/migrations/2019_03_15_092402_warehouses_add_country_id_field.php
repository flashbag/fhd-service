<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WarehousesAddCountryIdField extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('warehouses', function (Blueprint $table) {

			$table->integer('country_id')->unsigned()->nullable()->after('id');
			$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('warehouses', function (Blueprint $table) {

			$table->dropForeign(['country_id']);
			$table->dropColumn('country_id');

		});
	}
}
