<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->double('uah')->nullable();
            $table->double('usd')->nullable();
            $table->double('eur')->nullable();
            $table->double('rub')->nullable();
            $table->double('try')->nullable();
            $table->double('cad')->nullable();
            $table->double('aed')->nullable();
            $table->double('ron')->nullable();
            $table->double('ils')->nullable();
            $table->double('sgd')->nullable();
            $table->double('krw')->nullable();
            $table->double('jpy')->nullable();
            $table->double('idr')->nullable();
            $table->double('chf')->nullable();
            $table->double('aud')->nullable();
            $table->double('brl')->nullable();
            $table->double('ars')->nullable();
            $table->double('mdl')->nullable();
            $table->double('mxn')->nullable();
            $table->double('jod')->nullable();
            $table->double('bgn')->nullable();
            $table->double('byn')->nullable();
            $table->double('inr')->nullable();
            $table->double('azn')->nullable();
            $table->double('gel')->nullable();
            $table->double('tjs')->nullable();
            $table->double('uzs')->nullable();
            $table->double('kzt')->nullable();
            $table->double('egp')->nullable();
            $table->double('lyd')->nullable();
            $table->double('tnd')->nullable();
            $table->double('gbp')->nullable();
            $table->double('pln')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
