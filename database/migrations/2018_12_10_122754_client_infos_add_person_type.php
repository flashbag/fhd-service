<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientInfosAddPersonType extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_infos', function(Blueprint $table) {

			$table->tinyInteger('person_type')->nullable()->default(1)->after('type');

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('client_infos', function(Blueprint $table) {

			$table->dropColumn('person_type');

		});
	}
}
