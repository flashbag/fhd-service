<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddVolumeWeightFtField extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::table('trackers', function(Blueprint $table){

			$table->double('volume_weight_ft')->nullable()->after('volume_weight');

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('trackers', function(Blueprint $table){

			$table->dropColumn('volume_weight_ft');

		});

	}
}
