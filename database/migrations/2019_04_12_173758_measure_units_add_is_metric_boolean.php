<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MeasureUnitsAddIsMetricBoolean extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('measure_units', function (Blueprint $table) {

			$table->boolean('is_metric')->default(1)->after('type');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('measure_units', function (Blueprint $table) {

			$table->dropColumn('is_metric');

		});
	}
}
