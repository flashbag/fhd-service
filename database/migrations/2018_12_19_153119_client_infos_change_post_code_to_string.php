<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientInfosChangePostCodeToString extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_infos', function(Blueprint $table) {

			$table->string('index')->nullable()->change();

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('client_infos', function(Blueprint $table) {

			$table->integer('index')->nullable()->change();

		});
	}
}
