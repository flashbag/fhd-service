<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CargosAddReceptacleId extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cargos', function(Blueprint $table) {
			$table->integer('receptacle_id')->unsigned()->nullable()->after('tracker_id');
			$table->foreign('receptacle_id')->references('id')->on('receptacles')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cargos', function(Blueprint $table) {
			$table->dropForeign(['receptacle_id']);
			$table->dropColumn('receptacle_id');
		});
	}
}
