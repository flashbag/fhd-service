<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeVerifiedUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_email_verified')->default(0)->after('is_verified');
            $table->string('email_verify_hash')->after('is_email_verified');
            $table->renameColumn('is_verified', 'is_phone_verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('is_phone_verified', 'is_verified');
            $table->dropColumn('is_email_verified');
            $table->dropColumn('email_verify_hash');
        });
    }
}
