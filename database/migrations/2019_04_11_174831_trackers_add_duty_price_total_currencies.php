<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddDutyPriceTotalCurrencies extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->integer('duty_price_currency')->unsigned()->nullable()->after('total_price');
			$table->foreign('duty_price_currency')->references('id')->on('currencies')->onDelete('cascade');

			$table->integer('total_price_currency')->unsigned()->nullable()->after('duty_price_currency');
			$table->foreign('total_price_currency')->references('id')->on('currencies')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->dropForeign(['duty_price_currency']);
			$table->dropColumn('duty_price_currency');

			$table->dropForeign(['total_price_currency']);
			$table->dropColumn('total_price_currency');

		});
	}
}
