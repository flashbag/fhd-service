<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceptaclesAddCurrencyTypeField extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receptacles', function (Blueprint $table) {

			$table->integer('currency_type')->unsigned()->nullable()->after('unit_of_weight_id');
			$table->foreign('currency_type')->references('id')->on('currencies')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receptacles', function (Blueprint $table) {

			$table->dropForeign(['currency_type']);
			$table->dropColumn('currency_type');

		});
	}
}
