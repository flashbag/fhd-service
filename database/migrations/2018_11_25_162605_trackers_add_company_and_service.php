<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddCompanyAndService extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function(Blueprint $table){

			$table->integer('sender_service_delivery_id')->unsigned()->nullable()->after('freight_service_id');;
			$table->foreign('sender_service_delivery_id')->references('id')->on('service_deliveries')->onDelete('cascade');

			$table->integer('recipient_service_delivery_id')->unsigned()->nullable()->after('sender_service_delivery_id');;
			$table->foreign('recipient_service_delivery_id')->references('id')->on('service_deliveries')->onDelete('cascade');

		});

		Schema::table('trackers', function(Blueprint $table){

			$table->string('sender_full_name')->nullable()->change();
			$table->string('sender_company_name')->nullable()->after('sender_full_name');
			$table->string('sender_number_service')->nullable()->after('sender_number_account');

			$table->string('recipient_full_name')->nullable()->change();
			$table->string('recipient_company_name')->nullable()->after('recipient_full_name');
			$table->string('recipient_number_service')->nullable()->after('recipient_number_account');

			$table->string('sender_service_delivery_other')->nullable()->after('recipient_service_delivery_id');
			$table->string('recipient_service_delivery_other')->nullable()->after('sender_service_delivery_other');

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function(Blueprint $table){

			$table->dropForeign(['sender_service_delivery_id']);
			$table->dropColumn('sender_service_delivery_id');

			$table->dropForeign(['recipient_service_delivery_id']);
			$table->dropColumn('recipient_service_delivery_id');

		});

		Schema::table('trackers', function(Blueprint $table){

			$table->string('sender_full_name')->change();
			$table->dropColumn('sender_company_name');
			$table->dropColumn('sender_number_service');

			$table->string('recipient_full_name')->change();
			$table->dropColumn('recipient_company_name');
			$table->dropColumn('recipient_number_service');

			$table->dropColumn('sender_service_delivery_other');
			$table->dropColumn('recipient_service_delivery_other');

		});

	}
}
