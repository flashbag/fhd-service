<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableFreightServicesAddOrderNumberColumn extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('freight_services', function (Blueprint $table) {

			$table->integer('order_number')->unsigned()->default(0)->after('id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('freight_services', function (Blueprint $table) {

			$table->dropColumn('order_number');

		});
	}
}
