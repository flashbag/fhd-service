<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientInfosAddUserIdField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('client_infos', function(Blueprint $table) {

			$table->integer('user_id')->nullable()->unsigned()->after('id');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('client_infos', function(Blueprint $table) {

			$table->dropForeign(['user_id']);
			$table->dropColumn('user_id');

		});
    }
}
