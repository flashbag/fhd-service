<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchesTrackersHistoryTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('batches_trackers_history', function (Blueprint $table) {

			$table->increments('id');

			$table->integer('batch_id')->unsigned();
			$table->foreign('batch_id')->references('id')->on('batches')->onDelete('cascade');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');

			$table->integer('tracker_id')->unsigned();
			$table->foreign('tracker_id')->references('id')->on('trackers');

			$table->integer('warehouse_id')->unsigned();
			$table->foreign('warehouse_id')->references('id')->on('warehouses');

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('batches_trackers_history');
	}
}
