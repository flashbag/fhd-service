<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetupCountriesTable extends Migration {

	// https://github.com/webpatser/laravel-countries

	/**
	 * Run the migrations.
	 *
	 * @return  void
	 */
	public function up()
	{
		// Creates the countries table

		Schema::create('countries', function(Blueprint $table) {

			$table->increments('id');

			$table->string('iso_3166_2', 2)->default('');
			$table->string('iso_3166_3', 3)->default('');

			$table->string('country_code', 3)->default('');

			$table->string('name', 255)->default('');
			$table->string('full_name', 255)->nullable();

			$table->string('phone_code', 3)->nullable();

			$table->boolean('eea')->default(0);

		    $table->string('capital', 255)->nullable();
		    $table->string('citizenship', 255)->nullable();

			$table->string('region_code', 3)->default('');
			$table->string('sub_region_code', 3)->default('');

		    $table->string('currency', 255)->nullable();
		    $table->string('currency_code', 255)->nullable();
		    $table->string('currency_sub_unit', 255)->nullable();
            $table->string('currency_symbol', 3)->nullable();
            $table->integer('currency_decimals')->nullable();

		    $table->string('flag', 6)->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return  void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}
