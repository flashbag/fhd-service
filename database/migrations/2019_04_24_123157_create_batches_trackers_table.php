<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchesTrackersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('batches_trackers', function (Blueprint $table) {

			$table->integer('batch_id')->unsigned();
			$table->foreign('batch_id')->references('id')->on('batches')->onDelete('cascade');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');

			$table->integer('tracker_id')->unsigned();
			$table->foreign('tracker_id')->references('id')->on('trackers');

			$table->integer('warehouse_id')->unsigned();
			$table->foreign('warehouse_id')->references('id')->on('warehouses');

			$table->unique([
				'batch_id', 'user_id', 'tracker_id', 'warehouse_id'
			]);



		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('batches_trackers');
	}
}
