<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddIsDraftFlag extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->boolean('is_draft')->default(0)->after('is_paid');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->dropColumn('is_draft');

		});
	}
}
