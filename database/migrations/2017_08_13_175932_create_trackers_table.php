<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_tracker', 12);
            $table->integer('id_moderator')->nullable();
            $table->integer('number_invoice');
            $table->integer('id_type_tracker')->nullable();
            $table->date('date_registered');
            $table->date('date_delivery')->nullable();
            $table->integer('id_status_tracker')->nullable();
            $table->string('carrier')->nullable();
            $table->integer('container_id')->nullable();

            $table->integer('number_seats');
            $table->integer('type_inventories');
            $table->double('width_cm')->nullable();
            $table->double('length_cm')->nullable();
            $table->double('height_cm')->nullable();
            $table->double('actual_weight');
            $table->double('volume_weight')->nullable();
            $table->double('assessed_price')->nullable();
            $table->integer('currency_type')->nullable();
            $table->double('duty_price')->nullable();
            $table->double('total_price');

            $table->double('delivery_rate_kg')->nullable();
            $table->integer('delivery_rate_cur')->nullable();
            $table->double('total_ship_cost')->nullable();
            $table->integer('ship_rate_cur')->nullable();

            $table->text('description_item');
            $table->text('note_item')->nullable();
            $table->text('note_moder')->nullable();

            $table->integer('sender_id')->unsigned();
            $table->string('sender_number');
            $table->string('sender_full_name');
            $table->string('sender_email');
            $table->string('sender_inn')->nullable();
            $table->string('sender_passport_data')->nullable();
            $table->string('sender_country');
            $table->string('sender_city');
            $table->string('sender_address');
            $table->integer('sender_index');
            $table->integer('sender_document_id');

            $table->integer('recipient_id')->unsigned();
            $table->string('recipient_number');
            $table->string('recipient_full_name');
            $table->string('recipient_email');
            $table->string('recipient_country');
            $table->string('recipient_city');
            $table->string('recipient_address');
            $table->integer('recipient_index');
            $table->string('recipient_representative_recipient')->nullable();
            $table->integer('recipient_document_id');

            $table->string('name_employee_issued')->nullable();
            $table->string('name_employee_accepted')->nullable();
            $table->boolean('is_sandbox')->default(0);
            $table->boolean('is_sandbox_lvl2')->default(0);
            $table->boolean('is_active')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackers');
    }
}
