<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackerAddMeasureLengthUnitId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('trackers', function(Blueprint $table){

			$table->integer('unit_of_length_id')->unsigned()->nullable()->after('volume_weight');;
			$table->foreign('unit_of_length_id')->references('id')->on('measure_units')->onDelete('cascade');

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('trackers', function(Blueprint $table){

			$table->dropForeign(['unit_of_length_id']);
			$table->dropColumn('unit_of_length_id');

		});
    }
}
