<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddBillDutyRelationKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trackers', function(Blueprint $table){
            $table->dropColumn('bill_duties');

        });

        Schema::table('trackers', function(Blueprint $table){
            $table->integer('bill_duty_id')->unsigned()->nullable()->after('recipient_document_id');;
            $table->foreign('bill_duty_id')->references('id')->on('bill_duties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trackers', function(Blueprint $table){
            $table->dropForeign(['bill_duty_id']);
            $table->dropColumn('bill_duty_id');

        });

        Schema::table('trackers', function(Blueprint $table){
            $table->string('bill_duties', 100)->default('sender')->after('recipient_document_id')->nullable();
        });

    }
}
