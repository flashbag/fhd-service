<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceptaclesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receptacles', function (Blueprint $table) {

			$table->increments('id');

			$table->integer('tracker_id')->unsigned();
			$table->foreign('tracker_id')->references('id')->on('trackers')->onDelete('cascade');

			$table->integer('type_inventory_id')->unsigned();
			$table->foreign('type_inventory_id')->references('id')->on('type_inventories')->onDelete('cascade');

			$table->integer('unit_of_length_id')->unsigned()->nullable();
			$table->foreign('unit_of_length_id')->references('id')->on('measure_units')->onDelete('cascade');

			$table->double('width_cm')->nullable();
			$table->double('length_cm')->nullable();
			$table->double('height_cm')->nullable();

			$table->double('actual_weight');
			$table->double('volume_weight')->nullable();
			$table->double('volume_weight_ft')->nullable();
			$table->double('assessed_price')->nullable();

			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('receptacles');
	}
}
