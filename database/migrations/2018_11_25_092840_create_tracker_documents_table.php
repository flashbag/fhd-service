<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackerDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracker_documents', function (Blueprint $table) {
            $table->increments('id');

			$table->integer('tracker_id')->unsigned();
			$table->foreign('tracker_id')->references('id')->on('trackers')->onDelete('cascade');

			$table->integer('type');
			$table->integer('index');

			$table->string('filename');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracker_documents');
    }
}
