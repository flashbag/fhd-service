<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddSpecialHandlingRelationKey extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function(Blueprint $table){
			$table->dropColumn('special_handling');

		});

		Schema::table('trackers', function(Blueprint $table){
			$table->integer('special_handling_id')->unsigned()->nullable()->after('bill_transportation_id');;
			$table->foreign('special_handling_id')->references('id')->on('special_handlings')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function(Blueprint $table){
			$table->dropForeign(['special_handling_id']);
			$table->dropColumn('special_handling_id');

		});

		Schema::table('trackers', function(Blueprint $table){
			$table->string('special_handling', 100)->default('hold at FHD warehouse')->after('freight_service_id');
		});

	}
}
