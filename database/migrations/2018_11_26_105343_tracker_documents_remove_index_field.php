<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackerDocumentsRemoveIndexField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('tracker_documents', function(Blueprint $table){

			$table->dropColumn('index');

		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('tracker_documents', function(Blueprint $table){

			$table->integer('index')->after('type');

		});


    }
}
