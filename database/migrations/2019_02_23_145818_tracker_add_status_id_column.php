<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackerAddStatusIdColumn extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->dropColumn('id_status_tracker');

			$table->integer('status_id')->unsigned()->nullable()->after('id_tracker');
			$table->foreign('status_id')->references('id')->on('tracker_statuses')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->integer('id_status_tracker')->unsigned()->nullable();

			$table->dropForeign(['status_id']);
			$table->dropColumn('status_id');

		});
	}
}
