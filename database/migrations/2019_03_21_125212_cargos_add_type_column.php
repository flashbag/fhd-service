<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CargosAddTypeColumn extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cargos', function (Blueprint $table) {
			$table->boolean( 'is_used')->unsigned()->after('id')->default(0);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cargos', function (Blueprint $table) {
			$table->dropColumn( 'is_used');
		});
	}
}
