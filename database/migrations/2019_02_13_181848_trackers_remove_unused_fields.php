<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersRemoveUnusedFields extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function($table) {

			$table->dropColumn('sender_id');
			$table->dropColumn('sender_number');
			$table->dropColumn('sender_full_name');
			$table->dropColumn('sender_company_name');

			$table->dropColumn('sender_email');
			$table->dropColumn('sender_inn');
			$table->dropColumn('sender_passport_data');

			$table->dropColumn('sender_country');
			$table->dropColumn('sender_city');
			$table->dropColumn('sender_address');
			$table->dropColumn('sender_index');

			$table->dropColumn('sender_vat');
			$table->dropColumn('sender_document_id');

			$table->dropColumn('recipient_id');
			$table->dropColumn('recipient_number');
			$table->dropColumn('recipient_email');
			$table->dropColumn('recipient_full_name');

			$table->dropColumn('recipient_company_name');
			$table->dropColumn('recipient_country');
			$table->dropColumn('recipient_city');
			$table->dropColumn('recipient_address');

			$table->dropColumn('recipient_index');
			$table->dropColumn('recipient_custom_identify');
			$table->dropColumn('recipient_representative_recipient');
			$table->dropColumn('recipient_document_id');

			$table->dropColumn('sender_number_account');
			$table->dropColumn('sender_number_service');
			$table->dropForeign(['sender_service_delivery_id']);
			$table->dropColumn('sender_service_delivery_id');
			$table->dropColumn('sender_service_delivery_other');

			$table->dropColumn('recipient_number_account');
			$table->dropColumn('recipient_number_service');
			$table->dropForeign(['recipient_service_delivery_id']);
			$table->dropColumn('recipient_service_delivery_id');
			$table->dropColumn('recipient_service_delivery_other');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function($table) {

			$table->integer('sender_id')->unsigned();
			$table->string('sender_number')->nullable();
			$table->string('sender_full_name')->nullable();
			$table->string('sender_company_name')->nullable();

			$table->string('sender_email')->nullable();
			$table->string('sender_inn')->nullable();
			$table->string('sender_passport_data')->nullable();

			$table->string('sender_country');
			$table->string('sender_city');
			$table->string('sender_address');
			$table->integer('sender_index');

			$table->string('sender_vat')->after('sender_index')->nullable();
			$table->integer('sender_document_id');

			$table->integer('recipient_id')->unsigned();
			$table->string('recipient_number');
			$table->string('recipient_email');
			$table->string('recipient_full_name');

			$table->string('recipient_company_name');
			$table->string('recipient_country');
			$table->string('recipient_city');
			$table->string('recipient_address');

			$table->integer('recipient_index');
			$table->string('recipient_custom_identify')->after('recipient_index')->nullable();
			$table->string('recipient_representative_recipient')->after('recipient_custom_identify')->nullable();
			$table->integer('recipient_document_id')->after('recipient_representative_recipient');

			$table->integer('sender_number_account')->nullable();
			$table->string('sender_number_service')->nullable()->after('sender_number_account');
			$table->string('sender_service_delivery_other')->nullable();

			$table->integer('recipient_number_account')->after('recipient_custom_identify')->nullable();
			$table->string('recipient_number_service')->nullable()->after('recipient_number_account');
			$table->string('recipient_service_delivery_other')->nullable()->after('sender_service_delivery_other');

			$table->integer('sender_service_delivery_id')->unsigned()->nullable()->after('freight_service_id');;
			$table->foreign('sender_service_delivery_id')->references('id')->on('service_deliveries')->onDelete('cascade');

			$table->integer('recipient_service_delivery_id')->unsigned()->nullable()->after('sender_service_delivery_id');;
			$table->foreign('recipient_service_delivery_id')->references('id')->on('service_deliveries')->onDelete('cascade');
		});
	}
}
