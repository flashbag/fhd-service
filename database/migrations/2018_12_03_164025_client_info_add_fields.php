<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientInfoAddFields extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_infos', function(Blueprint $table) {

			$table->integer('tracker_id')->unsigned()->nullable()->after('client_id');
			$table->tinyInteger('type')->nullable()->default(-1)->after('tracker_id');

			$table->string('number')->after('type');
			$table->string('email')->after('number');
			$table->string('full_name')->after('number')->nullable();
			$table->string('company_name')->nullable()->after('full_name');

			$table->string('vat')->after('index')->nullable();
			$table->integer('number_account')->after('vat')->nullable();
			$table->string('number_service')->nullable()->after('number_account');
			$table->string('custom_identify')->after('number_service')->nullable();

			$table->integer('service_delivery_id')->unsigned()->nullable()->after('custom_identify');
			$table->string('service_delivery_other')->nullable()->after('service_delivery_id');
			$table->string('representative_recipient')->after('service_delivery_other')->nullable();

		});

		Schema::table('client_infos', function(Blueprint $table) {
			$table->foreign('service_delivery_id')->references('id')->on('service_deliveries')->onDelete('cascade');
			$table->foreign('tracker_id')->references('id')->on('trackers')->onDelete('cascade');

			$table->unique(['type', 'client_id', 'tracker_id'], 'client_infos_type_client_id_tracker_id_unique');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_infos', function(Blueprint $table) {
			$table->dropForeign(['service_delivery_id']);
			$table->dropForeign(['tracker_id']);

			$table->dropIndex('client_infos_type_client_id_tracker_id_unique');
		});

		Schema::table('client_infos', function(Blueprint $table) {

			$table->dropColumn('type');
			$table->dropColumn('tracker_id');

			$table->dropColumn('number');
			$table->dropColumn('email');
			$table->dropColumn('full_name');
			$table->dropColumn('company_name');

			$table->dropColumn('vat');
			$table->dropColumn('number_account');
			$table->dropColumn('number_service');
			$table->dropColumn('custom_identify');

			$table->dropColumn('service_delivery_id');
			$table->dropColumn('service_delivery_other');
			$table->dropColumn('representative_recipient');

		});
	}
}
