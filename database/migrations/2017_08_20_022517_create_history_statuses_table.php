<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_status')->unsigned();
            $table->foreign('id_status')->references('id')->on('status_trackers')->onDelete('cascade');
            $table->integer('id_tracker')->unsigned();
            $table->foreign('id_tracker')->references('id')->on('trackers')->onDelete('cascade');
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_statuses');
    }
}
