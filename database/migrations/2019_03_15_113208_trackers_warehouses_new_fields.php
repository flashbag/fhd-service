<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersWarehousesNewFields extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->dropColumn( 'warehouse_id');

		});

		Schema::table('trackers', function (Blueprint $table) {

			$table->integer('from_warehouse_id')->unsigned()->nullable()->after('id_tracker');
			$table->foreign('from_warehouse_id')->references('id')->on('warehouses')->onDelete('cascade');

			$table->integer('to_warehouse_id')->unsigned()->nullable()->after('from_warehouse_id');
			$table->foreign('to_warehouse_id')->references('id')->on('warehouses')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trackers', function (Blueprint $table) {

			$table->string( 'warehouse_id')->after('id_moderator')->nullable();

		});

		Schema::table('trackers', function (Blueprint $table) {

			$table->dropForeign(['from_warehouse_id']);
			$table->dropColumn('from_warehouse_id');

			$table->dropForeign(['to_warehouse_id']);
			$table->dropColumn('to_warehouse_id');

		});

	}
}
