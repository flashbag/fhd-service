<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmsCodesChangeUserIdAddPhone extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::table('sms_codes', function(Blueprint $table) {

			$table->dropForeign(['user_id']);
			$table->dropColumn('user_id');

		});

		Schema::table('sms_codes', function(Blueprint $table) {

			$table->integer('user_id')->nullable()->unsigned()->after('id');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->string('phone')->after('code');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('sms_codes', function(Blueprint $table) {

			$table->dropForeign(['user_id']);
			$table->dropColumn('user_id');

		});

		Schema::table('sms_codes', function(Blueprint $table) {

			$table->integer('user_id')->unsigned()->after('id');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->dropColumn('phone');

		});
	}
}
