<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryWarehousesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('history_warehouses', function (Blueprint $table) {

			$table->increments('id');

			$table->integer('warehouse_id')->unsigned();
			$table->foreign('warehouse_id')->references('id')->on('warehouses')->onDelete('cascade');

			$table->integer('tracker_id')->unsigned();
			$table->foreign('tracker_id')->references('id')->on('trackers')->onDelete('cascade');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->text('notes')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('history_warehouses');
	}

}
