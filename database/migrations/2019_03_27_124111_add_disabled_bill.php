<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisabledBill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_duties', function (Blueprint $table) {
            $table->boolean('disabled')->default(0)->after('type');
        });

        Schema::table('bill_transportations', function (Blueprint $table) {
            $table->boolean('disabled')->default(0)->after('type');
        });

        Schema::table('freight_services', function (Blueprint $table) {
            $table->boolean('disabled')->default(0)->after('type');
        });

        Schema::table('special_handlings', function (Blueprint $table) {
            $table->boolean('disabled')->default(0)->after('type');
        });

        Schema::table('type_transports', function (Blueprint $table) {
            $table->boolean('disabled')->default(0)->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_duties', function (Blueprint $table) {
            $table->dropColumn('disabled');
        });
        Schema::table('bill_transportations', function (Blueprint $table) {
            $table->dropColumn('disabled');
        });
        Schema::table('freight_services', function (Blueprint $table) {
            $table->dropColumn('disabled');
        });
        Schema::table('special_handlings', function (Blueprint $table) {
            $table->dropColumn('disabled');
        });
        Schema::table('type_transports', function (Blueprint $table) {
            $table->dropColumn('disabled');
        });
    }
}
