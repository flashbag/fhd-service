<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackersAddBillTransportaionRelationKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trackers', function(Blueprint $table){
            $table->dropColumn('bill_transportation');

        });

        Schema::table('trackers', function(Blueprint $table){
            $table->integer('bill_transportation_id')->unsigned()->nullable()->after('bill_duty_id');;
            $table->foreign('bill_transportation_id')->references('id')->on('bill_transportations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trackers', function(Blueprint $table){
            $table->dropForeign(['bill_transportation_id']);
            $table->dropColumn('bill_transportation_id');

        });

        Schema::table('trackers', function(Blueprint $table){

            $table->string('bill_transportation', 100)->default('sender')->after('bill_duty_id')->nullable();
        });

    }
}
