<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_site');
            $table->string('short_description');
            $table->string('email');
            $table->integer('count_news_admin');
            $table->integer('count_news_blog');
            $table->integer('count_trackers_admin');
            $table->integer('count_result_trackers_admin');
            $table->boolean('is_offline')->default(0);
            $table->text('desc_offline_site');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
