<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBatchs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batches', function(Blueprint $table){

			$table->increments('id');

			$table->addColumn('integer', 'number', [
				'unsigned' => true, 'length' => 11
			])->nullable()->unique();

			$table->integer('warehouse_id')->unsigned();
			$table->foreign('warehouse_id')->references('id')->on('warehouses');

			$table->string('name')->nullable();

			$table->timestamps();
			$table->softDeletes();

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('batches');
    }
}
