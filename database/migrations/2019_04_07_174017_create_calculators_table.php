<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalculatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculators', function (Blueprint $table) {
            $table->increments('id');

			$table->string('type_transport')->nullable();

			$table->string('country_from')->nullable();
			$table->string('country_to')->nullable();

			$table->boolean('is_imperial')->default(0)->nullable();
			$table->boolean('with_dimensions')->default(0)->nullable();
			$table->boolean('with_custom_duty')->default(0)->nullable();

			$table->double('actual_weight')->nullable();

			$table->double('width')->nullable();
			$table->double('length')->nullable();
			$table->double('height')->nullable();

			$table->double('assessed_price')->nullable();

			$table->string('currency')->nullable();

			$table->string('results')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculators');
    }
}
