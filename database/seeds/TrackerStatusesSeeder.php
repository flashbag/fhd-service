<?php

use App\Models\TrackerStatus;
use Illuminate\Database\Seeder;

class TrackerStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trackerStatuses = [
            [
                'number' => 0,
                'title' => 'Ожидается на складе'
            ],
        	[
        		'number' => 10,
				'title' => 'Доставлено на склад страны отправителя'
			],
			[
				'number' => 15,
				'title' => 'В процессе подготовки документов в стране отправителя'
			],
			[
				'number' => 20,
				'title' => 'В процессе таможенного оформления в стране отправителя'
			],
			[
				'number' => 30,
				'title' => 'Таможенный контроль в стране отправителя пройден, ваша посылка следует в страну получателя'
			],
			[
				'number' => 49,
				'title' => 'В пути следования в страну получателя'
			],
			[
				'number' => 50,
				'title' => 'Отправление прибыло в страну получателя'
			],
			[
				'number' => 60,
				'title' => 'Доставлено на склад страны получателя, идёт подготовка к таможенному оформлению'
			],
			[
				'number' => 70,
				'title' => 'В процессе таможенного оформления в стране получателя'
			],
			[
				'number' => 80,
				'title' => 'Передано внутреннему оператору для адресной доставки получателю'
			],
			[
				'number' => 99,
				'title' => 'Доставлено'
			]
		];

		foreach ($trackerStatuses as $trackerStatus) {
			TrackerStatus::firstOrCreate([
				'number' => $trackerStatus['number']
			], $trackerStatus);
        }
    }
}
