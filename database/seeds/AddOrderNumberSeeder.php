<?php

use Illuminate\Database\Seeder;
use App\Models\Additional\BillTransportation;
use \App\Models\Additional\BillDuty;
use \App\Models\Additional\SpecialHandling;

class AddOrderNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillTransportation::where('type', 'sender')->update([
                'order_number' => 2,
            ]);
        BillTransportation::where('type', 'recipient')->update([
            'order_number' => 3,
        ]);
        BillTransportation::where('type', 'third_party')->update([
            'order_number' => 4,
        ]);
        BillTransportation::where('type', 'customer')->update([
            'order_number' => 1,
        ]);

        BillDuty::where('type', 'sender')->update([
            'order_number' => 2,
        ]);
        BillDuty::where('type', 'recipient')->update([
            'order_number' => 1,
        ]);
        BillDuty::where('type', 'third_party')->update([
            'order_number' => 3,
        ]);
        BillDuty::where('type', 'fhd_account')->update([
            'order_number' => 4,
        ]);

        SpecialHandling::where('type', 'address_delivery')->update([
            'order_number' => 1,
        ]);
        SpecialHandling::where('type', 'hold_at_fhd_warehouse')->update([
            'order_number' => 2,
        ]);
        SpecialHandling::where('type', 'saturday_delivery')->update([
            'order_number' => 3,
        ]);
    }
}
