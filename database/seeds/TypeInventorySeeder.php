<?php

use App\TypeInventory;
use Illuminate\Database\Seeder;

class TypeInventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeInventory = new TypeInventory();
        $typeInventory->title = 'Коробка';
        $typeInventory->save();

        $typeInventory = new TypeInventory();
        $typeInventory->title = 'Палета';
        $typeInventory->save();

        $typeInventory = new TypeInventory();
        $typeInventory->title = 'Конверт';
        $typeInventory->save();
    }
}
