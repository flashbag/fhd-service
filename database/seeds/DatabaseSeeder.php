<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CurrenciesSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(SettingsSeeder::class);
//        $this->call(TypeContainersSeeder::class);
//        $this->call(TypeInventorySeeder::class);
        $this->call(TypeTransportsSeeder::class);
		$this->call(MeasureUnitsSeeder::class);

        $this->call(Additional\BillDutiesSeeder::class);
        $this->call(Additional\BillTransportationsSeeder::class);
        $this->call(Additional\FreightServicesSeeder::class);
		$this->call(Additional\ServiceDeliverySeeder::class);
        $this->call(Additional\SpecialHandlingsSeeder::class);
        $this->call(Additional\TypeTaxesSeeder::class);

		$this->call(CountriesSeeder::class);
		$this->call(WarehousesSeeder::class);
		$this->call(WarehouseUsersSeeder::class);
		$this->call(TrackerStatusesSeeder::class);
		$this->call(ServiceDisabledAddSeeder::class);
		$this->call(AddOrderNumberSeeder::class);
		$this->call(EmailVerifiedUser::class);

		if (config('app.env') != 'production') {
			$this->call(Batches\BatchesSeeder::class);
			$this->call(Batches\BatchesTrackersSeeder::class);
		}

    }
}
