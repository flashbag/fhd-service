<?php

namespace Batches;

use App\Models\Batch;
use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class BatchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$warehouses = Warehouse::all();

		$warehousesPlucked = $warehouses->pluck('id');

        $names = [
        	'БПП',
			'Партія Зелених',
			'ПЧЖ (Партія Червоних Жигулів)',
			"Condoms for South Africa",
			"Швейцарські годинники",
			"Китайські батарейки",
			"Львівські Карпати",
			"ВИА Пламя",
			"Piratpartiet"
		];

        foreach ($names as $name) {
        	Batch::firstOrCreate([
        		'name' => $name,
			], [
				'name' => $name,
				'warehouse_id' => $warehousesPlucked->random()
			]);
		}
    }
}
