<?php

namespace Batches;

use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class BatchesTrackersSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$warehouses = Warehouse::all();

		foreach ($warehouses as $warehouse) {
			$this->processSingleWarehouse($warehouse);
		}
	}

	private function processSingleWarehouse(Warehouse $warehouse)
	{
		$warehouseBatchesIDs = $warehouse->batches->pluck('id');
		$warehouseOperatorsIDs = $warehouse->operators->pluck('id');

		$warehouseTrackersFrom = $warehouse->trackersFrom;

		if (!$warehouseBatchesIDs->count() ||
			!$warehouseOperatorsIDs->count() ||
			!$warehouseTrackersFrom->count()) {
			return;
		}

		// each tracker
		foreach ($warehouseTrackersFrom as $tracker) {

			$magicArray = [
				$warehouseBatchesIDs->random() => [
					'user_id' => $warehouseOperatorsIDs->random(),
					'warehouse_id' => $warehouse->id
				]
			];

			// sync to the batch with random warehouse operator id
			$tracker->batches()->sync($magicArray);

			$tracker->batchHistory()->attach($magicArray);
		}
	}
}
