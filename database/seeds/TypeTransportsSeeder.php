<?php

use App\Models\TypeTransport;
use Illuminate\Database\Seeder;

class TypeTransportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['air','sea','road','train'];
        
        foreach ($types as $key => $type) {

            $typeInDb = TypeTransport::where([
                'type' => $type
            ])->first();

            if (!$typeInDb) {
                $typeInDb = new TypeTransport();
                $typeInDb->type = $type;
                $typeInDb->save();

            }

        }

    }
}
