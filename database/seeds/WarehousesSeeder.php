<?php

use App\Models\Country;
use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class WarehousesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 * @throws Exception
	 */
    public function run()
    {

        // sync existing warehouses with countries
        $warehouses = Warehouse::all();

		$countryUkraine = Country::where('iso_3166_2', 'UA')->first();
		$countryUSA = Country::where('iso_3166_2', 'US')->first();
		$countryChina = Country::where('iso_3166_2', 'CN')->first();

		foreach ($warehouses as $warehouse) {

			switch ($warehouse->short_country) {

				case 'UA':

					$warehouse->update([
						'country_id' => $countryUkraine->id,
						'unique_id' => $countryUkraine->iso_3166_2 . '_' . 1
					]);

					break;

				case 'USA':

					$warehouse->update([
						'country_id' => $countryUSA->id,
						'unique_id' => $countryUSA->iso_3166_2 . '_' . 1
					]);

					break;

				case 'CH':

					$warehouse->update([
						'country_id' => $countryChina->id,
						'unique_id' => $countryChina->iso_3166_2 . '_' . 1
					]);

					break;

				default:
					throw new \Exception('You better follow the recommendations of the main developer from Telegram channel');

					break;
			}
		}
    }
}
