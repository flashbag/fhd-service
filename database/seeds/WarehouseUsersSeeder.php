<?php

use App\Models\ClientInfo;
use App\Roles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WarehouseUsersSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$roleWarehouse = Roles::where('name', 'Warehouse')->first();

		$warehouseUsersArray = [
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Leonard Britt',
					'email' => 'leonard.britt@jourrapide.com',
					'number' => '+13027324442',
					'password' => bcrypt('qwerty'),
				]
			],

			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Priscilla Staggs',
					'email' => 'priscilla.staggs@dayrep.com',
					'number' => '+13034716192',
					'password' => bcrypt('qwerty'),
				]
			],

			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Matthew Melton',
					'email' => 'matthew.melton@teleworm.us',
					'number' => '+17702327603',
					'password' => bcrypt('qwerty'),
				]
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Jennifer Laird',
					'email' => 'jennifer.laird@dayrep.com',
					'number' => '+19405380043',
					'password' => bcrypt('qwerty'),
				]
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Остап Капустин',
					'email' => 'ostapkapusta@armyspy.com',
					'number' => '+380661110011',
					'password' => bcrypt('qwerty'),
				]
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Нора Прохорова',
					'email' => 'nedich@jourrapide.com',
					'number' => '+380661110012',
					'password' => bcrypt('qwerty'),
				]
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Никита Дми́триев',
					'email' => 'nikita.dmitrev@armyspy.com',
					'number' => '+380661110013',
					'password' => bcrypt('qwerty'),
				]
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Камилла Селезнёва',
					'email' => 'cattat@dayrep.com',
					'number' => '+380661110014',
					'password' => bcrypt('qwerty'),
				]
			],
		];

		DB::beginTransaction();

		foreach ($warehouseUsersArray as $userData) {

			$user = User::firstOrCreate([
				'number' => $userData['user']['number']
			], $userData['user']);


			ClientInfo::firstOrCreate([
				'user_id' => $user->id,
				'client_id' => $user->id_client,
				'type' => ClientInfo::TYPE_USER
			], array_only($userData['user'], ['number','email']));

			$user->roles()->sync($roleWarehouse, ['id' => $user->id]);

		}

		DB::commit();

	}
}
