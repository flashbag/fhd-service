<?php

use Illuminate\Database\Seeder;

class ServiceDisabledAddSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Additional\BillTransportation::firstOrCreate([
           'type' => 'customer',
            'disabled' => '0'
        ]);

        \App\Models\Additional\BillTransportation::where('type', 'sender')
                ->orWhere('type', 'recipient')
                ->orWhere('type', 'third_party')->update([
            'disabled' => 1,
        ]);

        \App\Models\Additional\BillDuty::where('type', 'fhd_account')
                ->orWhere('type', 'sender')
                ->orWhere('type', 'third_party')->update([
           'disabled' => 1
        ]);

        \App\Models\Additional\FreightService::where('type', 'priority_freight')
            ->orWhere('type', 'booking_number')->update([
            'disabled' => 1
        ]);

        \App\Models\Additional\SpecialHandling::where('type', 'saturday_delivery')->update([
            'disabled' => 1
        ]);

        \App\Models\TypeTransport::where('type', 'road')
            ->orWhere('type', 'train')->update([
            'disabled' => 1
        ]);
    }
}
