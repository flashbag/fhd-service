<?php

use App\TypeContainers;
use Illuminate\Database\Seeder;

class TypeContainersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeContainer = new TypeContainers();
        $typeContainer->title = '20-ти футовый стандартный контейнер';
        $typeContainer->save();

        $typeContainer = new TypeContainers();
        $typeContainer->title = '40-ка футовый стандартный контейнер';
        $typeContainer->save();

        $typeContainer = new TypeContainers();
        $typeContainer->title = '40-ка футовый "high cube" контейнер';
        $typeContainer->save();

        $typeContainer = new TypeContainers();
        $typeContainer->title = '20-ти футовый Open Top контейнер';
        $typeContainer->save();

        $typeContainer = new TypeContainers();
        $typeContainer->title = '40-ка футовый Open Top контейнер';
        $typeContainer->save();
    }
}
