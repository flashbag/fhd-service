<?php

namespace Additional;

use App\Models\Additional\BillDuty;
use Illuminate\Database\Seeder;

class BillDutiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = ['sender','recipient','third_party','fhd_account'];
        
        foreach ($elements as $key => $element) {

            $elementInDB = BillDuty::where([
                'type' => $element
            ])->first();

            if (!$elementInDB) {
                $elementInDB = new BillDuty();
                $elementInDB->type = $element;
                $elementInDB->save();

            }

        }

    }
}
