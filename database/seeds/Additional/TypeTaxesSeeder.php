<?php

namespace Additional;

use App\Models\Additional\TypeTax;
use Illuminate\Database\Seeder;

class TypeTaxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = ['bill_duty','bill_transportation'];
        
        foreach ($elements as $key => $element) {

            $elementInDB = TypeTax::where([
                'type' => $element
            ])->first();

            if (!$elementInDB) {
                $elementInDB = new TypeTax();
                $elementInDB->type = $element;
                $elementInDB->save();

            }

        }

    }
}
