<?php

namespace Additional;

use App\Models\Additional\BillTransportation;
use Illuminate\Database\Seeder;

class BillTransportationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = ['sender','recipient','third_party'];
        
        foreach ($elements as $key => $element) {

            $elementInDB = BillTransportation::where([
                'type' => $element
            ])->first();

            if (!$elementInDB) {
                $elementInDB = new BillTransportation();
                $elementInDB->type = $element;
                $elementInDB->save();

            }

        }

    }
}
