<?php

namespace Additional;

use App\Models\Additional\SpecialHandling;
use Illuminate\Database\Seeder;

class SpecialHandlingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = ['hold_at_fhd_warehouse','saturday_delivery', 'address_delivery'];
        
        foreach ($elements as $key => $element) {

            $elementInDB = SpecialHandling::where([
                'type' => $element
            ])->first();

            if (!$elementInDB) {
                $elementInDB = new SpecialHandling();
                $elementInDB->type = $element;
                $elementInDB->save();

            }

        }

    }
}
