<?php

namespace Additional;
use Illuminate\Database\Seeder;
use App\Models\Additional\ServiceDelivery;

class ServiceDeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$elements = [
			'DHL',
			'Fedex',
			'UPS',
			'USPS',
			'Новая почта',
			'American Express',
		];

		foreach ($elements as $key => $element) {

			$elementInDB = ServiceDelivery::where([
				'name' => $element
			])->first();

			if (!$elementInDB) {
				$elementInDB = new ServiceDelivery();
				$elementInDB->name = $element;
				$elementInDB->save();
			}
		}

		ServiceDelivery::where('name', 'DHL')->update([
            'type' => 'dhl',
        ]);

        ServiceDelivery::where('name', 'Fedex')->update([
            'type' => 'fedex',
        ]);

        ServiceDelivery::where('name', 'UPS')->update([
            'type' => 'ups',
        ]);

        ServiceDelivery::where('name', 'USPS')->update([
            'type' => 'usps',
        ]);

        ServiceDelivery::where('name', 'Новая почта')->update([
            'type' => 'novaposhta',
        ]);

        ServiceDelivery::where('name', 'American Express')->update([
            'type' => 'american_express',
        ]);
    }
}
