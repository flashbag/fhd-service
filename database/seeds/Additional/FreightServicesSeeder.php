<?php

namespace Additional;

use App\Models\Additional\FreightService;
use Illuminate\Database\Seeder;

class FreightServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = [
        	'priority_freight',
			'economy_freight',
			'booking_number'
		];
        
        foreach ($elements as $key => $element) {

            $elementInDB = FreightService::where([
                'type' => $element
            ])->first();

            if (!$elementInDB) {
                $elementInDB = new FreightService();
                $elementInDB->type = $element;
                $elementInDB->save();

            }

        }


		$freight_services = FreightService::all();

        foreach ($freight_services as $freight_service) {

        	$freight_service_update = false;

        	switch ($freight_service->type) {
				case 'priority_freight':
					$freight_service->order_number = 2;
					$freight_service_update = true;
					break;
				case 'economy_freight':
					$freight_service->order_number = 1;
					$freight_service_update = true;
					break;
				case 'booking_number':
					$freight_service->order_number = 3;
					$freight_service_update = true;
					break;
			}
			if ($freight_service_update) {
				$freight_service->save();
			}

		}

    }
}
