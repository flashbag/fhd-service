<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return bool|void
     */
    public function run()
    {
		$countriesFilePath = realpath(__DIR__  . '/data/countries.json');

		if (!$countriesFilePath) {
			return false;
		}

		$countriesJSON = json_decode(file_get_contents($countriesFilePath), JSON_UNESCAPED_UNICODE);

        foreach ($countriesJSON as $countryId => $country){

        	$data = [
				'capital' => ((isset($country['capital'])) ? $country['capital'] : null),
				'citizenship' => ((isset($country['citizenship'])) ? $country['citizenship'] : null),
				'country_code' => $country['country-code'],
				'currency' => ((isset($country['currency'])) ? $country['currency'] : null),
				'currency_code' => ((isset($country['currency_code'])) ? $country['currency_code'] : null),
				'currency_sub_unit' => ((isset($country['currency_sub_unit'])) ? $country['currency_sub_unit'] : null),
				'currency_decimals' => ((isset($country['currency_decimals'])) ? $country['currency_decimals'] : null),
				'full_name' => ((isset($country['full_name'])) ? $country['full_name'] : null),
				'iso_3166_2' => $country['iso_3166_2'],
				'iso_3166_3' => $country['iso_3166_3'],
				'name' => $country['name'],
				'region_code' => $country['region-code'],
				'sub_region_code' => $country['sub-region-code'],
				'eea' => (bool)$country['eea'],
				'phone_code' => $country['phone_code'],
				'currency_symbol' => ((isset($country['currency_symbol'])) ? $country['currency_symbol'] : null),
				'flag' =>((isset($country['flag'])) ? $country['flag'] : null),
			];

			Country::firstOrCreate([
				'iso_3166_2' => $country['iso_3166_2'],
				'iso_3166_3' => $country['iso_3166_3'],
			], $data);

        }
    }
}
