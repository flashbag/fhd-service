<?php

use App\Models\MeasureUnit;
use Illuminate\Database\Seeder;

class MeasureUnitsSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$measureLengthUnits = [
			[
				'type' => 'length',
				'text' => 'cm',
				'multiplier_to_base' => 1 // cm is base length measure unit
			],
			[
				'type' => 'length',
				'text' => 'in',
				'multiplier_to_base' => 2.54 // 1 in = 2.54 cm
			],
			[
				'type' => 'weight',
				'text' => 'kg',
				'multiplier_to_base' => 1 // kg is base weight measure unit
			],
			[
				'type' => 'weight',
				'text' => 'lb',
				'multiplier_to_base' => 2.204622622 // 1 kg = 2.205 lb
			]
		];

		foreach ($measureLengthUnits as $key => $measureLengthUnit) {
			MeasureUnit::firstOrCreate($measureLengthUnit, $measureLengthUnit);
		}

		MeasureUnit::where([
			'type' => 'length',
			'text' => 'in'
		])->update(['is_metric' => 0]);

		MeasureUnit::where([
			'type' => 'weight',
			'text' => 'lb'
		])->update(['is_metric' => 0]);

	}
}
