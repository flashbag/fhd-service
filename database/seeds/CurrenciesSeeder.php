<?php

use App\Models\Currency;
use App\Console\Commands\UpdateCurrenciesExchangeCommand;
use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return bool
	 */
    public function run()
	{
		$currencies = [
			[
				"name" =>  "Украинская гривна",
				"code" =>  "UAH"
			],
			[
				"name" =>  "Доллар США",
				"code" =>  "USD"
			],
			[
				"name" =>  "Евро",
				"code" =>  "EUR"
			],
			[
				"name" =>  "Российский рубль",
				"code" =>  "RUB"
			],
			[
				"name" =>  "Турецкая лира",
				"code" =>  "TRY"
			],
			[
				"name" =>  "Канадский доллар",
				"code" =>  "CAD"
			],
			[
				"name" =>  "Дирхам",
				"code" =>  "AED"
			],
			[
				"name" =>  "Румынский лей",
				"code" =>  "RON"
			],
			[
				"name" =>  "Израильский шекель",
				"code" =>  "ILS"
			],
			[
				"name" =>  "Сингапурский доллар",
				"code" =>  "SGD"
			],
			[
				"name" =>  "Южнокорейская вона",
				"code" =>  "KRW"
			],
			[
				"name" =>  "Иена",
				"code" =>  "JPY"
			],
			[
				"name" =>  "Индонезийская рупия",
				"code" =>  "IDR"
			],
			[
				"name" =>  "Швейцарский франк",
				"code" =>  "CHF"
			],
			[
				"name" =>  "Австралийский доллар",
				"code" =>  "AUD"
			],
			[
				"name" =>  "Бразильский реал",
				"code" =>  "BRL"
			],
			[
				"name" =>  "Аргентинское песо",
				"code" =>  "ARS"
			],
			[
				"name" =>  "Молдавский лей",
				"code" =>  "MDL"
			],
			[
				"name" =>  "Мексиканское песо",
				"code" =>  "MXN"
			],
			[
				"name" =>  "Иорданский динар",
				"code" =>  "JOD"
			],
			[
				"name" =>  "Болгарский лев",
				"code" =>  "BGN"
			],
			[
				"name" =>  "Белорусский рубль",
				"code" =>  "BYN"
			],
			[
				"name" =>  "Индийская рупия",
				"code" =>  "INR"
			],
			[
				"name" =>  "Азербайджанский манат",
				"code" =>  "AZN"
			],
			[
				"name" =>  "Лари",
				"code" =>  "GEL"
			],
			[
				"name" =>  "Таджикский сомони",
				"code" =>  "TJS"
			],
			[
				"name" =>  "Узбекский сум",
				"code" =>  "UZS"
			],
			[
				"name" =>  "Тенге",
				"code" =>  "KZT"
			],
			[
				"name" =>  "Египетский фунт",
				"code" =>  "EGP"
			],
			[
				"name" =>  "Ливийский динар",
				"code" =>  "LYD"
			],
			[
				"name" =>  "Тунисский динар",
				"code" =>  "TND"
			],
			[
				"name" =>  "Фунт стерлингов",
				"code" =>  "GBP"
			],
			[
				"name" =>  "Злотый",
				"code" =>  "PLN"
			],
			[
				"name" => "Китайский юань",
				"code" => "CNY"
			]
		];

		foreach ($currencies as $key => $currency) {
			Currency::firstOrCreate([ 'code' => $currency['code'] ], $currency);
		}

		$command = new UpdateCurrenciesExchangeCommand();
		$command->handle();
	}
}
