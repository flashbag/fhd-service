<?php

use Illuminate\Database\Seeder;

class EmailVerifiedUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();
        foreach ($users as $user){
            $user->update([
//                'is_email_verified' => 1,
                'email_verify_hash' => str_random(20),
                'reset_password_token'=> str_random(20),
            ]);
        }
    }
}
