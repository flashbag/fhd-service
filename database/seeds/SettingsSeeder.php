<?php

use App\Settings;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$settings = Settings::find(1);

		if (!$settings) {
			$settings = new Settings();
			$settings->title_site = 'FHD delivery';
			$settings->short_description = 'Краткое описание сайта';
			$settings->email = 'admin@gmail.com';
			$settings->count_news_admin = 10;
			$settings->count_news_blog = 10;
			$settings->count_trackers_admin = 10;
			$settings->count_result_trackers_admin = 10;
			$settings->is_offline = 0;
			$settings->desc_offline_site = 'Сообщение offline сайта...';
			$settings->save();
		}

    }
}
