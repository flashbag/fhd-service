<?php

use App\Models\ClientInfo;
use App\Roles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$roleAdmin = Roles::where('name', 'Admin')->first();
    	$roleModerator = Roles::where('name', 'Moderator')->first();
    	$roleUser = Roles::where('name', 'User')->first();
		$roleWarehouse = Roles::where('name', 'Warehouse')->first();

    	$usersArray = [
    		[
    			'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Admin',
					'email' => 'admin@gmail.com',
					'number' => '+380660001121',
					'password' => bcrypt('admin'),
				],
				'attachRole' => $roleAdmin,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Moderator1',
					'email' => 'moderator1@gmail.com',
					'number' => '+380660001122',
					'password' => bcrypt('moderator1'),
				],
				'attachRole' => $roleModerator,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Moderator2',
					'email' => 'moderator2@gmail.com',
					'number' => '+380660001123',
					'password' => bcrypt('moderator2'),
				],
				'attachRole' => $roleModerator,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Moderator3',
					'email' => 'moderator3@gmail.com',
					'number' => '+380660001124',
					'password' => bcrypt('moderator3'),
				],
				'attachRole' => $roleModerator,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Tester1',
					'email' => 'tester1@gmail.com',
					'number' => '+380660001125',
					'password' => bcrypt('tester1'),
				],
				'attachRole' => $roleUser,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Warehouse',
					'email' => 'warehouse@gmail.com',
					'number' => '+380660001177',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],

			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Leonard Britt',
					'email' => 'leonard.britt@jourrapide.com',
					'number' => '+13027324442',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],

			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Priscilla Staggs',
					'email' => 'priscilla.staggs@dayrep.com',
					'number' => '+13034716192',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],

			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Matthew Melton',
					'email' => 'matthew.melton@teleworm.us',
					'number' => '+17702327603',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Jennifer Laird',
					'email' => 'jennifer.laird@dayrep.com',
					'number' => '+19405380043',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Остап Капустин',
					'email' => 'ostapkapusta@armyspy.com',
					'number' => '+380661110011',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Нора Прохорова',
					'email' => 'nedich@jourrapide.com',
					'number' => '+380661110012',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Никита Дми́триев',
					'email' => 'ostapkapusta@armyspy.com',
					'number' => '+380661110011',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],
			[
				'user' => [
					'id_client' => User::createRandomClientID(),
					'name' => 'Камилла Селезнёва',
					'email' => 'cattat@dayrep.com',
					'number' => '+380661110014',
					'password' => bcrypt('qwerty'),
				],
				'attachRole' => $roleWarehouse,
			],
		];

    	DB::beginTransaction();

    	foreach ($usersArray as $userData) {

    		$user = User::firstOrCreate([
    			'number' => $userData['user']['number']
			], $userData['user']);


    		ClientInfo::firstOrCreate([
    			'user_id' => $user->id,
    			'client_id' => $user->id_client,
				'type' => ClientInfo::TYPE_USER
			], array_only($userData['user'], ['number','email']));

			$user->roles()->sync($userData['attachRole'], ['id' => $user->id]);

		}

		DB::commit();

    }
}
