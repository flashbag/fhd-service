<?php

use App\Roles;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$rolesNames = [
    		'User',
			'Moderator',
			'Admin',
			'Custom',
			'Warehouse Operator',
			'Warehouse Admin',
			'Warehouse Courier'
		];

		foreach($rolesNames as $roleName) {
			Roles::firstOrCreate(['name' => $roleName]);
		}

    }
}
