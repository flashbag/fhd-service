To run queue worker continuously the better way is to manage it by supervisor

There are [instructions](https://laravel.com/docs/5.7/queues#supervisor-configuration) on official site how to install it, and run.

But there is a modified config for productions server for proper work and better performance:


[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
 command=/usr/bin/php /FULL_PATH_TO_PROJECT/artisan queue:work --sleep=3 --tries=3
 autostart=true
 autorestart=true
 user=www-data
 numprocs=2
 stdout_logfile=/var/log/supervisor/laravel-worker-stdout.log  
 stderr_logfile=/var/log/supervisor/laravel-worker-stderr.log
